package com.connecteye.ims.springboot;

import com.connecteye.ims.springboot.web.controller.api.dto.MailDto;
import com.connecteye.ims.springboot.web.service.MailService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class SendMailTest {

  @Autowired
  MailService service;

  @Test
  public void sendEmail() throws Exception {
    MailDto dto = MailDto.generateMailDto("oliv2young@vng.kr", "a12345678");
    /*dto.setFromAddress("connect.eye.ims@gmail.com");
    List<String> toAddress = new ArrayList<String>();
    toAddress.add("oliv2young@vng.kr");
    dto.setToAddressList(toAddress);
    dto.setSubject("안녕하세요");
    dto.setContent("This is Test");*/
    service.sendMail(dto);
  }
}
