package com.connecteye.ims.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

// @SpringBootApplication 로 인해 스프링부트 자동설정, Bean 읽기와 생성 모두 자동
// 여기서부터 읽어나가기 시작함 항상 Application 은 상위에 존재 해야함
@EnableRedisHttpSession(maxInactiveIntervalInSeconds = 43200)
@SpringBootApplication
public class ImsApplication extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return super.configure(builder);
	}

	public static void main(String[] args) {
		SpringApplication.run(ImsApplication.class, args);
	}

	/**
	 * 단방향 해시로 인코딩
	 */
	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}

	/**
	 * 메일
	 */
	@Bean
	public JavaMailSenderImpl javaMailSender() { return new JavaMailSenderImpl(); }
}