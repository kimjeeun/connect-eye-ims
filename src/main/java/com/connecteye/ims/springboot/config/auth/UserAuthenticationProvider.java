package com.connecteye.ims.springboot.config.auth;

import com.connecteye.ims.springboot.web.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.ArrayList;

public class UserAuthenticationProvider implements AuthenticationProvider {

    /* 시큐리티 유저상세정보 서비스 */
    @Autowired
    private CustomUserService customUserService;

    /* 로그인정보 서비스 */
    private LoginService loginService;

    /**
     * 인증 요청
     *
     * @param authentication 인증 요청 객체
     */
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String username = authentication.getName();
        String password = (String) authentication.getCredentials();

        PrincipalDetails userDetails = (PrincipalDetails) customUserService.loadUserByUsername(username, password);
        if (userDetails == null) {
            throw new BadCredentialsException("Login Error!");
        }

        ArrayList<SimpleGrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority("ROLE_" + userDetails.getUserRole()));
        return new UsernamePasswordAuthenticationToken(userDetails, null, authorities);
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return true;
    }
}
