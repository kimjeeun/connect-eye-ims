package com.connecteye.ims.springboot.config.auth.dto;

import com.connecteye.ims.springboot.util.WebConstants;
import lombok.Builder;
import lombok.Getter;

import java.io.Serializable;
import java.util.Map;

@Getter
public class OAuthAttributes implements Serializable {
    private Map<String, Object> attributes;
    private String nameAttributeKey;
    private String id;
    private String name;
    private String email;
    private String picture;
    private String loginSns;
    private String token;

    @Builder
    public OAuthAttributes(
            Map<String, Object> attributes,
            String nameAttributeKey,
            String id,
            String name,
            String email,
            String picture,
            String loginSns,
            String token) {

        this.attributes = attributes;
        this.nameAttributeKey = nameAttributeKey;
        this.id = id;
        this.name = name;
        this.email = email;
        this.picture = picture;
        this.loginSns = loginSns;
        this.token = token;
    }


    // of() : OAuth2User에서 반환하는 사용자정보는 Map이라서 값을 하나하나 변환해야 한다
    public static OAuthAttributes of(String registrationId, String userToken, String userNameAttributeName, Map<String, Object> attributes) {
        switch (registrationId) {
            case "kakao":
                return ofKakao(userToken, userNameAttributeName, attributes);
            case "naver":
                return ofNaver(userToken, userNameAttributeName, attributes);
        }

        return null;
    }

    private static OAuthAttributes ofKakao(String userToken, String userNameAttributeName, Map<String, Object> attributes) {
        Map<String, Object> kakaoAccount = (Map<String, Object>) attributes.get("kakao_account"); // 카카오계정 정보
        Map<String, Object> properties = (Map<String, Object>) attributes.get("properties"); // 프로퍼티

        return OAuthAttributes.builder()
                .id(attributes.get("id").toString())
                .name((String) properties.get("nickname"))
                .email((String) kakaoAccount.get("email"))
                .picture((String) properties.get("profile_image"))
                .loginSns(WebConstants.LoginTypeCode.KAKAO)
                .attributes(attributes)
                .nameAttributeKey(userNameAttributeName)
                .token(userToken)
                .build();
    }

    private static OAuthAttributes ofNaver(String userToken, String userNameAttributeName, Map<String, Object> attributes) {
        Map<String, Object> response = (Map<String, Object>) attributes.get("response"); // 네이버

        return OAuthAttributes.builder()
                .id((String) response.get("id"))
                .name((String) response.get("name"))
                .email((String) response.get("email"))
                .picture((String) response.get("profile_image"))
                .loginSns(WebConstants.LoginTypeCode.NAVER)
                .attributes(attributes)
                .nameAttributeKey(userNameAttributeName)
                .token(userToken)
                .build();
    }
}