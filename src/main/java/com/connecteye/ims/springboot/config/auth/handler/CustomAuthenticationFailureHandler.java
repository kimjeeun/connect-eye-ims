package com.connecteye.ims.springboot.config.auth.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class CustomAuthenticationFailureHandler implements AuthenticationFailureHandler {

    private static final Logger logger = LoggerFactory.getLogger(CustomAuthenticationFailureHandler.class);

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException authEx) throws IOException {

        String userId = request.getParameter("userId");
        HttpSession session = request.getSession();
        session.setAttribute("userId", userId);

        session.setAttribute("newPassword", false);
        logger.info("auth failure: {}/{}", userId, authEx.getClass().toString());

        /*if (authEx.getClass() == PasswordResetRequiredException.class) {
            session.setAttribute("newPassword", true);
            session.setAttribute("message", "쉬운 비밀번호나 자주 쓰는 사이트의 비밀번호가 같을 경우,도용되기 쉬워 주기적으로변경하여 사용하는 것이 좋습니다.<br>비밀번호에 특수문자를 추가하여 사용하시면 기억하기도 쉽고, 비밀번호 안전도가 높아져 도용의 위험이 줄어듭니다.");
        } else*/ if (authEx.getClass() == LockedException.class) {
            session.setAttribute("message", "잘못된 비밀번호로 로그인을 여러번 시도하여 계정 사용이 불가능합니다.<br>관리자에게 문의해 주세요.");
        } else if (authEx.getClass() == CredentialsExpiredException.class) {
            session.setAttribute("message", "비밀번호 유효기간이 만료되었습니다.<br>관리자에게 문의해 주세요.");
        } else if (authEx.getClass() == BadCredentialsException.class) {
            session.setAttribute("message", "입력하신 비밀번호가 잘못되었습니다.");
        } /*if (authEx.getClass() == InvalidSessionInfoException.class) {
            session.setAttribute("message", "세션정보가 정확하지 않습니다.<br>다시 시도하세요.");
        } else*/ {
            session.setAttribute("message", "아이디 또는 비밀번호를 다시 확인하세요.<br>시스템에 등록되지 않은 아이디이거나, 아이디 또는 비밀번호를 잘못 입력하셨습니다.");
        }

        // FIXME: 2022-04-26 if else 문줄이기
//        Map <Object, String> messageMap = new HashMap() {{
//            // 계정 막힘
//            put(LockedException.class, WebConstants.AuthFailureMessage.LOCKED_EXCEPTION);
//            // 계정 유효기간 만료
//            put(CredentialsExpiredException.class, WebConstants.AuthFailureMessage.CREDENTIALS_EXPIRED_EXCEPTION);
//            // 계정 정보 잘못됨
//            put(BadCredentialsException.class, WebConstants.AuthFailureMessage.BAD_CREDENTIALS_EXCEPTION);
//        }};
//
//        session.setAttribute("message", messageMap.get(authEx.getClass()));

        response.sendRedirect(request.getContextPath() + "/loginPage.ce");
    }

}
