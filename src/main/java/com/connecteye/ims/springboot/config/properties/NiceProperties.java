package com.connecteye.ims.springboot.config.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 2022.05.12
 * NiceProperties
 * Olive
 */
@Getter
@Setter
@ConfigurationProperties(prefix="nice.site")
@Component
public class NiceProperties {
  private String code;
  private String password;
}
