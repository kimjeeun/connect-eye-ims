package com.connecteye.ims.springboot.config.auth;

import com.connecteye.ims.springboot.config.auth.dto.SessionUser;
import com.connecteye.ims.springboot.config.auth.handler.CustomAuthenticationFailureHandler;
import com.connecteye.ims.springboot.config.auth.handler.CustomAuthenticationSuccessHandler;
import com.connecteye.ims.springboot.config.auth.handler.CustomLogoutHandler;
import com.connecteye.ims.springboot.filter.AjaxSessionTimeoutFilter;
import com.connecteye.ims.springboot.util.WebConstants;
import com.connecteye.ims.springboot.web.service.LoginService;
import com.connecteye.ims.springboot.web.service.RedisService;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.access.ExceptionTranslationFilter;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import java.io.IOException;

/**
 * 2021.11.03
 * oauth2 config service
 * kyle
 */
@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    /* 커스텀 OAuth2Service 등록 */
    private final CustomOAuth2UserService customOAuth2UserService;

    /* 커스텀 UserDetailService 등록 */
    private final CustomUserService customUserService;

    /* 로그인정보 서비스 */
    private final LoginService loginService;

    @Bean
    public AuthenticationProvider authenticationProvider() {
        return new UserAuthenticationProvider();
    }

    @Bean
    public AuthenticationFailureHandler authenticationFailureHandler() {
        return new CustomAuthenticationFailureHandler();
    }

    @Bean
    public AuthenticationSuccessHandler authenticationSuccessHandler() {
        return new CustomAuthenticationSuccessHandler();
    }

    @Bean
    public LogoutHandler logoutHandler() {
        return new CustomLogoutHandler();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // CSRF 적용
        http.csrf().csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse())
            .and()
                .authorizeRequests()
                .antMatchers("/static/**", "/js/**", "/css/**", "/fonts/**", "/images/**").permitAll()
                .antMatchers("/profile", "/health", "/actuator/health").permitAll()
                .antMatchers("/", "/login*", "/logout*", "/signUp", "/oauth2/**").permitAll()
                .antMatchers("/findUser.ce", "/signUp.ce", "/mobileAuth.ce", "/mobileAuthPopup.ce", "/closeMobileAuthPopup.ce", "/login/**").permitAll()
                .antMatchers("/login/successMobileAuth", "/login/failureMobileAuth").permitAll()
                .antMatchers("/api/**", "/page/**")
                .hasAnyRole(
                    WebConstants.UserCode.NORMAL.getKey(),
                    WebConstants.UserCode.SUPER.getKey()
                )
                .anyRequest().authenticated()
            .and()
                .logout()
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .addLogoutHandler(logoutHandler())
                .invalidateHttpSession(true)
                .deleteCookies("JSESSIONID")
            .and()
                .formLogin()
                .loginPage("/loginPage.ce")
                .loginProcessingUrl("/login.ce")
                .usernameParameter("username")
                .passwordParameter("password")
                .defaultSuccessUrl("/loginSuccess", true)
                .successHandler(authenticationSuccessHandler())
                .failureHandler(authenticationFailureHandler())
            .and()
                .oauth2Login()
                .defaultSuccessUrl("/loginSuccess", true)
                .userInfoEndpoint().userService(customOAuth2UserService);

        // Ajax 요청 시 세션 체크 필터
        http.addFilterAfter(new AjaxSessionTimeoutFilter(), ExceptionTranslationFilter.class);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) {
        auth.authenticationProvider(authenticationProvider());
    }
}

