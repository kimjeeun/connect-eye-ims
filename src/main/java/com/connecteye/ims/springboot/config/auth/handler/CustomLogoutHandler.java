package com.connecteye.ims.springboot.config.auth.handler;

import com.connecteye.ims.springboot.config.auth.dto.SessionUser;
import com.connecteye.ims.springboot.web.service.RedisService;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CustomLogoutHandler implements LogoutHandler {

    /* Redis 서비스 */
    @Autowired
    private RedisService redisService;

    @SneakyThrows
    @Override
    public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
        SessionUser userInfo = (SessionUser) request.getSession().getAttribute("user");
        redisService.deleteStringValue(userInfo.getId());
        response.sendRedirect("/logoutSuccess");
    }
}
