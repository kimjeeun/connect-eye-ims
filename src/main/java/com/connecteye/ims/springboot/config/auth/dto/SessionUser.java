package com.connecteye.ims.springboot.config.auth.dto;

import com.connecteye.ims.springboot.config.auth.vo.UserVo;
import lombok.Getter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;

@Getter
public class SessionUser implements Serializable {

    private String Id;
    private String name;
    private String email;
    private String picture;
    private int optId;
    private int optUpperId;
    private String optTypeCode;

    public SessionUser(UserVo userVo) {
        this.Id = userVo.getUserId();
        this.name= userVo.getUserName();
        this.email = userVo.getEmail();
        this.picture = null;
    }
    public SessionUser(OAuthAttributes attributes){
        this.Id = attributes.getId();
        this.name = attributes.getName();
        this.email = attributes.getEmail();
        this.picture = attributes.getPicture();
    }

    @Override
    public String toString() {
        // FIXME: Util
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }
}
