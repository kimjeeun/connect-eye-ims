package com.connecteye.ims.springboot.config.properties;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;
import org.springframework.context.annotation.Configuration;

/**
 * 2021.11.22
 * AwsProperties
 * kyle
 */
@Getter
@ConstructorBinding
@RequiredArgsConstructor
@Configuration
@ConfigurationProperties(prefix = "cloud.aws")
public class AwsProperties {
    private final Credentials credentials = new Credentials();
    private final S3 s3 = new S3();
    private final Region region = new Region();

    @Getter
    @Setter
    public static class Credentials {

        private String accessKey;
        private String secretKey;
    }

    @Getter
    @Setter
    public static class S3 {
        private String origin;
        private String resized;
    }

    @Getter
    @Setter
    public static class Region {

        private String statics;
    }

    public String getAccessKey() {
        return credentials.getAccessKey();
    }

    public String getSecretKey() {
        return credentials.getSecretKey();
    }

    public String getOriginBucket() {
        return s3.getOrigin();
    }

    public String getResizedBucket() {
        return s3.getResized();
    }

    public String getRegionStatic() {
        return region.getStatics();
    }
}
