package com.connecteye.ims.springboot.config.auth;

import com.connecteye.ims.springboot.config.auth.dto.SessionUser;
import com.connecteye.ims.springboot.config.auth.vo.UserVo;
import com.connecteye.ims.springboot.util.WebConstants;
import com.connecteye.ims.springboot.web.mapper.UserMapper;
import com.connecteye.ims.springboot.web.service.RedisService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

@Service
@RequiredArgsConstructor
public class CustomUserService implements UserDetailsService {

    /* 단방향 해시 인코딩 */
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    /* 세션 */
    private final HttpSession httpSession;

    /* Redis 서비스 */
    private final RedisService redisService;

    /* 사용자 Mapper */
    private final UserMapper userMapper;

    private SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:sss");
    private Date time = new Date();
    String localTime = format.format(time);

    /**
     * 회원가입
     * @param userVo
     */
    public void joinUser(UserVo userVo) throws UnsupportedEncodingException {
        userVo.setRole(WebConstants.AuthorizationCode.ROLE_USER);
        userVo.setUserTypeCode(WebConstants.UserCode.NORMAL.getKey());
        userVo.setUserPassword(bCryptPasswordEncoder.encode(URLEncoder.encode(userVo.getUserPassword(), StandardCharsets.UTF_8)));
        userMapper.saveUser(userVo);
        if (Objects.nonNull(userVo.getLoginTypeCode())) {
            userMapper.saveUserLogin(userVo);
        }
    }

    @Override
    public UserDetails loadUserByUsername(String userId) throws UsernameNotFoundException {
        //여기서 받은 유저 패스워드와 비교하여 로그인 인증
        UserVo userVo = userMapper.getUserAccount(userId);
        if (userVo == null) {
            throw new UsernameNotFoundException("User not authorized.");
        }

        // 유저정보 세션등록
        SessionUser sessionInfo = new SessionUser(userVo);
        httpSession.setAttribute("user", sessionInfo); // SessionUser (직렬화된 dto 클래스 사용)
        redisService.setStringValue(sessionInfo.getId(), sessionInfo.toString());

        return new PrincipalDetails(userVo);
    }

    public UserDetails loadUserByUsername(String userId, String password) throws UsernameNotFoundException {
        UserVo userVo = userMapper.getUserAccount(userId);
        if (userVo == null) {
            throw new UsernameNotFoundException("사용자를 찾을 수 없습니다.");
        }
        if (userVo.getUserPassword() == null && ("a12345".equals(password))) {
            // TODO: 바꿀 고임
            throw new UsernameNotFoundException("비밀번호 설정이 필요합니다.");
        }
        if (!bCryptPasswordEncoder.matches(password, userVo.getUserPassword())) {
            throw new BadCredentialsException("입력하신 아이디와 비밀번호를 다시 한 번만 확인해 주세요.");
        }

        // 유저정보 세션등록
        SessionUser sessionInfo = new SessionUser(userVo);
        httpSession.setAttribute("user", sessionInfo); // SessionUser (직렬화된 dto 클래스 사용)
        redisService.setStringValue(sessionInfo.getId(), sessionInfo.toString());

        return new PrincipalDetails(userVo);
    }

    public UserVo getUserAccountByUserDi(String userDi) {
        return userMapper.getUserAccountByUserDi(userDi);
    }

}
