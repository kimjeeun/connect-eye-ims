package com.connecteye.ims.springboot.config.auth.vo;

import com.connecteye.ims.springboot.config.auth.dto.OAuthAttributes;
import com.connecteye.ims.springboot.util.WebConstants;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
public class UserVo implements Serializable {
    private String userId;
    private String userName;
    private String userPassword;
    private String userTypeCode;
    private String userDi;
    private String tel;
    private String email;
    private String loginTypeCode;
    private String token;
    private WebConstants.AuthorizationCode role;

    public static UserVo of(OAuthAttributes attributes) {
        UserVo user = new UserVo();
        user.setUserId(attributes.getId());
        user.setUserName(attributes.getName());
        user.setEmail(attributes.getEmail());
        user.setUserTypeCode(WebConstants.UserCode.NORMAL.getKey());
        user.setLoginTypeCode(attributes.getLoginSns());
        user.setToken(attributes.getToken());
        return user;
    }

}
