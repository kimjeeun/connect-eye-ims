package com.connecteye.ims.springboot.config.auth;

import com.connecteye.ims.springboot.config.auth.vo.UserVo;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;

public class PrincipalDetails implements UserDetails {

    private UserVo userVo;

    public PrincipalDetails(UserVo userVo) {
        this.userVo = userVo;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Collection<GrantedAuthority> collect = new ArrayList<>();
        collect.add((GrantedAuthority) () -> "ROLE_" + userVo.getUserTypeCode());
        return collect;
    }

    @Override
    public String getPassword() {
        return userVo.getUserPassword();
    }

    @Override
    public String getUsername() {
        return userVo.getUserName();
    }

    public String getUserRole() { return userVo.getUserTypeCode(); }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
