package com.connecteye.ims.springboot.config.auth;

import com.connecteye.ims.springboot.config.auth.dto.OAuthAttributes;
import com.connecteye.ims.springboot.config.auth.dto.SessionUser;
import com.connecteye.ims.springboot.config.auth.vo.UserVo;
import com.connecteye.ims.springboot.web.mapper.UserMapper;
import com.connecteye.ims.springboot.web.service.RedisService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.client.userinfo.DefaultOAuth2UserService;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserRequest;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserService;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.user.DefaultOAuth2User;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.Collections;
import java.util.Objects;

@Service
@RequiredArgsConstructor
public class CustomOAuth2UserService implements OAuth2UserService<OAuth2UserRequest, OAuth2User> {

    /* 세션 */
    private final HttpSession httpSession;

    /* Redis 서비스 */
    private final RedisService redisService;

    /* 사용자 Mapper */
    private final UserMapper userMapper;

    @Override
    public OAuth2User loadUser(OAuth2UserRequest userRequest) throws OAuth2AuthenticationException {
        OAuth2UserService<OAuth2UserRequest, OAuth2User> delegate = new DefaultOAuth2UserService();
        OAuth2User oAuth2User = delegate.loadUser(userRequest);

        // OAuth2 서비스 id (구글, 카카오, 네이버)
        String registrationId = userRequest.getClientRegistration().getRegistrationId();
        // OAuth2 로그인 진행 시 키가 되는 필드 값(PK)
        String userNameAttributeName = userRequest.getClientRegistration().getProviderDetails().getUserInfoEndpoint().getUserNameAttributeName();
        // 토큰 정보
        String userToken = userRequest.getAccessToken().getTokenValue();

        // OAuth2UserService
        OAuthAttributes attributes = OAuthAttributes.of(registrationId, userToken, userNameAttributeName, oAuth2User.getAttributes());

        // 유저정보 등록
        Objects.requireNonNull(attributes);

        UserVo loginUser = UserVo.of(attributes);
        UserVo user = userMapper.getUserAccount(loginUser.getUserId());

        if (Objects.isNull(user)) {
            userMapper.saveUser(loginUser);
            userMapper.saveUserLogin(loginUser);
        } else {
            userMapper.updateUser(loginUser);
            userMapper.updateUserLogin(loginUser);
        }

        // 유저정보 세션등록
        SessionUser sessionUser = new SessionUser(attributes);
        httpSession.setAttribute("user", sessionUser); // SessionUser (직렬화된 dto 클래스 사용)
        redisService.setStringValue(sessionUser.getId(), sessionUser.toString());

        return new DefaultOAuth2User(Collections.singleton(new SimpleGrantedAuthority("ROLE_" + user.getUserTypeCode())),
                attributes.getAttributes(),
                attributes.getNameAttributeKey());
    }

}