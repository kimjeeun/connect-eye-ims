package com.connecteye.ims.springboot.util;

/**
 * 2021.11.03
 * 시스템 상수관리
 * kyle
 */
public final class WebConstants {

    public class AuthFailureMessage {
        public static final String LOCKED_EXCEPTION = "잘못된 비밀번호로 로그인을 여러번 시도하여 계정 사용이 불가능합니다.<br>관리자에게 문의해 주세요.";
        public static final String CREDENTIALS_EXPIRED_EXCEPTION = "비밀번호 유효기간이 만료되었습니다.<br>관리자에게 문의해 주세요.";
        public static final String BAD_CREDENTIALS_EXCEPTION = "입력하신 비밀번호가 잘못되었습니다.";
    }

    public class Error {
        public static final String CODE_404 = "페이지를 찾을 수 없습니다.";
        public static final String CODE_500 = "서버 내부의 오류로 인해 서비스를 제공할 수 없습니다.";
    }

    public class ProgStatCode {
        public static final String COMPLETE = "ST103";
    }

    public class WearingStatus {
        public static final String WEARING = "ST112";
        public static final String CANCEL = "ST113";
    }

    public class CommonCode {
        public static final String RIGHT = "RIGHT";
    }

    public class RpsTypeCode {
        public static final String WEARING = "BY101";
        public static final String CONSIGNMENT = "BY102";
        public static final String RETURN = "BY103";
        public static final String CONSIGNMENT_RETURN = "BY104";
    }

    public class LoginTypeCode {
        public static final String NAVER = "LG101";
        public static final String KAKAO = "LG102";
        public static final String GOOGLE = "LG103";
    }

    // Spring Security 설정을 위한 시스템 사용 인가(Authorization) Code
    public enum AuthorizationCode {
        ROLE_USER("사용자"),
        ROLE_ADMIN("관리자");

        private String title;

        AuthorizationCode(String title) {
            this.title = title;
        }

        public String getName() {
            return name();
        }

        public String getTitle() {
            return this.title;
        }
    }

    public enum UserCode {
        GUEST("GUEST", "손님"),
        NORMAL("UR101", "일반"),
        SUPER("UR102", "체인점본사");

        private String key;
        private String title;

        UserCode(String key, String title) {
            this.key = key;
            this.title = title;
        }

        public String getName() {
            return name();
        }

        public String getKey() {
            return this.key;
        }

        public String getTitle() {
            return this.title;
        }

    }
}
