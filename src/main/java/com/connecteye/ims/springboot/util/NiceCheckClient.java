package com.connecteye.ims.springboot.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpSession;
import java.util.HashMap;

public class NiceCheckClient {
  private static final Logger logger = LoggerFactory.getLogger(NiceCheckClient.class);

  private NiceID.Check.CPClient client = new NiceID.Check.CPClient();
  private final String siteCode;
  private final String password;

  private String resNumber = null;
  private String authType = null;
  private String name = null;
  private String birthDate = null;
  private String gender = null;
  private String nationalInfo = null;
  private String dupInfo = null;
  private String connInfo = null;
  private String authErrorCode = null;
  private String mobileNumber = null;

  public String getResNumber() {
    return resNumber;
  }

  public String getAuthType() {
    return authType;
  }

  public String getName() {
    return name;
  }

  public String getBirthDate() {
    return birthDate;
  }

  public String getGender() {
    return gender;
  }

  public String getNationalInfo() {
    return nationalInfo;
  }

  public String getDupInfo() {
    return dupInfo;
  }

  public String getConnInfo() {
    return connInfo;
  }

  public String getAuthErrorCode() {
    return authErrorCode;
  }

  public String getMobileNumber() {
    return mobileNumber;
  }

  public void setMobileNumber(String mobileNumber) {
    this.mobileNumber = mobileNumber;
  }

  public NiceCheckClient(String siteCode, String password) {
    this.siteCode = siteCode;
    this.password = password;
  }

  public String encode(HttpSession session, String returnUrl, String errorUrl) {
    String requestNumber = client.getRequestNO(siteCode);
    session.setAttribute("NICE_REQ_NUMBER", requestNumber);

    String authType = ""; // "": 기본 선택화면, "M": 핸드폰, "C": 신용카드, "X": 공인인증서
    String customize = ""; // "": 기본웹페이지, "Mobile": 모바일페이지

    StringBuilder sb = new StringBuilder();
    sb.append("7:REQ_SEQ").append(requestNumber.getBytes().length).append(":").append(requestNumber)
            .append("8:SITECODE").append(siteCode.getBytes().length).append(":").append(siteCode)
            .append("9:AUTH_TYPE").append(authType.getBytes().length).append(":").append(authType)
            .append("7:RTN_URL").append(returnUrl.getBytes().length).append(":").append(returnUrl)
            .append("7:ERR_URL").append(errorUrl.getBytes().length).append(":").append(errorUrl)
            .append("9:CUSTOMIZE").append(customize.getBytes().length).append(":").append(customize);

    String plainData = sb.toString();

    int result = client.fnEncode(siteCode, password, plainData);
    switch (result) {
      case 0:
        return client.getCipherData();
      case -1:
        logger.debug("암호화 시스템 에러입니다.");
        break;
      case -2:
        logger.debug("암호화 처리오류입니다.");
        break;
      case -3:
        logger.debug("암호화 데이터 오류입니다.");
        break;
      case -9:
        logger.debug("입력데이터 오류입니다.");
        break;
      default:
        logger.debug("알 수 없는 에러입니다.: {}", result);
        break;
    }
    return null;

  }

  private String escapeData(String data) {
    if (data != null) {
      data = data.replaceAll("<", "&lt;").replaceAll(">", "&gt;");
      data = data.replaceAll("\\*", "");
      data = data.replaceAll("\\?", "");
      data = data.replaceAll("\\[", "");
      data = data.replaceAll("\\{", "");
      data = data.replaceAll("\\(", "");
      data = data.replaceAll("\\)", "");
      data = data.replaceAll("\\^", "");
      data = data.replaceAll("\\$", "");
      data = data.replaceAll("'", "");
      data = data.replaceAll("@", "");
      data = data.replaceAll("%", "");
      data = data.replaceAll(";", "");
      data = data.replaceAll(":", "");
      data = data.replaceAll("-", "");
      data = data.replaceAll("#", "");
      data = data.replaceAll("--", "");
      data = data.replaceAll("-", "");
      data = data.replaceAll(",", "");
      return data;
    }
    return "";
  }

  public boolean decodeError(String encodeData) {
    int ret = client.fnDecode(siteCode, password, escapeData(encodeData));
    if (ret == 0) {
      String plainData = client.getPlainData();
      @SuppressWarnings("rawtypes")
      HashMap result = client.fnParse(plainData);

      authErrorCode = (String) result.get("ERR_CODE");
      authType = (String) result.get("AUTH_TYPE");
      return true;
    }

    switch (ret) {
      case -1:
        logger.error("복호화 시스템 에러입니다.");
        break;
      case -4:
        logger.error("복호화 처리 오류입니다.");
        break;
      case -5:
        logger.error("복호화 해쉬 오류입니다.");
        break;
      case -6:
        logger.error("복호화 데이터 오류입니다.");
        break;
      case -9:
        logger.error("입력 데이터 오류입니다.");
        break;
      case -12:
        logger.error("사이트 패스워드 오류입니다.");
        break;
      default:
        logger.debug("알 수 없는 에러입니다.: {}", ret);
        break;
    }

    return false;
  }

  public boolean decode(HttpSession session, String encodeData) {
    int ret = client.fnDecode(siteCode, password, escapeData(encodeData));
    String sessionReqNumber = (String) session.getAttribute("NICE_REQ_NUMBER");

    if (ret == 0) {
      String plainData = client.getPlainData();
      //String cipherTime = client.getCipherDateTime();
      @SuppressWarnings("rawtypes")
      HashMap result = client.fnParse(plainData);

      String reqNumber = (String) result.get("REQ_SEQ");
      if (!reqNumber.equals(sessionReqNumber)) {
        logger.error("세션값이 다릅니다. 올바른 경로로 접근하시기 바랍니다.");
        return false;
      }
      resNumber = (String) result.get("RES_SEQ");
      authType = (String) result.get("AUTH_TYPE");
      name = (String) result.get("NAME");
      birthDate = (String) result.get("BIRTHDATE");
      gender = (String) result.get("GENDER");
      nationalInfo = (String) result.get("NATIONALINFO");
      dupInfo = (String) result.get("DI");
      connInfo = (String) result.get("CI");
      mobileNumber = (String) result.get("MOBILE_NO");
      return true;
    }

    switch (ret) {
      case -1:
        logger.error("복호화 시스템 에러입니다.");
        break;
      case -4:
        logger.error("복호화 처리 오류입니다.");
        break;
      case -5:
        logger.error("복호화 해쉬 오류입니다.");
        break;
      case -6:
        logger.error("복호화 데이터 오류입니다.");
        break;
      case -9:
        logger.error("입력 데이터 오류입니다.");
        break;
      case -12:
        logger.error("사이트 패스워드 오류입니다.");
        break;
      default:
        logger.debug("알 수 없는 에러입니다.: {}", ret);
        break;
    }

    return false;
  }
}
