package com.connecteye.ims.springboot.util;

import com.github.pagehelper.PageHelper;

import java.util.Map;
import java.util.Optional;

/**
 * 2021.11.30
 * 페이징 유틸
 * kyle
 */
public class PagingUtils {

    /**
     * 시작페이지 설정
     * @param
     * @return
     */
    public static void setPage(Map map) {
        int page = Integer.parseInt((String) map.get("page"));
        int perPage = Integer.parseInt((String) map.get("perPage"));
        String orderBy = String.valueOf(Optional.ofNullable(map.get("orderBy")).orElse(""));

        PageHelper.startPage(page, perPage).setOrderBy(orderBy);
    }
}
