package com.connecteye.ims.springboot.util;

import com.github.pagehelper.PageInfo;

import java.util.*;

/**
 * 2021.11.30
 * 결과 유틸
 * kyle
 */
public class ResultUtils {

    /**
     * 그리드 결과 response
     * @param
     * @return
     */
    public static Object getGridResult(Object result) {
        Map<String, Object> resultMap = new HashMap<>();
        Map<String, Object> dataMap = new HashMap<>();
        Map<String, Object> pageOptMap = new HashMap<>();

        List contentsList = ((PageInfo) result).getList();

        // null 체크
        Optional.of(contentsList).ifPresent(list -> {
            // 쿼리 내용 리스트
            try {
                resultMap.put("result", true);

                pageOptMap.put("page", ((PageInfo) result).getPageNum());
                pageOptMap.put("totalCount", ((PageInfo) result).getTotal());

                dataMap.put("contents", contentsList);
                dataMap.put("pagination", pageOptMap);
                resultMap.put("data", dataMap);
            } catch (Exception e) {
                resultMap.put("result", false);
                resultMap.put("message", WebConstants.Error.CODE_500);
            }
        });
        return resultMap;
    }

    /**
     * 쿼리 결과 response
     * @param
     * @return
     */
    public static Object getQueryResult(Object result) {
        Map<String, Object> resultMap = new HashMap<>();

        // null 체크
        Optional.of(result).ifPresent(object -> {
            // 쿼리 내용 리스트
            try {
                resultMap.put("result", true);
                resultMap.put("data", object);
            } catch (Exception e) {
                resultMap.put("result", false);
                resultMap.put("message", WebConstants.Error.CODE_500);
            }
        });

        return resultMap;
    }

    /**
     * SearchBox 결과 response
     * @param
     * @return
     */
    public static Object getSearchBoxResult(Object result) {
        Map<String, Object> resultMap = new HashMap<>();
        List contentsList = ((PageInfo) result).getList();

        // null 체크
        Optional.of(contentsList).ifPresent(list -> {
            // 쿼리 내용 리스트
            try {
                resultMap.put("result", true);
                resultMap.put("data", list);
                resultMap.put("page", ((PageInfo) result).getPageNum());
                resultMap.put("totalCount", ((PageInfo) result).getTotal());
            } catch (Exception e) {
                resultMap.put("result", false);
                resultMap.put("message", WebConstants.Error.CODE_500);
            }
        });
        return resultMap;
    }
}
