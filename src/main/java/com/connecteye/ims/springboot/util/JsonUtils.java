package com.connecteye.ims.springboot.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import java.util.List;
import java.util.Map;

/**
 * 2022.04.15
 * JSON 유틸
 * kyle
 */
public class JsonUtils {

    /**
     * JSON 형태의 문자열에서 자료구조 변환
     * @param map, objName
     * @return List
     */
    public static List convertJsonStrToMapList(Map map, String objName) throws JsonProcessingException {
        return new ObjectMapper().readValue(map.get(objName).toString(), List.class);
    }

    /**
     * JSON 형태의 문자열에서 Object로 변환
     * @param jsonStr json 문자열
     * @param className 변환하려는 Object의 class name
     * @return
     */
    public static Object convertObjectFromJsonStr(String jsonStr, Class<?> className) {
        return new Gson().fromJson(jsonStr, className);
    }
}
