package com.connecteye.ims.springboot.util;

import java.util.UUID;

/**
 * 2021.11.22
 * 파일명 유틸
 * kyle
 */
public class FileNameUtils {

    /**
     * 이미지 파일 체크
     * @param
     * @return
     */
    public static void checkImageMimeType(String mimeType) {
        if (!(mimeType.equals("image/jpg") || mimeType.equals("image/jpeg") || mimeType.equals("image/png"))) {
            throw new IllegalArgumentException();
        }
    }

    /**
     * 파일명 변환
     * @param
     * @return
     */
    public static String fileNameConvert(String fileName) {
        StringBuilder builder = new StringBuilder();
        UUID uuid = UUID.randomUUID();
        String extension = getExtension(fileName).replace("jfif", "jpeg");

        builder.append(uuid).append(".").append(extension);

        return builder.toString();
    }

    /**
     * 확장자
     * @param
     * @return
     */
    private static String getExtension(String fileName) {
        int pos = fileName.lastIndexOf(".");

        return fileName.substring(pos + 1);
    }

    /**
     * 파일명
     * @param
     * @return
     */
    public static String getFileName(String path) {
        int idx = path.lastIndexOf("/");

        return path.substring(idx + 1);
    }

    /**
     * amazon s3 bucket 이름 변경 후 리사이징
     * @param
     * @return
     */
    public static String toResized(String src) {
        return src.replace("connect-eye", "connect-eye-resized");
    }
}
