package com.connecteye.ims.springboot.util;

import javax.mail.internet.InternetAddress;
import java.util.List;

/**
 * 2022.05.25
 * Mail Utils
 * kyle
 */
public class MailUtils {

  public static InternetAddress[] convertListToInternetAddressArray(List<String> list, String encoding) throws Exception {
    InternetAddress[] addresses = new InternetAddress[list.size()];

    int index = 0;
    for (String str : list) {
      InternetAddress address = new InternetAddress(str, str, encoding);
      addresses[index++] = address;
    }

    return addresses;
  }

}
