package com.connecteye.ims.springboot.web.controller.api;

import com.connecteye.ims.springboot.util.ResultUtils;
import com.connecteye.ims.springboot.web.service.SalesStatusInfoService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * 2022.06.16
 * SalesStatusInfoApiController
 * kyle
 */
@RestController
@RequestMapping(value = "/api/salesStatusInfo")
@RequiredArgsConstructor
public class SalesStatusInfoApiController {

    /* 매출현황 서비스 */
    private final SalesStatusInfoService salesStatusInfoService;

    /**
     * 매출현황 목록 가져오기
     * @param
     * @return
     */
    @GetMapping("/getSalesStatusInfoList")
    public Object getSalesStatusInfoList(@RequestParam Map paramMap) {
        return ResultUtils.getQueryResult(salesStatusInfoService.getSalesStatusInfoList(paramMap));
    }

    /**
     * 매출현황 매입처 가져오기
     * @param
     * @return
     */
    @GetMapping("/getSalesStatusSupplierInfo")
    public Object getSalesStatusSupplierInfo(@RequestParam Map paramMap) {
        return ResultUtils.getQueryResult(salesStatusInfoService.getSalesStatusSupplierInfo(paramMap));
    }

    /**
     * 매출현황 정보
     * @param
     * @return
     */
    @GetMapping("/getSalesStatusInfo")
    public Object getSalesStatusInfo(@RequestParam Map paramMap) {
        return ResultUtils.getQueryResult(salesStatusInfoService.getSalesStatusInfo(paramMap));
    }

}
