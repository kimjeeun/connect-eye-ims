package com.connecteye.ims.springboot.web.controller.api.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * 2022.04.27
 * OpticianInfoDto
 * bella
 */
@Getter
@Setter
public class OpticianInfoDto {

    /*
     *  안경원_id
     */
    private int regSupId;

    /*
     *  안경원_명
     */
    private String regBrnId;

    /*
     *  안경원_상위_id
     */
    private String regPrdtId;

    /*
     *  주소
     */
    private String regSupPrdtId;

    /*
     *  전화번호
     */
    private String regPrdtName;

    /*
     *  약관동의여부
     */
    private int regBarcode;

    /*
     *  사용여부
     */
    private int regPrdtDivCode;

}
