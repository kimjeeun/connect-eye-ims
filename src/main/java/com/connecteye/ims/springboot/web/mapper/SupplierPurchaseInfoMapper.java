package com.connecteye.ims.springboot.web.mapper;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 2021.11.03
 * 매입서_입고 Mapper
 * kyle
 */
@Mapper
public interface SupplierPurchaseInfoMapper {
    /**
     * 매입_입고  리스트
     * @param
     * @return
     */
    List<Map<String, Object>> getSupplierPurchaseInfoList(Map paramMap);

    /**
     * 매입_입고  현황
     * @param
     * @return
     */
    List<Map<String, Object>> getSupplierPurchaseStatus(Map paramMap);

    /**
     * 매입 입고 거래내역 문서번호 매입처, 날짜 같은 갯수
     * @param
     * @return
     */
    List<Map<String, Object>> getPurIdCount(String purName);

    /**
     * 미지급/미수금이 발생한 매입처 가져오기
     * @param
     * @return
     */
    List<Map<String, Object>> getSupplierInfoWithUnpaid(Map paramMap);

    /**
     * 입고/취소이력 가져오기
     * @param
     * @return
     */
    List<Map<String, Object>> getHistoryInfo(Map paramMap);

    /**
     * 매입 입고 거래내역 추가
     * @param
     * @return
     */
    int insertSupplierPurchaseInfo(Map paramMap);

    /**
     * 매입 입고 입고등록
     * @param
     * @return
     */
    int insertPurchaseRegistInfo(Map paramMap);

    /**
     * 매입 입고이력 삭제
     * @param
     * @return
     */
    int deletePurchaseHistory(Map paramMap);

    /**
     * 재고등록
     * @param
     * @return
     */
    int insertStockInfo(Map paramMap);
}
