package com.connecteye.ims.springboot.web.service.storage;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.connecteye.ims.springboot.config.properties.AwsProperties;
import com.connecteye.ims.springboot.util.FileNameUtils;
import com.connecteye.ims.springboot.web.controller.api.dto.StandardInfoDto;
import com.connecteye.ims.springboot.web.mapper.StandardInfoMapper;
import lombok.RequiredArgsConstructor;
import org.apache.tika.Tika;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.*;

/**
 * 2021.11.22
 * aws s3 service
 * kyle
 */
@Service
@RequiredArgsConstructor
public class AwsS3Service {

    /* 기준정보 Mapper */
    private final StandardInfoMapper standardInfoMapper;

    /* AWS Properties */
    private final AwsProperties awsProperties;

    /* AWS Client */
    private AmazonS3 s3Client;

    /**
     * bean이 초기화 될 때 한 번 AmazonS3Client 객체를 빌드한다.
     * getInputStream() 메서드는 BLOB 객체를 리턴하며 파일에 대한 정보(파일명/수정일/크기/타입 등등)가 저장되어 있다.
     * Tika 객체를 생성하여서 detect 메소드를 활용하면 File의 MIME Type을 String 문자열로 리턴 받을 수 있다.
     */
    @PostConstruct
    private void setS3Client() {
        AWSCredentials credentials = new BasicAWSCredentials(awsProperties.getAccessKey(),
                awsProperties.getSecretKey());

        s3Client = AmazonS3ClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .withRegion(awsProperties.getRegionStatic())
                .build();
    }

    /**
     * 업로드파일 객체 생성 후 s3서버에 업로드
     */
    public String upload(MultipartFile file, String bucket) {
        String fileName = file.getOriginalFilename();
        String convertedFileName = FileNameUtils.fileNameConvert(fileName);

        try {
            String mimeType = new Tika().detect(file.getInputStream());
            ObjectMetadata metadata = new ObjectMetadata();

            FileNameUtils.checkImageMimeType(mimeType);
            metadata.setContentType(mimeType);
            s3Client.putObject(
                    new PutObjectRequest(bucket, convertedFileName, file.getInputStream(), metadata)
                            .withCannedAcl(CannedAccessControlList.PublicRead));
        } catch (IOException exception) {
            throw new RuntimeException();
        }

        return s3Client.getUrl(bucket, convertedFileName).toString();
    }

    /**
     * 상품 이미지 파일 업로드
     */
    public String uploadProductImage(MultipartFile file) {
        return upload(file, awsProperties.getOriginBucket());
    }

    /**
     * 상품 이미지 삭제
     */
    public void deleteProductImage(String key) {
        delete(awsProperties.getOriginBucket(), key);
        delete(awsProperties.getResizedBucket(), key);
    }

    /**
     * s3 서버 이미지 객체 삭제
     */
    public void delete(String bucket, String key) {
        s3Client.deleteObject(bucket, key);
    }

    /**
     * 사진 저장
     */
    public void saveUploadImage(StandardInfoDto formData, MultipartFile productImage) {
        if (Objects.nonNull(productImage)) {
            // amazon s3 bucket 에 올림
            String originImgPath = uploadProductImage(productImage);
            // resize s3 bucket
            String previewImagePath = FileNameUtils.toResized(originImgPath);

            formData.setOriginImgPath(originImgPath);
            formData.setPreviewImgPath(previewImagePath);
        } else {
            formData.setOriginImgPath("");
            formData.setPreviewImgPath("");
        }
    }

    /**
     * 저장된 사진 파일이 있는지 여부 확인 후 삭제
     */
    public void deleteInValidSavedImage(StandardInfoDto formData, List productList) {
        // DB에서 가져온 상품정보에서 originImgPath를 가져옴
        Map productMap = (Map) productList.get(0);
        String savedImagePath = productMap.get("originImgPath").toString();
        String savedImagePathName = FileNameUtils.getFileName(savedImagePath);

        // originImgPath null이 아니면 aws s3에서 이미지 삭제
        Optional.of(savedImagePathName).ifPresent(this::deleteProductImage);
    }
}

