package com.connecteye.ims.springboot.web.service;

import com.connecteye.ims.springboot.util.PagingUtils;
import com.connecteye.ims.springboot.web.mapper.SupplierInfoMapper;
import com.github.pagehelper.PageInfo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 2021.11.03
 * 매입처_마스터 Service
 * kyle
 */
@Service
@RequiredArgsConstructor
public class SupplierInfoService {

    /* 매입처 목록 Mapper */
    private final SupplierInfoMapper supplierInfoMapper;

    /**
     * 매입처 목록 가져오기
     * @param
     * @return
     */
    public Object getSupplierInfo(Map paramMap) {
        PagingUtils.setPage(paramMap);
        return PageInfo.of(supplierInfoMapper.getSupplierInfo(paramMap));
    }

    /**
     * 매입처 사용 설정
     * @param
     * @return
     */
    public Object updateSupplierInfoUse(Map paramMap) {
        return supplierInfoMapper.updateSupplierInfoUse(paramMap);

    }

    /**
     * 매입처 추가
     * @param
     * @return
     */
    public Object insertSupplierInfo(Map paramMap) {
        return supplierInfoMapper.insertSupplierInfo(paramMap);
    }

    /**
     * 매입처 수정
     * @param
     * @return
     */
    public Object updateSupplierInfo(Map paramMap) {
        return supplierInfoMapper.updateSupplierInfo(paramMap);
    }

    /**
     * 매입처 삭제
     * @param
     * @return
     */
    public Object deleteSupplierInfo(List pks) {
        return supplierInfoMapper.deleteSupplierInfo(pks);
    }
}
