package com.connecteye.ims.springboot.web.service;

import com.connecteye.ims.springboot.util.PagingUtils;
import com.connecteye.ims.springboot.web.mapper.StockStatusInfoMapper;
import com.github.pagehelper.PageInfo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 2022.04.20
 * 재고현황 Service
 * kyle
 */
@Service
@RequiredArgsConstructor
public class StockStatusInfoService {

    /* 재고현황 Mapper */
    private final StockStatusInfoMapper stockStatusInfoMapper;

    /**
     * 재고 목록 가져오기
     * @param
     * @return
     */
    public Object getStockInfo(Map paramMap) {
        PagingUtils.setPage(paramMap);
        return PageInfo.of(stockStatusInfoMapper.getStockInfo(paramMap));
    }

    /**
     * 재고 현황 가져오기
     * @param
     * @return
     */
    public Object getStockStatusInfo() {
        return stockStatusInfoMapper.getStockStatusInfo();
    }

    /**
     * 출고 내역 가져오기
     * @param
     * @return
     */
    public Object getReleaseHistoryInfo(Map paramMap) {
        return stockStatusInfoMapper.getReleaseHistoryInfo(paramMap);
    }

    /**
     * 입고/반품 내역 가져오기
     * @param
     * @return
     */
    public Object getWrngHistoryInfo(Map paramMap) {
        return stockStatusInfoMapper.getWrngHistoryInfo(paramMap);
    }

    /**
     * 재고 수정 목록 가져오기
     * @param
     * @return
     */
    public Object getStockModifyHistoryInfo(Map paramMap) {
        return stockStatusInfoMapper.getStockModifyHistoryInfo(paramMap);
    }

    /**
     * 재고 수정 등록
     * @param
     * @return
     */
    public Object insertStockModifyInfo(Map paramMap) {
        return stockStatusInfoMapper.insertStockModifyInfo(paramMap);
    }
}
