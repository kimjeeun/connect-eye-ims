package com.connecteye.ims.springboot.web.mapper;

import com.connecteye.ims.springboot.web.controller.api.dto.StandardInfoDto;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 2022.01.17
 * PRDT_MA, BRN_MA 브랜드/상품_마스터 Mapper
 * kyle
 */
@Mapper
public interface BrandProductInfoMapper {
    /**
     * 브랜드/상품 정보 가져오기
     * @param
     * @return
     */
    List<Map<String, Object>> getBrandProductInfo(Map paramMap);

    /**
     * 브랜드/상품 사용 설정
     * @param
     * @return
     */
    int updateBrandProductInfoUse(Map paramMap);

    /**
     * 브랜드/상품 추가
     * @param
     * @return
     */
    int insertBrandProductInfo(StandardInfoDto formData);

    /**
     * 브랜드/상품 수정
     * @param
     * @return
     */
    int updateBrandProductInfo(StandardInfoDto formData);

    /**
     * 브랜드/상품 삭제
     * @param
     * @return
     */
    int deleteBrandProductInfo(List pks);
}
