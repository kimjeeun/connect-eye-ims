package com.connecteye.ims.springboot.web.service;

import com.connecteye.ims.springboot.util.JsonUtils;
import com.connecteye.ims.springboot.util.WebConstants;
import com.connecteye.ims.springboot.web.aspect.PurchaseInfoAspect;
import com.connecteye.ims.springboot.web.mapper.OptometrySalesInfoMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.*;

/**
 * 2022.05.17
 * 검안매출 Service
 * kyle
 */
@Service
@RequiredArgsConstructor
public class OptometrySalesInfoService {

    /* 검안매출 Mapper */
    private final OptometrySalesInfoMapper optometrySalesInfoMapper;

    /**
     * 고객 목록 가져오기
     *
     * @param
     * @return
     */
    public Object getCustList(Map paramMap) {
        return optometrySalesInfoMapper.getCustList(paramMap);
    }

    /**
     * 매출서 목록 가져오기
     *
     * @param
     * @return
     */
    public Object getSalesInfoList(Map paramMap) {
        // 고객 정보
        List custList = optometrySalesInfoMapper.getCustList(paramMap);
        // 매출서 정보
        List<Map<String, Object>> salesInfoList = optometrySalesInfoMapper.getSalesInfoList(paramMap);
        // 매출서 정보 세팅
        salesInfoList.forEach(mapInfo -> {
            // 매출서 상세정보
            List salesDetailInfoList = optometrySalesInfoMapper.getSalesDetailInfoList(mapInfo);
            // 검안 정보
            List optometryInfoList = optometrySalesInfoMapper.getOptometryInfoList(mapInfo);
            mapInfo.put("salesDetailInfoList", salesDetailInfoList);
            mapInfo.put("optometryInfoList", optometryInfoList);
        });
        return new HashMap() {{
            put("custList", custList);
            put("salesInfoList", salesInfoList);
        }};
    }

    /**
     * 최근 검안정보 목록 가져오기
     *
     * @param
     * @return
     */
    public Object getOptometryInfoList(Map paramMap) {
        return optometrySalesInfoMapper.getOptometryInfoList(paramMap);
    }

    /**
     * 검안매출 거래내역 추가
     *
     * @param
     * @return
     */
    public Object getInsertOptometrySaleInfo(HttpSession httpSession, Map optmtSalesInfo, Map optometryList, String optometryPayInfo) throws JsonProcessingException {
        // 매출 테이블에 넣을 데이터 가공
        List optmtSalesDetailDataList = JsonUtils.convertJsonStrToMapList(optmtSalesInfo, "optmtSalesInfo");
        // 검안 테이블에 넣을 데이터 가공
        List optometryInfoList = JsonUtils.convertJsonStrToMapList(optometryList, "optometryList");
        // 검안 납부 내역 데이터
        Map optometryPayInfoObj = (Map) JsonUtils.convertObjectFromJsonStr(optometryPayInfo, Map.class);

        Map paramMap = new HashMap() {{
            put("optmtSalesInfo", optmtSalesInfo);
            put("optmtSalesDetailDataList", optmtSalesDetailDataList);
            put("optometryInfoList", optometryInfoList);
            put("optometryPayInfo", optometryPayInfoObj);
        }};
        // 매출 재고 정보 업데이트
        optometrySalesInfoMapper.updateStockInfo(optmtSalesDetailDataList);
        return optometrySalesInfoMapper.getInsertOptometrySaleInfo(paramMap);
    }

    /**
     * 기타매출 거래내역 추가
     *
     * @param
     * @return
     */
    public Object getInsertExtSalesInfo(HttpSession httpSession, Map extSalesInfo) throws JsonProcessingException {
        // 매출 테이블에 넣을 데이터 가공
        List extSalesDetailDataList = JsonUtils.convertJsonStrToMapList(extSalesInfo, "extSalesInfo");
        Map paramMap = new HashMap() {{
            put("extSalesInfo", extSalesInfo);
            put("extSalesDetailDataList", extSalesDetailDataList);
        }};
        // 매출 재고 정보 업데이트
        optometrySalesInfoMapper.updateStockInfo(extSalesDetailDataList);
        return optometrySalesInfoMapper.getInsertExtSalesInfo(paramMap);
    }

    /**
     * 검안매출 거래내역 수정
     *
     * @param
     * @return
     */
    public Object updateOptometrySales(Map paramMap) {
        return optometrySalesInfoMapper.updateOptometrySales(paramMap);
    }

    /**
     * 검안매출 거래내역 삭제
     *
     * @param
     * @return
     */
    public Object deleteOptometrySales(Map paramMap) {
        // 매출서 수정 정보 가져오기
        List<Map<String, Object>> salesInfo = optometrySalesInfoMapper.getUpdateSalesInfo(paramMap);

        // 재고 정보 되돌리기
        optometrySalesInfoMapper.updateRollBackStockInfo(salesInfo);
        return optometrySalesInfoMapper.deleteOptometrySales(paramMap);
    }

    /**
     * 고객별 금전출납 납부내역 가져오기
     *
     * @param
     * @return
     */
    public Object getOptometryCustPayHistory(Map paramMap) {
        return optometrySalesInfoMapper.getOptometryCustPayHistory(paramMap);
    }

    /**
     * 고객별 금전출납 납부내역 등록
     *
     * @param
     * @return
     */
    public Object insertOptometryCustPayHistory(Map paramMap) {
        return optometrySalesInfoMapper.insertOptometryCustPayHistory(paramMap);
    }

    /**
     * 고객별 금전출납 납부내역 삭제
     *
     * @param
     * @return
     */
    public Object deleteOptometryCustPayHistory(Map paramMap) {
        return optometrySalesInfoMapper.deleteOptometryCustPayHistory(paramMap);
    }

    /**
     * 고객 추가 후 정보 가져오기
     *
     * @param
     * @return
     */
    public Object getInsertCustInfo(Map paramMap) {
        return optometrySalesInfoMapper.getInsertCustInfo(paramMap);
    }

    /**
     * 고객별 금전출납 목록 가져오기
     *
     * @param
     * @return
     */
    public Object getOptometryCustPayInfo(Map paramMap) {
        return optometrySalesInfoMapper.getOptometryCustPayInfo(paramMap);
    }

    /**
     * 고객 가족관계 정보 저장
     *
     * @param
     * @return
     */
    public Object saveCustFamilyInfo(String custInfo, String custFamilyInfo) {
        // 고객 정보
        Map custInfoMap = (Map) JsonUtils.convertObjectFromJsonStr(custInfo, Map.class);
        optometrySalesInfoMapper.updateCustInfo(custInfoMap);

        // 고객 가족관계 정보
        List paramList = (List) JsonUtils.convertObjectFromJsonStr(custFamilyInfo, List.class);
        return optometrySalesInfoMapper.saveCustFamilyInfo(paramList);
    }

    /**
     * 고객 가족관계 정보 가져오기
     *
     * @param
     * @return
     */
    public Object getCustFamilyInfo(Map paramMap) {
        // 고객 가족관계 정보
        return optometrySalesInfoMapper.getCustFamilyInfo(paramMap);
    }
}
