package com.connecteye.ims.springboot.web.controller.api;

import com.connecteye.ims.springboot.util.ResultUtils;
import com.connecteye.ims.springboot.web.service.SupplierPurchaseInfoService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.nimbusds.oauth2.sdk.ParseException;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.xml.transform.Result;
import java.util.Map;

/**
 * 2022.02.03
 * SupplierPurchaseInfoApiController
 * kyle
 */
@RestController
@RequestMapping(value = "/api/supplierPurchaseInfo")
@RequiredArgsConstructor
public class SupplierPurchaseInfoApiController {

    /* 매입 입고 서비스 */
    private final SupplierPurchaseInfoService supplierPurchaseInfoService;

    /**
     * 매입 입고 목록 가져오기
     * @param
     * @return
     */
    @GetMapping("/getSupplierPurchaseInfo")
    public Object getSupplierPurchaseInfoList(@RequestParam Map paramMap) {
        return ResultUtils.getQueryResult(supplierPurchaseInfoService.getPurchaseInfoList(paramMap));
    }

    /**
     * 매입 입고 현황 가져오기
     * @param
     * @return
     */
    @GetMapping("/getSupplierPurchaseStatus")
    public Object getSupplierPurchaseStatus(@RequestParam Map paramMap) {
        return ResultUtils.getQueryResult(supplierPurchaseInfoService.getSupplierPurchaseStatus(paramMap));
    }

    /**
     * 매입 입고 거래내역 문서번호 매입처, 날짜 같은 갯수
     * @param
     * @return
     */
    @GetMapping("/getPurIdCount")
    public Object getPurIdCount(@RequestParam String purName) {
        return ResultUtils.getQueryResult(supplierPurchaseInfoService.getPurIdCount(purName));
    }

    /**
     * 미지급/미수금이 발생한 매입처 가져오기
     * @param
     * @return
     */
    @GetMapping("/getSupplierInfoWithUnpaid")
    public Object getSupplierInfoWithUnpaid(@RequestParam Map paramMap) {
        return ResultUtils.getQueryResult(supplierPurchaseInfoService.getSupplierInfoWithUnpaid(paramMap));
    }

    /**
     * 입고/취소이력 가져오기
     * @param
     * @return
     */
    @GetMapping("/getHistoryInfo")
    public Object getHistoryInfo(@RequestParam Map paramMap) {
        return ResultUtils.getQueryResult(supplierPurchaseInfoService.getHistoryInfo(paramMap));
    }

    /**
     * 매입 입고 거래내역 추가
     * @param
     * @return
     */
    @PostMapping("/insertSupplierPurchaseInfo")
    public void insertSupplierPurchaseInfo(HttpSession httpSession, @RequestParam Map purchaseInfo) throws JsonProcessingException {
        supplierPurchaseInfoService.insertSupplierPurchaseInfo(httpSession, purchaseInfo);
    }

    /**
     * 매입 입고 입고등록
     * @param
     * @return
     */
    @PostMapping("/insertPurchaseRegistInfo")
    public Object insertPurchaseRegistInfo(HttpSession httpSession, @RequestParam Map purchaseInfo) throws JsonProcessingException {
        return ResultUtils.getQueryResult(supplierPurchaseInfoService.insertPurchaseRegistInfo(httpSession, purchaseInfo));
    }

    /**
     * 매입 입고이력 삭제
     * @param
     * @return
     */
    @PostMapping("/deletePurchaseHistory")
    public Object deletePurchaseHistory(@RequestParam Map paramMap) {
        return ResultUtils.getQueryResult(supplierPurchaseInfoService.deletePurchaseHistory(paramMap));
    }
}