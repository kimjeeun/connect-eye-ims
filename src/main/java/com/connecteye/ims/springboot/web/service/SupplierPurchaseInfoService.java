package com.connecteye.ims.springboot.web.service;

import com.connecteye.ims.springboot.util.JsonUtils;
import com.connecteye.ims.springboot.util.WebConstants;
import com.connecteye.ims.springboot.web.aspect.PurchaseInfoAspect;
import com.connecteye.ims.springboot.web.mapper.SupplierPurchaseInfoMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.StringUtils;

import javax.servlet.http.HttpSession;
import javax.swing.text.html.Option;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 2022.02.03
 * 매입_입고 Service
 * kyle
 */
@Service
@RequiredArgsConstructor
public class SupplierPurchaseInfoService extends PurchaseInfoAspect {

    /* 매입 입고 Mapper */
    private final SupplierPurchaseInfoMapper supplierPurchaseInfoMapper;

    /**
     * 매입 입고 현황 가져오기
     *
     * @param
     * @return
     */
    public Object getSupplierPurchaseStatus(Map paramMap) {
        return supplierPurchaseInfoMapper.getSupplierPurchaseStatus(paramMap);
    }

    /**
     * 매입 입고 거래내역 문서번호 매입처, 날짜 같은 갯수
     *
     * @param
     * @return
     */
    public Object getPurIdCount(String purName) {
        return supplierPurchaseInfoMapper.getPurIdCount(purName);
    }

    /**
     * 미지급/미수금이 발생한 매입처 가져오기
     *
     * @param
     * @return
     */
    public Object getSupplierInfoWithUnpaid(Map paramMap) {
        return supplierPurchaseInfoMapper.getSupplierInfoWithUnpaid(paramMap);
    }

    /**
     * 입고/취소이력 가져오기
     *
     * @param
     * @return
     */
    public Object getHistoryInfo(Map paramMap) {
        return supplierPurchaseInfoMapper.getHistoryInfo(paramMap);
    }

    /**
     * 매입 입고 목록 가져오기
     *
     * @param
     * @return
     */
    @Override
    public Object getPurchaseInfoList(Map paramMap) {
        // 매입 입고 목록
        List<Map<String, Object>> infoList = supplierPurchaseInfoMapper.getSupplierPurchaseInfoList(paramMap);

        // 매입 입고 목록 세팅
        return setPurchaseInfoList(infoList, "purId", "purDetailSeq");
    }

    /**
     * 매입 입고 Tree 구조로 세팅
     *
     * @param
     * @return
     */
    @Override
    public void setTreePurchaseInfo(Map rtnMap, List<Map<String, Object>> mapList) {
        // 거래정보 기본정보 Tree 구조 세팅
        setDefaultTreePurchaseInfo(rtnMap, mapList);

        Map currentObj = mapList.get(0);
        rtnMap.put("supPrdtId", currentObj.get("supPrdtId"));
        rtnMap.put("regDate", currentObj.get("regDate"));
        rtnMap.put("purType", currentObj.get("purType"));
        rtnMap.put("supName", currentObj.get("supName"));
        rtnMap.put("stadyCnt", currentObj.get("stadyCnt"));
        rtnMap.put("purCnt", currentObj.get("purCnt"));
        rtnMap.put("cancelCnt", currentObj.get("cancelCnt"));
    }

    /**
     * 매입 입고 거래내역 추가
     *
     * @param
     * @return
     */
    public void insertSupplierPurchaseInfo(HttpSession httpSession, Map purchaseInfo) throws JsonProcessingException {
        // 세션에서 현재 로그인된 아이디 값 가져옴
        setUserInfo(httpSession, purchaseInfo);

        // 매입상세, 매입입고 테이블에 넣을 데이터 가공
        List purchaseDetailDataList = JsonUtils.convertJsonStrToMapList(purchaseInfo, "purchaseData");
        Map paramMap = new HashMap() {{
            put("purchaseInfo", purchaseInfo);
            put("purchaseDetailDataList", purchaseDetailDataList);
        }};

        // 거래내역 추가
        supplierPurchaseInfoMapper.insertSupplierPurchaseInfo(paramMap);

        try {
            // 반품일 경우 재고등록 처리
            insertReturnPurchaseInfo(purchaseInfo, paramMap);
            // 즉시입고
            insertRightPurchaseInfo(httpSession, purchaseInfo, purchaseDetailDataList);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 매입 입고이력 삭제
     * @param
     * @return
     */
    public Object deletePurchaseHistory(Map paramMap) {
        return supplierPurchaseInfoMapper.deletePurchaseHistory(paramMap);
    }

    /**
     * 매입 입고 입고등록
     *
     * @param
     * @return
     */
    public Object insertPurchaseRegistInfo(HttpSession httpSession, Map purchaseInfo) throws JsonProcessingException {
        // 세션에서 현재 로그인된 아이디 값 가져옴
        setUserInfo(httpSession, purchaseInfo);

        // 매입입고 입고등록
        List purchaseDetailDataList = JsonUtils.convertJsonStrToMapList(purchaseInfo, "purchaseData");

        // 입고등록 테이블에 들어갈 파라미터 세팅
        setWrngInfo(purchaseInfo, purchaseDetailDataList);
        Map paramMap = new HashMap() {{
            put("purchaseInfo", purchaseInfo);
            put("purchaseDetailDataList", purchaseDetailDataList);
        }};

        // 재고등록
        supplierPurchaseInfoMapper.insertStockInfo(paramMap);

        return supplierPurchaseInfoMapper.insertPurchaseRegistInfo(paramMap);
    }

    /**
     * 입고등록 테이블에 들어갈 파라미터 세팅
     *
     * @param
     * @return
     */
    private void setWrngInfo(Map purchaseInfo, List purchaseDetailDataList) {

        // 대기 갯수
        AtomicInteger totalStadyCnt = Objects.isNull(purchaseInfo.get("stadyCnt"))
            ? new AtomicInteger(Integer.parseInt("0"))
            : new AtomicInteger(Integer.parseInt((String) purchaseInfo.get("stadyCnt")));

        // 진행상세 업데이트에 필요한 파라미터 넣어줌
        purchaseDetailDataList.forEach(map -> {
            // 대기 갯수
            AtomicInteger stadyCnt = Objects.isNull(purchaseInfo.get("stadyCnt"))
                ? new AtomicInteger(Integer.parseInt("0"))
                : new AtomicInteger(Integer.parseInt((String) purchaseInfo.get("stadyCnt")));

            // 입고 갯수
            AtomicInteger purCnt = Objects.isNull(purchaseInfo.get("purCnt"))
                ? new AtomicInteger(Integer.parseInt("0"))
                : new AtomicInteger(Integer.parseInt((String) purchaseInfo.get("purCnt")));

            // 취소 갯수
            AtomicInteger cancelCnt = Objects.isNull(purchaseInfo.get("cancelCnt"))
                ? new AtomicInteger(Integer.parseInt("0"))
                : new AtomicInteger(Integer.parseInt((String) purchaseInfo.get("cancelCnt")));

            // 단가
            AtomicInteger unitPrice = Objects.isNull(purchaseInfo.get("unitPrice"))
                ? new AtomicInteger(Integer.parseInt(String.valueOf(((Map) map).get("unitPrice"))))
                : new AtomicInteger(Integer.parseInt(String.valueOf(purchaseInfo.get("unitPrice"))));

            // 입고유형
            String typeCode = ((Map) map).get("typeCode").toString();

            // 갯수
            String amount = ((Map) map).get("amount").toString();

            // 즉시 입고 일 경우
            if (totalStadyCnt.toString().equals("0")) {
                stadyCnt.set(Integer.parseInt(amount));
            }

            int numAmount = Integer.parseInt(amount);
            switch (typeCode) {
                // 입고
                case WebConstants.WearingStatus.WEARING:
                    purCnt.set(purCnt.get() + numAmount);
                    ((Map) map).put("purCnt", purCnt.toString());
                    break;
                // 취소
                case WebConstants.WearingStatus.CANCEL:
                    cancelCnt.set(cancelCnt.get() + numAmount);
                    ((Map) map).put("cancelCnt", cancelCnt.toString());
                    break;
            }

            totalStadyCnt.set(totalStadyCnt.get() - numAmount);

            stadyCnt.set(stadyCnt.get() - numAmount);
            ((Map) map).put("stadyCnt", Integer.parseInt(stadyCnt.toString()));

            unitPrice.set(unitPrice.get() * numAmount);
            ((Map) map).put("price", Integer.parseInt(unitPrice.toString()));

            // 대기건수가 음수일 경우 0 넣어줌
            if (Integer.parseInt(totalStadyCnt.toString()) < 0) {
                totalStadyCnt.set(0);
            }

            // 대기건수가 0일 경우 상태 업데이트 파라미터 세팅
            if (totalStadyCnt.toString().equals("0")) {
                ((Map) map).put("updateProgStatCode", WebConstants.ProgStatCode.COMPLETE);
            }
        });

        // 대기건수 계산 후 넣어줌
        purchaseInfo.put("stadyCnt", totalStadyCnt.toString());
    }

    /**
     * 반품 처리
     *
     * @param
     * @return
     */
    private void insertReturnPurchaseInfo(Map purchaseInfo, Map paramMap) throws Exception {
        String purTypeCode = purchaseInfo.get("purTypeCode").toString();
        Optional.of(purTypeCode).ifPresent(s -> {
            if (Objects.equals(s, WebConstants.RpsTypeCode.RETURN) ||
                    Objects.equals(s, WebConstants.RpsTypeCode.CONSIGNMENT_RETURN)) {
                supplierPurchaseInfoMapper.insertStockInfo(paramMap);
            }
        });
    }

    /**
     * 즉시 입고 처리
     *
     * @param
     * @return
     */
    private void insertRightPurchaseInfo(HttpSession httpSession, Map purchaseInfo, List purchaseDetailDataList) throws Exception {
        String purRightRegCheck = purchaseInfo.get("purRightRegCheck").toString();
        Optional.of(purRightRegCheck).ifPresent(s -> {
            if (Objects.equals(s, WebConstants.CommonCode.RIGHT)) {
                purchaseInfo.put("purId", purchaseInfo.get("purchaseDocNumber"));
                try {
                    insertPurchaseRegistInfo(httpSession, purchaseInfo);
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
