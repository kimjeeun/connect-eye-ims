package com.connecteye.ims.springboot.web.mapper;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 2022.04.15
 * 안경원 Mapper
 * bella
 */
@Mapper
public interface OpticianInfoMapper {

    /**
     * 안경원정보 목록 가져오기
     * @param paramMap 파라미터
     * @return List<Map<String, Object>>
     */
    List<Map<String, Object>> getOpticianInfoList(Map paramMap);

    /**
     * 안경원정보 생성
     * @param formData form데이터
     * @return int
     */
    int insertOpticianInfo(Map formData);
}
