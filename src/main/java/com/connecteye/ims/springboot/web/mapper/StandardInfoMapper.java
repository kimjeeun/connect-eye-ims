package com.connecteye.ims.springboot.web.mapper;

import com.connecteye.ims.springboot.web.controller.api.dto.StandardInfoDto;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 2021.11.03
 * SUP_PRDT 매입처_상품 Mapper
 * kyle
 */
@Mapper
public interface StandardInfoMapper {
    /**
     * 기준정보 목록 가져오기
     * @param
     * @return
     */
    List<Map<String, Object>> getStandardInfoList(Map paramMap);

    /**
     * 기준정보 현황 가져오기
     * @param
     * @return
     */
    List<Map<String, Object>> getStandardInfoStatus();

    /**
     * 기준정보 사용 설정
     * @param
     * @return
     */
    int updateStandardInfoUse(Map paramMap);

    /**
     * 매입처 리스트
     * @param
     * @return
     */
    List<Map<String, Object>> getSupplierList(Map paramMap);

    /**
     * 브랜드 리스트
     * @param
     * @return
     */
    List<Map<String, Object>> getBrandList(Map paramMap);

    /**
     * 기준정보 상품 추가
     * @param
     * @return
     */
    int insertStandardInfo(StandardInfoDto formData);

    /**
     * 엑셀 기준정보 상품 추가
     * @param
     * @return
     */
    int insertExcelStandardInfoList(List dataList);

    /**
     * 기준정보 상품 수정
     * @param
     * @return
     */
    int updateStandardInfo(StandardInfoDto formData);

    /**
     * 기준정보 상품 삭제
     * @param
     * @return
     */
    int deleteStandardInfo(List pks);

    /**
     * 브랜드 추가 후 정보 가져오기
     * @param
     * @return
     */
    List<Map<String, Object>> getInsertBrandInfo(Map paramMap);

    /**
     * 매입처 추가 후 정보 가져오기
     * @param
     * @return
     */
    List<Map<String, Object>> getInsertSupplierInfo(Map paramMap);
}
