package com.connecteye.ims.springboot.web.mapper;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 2022.03.03
 * 금전출납 Mapper
 * kyle
 */
@Mapper
public interface CashPaymentInfoMapper {
    /**
     * 금전출납 리스트
     * @param
     * @return
     */
    List<Map<String, Object>> getCashPaymentInfo(Map paramMap);

    /**
     * 금전출납 현황
     * @param
     * @return
     */
    List<Map<String, Object>> getCashPaymentStatus(Map paramMap);

    /**
     * 금전출납 납부내역
     * @param
     * @return
     */
    List<Map<String, Object>> getCashPaymentHistory(Map paramMap);

    /**
     * 금전출납 납부내역 등록
     * @param
     * @return
     */
    int insertCashPaymentHistory(Map paramMap);

    /**
     * 금전출납 납부내역 삭제
     * @param
     * @return
     */
    int deleteCashPaymentHistory(Map paramMap);
}
