package com.connecteye.ims.springboot.web.controller.api;

import com.connecteye.ims.springboot.config.auth.CustomUserService;
import com.connecteye.ims.springboot.config.auth.vo.UserVo;
import com.connecteye.ims.springboot.config.properties.NiceProperties;
import com.connecteye.ims.springboot.util.NiceCheckClient;
import com.connecteye.ims.springboot.web.service.MailService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * 2021.11.22
 * LoginApiController
 * kyle
 */
@Controller
@RequiredArgsConstructor
public class LoginApiController {

    /* 커스텀 유저 서비스 */
    private final CustomUserService userService;

    /* NICE 인증 속성 */
    private final NiceProperties niceProperties;

    /* 메일 서비스 */
    private final MailService mailService;


    /**
     * 중복된 회원 ID 찾기
     * @param paramMap
     * @return Map
     */
    @PostMapping(value="/api/getUser")
    public Object getUser(@RequestParam Map paramMap) {
        Map<String, Object> result = new HashMap<>();
        String userId = String.valueOf(paramMap.get("userId"));
        if (userId.equals("null")) {
            result.put("result", false);
            return result;
        }
        UserDetails user = userService.loadUserByUsername(userId);
        if (user != null) {
            result.put("result", true);
        } else {
            result.put("result", false);
        }
        return result;
    }

    /**
     * 회원가입
     * @param userVo
     * @return
     */
    @PostMapping(value = "/signUp")
    public String signUp(HttpServletRequest request, UserVo userVo) {
        try {
            userService.joinUser(userVo);
        } catch (UnsupportedEncodingException e) {
            HttpSession session = request.getSession();
            session.setAttribute("error", "비밀번호를 변환하던 도중 에러가 발생하였습니다. 입력한 비밀번호를 확인해 주세요.");
        }

        return "redirect:/loginPage.ce";
    }

    /**
     * 로그인 성공 시
     * @param
     * @return
     */
    @GetMapping(value = "/loginSuccess")
    public String loginSuccess() {
        return "redirect:/page/mainPage.ce";
    }

    /**
     * 로그아웃 성공 시
     * @param
     * @return
     */
    @GetMapping(value = "/logoutSuccess")
    public String logoutSuccess() {
        return "redirect:/logoutPage.ce";
    }

    /**
     * NICE 본인인증 성공
     * @param session
     * @param model
     * @param request
     * @param response
     * @param encodeData
     * @return
     */
    @GetMapping(value = "/login/successMobileAuth")
    public String successUserAuth(HttpSession session, ModelMap model,
                                  HttpServletRequest request, HttpServletResponse response,
                                  @RequestParam(value="EncodeData") String encodeData,
                                  @RequestParam(value="returnUrl", required = false) String returnUrl) throws UnsupportedEncodingException {
        model.put("RESULT_MSG", "본인 인증이 완료되었습니다.");

        NiceCheckClient client = new NiceCheckClient(niceProperties.getCode(), niceProperties.getPassword());
        boolean ret = client.decode(session, encodeData);
        if (ret) {
            session.setAttribute("NICE_AUTH_OK", true);
            session.setAttribute("NICE_AUTH_NAME", client.getName());
            session.setAttribute("NICE_AUTH_BIRTH_DATE", client.getBirthDate());
            session.setAttribute("NICE_AUTH_GENDER", client.getGender());
            session.setAttribute("NICE_AUTH_NATIONAL_INFO", client.getNationalInfo());
            session.setAttribute("NICE_AUTH_DUP_INFO", client.getDupInfo());
            session.setAttribute("NICE_AUTH_CONN_INFO", client.getConnInfo());
        }

        // TODO: Login 처리
        // 만약 해당하는 user_di가 있으면 "이미 가입된 회원입니다"
        // 만약 해당하는 user_di가 없으면 signup.ce
        UserVo userVo = userService.getUserAccountByUserDi(client.getDupInfo());
        if (Objects.isNull(userVo)) {
            returnUrl = "signUp.ce";
        } else {
            String RESULT_MSG = "이미 가입된 회원입니다.";
            returnUrl = "loginPage.ce?RESULT_MSG=" + URLEncoder.encode(RESULT_MSG, StandardCharsets.UTF_8);
        }

        String closeUrl = "redirect:/closeMobileAuthPopup.ce";
        closeUrl += "?returnUrl=/";
        closeUrl += returnUrl;
        return closeUrl;
    }

    /**
     * NICE 본인인증 실패
     * @param session
     * @param model
     * @param request
     * @param response
     * @param encodeData
     * @return
     */
    @GetMapping(value = "/login/failureMobileAuth")
    public String failureUserAuth(HttpSession session, ModelMap model,
                                  HttpServletRequest request, HttpServletResponse response,
                                  @RequestParam(value="EncodeData") String encodeData) {
        model.put("RESULT_MSG", "본인 인증이 실패하었습니다.");

        NiceCheckClient client = new NiceCheckClient(niceProperties.getCode(), niceProperties.getPassword());
        boolean ret = client.decodeError(encodeData);
        if (ret) {
            session.setAttribute("NICE_AUTH_OK", false);
            session.setAttribute("NICE_AUTH_NAME", null);
            session.setAttribute("NICE_AUTH_BIRTH_DATE", null);
            session.setAttribute("NICE_AUTH_GENDER", null);
            session.setAttribute("NICE_AUTH_NATIONAL_INFO", null);
            session.setAttribute("NICE_AUTH_DUP_INFO", null);
            session.setAttribute("NICE_AUTH_CONN_INFO", null);
        }

        return "redirect:/closeMobileAuthPopup.ce?returnUrl=/loginPage.ce";
    }
}