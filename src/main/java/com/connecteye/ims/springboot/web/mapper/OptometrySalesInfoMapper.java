package com.connecteye.ims.springboot.web.mapper;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 2022.04.15
 * 검안매출 Mapper
 * kyle
 */
@Mapper
public interface OptometrySalesInfoMapper {

    /**
     * 고객 목록 가져오기
     * @param paramMap
     * @return List<Map<String, Object>>
     */
    List<Map<String, Object>> getCustList(Map paramMap);

    /**
     * 매출서 목록 가져오기
     * @param paramMap
     * @return List<Map<String, Object>>
     */
    List<Map<String, Object>> getSalesInfoList(Map paramMap);

    /**
     * 매출서 상세 목록 가져오기
     * @param paramMap
     * @return List<Map<String, Object>>
     */
    List<Map<String, Object>> getSalesDetailInfoList(Map paramMap);

    /**
     * 검안 정보 가져오기
     * @param paramMap
     * @return List<Map<String, Object>>
     */
    List<Map<String, Object>> getOptometryInfoList(Map paramMap);

    /**
     * 검안매출 거래내역 추가
     * @param
     * @return
     */
    List<Map<String, Object>> getInsertOptometrySaleInfo(Map paramMap);

    /**
     * 검안매출 거래내역 수정
     * @param
     * @return
     */
    int updateOptometrySales(Map paramMap);

    /**
     * 검안매출 거래내역 삭제
     * @param
     * @return
     */
    int deleteOptometrySales(Map paramMap);

    /**
     * 기타매출 거래내역 추가
     * @param
     * @return
     */
    List<Map<String, Object>> getInsertExtSalesInfo(Map paramMap);

    /**
     * 고객별 금전출납 리스트
     * @param
     * @return
     */
    List<Map<String, Object>> getOptometryCustPayInfo(Map paramMap);

    /**
     * 매출 재고 정보 업데이트
     * @param
     * @return
     */
    int updateStockInfo(List dataList);

    /**
     * 매출 삭제 재고 정보 업데이트
     * @param
     * @return
     */
    int updateRollBackStockInfo(List dataList);

    /**
     * 고객별 금전출납 납부내역 가져오기
     * @param
     * @return
     */
    List<Map<String, Object>> getOptometryCustPayHistory(Map paramMap);

    /**
     * 고객별 금전출납 납부내역 등록
     * @param
     * @return
     */
    int insertOptometryCustPayHistory(Map paramMap);

    /**
     * 고객별 금전출납 납부내역 삭제
     * @param
     * @return
     */
    int deleteOptometryCustPayHistory(Map paramMap);

    /**
     * 고객 추가 후 정보 가져오기
     * @param
     * @return
     */
    List<Map<String, Object>> getInsertCustInfo(Map paramMap);

    /**
     * 매출서 수정 정보 가져오기
     * @param
     * @return
     */
    List<Map<String, Object>> getUpdateSalesInfo(Map paramMap);

    /**
     * 고객 가족관계 정보 저장
     *
     * @param
     * @return
     */
    int saveCustFamilyInfo(List paramList);


    /**
     * 고객 가족관계 정보 가져오기
     * @param
     * @return
     */
    List<Map<String, Object>> getCustFamilyInfo(Map paramMap);

    /**
     * 고객 정보 수정
     * @param
     * @return
     */
    int updateCustInfo(Map paramMap);
}
