package com.connecteye.ims.springboot.web.aspect;

import com.connecteye.ims.springboot.config.auth.dto.SessionUser;
import com.github.pagehelper.util.StringUtil;

import javax.servlet.http.HttpSession;
import java.util.*;

import static java.util.stream.Collectors.groupingBy;

public abstract class PurchaseInfoAspect {

    /**
     * 거래정보 목록 가져오기
     * @param
     * @return
     */
    public abstract Object getPurchaseInfoList(Map paramMap);

    /**
     * 매입 입고 목록 세팅
     * @param
     * @return
     */
    protected List<Map<String, Object>> setPurchaseInfoList(List<Map<String, Object>> infoList, String pk, String seq) {
        // Tree 형태를 위한 pk로 그룹핑
        Map<String, List<Map<String, Object>>> groupByMap = infoList.stream().collect(groupingBy(m -> (String) m.get(pk)));
        // 리턴 시킬 Tree 형태 리스트 목록
        List resultList = new ArrayList();
        groupByMap.forEach((key, mapList) -> {
            // 매입 입고 Tree 구조로 세팅
            Map rtnMap = new HashMap();
            setTreePurchaseInfo(rtnMap, mapList);
            resultList.add(rtnMap);
        });
        //내림 차순으로 정렬하기
        resultList.sort((s1, s2) ->
            Integer.parseInt(String.valueOf(((Map) s2).get(seq)))
            - Integer.parseInt(String.valueOf(((Map) s1).get(seq)))
        );
        return resultList;
    }

    /**
     * 거래정보 Tree 구조 세팅
     * @param
     * @return
     */
    public abstract void setTreePurchaseInfo(Map rtnMap, List<Map<String, Object>> mapList);

    /**
     * 거래정보 기본정보 Tree 구조 세팅
     * @param
     * @return
     */
    protected void setDefaultTreePurchaseInfo(Map rtnMap, List<Map<String, Object>> mapList) {
        String brnCnt = getInvalidMapKeySize(mapList, "brnName");
        String prdtCnt = getInvalidMapKeySize(mapList, "prdtName");
        String prdtDivCnt = getInvalidMapKeySize(mapList, "prdtDiv");

        int totalAmount = getInvalidMapKeySummary(mapList, "amount");
        int totalPrice = getInvalidMapKeySummary(mapList, "price");
        int avgUnitPrice = totalPrice / totalAmount;

        int totalCompleteProStat = (int) mapList.stream().filter(x -> x.get("progStat").equals("완료")).count();
        String progStat = (mapList.size() == totalCompleteProStat) ? "완료" : "진행중";

        Map currentObj = mapList.get(0);
        rtnMap.put("purDetailSeq", currentObj.get("purDetailSeq"));
        rtnMap.put("purId", currentObj.get("purId"));
        rtnMap.put("purType", currentObj.get("purType"));
        rtnMap.put("progStat", progStat);
        rtnMap.put("avgPrice", avgUnitPrice);
        rtnMap.put("amount", totalAmount);
        rtnMap.put("price", totalPrice);
        rtnMap.put("prdtDiv", currentObj.get("prdtDiv") + getCountStr(prdtDivCnt));
        rtnMap.put("brnName", currentObj.get("brnName") + getCountStr(brnCnt));
        rtnMap.put("prdtName", currentObj.get("prdtName") + getCountStr(prdtCnt));
        rtnMap.put("_children", mapList);
    }

    /**
     * 문자열로 바꾼 후 문자가 0일 경우 빈값으로
     * @param
     * @return
     */
    private String replaceString(String str) {
        return str.replace("0", "");
    }

    /**
     * 갯수 가져오기
     * @param
     * @return
     */
    private String getCountStr(String count) {
        return StringUtil.isNotEmpty(count) ? " 외 " + count : "";
    }

    /**
     * Map user 정보를 담아줌
     * @param
     * @return
     */
    protected void setUserInfo(HttpSession httpSession, Map map) {
        SessionUser user = (SessionUser) httpSession.getAttribute("user");
        map.put("regId", user.getId());
    }

    /**
     * Map key 별로 중복된 Object 사이즈 가져오기
     * @param
     * @return
     */
    protected String getInvalidMapKeySize(List<Map<String, Object>> mapList, String key) {
        List list = new ArrayList();
        for (Map map : mapList) {
            if (Objects.nonNull(map.get(key))) {
                list.add(map.get(key).toString());
            }
        }
        return replaceString(String.valueOf(new HashSet<String>(list).size() - 1));
    }

    /**
     * Map key 별로 중복된 Object 사이즈의 합 가져오기
     * @param
     * @return
     */
    protected int getInvalidMapKeySummary(List<Map<String, Object>> mapList, String key) {
        int sum = 0;
        for (Map map : mapList) {
            if (Objects.nonNull(map.get(key))) {
                sum += Integer.parseInt(map.get(key).toString());
            }
        }
        return sum;
    }
}
