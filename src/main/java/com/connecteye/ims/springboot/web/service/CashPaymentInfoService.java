package com.connecteye.ims.springboot.web.service;

import com.connecteye.ims.springboot.util.JsonUtils;
import com.connecteye.ims.springboot.web.aspect.PurchaseInfoAspect;
import com.connecteye.ims.springboot.web.mapper.CashPaymentInfoMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 2022.02.03
 * 금전출납 Service
 * kyle
 */
@Service
@RequiredArgsConstructor
public class CashPaymentInfoService {

    /* 금전출납 Mapper */
    private final CashPaymentInfoMapper cashPaymentInfoMapper;

    /**
     * 금전출납 목록 가져오기
     * @param
     * @return
     */
    public Object getCashPaymentInfoList(Map paramMap) {
        return cashPaymentInfoMapper.getCashPaymentInfo(paramMap);
    }

    /**
     * 금전출납 현황 가져오기
     * @param
     * @return
     */
    public Object getCashPaymentStatus(Map paramMap) {
        return cashPaymentInfoMapper.getCashPaymentStatus(paramMap);
    }

    /**
     * 금전출납 납부내역 가져오기
     * @param
     * @return
     */
    public Object getCashPaymentHistory(Map paramMap) {
        return cashPaymentInfoMapper.getCashPaymentHistory(paramMap);
    }

    /**
     * 금전출납 납부내역 등록
     * @param
     * @return
     */
    public Object insertCashPaymentHistory(Map paramMap) {
        return cashPaymentInfoMapper.insertCashPaymentHistory(paramMap);
    }

    /**
     * 금전출납 납부내역 삭제
     * @param
     * @return
     */
    public Object deleteCashPaymentHistory(Map paramMap) {
        return cashPaymentInfoMapper.deleteCashPaymentHistory(paramMap);
    }
}
