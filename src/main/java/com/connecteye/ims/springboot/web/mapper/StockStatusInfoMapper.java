package com.connecteye.ims.springboot.web.mapper;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 2022.04.20
 * 재고현황 Mapper
 * kyle
 */
@Mapper
public interface StockStatusInfoMapper {

    /**
     * 재고 목록 가져오기
     * @param
     * @return
     */
    List<Map<String, Object>> getStockInfo(Map paramMap);

    /**
     * 재고 현황 가져오기
     * @param
     * @return
     */
    List<Map<String, Object>> getStockStatusInfo();

    /**
     * 출고 내역 가져오기
     * @param
     * @return
     */
    List<Map<String, Object>> getReleaseHistoryInfo(Map paramMap);

    /**
     * 입고/반품 내역 가져오기
     * @param
     * @return
     */
    List<Map<String, Object>> getWrngHistoryInfo(Map paramMap);

    /**
     * 재고 수정 목록 가져오기
     * @param
     * @return
     */
    List<Map<String, Object>> getStockModifyHistoryInfo(Map paramMap);

    /**
     * 재고 수정 등록
     * @param
     * @return
     */
    int insertStockModifyInfo(Map paramMap);
}
