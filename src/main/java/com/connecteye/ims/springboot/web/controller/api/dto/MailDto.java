package com.connecteye.ims.springboot.web.controller.api.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * 2022.05.25
 * MailDto
 * Olive
 */
@Getter
@Setter
public class MailDto {
  private final String fromAddress = "connect.eye.ims@gmail.com";
  private List<String> toAddressList = new ArrayList<>();
  private List<String> ccAddressList = new ArrayList<>();
  private List<String> bccAddressList = new ArrayList<>();
  private String subject; // 제목
  private String content; // 메일 내용
  private boolean isUseHtml = true; // 메일 형식이 HTML인지 여부(true, false)
  private List<AttachFileDto> attachFileList = new ArrayList<>();

  public static MailDto generateMailDto(String toAddress, String password) {
    MailDto mailDto = new MailDto();
    List<String> toAddressList = new ArrayList<>();
    toAddressList.add(toAddress);
    mailDto.setToAddressList(toAddressList);
    mailDto.setSubject("[커넥트아이] 비밀번호 찾기 결과");
    mailDto.setContent("임시 비밀번호가 발급되었습니다." + password + " 행복하세요.");
    return mailDto;
  }
}
