package com.connecteye.ims.springboot.web.controller.api;

import com.connecteye.ims.springboot.util.ResultUtils;
import com.connecteye.ims.springboot.web.service.SupplierInfoService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 2022.01.17
 * SupplierInfoApiController
 * kyle
 */
@RestController
@RequestMapping(value = "/api/supplierInfo")
@RequiredArgsConstructor
public class SupplierInfoApiController {

    /* 매입처정보 서비스 */
    private final SupplierInfoService supplierInfoService;

    /**
     * 매입처정보 가져오기
     * @param
     * @return
     */
    @GetMapping("/getSupplierInfo")
    public Object getSupplierInfo(@RequestParam Map paramMap) {
        return ResultUtils.getGridResult(supplierInfoService.getSupplierInfo(paramMap));
    }


    /**
     * 매입처 사용 설정
     * @param
     * @return
     */
    @PutMapping("/updateSupplierInfoUse")
    public Object updateSupplierInfoUse(@RequestParam Map paramMap) {
        return ResultUtils.getQueryResult(supplierInfoService.updateSupplierInfoUse(paramMap));
    }

    /**
     * 매입처 추가
     * @param
     * @return
     */
    @PostMapping("/insertSupplierInfo")
    public Object insertSupplierInfo(@RequestParam Map paramMap) {
        return ResultUtils.getQueryResult(supplierInfoService.insertSupplierInfo(paramMap));
    }

    /**
     * 매입처 수정
     * @param
     * @return
     */
    @PostMapping("/updateSupplierInfo")
    public Object updateSupplierInfo(@RequestParam Map paramMap) {
        return ResultUtils.getQueryResult(supplierInfoService.updateSupplierInfo(paramMap));
    }

    /**
     * 매입처 삭제
     * @param
     * @return
     */
    @DeleteMapping("/deleteSupplierInfo")
    public Object deleteSupplierInfo(@RequestParam(value = "pks[]") List pks) {
        return ResultUtils.getQueryResult(supplierInfoService.deleteSupplierInfo(pks));
    }
}
