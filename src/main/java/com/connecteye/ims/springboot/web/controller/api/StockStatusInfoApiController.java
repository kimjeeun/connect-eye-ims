package com.connecteye.ims.springboot.web.controller.api;

import com.connecteye.ims.springboot.util.ResultUtils;
import com.connecteye.ims.springboot.web.service.StockStatusInfoService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * 2022.04.20
 * StockStatusInfoApiController
 * kyle
 */
@RestController
@RequestMapping(value = "/api/stockStatusInfo")
@RequiredArgsConstructor
public class StockStatusInfoApiController {

    /* 재고현황 서비스 */
    private final StockStatusInfoService stockStatusInfoService;

    /**
     * 재고 목록 가져오기
     * @param
     * @return
     */
    @GetMapping("/getStockInfo")
    public Object getStockInfo(@RequestParam Map paramMap) {
        return ResultUtils.getGridResult(stockStatusInfoService.getStockInfo(paramMap));
    }

    /**
     * 재고 현황 가져오기
     * @param
     * @return
     */
    @GetMapping("/getStockStatusInfo")
    public Object getStockStatusInfo(@RequestParam Map paramMap) {
        return ResultUtils.getQueryResult(stockStatusInfoService.getStockStatusInfo());
    }

    /**
     * 입고/반품 내역 가져오기
     * @param
     * @return
     */
    @GetMapping("/getWrngHistoryInfo")
    public Object getWrngHistoryInfo(@RequestParam Map paramMap) {
        return ResultUtils.getQueryResult(stockStatusInfoService.getWrngHistoryInfo(paramMap));
    }

    /**
     * 출고 내역 가져오기
     * @param
     * @return
     */
    @GetMapping("/getReleaseHistoryInfo")
    public Object getReleaseHistoryInfo(@RequestParam Map paramMap) {
        return ResultUtils.getQueryResult(stockStatusInfoService.getReleaseHistoryInfo(paramMap));
    }

    /**
     * 재고 수정 가져오기
     * @param
     * @return
     */
    @GetMapping("/getStockModifyHistoryInfo")
    public Object getStockModifyHistoryInfo(@RequestParam Map paramMap) {
        return ResultUtils.getQueryResult(stockStatusInfoService.getStockModifyHistoryInfo(paramMap));
    }

    /**
     * 재고 수정 등록
     * @param
     * @return
     */
    @PostMapping("/insertStockModifyInfo")
    public Object insertStockModifyInfo(@RequestParam Map paramMap) {
        return ResultUtils.getQueryResult(stockStatusInfoService.insertStockModifyInfo(paramMap));
    }
}
