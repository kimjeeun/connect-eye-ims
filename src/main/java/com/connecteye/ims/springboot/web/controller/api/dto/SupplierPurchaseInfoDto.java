package com.connecteye.ims.springboot.web.controller.api.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.LinkedHashMap;

/**
 * 2022.02.03
 * SupplierPurchaseInfoDto
 * kyle
 */
@Getter
@Setter
public class SupplierPurchaseInfoDto {
    /*
     *  매입서 아이디
     */
    private String purId;

    /*
     *  유저 아이디
     */
    private String userId;

    /*
     *  유저 명
     */
    private String userName;

    /*
     *  브랜드 아이디
     */
    private int brnId;

    /*
     *  브랜드 명
     */
    private String brnName;

    /*
     *  상품 아이디
     */
    private int prdtId;

    /*
     *  상품 명
     */
    private String prdtName;

    /*
     *  상품 구분
     */
    private String prdtDiv;

    /*
     *  매입처 아이디
     */
    private int supId;

    /*
     *  매입처 명
     */
    private String supName;

    /*
     *  매입처상품 아이디
     */
    private int supPrdtId;

    /*
     *  단가
     */
    private int unitPrice;

    /*
     *  수량
     */
    private int amount;

    /*
     *  금액
     */
    private int price;

    /*
     *  매입서 유형
     */
    private String purType;

    /*
     *  매입서 상태
     */
    private String purStat;

    /*
     *  매입서 상세 상태
     */
    private String detailStat;

    /*
     *  매입서 등록일자
     */
    private String regDate;
}
