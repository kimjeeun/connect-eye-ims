package com.connecteye.ims.springboot.web.service;

import com.connecteye.ims.springboot.util.FileNameUtils;
import com.connecteye.ims.springboot.util.PagingUtils;
import com.connecteye.ims.springboot.web.controller.api.dto.StandardInfoDto;
import com.connecteye.ims.springboot.web.mapper.BrandProductInfoMapper;
import com.connecteye.ims.springboot.web.service.storage.AwsS3Service;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.pagehelper.PageInfo;
import com.sun.istack.Nullable;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.*;

/**
 * 2021.11.03
 * 브랜드_상품 Service
 * kyle
 */
@Service
@RequiredArgsConstructor
public class BrandProductInfoService {

    /* 브랜드/상품 Mapper */
    private final BrandProductInfoMapper brandProductInfoMapper;

    /* AWS S3 Service */
    private final AwsS3Service awsS3Service;

    /**
     * 브랜드/상품 목록 가져오기
     * @param
     * @return
     */
    public Object getBrandProductInfo(Map paramMap) {
        PagingUtils.setPage(paramMap);
        return PageInfo.of(brandProductInfoMapper.getBrandProductInfo(paramMap));
    }

    /**
     * 브랜드/상품 사용 설정
     * @param
     * @return
     */
    public Object updateBrandProductInfoUse(Map paramMap) {
        return brandProductInfoMapper.updateBrandProductInfoUse(paramMap);
    }

    /**
     * 브랜드/상품 추가
     * @param
     * @return
     */
    public Object insertBrandProductInfo(StandardInfoDto formData, @Nullable MultipartFile productImage) {
        awsS3Service.saveUploadImage(formData, productImage);
        return brandProductInfoMapper.insertBrandProductInfo(formData);
    }

    /**
     * 브랜드/상품 수정
     * @param
     * @return
     */
    public Object updateBrandProductInfo(StandardInfoDto formData) {
        return brandProductInfoMapper.updateBrandProductInfo(formData);
    }

    /**
     * 브랜드/상품 삭제
     * @param
     * @return
     */
    @SneakyThrows
    public Object deleteBrandProductInfo(String jsonStr) {
        List<String> pks = new ArrayList();
        ObjectMapper objectMapper = new ObjectMapper();
        Object paramMap = objectMapper.readValue(jsonStr, Object.class);

        for (Object object : (ArrayList) paramMap) {
            String prdtId = ((Map) object).get("prdtId").toString();
            String originImgPath = ((Map) object).get("originImgPath").toString();

            pks.add(prdtId);
            awsS3Service.deleteProductImage(FileNameUtils.getFileName(originImgPath));
        }
        return brandProductInfoMapper.deleteBrandProductInfo(pks);
    }
}
