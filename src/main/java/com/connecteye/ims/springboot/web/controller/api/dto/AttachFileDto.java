package com.connecteye.ims.springboot.web.controller.api.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * 2022.05.25
 * MailDto
 * Olive
 */
@Getter
@Setter
public class AttachFileDto {

  private String realFileName;
  private String attachFileName;
}
