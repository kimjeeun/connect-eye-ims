package com.connecteye.ims.springboot.web.mapper;
import com.connecteye.ims.springboot.config.auth.vo.UserVo;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper {

    /**
     * Login
     * @param userId
     * @return
     */
    UserVo getUserAccount(String userId);

    /**
     * Login
     * @param userDi
     * @return
     */
    UserVo getUserAccountByUserDi(String userDi);

    /**
     * SignUp
     * @param userVo
     * @return
     */
    void saveUser(UserVo userVo);

    /**
     * SignUp For OAuth user
     * @param userVo
     * @return
     */
    void saveUserLogin(UserVo userVo);

    /**
     * Update
     * @param userVo
     * @return
     */
    void updateUser(UserVo userVo);

    /**
     * Update For OAuth user
     * @param userVo
     * @return
     */
    void updateUserLogin(UserVo userVo);
}
