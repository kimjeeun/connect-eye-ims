package com.connecteye.ims.springboot.web.service;

import com.connecteye.ims.springboot.util.PagingUtils;
import com.connecteye.ims.springboot.web.controller.api.dto.StandardInfoDto;
import com.connecteye.ims.springboot.web.mapper.CommonMapper;
import com.connecteye.ims.springboot.web.mapper.StandardInfoMapper;
import com.connecteye.ims.springboot.web.service.storage.AwsS3Service;
import com.github.pagehelper.PageInfo;
import com.sun.istack.Nullable;
import lombok.RequiredArgsConstructor;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

import static java.lang.Math.min;

/**
 * 2021.11.03
 * 매입처_상품 Service
 * kyle
 */
@Service
@RequiredArgsConstructor
public class StandardInfoService {

    /* Common Mapper */
    private final CommonMapper commonMapper;

    /* 기준정보 Mapper */
    private final StandardInfoMapper standardInfoMapper;

    /* AWS S3 Service */
    private final AwsS3Service awsS3Service;

    /**
     * 기준정보 목록 가져오기
     *
     * @param
     * @return
     */
    public Object getStandardInfoList(Map paramMap) {
        PagingUtils.setPage(paramMap);
        return PageInfo.of(standardInfoMapper.getStandardInfoList(paramMap));
    }

    /**
     * 기준정보 상품 목록 가져오기
     *
     * @param
     * @return
     */
    public Object getStandardInfoProductList(Map paramMap) {
        return standardInfoMapper.getStandardInfoList(paramMap);
    }

    /**
     * 기준정보 현황 가져오기
     *
     * @param
     * @return
     */
    public Object getStandardInfoStatus() {
        return standardInfoMapper.getStandardInfoStatus();
    }

    /**
     * 기준정보 사용 설정
     *
     * @param
     * @return
     */
    public Object updateStandardInfoUse(Map paramMap) {
        return standardInfoMapper.updateStandardInfoUse(paramMap);
    }

    /**
     * 매입처 리스트
     *
     * @param
     * @return
     */
    public Object getSupplierList(Map paramMap) {
        return standardInfoMapper.getSupplierList(paramMap);
    }

    /**
     * 브랜드 리스트
     *
     * @param
     * @return
     */
    public Object getBrandList(Map paramMap) {
        return standardInfoMapper.getBrandList(paramMap);
    }

    /**
     * 기준정보 목록 상품 추가
     *
     * @param
     * @return
     */
    public void insertStandardInfoList(List<StandardInfoDto> standardInfoList, List<MultipartFile> productImages) {
        // 반복문 인덱스
        AtomicInteger indexHolder = new AtomicInteger();

        // 기준정보 목록 DB 저장
        standardInfoList.forEach((standardInfo) -> {
            // 기준정보 이미지 AWS 저장
            if (Objects.nonNull(productImages)) {
                awsS3Service.saveUploadImage(standardInfo, productImages.get(indexHolder.getAndIncrement()));
            }
            // 기준정보 DB 저장
            standardInfoMapper.insertStandardInfo(standardInfo);
        });
    }

    /**
     * 기준정보 상품 수정
     *
     * @param
     * @return
     */
    public Object updateStandardInfo(StandardInfoDto formData, @Nullable MultipartFile productImage) {
        // DB 에서 상품정보를 가져옴
        Map paramMap = new HashMap();
        paramMap.put("prdtId", formData.getRegPrdtId());
        List productList = standardInfoMapper.getStandardInfoList(paramMap);

        awsS3Service.deleteInValidSavedImage(formData, productList);
        awsS3Service.saveUploadImage(formData, productImage);
        return standardInfoMapper.updateStandardInfo(formData);
    }

    /**
     * 기준정보 상품 삭제
     *
     * @param
     * @return
     */
    public Object deleteStandardInfo(List pks) {
        return standardInfoMapper.deleteStandardInfo(pks);
    }

    /**
     * 엑셀 기준정보 목록 상품 추가
     *
     * @param
     * @return
     */
    public void insertExcelStandardInfoList(StandardInfoDto formData, MultipartFile excelFile) throws IOException {
        // 기준정보 엑셀 데이터 -> MapList
        List<Map<Object, String>> dataList = setExcelDataToMapList(formData, excelFile);

        // 100개 씩 분할 처리
        int limit = 100;
        for (int id = 0; id < dataList.size(); id += limit) {
            if (((HashMap) ((ArrayList) dataList).get(id)).size() > 0) {
                List<Map<Object, String>> limitDataList = new ArrayList<>(dataList.subList(id, min(id + limit, dataList.size())));
                standardInfoMapper.insertExcelStandardInfoList(limitDataList);
            }
        }
    }

    /**
     * 브랜드 추가 후 정보 가져오기
     *
     * @param
     * @return
     */
    public Object getInsertBrandInfo(Map paramMap) {
        return standardInfoMapper.getInsertBrandInfo(paramMap);
    }

    /**
     * 매입처 추가 후 정보 가져오기
     *
     * @param
     * @return
     */
    public Object getInsertSupplierInfo(Map paramMap) {
        return standardInfoMapper.getInsertSupplierInfo(paramMap);
    }

    /**
     * 기준정보 엑셀 데이터 -> MapList
     *
     * @param
     * @return
     */
    private List<Map<Object, String>> setExcelDataToMapList(StandardInfoDto formData, MultipartFile excelFile) throws IOException {
        // 공통 코드 리스트
        List codeList = commonMapper.getCmtCodeList();

        // 엑셀파일 -> Object 로 읽기
        FileInputStream file = (FileInputStream) excelFile.getInputStream();
        XSSFWorkbook workbook = new XSSFWorkbook(file);

        int rowIndex = 0;
        int columnIndex = 0;

        // 시트 수 (첫번째에만 존재하므로 0을 준다)
        XSSFSheet sheet = workbook.getSheetAt(0);

        // 행의 수
        int rows = sheet.getPhysicalNumberOfRows();

        // 데이터 리스트
        List<Map<Object, String>> dataList = new ArrayList<>();

        for (rowIndex = 0; rowIndex < rows; rowIndex++) {

            // 첫번째 행 헤더 제외
            if (rowIndex != 0) {
                // 행을 읽는다
                XSSFRow row = sheet.getRow(rowIndex);

                Map<Object, String> data = new HashMap<>();

                // 셀 빈값 체크
                if (row.getPhysicalNumberOfCells() > 0) {
                    // 셀의 수
                    int cells = row.getLastCellNum();
                    for (columnIndex = 0; columnIndex <= cells; columnIndex++) {

                        // 셀 값을 읽는다
                        XSSFCell cell = row.getCell(columnIndex);

                        // 셀이 빈값일경우를 위한 널체크
                        if (Objects.nonNull(cell)) {
                            String key = setStandardInfoKey(sheet.getRow(0).getCell(columnIndex).toString());
                            String value = "";

                            // 타입별로 내용 읽기
                            switch (cell.getCellType()) {
                                case FORMULA:
                                    value = cell.getCellFormula();
                                    break;
                                case NUMERIC:
                                    value = cell.getNumericCellValue() + "";
                                    break;
                                case STRING:
                                    value = cell.getStringCellValue() + "";
                                    break;
                                case BLANK:
                                    value = cell.getBooleanCellValue() + "";
                                    break;
                                case ERROR:
                                    value = cell.getErrorCellValue() + "";
                                    break;
                            }

                            // 코드값 변환
                            String finalValue = value;
                            for (Object codes : codeList) {
                                if (((Map) codes).get("codeValue").equals(finalValue)) {
                                    value = ((Map) codes).get("code").toString();
                                }
                            }
                            data.put("regSupId", String.valueOf(formData.getRegSupId()));
                            data.put("regBrnId", String.valueOf(formData.getRegBrnId()));
                            data.put(key, value);
                        }
                    }
                    dataList.add(data);
                }
            }
        }
        return dataList;
    }

    /**
     * 기준정보 키값 세팅
     *
     * @param
     * @return
     */
    private String setStandardInfoKey(String key) {
        switch (key) {
            case "상품구분":
                key = "regPrdtDivCode";
                break;
            case "상품명":
                key = "regPrdtName";
                break;
            case "유형":
                key = "regPrdtTypeCode";
                break;
            case "디자인":
                key = "regPrdtDesignCode";
                break;
            case "색상":
                key = "regPrdtColorCode";
                break;
            case "매입단가":
                key = "regPurPrice";
                break;
            case "판매단가":
                key = "regSalesPrice";
                break;
            case "비고":
                key = "regRemarks";
                break;
        }
        return key;
    }
}
