package com.connecteye.ims.springboot.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 2021.11.01
 * IndexController
 * kyle
 */
@Controller
public class IndexController {

    /**
     * 기본 페이지
     * @param
     * @return
     */
    @RequestMapping(value = {"/", "/index"})
    public String getIndex() {
        return "redirect:/loginPage.ce";
    }
}
