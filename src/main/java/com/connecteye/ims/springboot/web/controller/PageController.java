package com.connecteye.ims.springboot.web.controller;

import com.connecteye.ims.springboot.config.auth.dto.SessionUser;
import com.connecteye.ims.springboot.config.properties.NiceProperties;
import com.connecteye.ims.springboot.util.JsonUtils;
import com.connecteye.ims.springboot.util.NiceCheckClient;
import com.connecteye.ims.springboot.web.mapper.CommonMapper;
import com.connecteye.ims.springboot.web.service.RedisService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Objects;

/**
 * 2021.11.01
 * Page 컨트롤러
 * kyle
 */
@Controller
@RequiredArgsConstructor
public class PageController {
    /* 로그인, 메인페이지 URL */
    private static final String LOGIN_PAGE = "page/login";
    private static final String LOGOUT_PAGE = "page/logout";
    private static final String SIGN_UP_PAGE = "page/signUp";
    private static final String FIND_USER_PAGE = "page/findUser";
    private static final String MOBILE_AUTH_PAGE = "page/mobileAuth";
    private static final String MOBILE_AUTH_POPUP_PAGE = "page/mobileAuthPopup";
    private static final String CLOSE_MOBILE_AUTH_POPUP_PAGE = "page/closeMobileAuthPopup";
    private static final String MAIN_PAGE = "layout/defaultLayout";

    /* 서비스 페이지 URL */
    private static final String STANDARD_INFO_PAGE = "page/standardInfo";
    private static final String SUPPLIER_INFO_PAGE = "page/supplierInfo";
    private static final String BRAND_PRODUCT_INFO_PAGE = "page/brandProductInfo";
    private static final String SUPPLIER_PURCHASE_PAGE = "page/supplierPurchaseInfo";
    private static final String CASH_PAYMENT_PAGE = "page/cashPaymentInfo";
    private static final String CASH_PAYMENT_BY_SUPPLIER_PAGE = "page/cashPaymentBySupplier";
    private static final String CASH_PAYMENT_BY_CUST_PAGE = "page/cashPaymentByCust";
    private static final String STOCK_STATUS_PAGE = "page/stockStatusInfo";
    private static final String OPTICIAN_PAGE = "page/optician";
    private static final String OPTOMETRY_SALES_PAGE = "page/optometrySalesInfo";
    private static final String SALES_STATUS_INFO = "page/salesStatusInfo";

    /* REDIRECT URL */
    private static final String REDIRECT_LOGIN_SUCCESS = "redirect:/loginSuccess";

    /* 공통 Mapper */
    private final CommonMapper commonMapper;

    /* Redis 서비스 */
    private final RedisService redisService;

    /* NICE 인증 속성 */
    private final NiceProperties niceProperties;

    /**
     * 로그인 페이지
     * @param
     * @return
     */
    @GetMapping("/loginPage.ce")
    public String getLoginPage(HttpSession httpSession, ModelMap model, @RequestParam(value="RESULT_MSG", required = false) String RESULT_MSG) {
        SessionUser userInfo = (SessionUser) httpSession.getAttribute("user");
        if (Objects.nonNull(userInfo)) {
            return REDIRECT_LOGIN_SUCCESS;
        }
        if (Objects.nonNull(RESULT_MSG)) {
            model.put("RESULT_MSG", RESULT_MSG);
        }
        return LOGIN_PAGE;
    }

    /**
     * 로그아웃 페이지
     * @param
     * @return
     */
    @GetMapping("/logoutPage.ce")
    public String getLogoutPage() {
        return LOGOUT_PAGE;
    }


    /**
     * 본인인증 팝업 페이지
     * @param session
     * @param model
     * @param request
     * @return
     */
    @GetMapping(value = "/mobileAuthPopup.ce")
    public String getMobileAuthPopupPage(HttpSession session, ModelMap model, HttpServletRequest request) {
        NiceCheckClient client = new NiceCheckClient(niceProperties.getCode(), niceProperties.getPassword());
        String reqUrl = (request.isSecure() ? "https://" : "http://") + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
        String returnUrl = reqUrl + "/login/successMobileAuth";
        String errorUrl = reqUrl + "/login/failureMobileAuth";
        String niceAuthRequest = client.encode(session, returnUrl, errorUrl);

        model.put("NICE_AUTH_REQUEST", niceAuthRequest);
        return MOBILE_AUTH_POPUP_PAGE;
    }

    /**
     * 본인인증 팝업 닫기
     * @param session
     * @param model
     * @param request
     * @return
     */
    @GetMapping(value = "/closeMobileAuthPopup.ce")
    public String getCloseMobileAuthPopupPage(HttpSession session, ModelMap model, HttpServletRequest request, @RequestParam(value="returnUrl", required = false) String returnUrl) {
        model.addAttribute("returnUrl", returnUrl);
        return CLOSE_MOBILE_AUTH_POPUP_PAGE;
    }

    /**
     * 회원가입 페이지
     * @param
     * @return
     */
    @GetMapping("/signUp.ce")
    public String getSignUpPage(HttpSession httpSession) {
        return SIGN_UP_PAGE;
    }

    /**
     * 아이디/비밀번호 찾기 페이지
     * @param
     * @return
     */
    @GetMapping("/findUser.ce")
    public String getFindUserPage(HttpSession httpSession) {
        return FIND_USER_PAGE;
    }


    /**
     * 본인인증 시작 페이지
     * @param
     * @return
     */
    @GetMapping("/mobileAuth.ce")
    public String getMobileAuthPage(HttpSession httpSession) {
        return MOBILE_AUTH_PAGE;
    }

    /**
     * 메인 페이지
     * @param
     * @return
     */
    @GetMapping("/page/mainPage.ce")
    public String getMainPage(Model model, HttpSession httpSession) {
        SessionUser user = (SessionUser) httpSession.getAttribute("user");
        model.addAttribute("userInfo", user);
        model.addAttribute("code", commonMapper.getCmtCodeList());

        String result = redisService.getStringValue(user.getId());
        SessionUser sessionUser = (SessionUser) JsonUtils.convertObjectFromJsonStr(result, SessionUser.class);

        return MAIN_PAGE;
    }

    /**
     * 기준정보 페이지
     * @param
     * @return
     */
    @GetMapping("/page/standardInfoPage.ce")
    public String getStandardInfoPage() {
        return STANDARD_INFO_PAGE;
    }

    /**
     * 매입처 페이지
     * @param
     * @return
     */
    @GetMapping("/page/supplierInfoPage.ce")
    public String getSupplierInfoPage() {
        return SUPPLIER_INFO_PAGE;
    }

    /**
     * 브랜드/ 상품 페이지
     * @param
     * @return
     */
    @GetMapping("/page/brandProductInfoPage.ce")
    public String getBrandProductInfoPage() {
        return BRAND_PRODUCT_INFO_PAGE;
    }

    /**
     * 매입입고 페이지
     * @param
     * @return
     */
    @GetMapping("/page/supplierPurchaseInfoPage.ce")
    public String getSupplierPurchaseInfoPage() {
        return SUPPLIER_PURCHASE_PAGE;
    }

    /**
     * 금전출납 페이지
     * @param
     * @return
     */
    @GetMapping("/page/cashPaymentInfoPage.ce")
    public String getCashPaymentInfoPage() {
        return CASH_PAYMENT_PAGE;
    }

    /**
     * 금전출납 매입처별 납부 페이지
     * @param
     * @return
     */
    @GetMapping("/page/cashPaymentBySupplierPage.ce")
    public String getCashPaymentBySupplierPage(@RequestParam String supId, Model model) {
        model.addAttribute("supId", supId);
        return CASH_PAYMENT_BY_SUPPLIER_PAGE;
    }

    /**
     * 금전출납 고객별 미수금 납부 페이지
     * @param
     * @return
     */
    @GetMapping("/page/cashPaymentByCustPage.ce")
    public String cashPaymentByCustPage(@RequestParam String custId, Model model) {
        model.addAttribute("custId", custId);
        return CASH_PAYMENT_BY_CUST_PAGE;
    }

    /**
     * 재고현황 페이지
     * @param
     * @return
     */
    @GetMapping("/page/stockStatusInfoPage.ce")
    public String getStockStatusInfoPage() {
        return STOCK_STATUS_PAGE;
    }

    /**
     * 안경원 생성 및 관리 페이지
     * @param
     * @return
     */
    @GetMapping("/page/opticianPage.ce")
    public String getOpticianPage() {
        return OPTICIAN_PAGE;
    }

    /**
     * 검안매출 페이지
     * @param
     * @return
     */
    @GetMapping("/page/optometrySalesInfoPage.ce")
    public String getOptometrySalesInfoPage(@RequestParam(required = false) String custId, Model model) {
        model.addAttribute("custId", custId);
        return OPTOMETRY_SALES_PAGE;
    }

    /**
     * 매출현황 페이지
     * @param
     * @return
     */
    @GetMapping("/page/salesStatusInfoPage.ce")
    public String getSalesStatusInfoPage() {
        return SALES_STATUS_INFO;
    }
}
