package com.connecteye.ims.springboot.web.mapper;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 2021.11.03
 * 공통 Mapper
 * kyle
 */
@Mapper
public interface CommonMapper {
    /**
     * 공통 코드 리스트
     * @param
     * @return
     */
    List<Map<String, Object>> getCmtCodeList();
}
