package com.connecteye.ims.springboot.web.service;

import com.connecteye.ims.springboot.web.mapper.OpticianInfoMapper;
import com.github.pagehelper.PageInfo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 2022.04.15
 * 안경원 Service
 * bella
 */
@Service
@RequiredArgsConstructor
public class OpticianInfoService {

    /* 안경원정보 Mapper */
    private final OpticianInfoMapper opticianInfoMapper;

    /**
     * 안경원정보 목록 가져오기
     *
     * @param paramMap 파라미터
     * @return Object
     */
    public Object getOpticianInfoList(Map paramMap) {
        return PageInfo.of(opticianInfoMapper.getOpticianInfoList(paramMap));
    }

    /**
     * 안경원정보 생성
     *
     * @param formData form 데이터
     * @return Object
     */
    public Object insertOpticianInfo(Map formData) {
        return opticianInfoMapper.insertOpticianInfo(formData);
    }
}
