package com.connecteye.ims.springboot.web.mapper;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 2022.06.03
 * 매출현황 Mapper
 * kyle
 */
@Mapper
public interface SalesStatusInfoMapper {

    /**
     * 매출현황 목록 리스트
     * @param
     * @return
     */
    List<Map<String, Object>> getSalesStatusInfoList(Map paramMap);

    /**
     * 매출현황 매입처 목록 리스트
     * @param
     * @return
     */
    List<Map<String, Object>> getSalesStatusSupplierInfo(Map paramMap);

    /**
     * 매출현황 정보
     * @param
     * @return
     */
    List<Map<String, Object>> getSalesStatusInfo(Map paramMap);
}
