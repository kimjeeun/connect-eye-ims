package com.connecteye.ims.springboot.web.controller.api;

import com.connecteye.ims.springboot.util.ResultUtils;
import com.connecteye.ims.springboot.web.service.CashPaymentInfoService;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * 2022.01.17
 * CashPaymentInfoApiController
 * kyle
 */
@RestController
@RequestMapping(value = "/api/cashPaymentInfo")
@RequiredArgsConstructor
public class CashPaymentInfoApiController {

    /* 금전출납 서비스 */
    private final CashPaymentInfoService cashPaymentInfoService;

    /**
     * 금전출납 목록 가져오기
     * @param
     * @return
     */
    @GetMapping("/getCashPaymentInfo")
    public Object getCashPaymentInfo(@RequestParam Map paramMap) {
        return ResultUtils.getQueryResult(cashPaymentInfoService.getCashPaymentInfoList(paramMap));
    }

    /**
     * 금전출납 현황 가져오기
     * @param
     * @return
     */
    @GetMapping("/getCashPaymentStatus")
    public Object getCashPaymentStatus(@RequestParam Map paramMap) {
        return ResultUtils.getQueryResult(cashPaymentInfoService.getCashPaymentStatus(paramMap));
    }

    /**
     * 금전출납 납부내역 가져오기
     * @param
     * @return
     */
    @GetMapping("/getCashPaymentHistory")
    public Object getCashPaymentHistory(@RequestParam Map paramMap) {
        return ResultUtils.getQueryResult(cashPaymentInfoService.getCashPaymentHistory(paramMap));
    }

    /**
     * 금전출납 납부내역 등록
     * @param
     * @return
     */
    @PostMapping("/insertCashPaymentHistory")
    public Object insertCashPaymentHistory(@RequestParam Map paramMap) {
        return ResultUtils.getQueryResult(cashPaymentInfoService.insertCashPaymentHistory(paramMap));
    }

    /**
     * 금전출납 납부내역 삭제
     * @param
     * @return
     */
    @PostMapping("/deleteCashPaymentHistory")
    public Object deleteCashPaymentHistory(@RequestParam Map paramMap) {
        return ResultUtils.getQueryResult(cashPaymentInfoService.deleteCashPaymentHistory(paramMap));
    }
}
