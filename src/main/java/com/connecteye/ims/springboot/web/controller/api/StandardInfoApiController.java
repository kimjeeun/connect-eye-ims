package com.connecteye.ims.springboot.web.controller.api;

import com.connecteye.ims.springboot.util.ResultUtils;
import com.connecteye.ims.springboot.web.controller.api.dto.StandardInfoDto;
import com.connecteye.ims.springboot.web.service.StandardInfoService;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * 2021.11.01
 * StandardInfoApiController
 * kyle
 */
@RestController
@RequestMapping(value = "/api/standardInfo")
@RequiredArgsConstructor
public class StandardInfoApiController {

    /* 기준정보 서비스 */
    private final StandardInfoService standardInfoService;

    /**
     * 기준정보 목록 가져오기
     *
     * @param
     * @return
     */
    @GetMapping("/getStandardInfoList")
    public Object getStandardInfoList(@RequestParam Map paramMap) {
        return ResultUtils.getGridResult(standardInfoService.getStandardInfoList(paramMap));
    }

    /**
     * 기준정보 현황 가져오기
     *
     * @param
     * @return
     */
    @GetMapping("/getStandardInfoStatus")
    public Object getStandardInfoStatus() {
        return ResultUtils.getQueryResult(standardInfoService.getStandardInfoStatus());
    }

    /**
     * 기준정보 사용 설정
     *
     * @param
     * @return
     */
    @PutMapping("/updateStandardInfoUse")
    public Object updateStandardInfoUse(@RequestParam Map paramMap) {
        return ResultUtils.getQueryResult(standardInfoService.updateStandardInfoUse(paramMap));
    }

    /**
     * 매입처 리스트
     *
     * @param
     * @return
     */
    @GetMapping("/getSupplierList")
    public Object getSupplierList(@RequestParam Map paramMap) {
        return ResultUtils.getQueryResult(standardInfoService.getSupplierList(paramMap));
    }

    /**
     * 브랜드 리스트
     *
     * @param
     * @return
     */
    @GetMapping("/getBrandList")
    public Object getBrandList(@RequestParam Map paramMap) {
        return ResultUtils.getQueryResult(standardInfoService.getBrandList(paramMap));
    }

    /**
     * 기준정보 상품 리스트
     *
     * @param
     * @return
     */
    @GetMapping("/getStandardInfoProductList")
    public Object getStandardInfoProductList(@RequestParam Map paramMap) {
        return ResultUtils.getQueryResult(standardInfoService.getStandardInfoProductList(paramMap));
    }

    /**
     * 기준정보 검색박스 상품 리스트
     *
     * @param
     * @return
     */
    @GetMapping("/getStandardInfoSearchBoxProductList")
    public Object getStandardInfoSearchBoxProductList(@RequestParam Map paramMap) {
        return ResultUtils.getSearchBoxResult(standardInfoService.getStandardInfoList(paramMap));
    }

    /**
     * 기준정보 목록 상품 추가
     *
     * @param
     * @return
     */
    @PostMapping(value = "/insertStandardInfoList", consumes = {"multipart/form-data"})
    public void insertStandardInfoList(
            @RequestPart(value = "formData") List<StandardInfoDto> formDataList,
            @RequestPart(value = "productImages", required = false) List<MultipartFile> productImages) {
        standardInfoService.insertStandardInfoList(formDataList, productImages);
    }

    /**
     * 엑셀 기준정보 목록 상품 추가
     *
     * @param
     * @return
     */
    @PostMapping(value = "/insertExcelStandardInfoList")
    public void insertExcelStandardInfoList(@RequestPart(value = "formData") StandardInfoDto formData,
                                            @RequestPart(value = "excelFile", required = false) MultipartFile excelFile) throws IOException {
        standardInfoService.insertExcelStandardInfoList(formData, excelFile);
    }

    /**
     * 기준정보 상품 수정
     *
     * @param
     * @return
     */
    @PostMapping(value = "/updateStandardInfo", consumes = {"multipart/form-data"})
    public Object updateStandardInfo(
            @RequestPart(value = "formData") StandardInfoDto formData,
            @RequestPart(value = "productImage", required = false) MultipartFile productImage) {
        return ResultUtils.getQueryResult(standardInfoService.updateStandardInfo(formData, productImage));
    }

    /**
     * 기준정보 상품 삭제
     *
     * @param
     * @return
     */
    @DeleteMapping("/deleteStandardInfo")
    public Object deleteStandardInfo(@RequestParam(value = "pks[]") List pks) {
        return ResultUtils.getQueryResult(standardInfoService.deleteStandardInfo(pks));
    }

    /**
     * 브랜드 추가 후 정보 가져오기
     *
     * @param
     * @return
     */
    @PostMapping("/getInsertBrandInfo")
    public Object getInsertBrandInfo(@RequestParam Map paramMap) {
        return ResultUtils.getQueryResult(standardInfoService.getInsertBrandInfo(paramMap));
    }

    /**
     * 매입처 추가 후 정보 가져오기
     *
     * @param
     * @return
     */
    @PostMapping("/getInsertSupplierInfo")
    public Object getInsertSupplierInfo(@RequestParam Map paramMap) {
        return ResultUtils.getQueryResult(standardInfoService.getInsertSupplierInfo(paramMap));
    }
}
