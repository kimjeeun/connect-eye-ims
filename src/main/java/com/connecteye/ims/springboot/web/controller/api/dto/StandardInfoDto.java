package com.connecteye.ims.springboot.web.controller.api.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

/**
 * 2021.12.03
 * StandardInfoDto
 * kyle
 */
@Getter
@Setter
public class StandardInfoDto {

    /*
     *  매입처 아이디
     */
    private int regSupId;

    /*
     *  브랜드 아이디
     */
    private int regBrnId;

    /*
     *  상품 아이디
     */
    private int regPrdtId;

    /*
     *  매입처상품 아이디
     */
    private int regSupPrdtId;

    /*
     *  상품명
     */
    private String regPrdtName;

    /*
     *  브랜드명
     */
    private String regBrnName;

    /*
     *  바코드
     */
    private String regBarcode;

    /*
     *  상품구분코드
     */
    private String regPrdtDivCode;

    /*
     *  상품유형코드
     */
    private String regPrdtTypeCode;

    /*
     *  상품색상코드
     */
    private String regPrdtColorCode;

    /*
     *  상품디자인인코드
     */
    private String regPrdtDesignCode;

    /*
     *  매입금액
     */
    private int regPurPrice;

    /*
     *  판매금액
     */
    private int regSalesPrice;

    /*
     *  비고
     */
    private String regRemarks;

    /*
     *  원본이미지경로
     */
    private String originImgPath;

    /*
     *  미리보기이미지경로
     */
    private String previewImgPath;
}
