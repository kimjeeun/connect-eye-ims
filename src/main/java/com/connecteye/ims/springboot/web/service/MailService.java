package com.connecteye.ims.springboot.web.service;

import com.connecteye.ims.springboot.util.MailUtils;
import com.connecteye.ims.springboot.web.controller.api.dto.AttachFileDto;
import com.connecteye.ims.springboot.web.controller.api.dto.MailDto;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.tika.parser.mail.MailUtil;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;
import java.io.File;

@Service
@RequiredArgsConstructor
public class MailService {

  private final JavaMailSenderImpl javaMailSender;


  public void sendMail(MailDto mailDto) {
    MimeMessage mimeMessage = javaMailSender.createMimeMessage();

    try {
      MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true, "UTF-8"); // use multipart (true)

      InternetAddress[] toAddress = MailUtils.convertListToInternetAddressArray(mailDto.getToAddressList(), "UTF-8");
      InternetAddress[] ccAddress = MailUtils.convertListToInternetAddressArray(mailDto.getCcAddressList(), "UTF-8");
      InternetAddress[] bccAddress = MailUtils.convertListToInternetAddressArray(mailDto.getBccAddressList(), "UTF-8");

      mimeMessageHelper.setSubject(MimeUtility.encodeText(mailDto.getSubject(), "UTF-8", "B")); // Base64 encoding
      mimeMessageHelper.setText(mailDto.getContent(), mailDto.isUseHtml());
      mimeMessageHelper.setFrom(new InternetAddress(mailDto.getFromAddress(), mailDto.getFromAddress(), "UTF-8"));
      mimeMessageHelper.setTo(toAddress);
      mimeMessageHelper.setCc(ccAddress);
      mimeMessageHelper.setBcc(bccAddress);

      if (!CollectionUtils.isEmpty(mailDto.getAttachFileList())) {
        for (AttachFileDto attachFileDto : mailDto.getAttachFileList()) {
          FileSystemResource fileSystemResource = new FileSystemResource(new File(attachFileDto.getRealFileName()));
          mimeMessageHelper.addAttachment(MimeUtility.encodeText(attachFileDto.getAttachFileName(), "UTF-8", "B"), fileSystemResource);
        }
      }

      javaMailSender.send(mimeMessage);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

}
