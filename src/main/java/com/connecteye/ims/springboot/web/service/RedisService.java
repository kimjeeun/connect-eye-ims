package com.connecteye.ims.springboot.web.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

@Service
public class RedisService {

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    /**
     * Redis에 세팅할 set command
     * @param key redis key
     * @param value key에 저장될 value
     */
    public void setStringValue(String key, String value) {
        redisTemplate.opsForValue().set(key, value);
    }

    /**
     * Redis에서 가져올 get comand
     * @param key redis key
     * @return key값을 통해 저장된 value
     */
    public String getStringValue(String key) {
        return (String) redisTemplate.opsForValue().get(key);
    }

    /**
     * Redis delete command
     * @param key redis key
     */
    public void deleteStringValue(String key) {
        redisTemplate.delete(key);
    }
}
