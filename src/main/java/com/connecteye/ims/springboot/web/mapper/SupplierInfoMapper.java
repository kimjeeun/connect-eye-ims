package com.connecteye.ims.springboot.web.mapper;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 2022.01.17
 * SUP_MA 매입처_마스터 Mapper
 * kyle
 */
@Mapper
public interface SupplierInfoMapper {
    /**
     * 매입처 정보 가져오기
     * @param
     * @return
     */
    List<Map<String, Object>> getSupplierInfo(Map paramMap);

    /**
     * 매입처 사용 설정
     * @param
     * @return
     */
    int updateSupplierInfoUse(Map paramMap);

    /**
     * 매입처 추가
     * @param
     * @return
     */
    int insertSupplierInfo(Map paramMap);

    /**
     * 매입처 수정
     * @param
     * @return
     */
    int updateSupplierInfo(Map paramMap);

    /**
     * 매입처 삭제
     * @param
     * @return
     */
    int deleteSupplierInfo(List pks);
}
