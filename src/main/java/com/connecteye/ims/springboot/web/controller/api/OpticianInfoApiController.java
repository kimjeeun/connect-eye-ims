package com.connecteye.ims.springboot.web.controller.api;

import com.connecteye.ims.springboot.util.ResultUtils;
import com.connecteye.ims.springboot.web.controller.api.dto.OpticianInfoDto;
import com.connecteye.ims.springboot.web.controller.api.dto.StandardInfoDto;
import com.connecteye.ims.springboot.web.service.OpticianInfoService;
import com.connecteye.ims.springboot.web.service.StandardInfoService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

/**
 * 2022.04.15
 * OpticianInfoApiController
 * bella
 */
@RestController
@RequestMapping(value = "/api/optician")
@RequiredArgsConstructor
public class OpticianInfoApiController {

    /* 안경원정보 서비스 */
    private final OpticianInfoService opticianInfoService;

    /**
     * 안경원정보 목록 가져오기
     *
     * @param paramMap 파라미터
     * @return Object
     */
    @PostMapping("/getOpticianInfoList")
    public Object getOpticianInfoList(@RequestParam Map paramMap) {
        return ResultUtils.getQueryResult(opticianInfoService.getOpticianInfoList(paramMap));
    }

    /**
     * 안경원정보 생성
     *
     * @param formData form 데이터
     * @return Object
     */
    @PostMapping(value = "/insertOpticianInfo")
    public Object insertOpticianInfo(@RequestPart(value = "formData") Map formData) {
        return ResultUtils.getQueryResult(opticianInfoService.insertOpticianInfo(formData));
    }
}
