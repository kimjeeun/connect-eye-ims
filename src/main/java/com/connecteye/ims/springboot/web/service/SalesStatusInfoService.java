package com.connecteye.ims.springboot.web.service;

import com.connecteye.ims.springboot.web.mapper.SalesStatusInfoMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 2022.04.20
 * 재고현황 Service
 * kyle
 */
@Service
@RequiredArgsConstructor
public class SalesStatusInfoService {

    /* 매출현황 Mapper */
    private final SalesStatusInfoMapper salesStatusInfoMapper;

    /**
     * 매출현황 목록 가져오기
     * @param
     * @return
     */
    public Object getSalesStatusInfoList(Map paramMap) {
        return salesStatusInfoMapper.getSalesStatusInfoList(paramMap);
    }

    /**
     * 매출현황 매입처 가져오기
     * @param
     * @return
     */
    public Object getSalesStatusSupplierInfo(Map paramMap) {
        return salesStatusInfoMapper.getSalesStatusSupplierInfo(paramMap);
    }

    /**
     * 매출현황 정보
     * @param
     * @return
     */
    public Object getSalesStatusInfo(Map paramMap) {
        return salesStatusInfoMapper.getSalesStatusInfo(paramMap);
    }
}
