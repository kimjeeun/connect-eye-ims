package com.connecteye.ims.springboot.web.controller.api;

import lombok.RequiredArgsConstructor;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.Optional;

/**
 * 2021.11.01
 * web rest controller
 * kyle
 */
@RestController
@RequiredArgsConstructor
public class WebRestController {

    private final Environment env;

    /**
     * profile 가져오기
     * @param
     * @return
     */
    @GetMapping("/profile")
    public String getProfile() {
        return Arrays.stream(env.getActiveProfiles()).findFirst().orElse("");
    }

    /**
     * port 가져오기
     * @param
     * @return
     */
    @GetMapping("/port")
    public String getPort() {
        return Optional.ofNullable(env.getProperty("local.server.port")).orElse("");
    }

}
