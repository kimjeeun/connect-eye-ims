package com.connecteye.ims.springboot.web.controller.api;

import com.connecteye.ims.springboot.util.ResultUtils;
import com.connecteye.ims.springboot.web.service.OptometrySalesInfoService;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * 2022.05.17
 * OptometrySalesInfoApiController
 * kyle
 */
@RestController
@RequestMapping(value = "/api/optometrySalesInfo")
@RequiredArgsConstructor
public class OptometrySalesInfoApiController {

    /* 검안매출 서비스 */
    private final OptometrySalesInfoService optometrySalesInfoService;

    /**
     * 고객 목록 가져오기
     * @param
     * @return
     */
    @GetMapping("/getCustList")
    public Object getCustList(@RequestParam Map paramMap) {
        return ResultUtils.getQueryResult(optometrySalesInfoService.getCustList(paramMap));
    }

    /**
     * 매출서 목록 가져오기
     * @param
     * @return
     */
    @GetMapping("/getSalesInfoList")
    public Object getSalesInfoList(@RequestParam Map paramMap) {
        return ResultUtils.getQueryResult(optometrySalesInfoService.getSalesInfoList(paramMap));
    }

    /**
     * 최근 검안정보 목록 가져오기
     * @param
     * @return
     */
    @GetMapping("/getOptometryInfoList")
    public Object getOptometryInfoList(@RequestParam Map paramMap) {
        return ResultUtils.getQueryResult(optometrySalesInfoService.getOptometryInfoList(paramMap));
    }

    /**
     * 검안매출 거래내역 추가
     * @param
     * @return
     */
    @PostMapping("/getInsertOptometrySaleInfo")
    public Object getInsertOptometrySaleInfo(HttpSession httpSession,
                                             @RequestParam Map optmtSalesInfo,
                                             @RequestParam Map optometryList,
                                             @RequestParam String optometryPayInfo) throws JsonProcessingException {
        return ResultUtils.getQueryResult(optometrySalesInfoService.getInsertOptometrySaleInfo(httpSession, optmtSalesInfo, optometryList, optometryPayInfo));
    }

    /**
     * 기타매출 거래내역 추가
     * @param
     * @return
     */
    @PostMapping("/getInsertExtSalesInfo")
    public Object getInsertExtSalesInfo(HttpSession httpSession, @RequestParam Map extSalesInfo) throws JsonProcessingException {
        return ResultUtils.getQueryResult(optometrySalesInfoService.getInsertExtSalesInfo(httpSession, extSalesInfo));
    }

    /**
     * 검안매출 거래내역 수정
     * @param
     * @return
     */
    @PostMapping("/updateOptometrySales")
    public Object updateOptometrySales(@RequestParam Map paramMap) {
        return ResultUtils.getQueryResult(optometrySalesInfoService.updateOptometrySales(paramMap));
    }

    /**
     * 검안매출 거래내역 삭제
     * @param
     * @return
     */
    @PostMapping("/deleteOptometrySales")
    public Object deleteOptometrySales(@RequestParam Map paramMap) {
        return ResultUtils.getQueryResult(optometrySalesInfoService.deleteOptometrySales(paramMap));
    }

    /**
     * 고객별 금전출납 목록 가져오기
     * @param
     * @return
     */
    @GetMapping("/getOptometryCustPayInfo")
    public Object getOptometryCustPayInfo(@RequestParam Map paramMap) {
        return ResultUtils.getQueryResult(optometrySalesInfoService.getOptometryCustPayInfo(paramMap));
    }

    /**
     * 고객별 금전출납 납부내역 가져오기
     * @param
     * @return
     */
    @GetMapping("/getOptometryCustPayHistory")
    public Object getOptometryCustPayHistory(@RequestParam Map paramMap) {
        return ResultUtils.getQueryResult(optometrySalesInfoService.getOptometryCustPayHistory(paramMap));
    }

    /**
     * 고객별 금전출납 납부내역 등록
     * @param
     * @return
     */
    @PostMapping("/insertOptometryCustPayHistory")
    public Object insertOptometryCustPayHistory(@RequestParam Map paramMap) {
        return ResultUtils.getQueryResult(optometrySalesInfoService.insertOptometryCustPayHistory(paramMap));
    }

    /**
     * 고객별 금전출납 납부내역 삭제
     * @param
     * @return
     */
    @PostMapping("/deleteOptometryCustPayHistory")
    public Object deleteOptometryCustPayHistory(@RequestParam Map paramMap) {
        return ResultUtils.getQueryResult(optometrySalesInfoService.deleteOptometryCustPayHistory(paramMap));
    }

    /**
     * 고객 추가 후 정보 가져오기
     * @param
     * @return
     */
    @PostMapping("/getInsertCustInfo")
    public Object getInsertCustInfo(@RequestParam Map paramMap) {
        return ResultUtils.getQueryResult(optometrySalesInfoService.getInsertCustInfo(paramMap));
    }

    /**
     * 고객 가족관계 정보 가져오기
     * @param
     * @return
     */
    @GetMapping("/getCustFamilyInfo")
    public Object getCustFamilyInfo(@RequestParam Map paramMap) {
        return ResultUtils.getQueryResult(optometrySalesInfoService.getCustFamilyInfo(paramMap));
    }

    /**
     * 고객 가족관계 정보 저장
     * @param
     * @return
     */
    @PostMapping("/saveCustFamilyInfo")
    public Object saveCustFamilyInfo(@RequestParam String custFamilyInfo,
                                     @RequestParam String custInfo) {
        return ResultUtils.getQueryResult(optometrySalesInfoService.saveCustFamilyInfo(custInfo, custFamilyInfo));
    }
}
