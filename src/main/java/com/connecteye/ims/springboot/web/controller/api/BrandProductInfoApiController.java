package com.connecteye.ims.springboot.web.controller.api;

import com.connecteye.ims.springboot.util.ResultUtils;
import com.connecteye.ims.springboot.web.controller.api.dto.StandardInfoDto;
import com.connecteye.ims.springboot.web.service.BrandProductInfoService;
import com.connecteye.ims.springboot.web.service.StandardInfoService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

/**
 * 2021.11.01
 * BrandProductInfoApiController
 * kyle
 */
@RestController
@RequestMapping(value = "/api/brandProductInfo")
@RequiredArgsConstructor
public class BrandProductInfoApiController {

    /* 브랜드/상품 서비스 */
    private final BrandProductInfoService brandProductInfoService;

    /* 기준정보 서비스 */
    private final StandardInfoService standardInfoService;

    /**
     * 브랜드/상품 목록 가져오기
     * @param
     * @return
     */
    @GetMapping("/getBrandProductInfo")
    public Object getBrandProductInfo(@RequestParam Map paramMap) {
        return ResultUtils.getGridResult(brandProductInfoService.getBrandProductInfo(paramMap));
    }

    /**
     * 브랜드/상품 사용 설정
     * @param
     * @return
     */
    @PutMapping("/updateBrandProductInfoUse")
    public Object updateBrandProductInfoUse(@RequestParam Map paramMap) {
        return ResultUtils.getQueryResult(brandProductInfoService.updateBrandProductInfoUse(paramMap));
    }

    /**
     * 브랜드 리스트
     * @param
     * @return
     */
    @GetMapping("/getBrandList")
    public Object getBrandList(@RequestParam Map paramMap) {
        return ResultUtils.getQueryResult(standardInfoService.getBrandList(paramMap));
    }

    /**
     * 브랜드/상품 상품 추가
     * @param
     * @return
     */
    @PostMapping(value = "/insertBrandProductInfo", consumes = {"multipart/form-data"})
    public Object insertBrandProductInfo(
            @RequestPart(value = "formData") StandardInfoDto formData,
            @RequestPart(value = "productImage", required = false) MultipartFile productImage) {
        return ResultUtils.getQueryResult(brandProductInfoService.insertBrandProductInfo(formData, productImage));
    }

    /**
     * 브랜드/상품 상품 수정
     * @param
     * @return
     */
    @PostMapping(value = "/updateBrandProductInfo", consumes = {"multipart/form-data"})
    public Object updateBrandProductInfo(@RequestPart(value = "formData") StandardInfoDto formData) {
        return ResultUtils.getQueryResult(brandProductInfoService.updateBrandProductInfo(formData));
    }

    /**
     * 브랜드/상품 상품 삭제
     * @param
     * @return
     */
    @DeleteMapping("/deleteBrandProductInfo")
    public Object deleteBrandProductInfo(@RequestParam String jsonStr) {
        return ResultUtils.getQueryResult(brandProductInfoService.deleteBrandProductInfo(jsonStr));
    }

    /**
     * 브랜드 추가 후 정보 가져오기
     * @param
     * @return
     */
    @PostMapping("/getInsertBrandInfo")
    public Object getInsertBrandInfo(@RequestParam Map paramMap) {
        return ResultUtils.getQueryResult(standardInfoService.getInsertBrandInfo(paramMap));
    }
}
