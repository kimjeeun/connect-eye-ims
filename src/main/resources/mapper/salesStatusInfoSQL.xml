<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >
<mapper namespace="com.connecteye.ims.springboot.web.mapper.SalesStatusInfoMapper">

    <!-- 매출현황 목록 가져오기 -->
    <select id="getSalesStatusInfoList" parameterType="object" resultType="camelMap">
        SELECT A.SALES_ID
             , TO_CHAR(A.REG_DATE, 'YYYY-MM-DD')                                                 AS REG_DATE
             , (SELECT CODE_VALUE FROM CMT_CODE WHERE CODE = A.SALES_TYPE_CODE)                  AS SALES_TYPE
             , D.CUST_NAME
             , MAX(C.MNG_NAME)                                                                   AS MNG_NAME
             , COALESCE(C.CASH_PRICE, 0)
              + COALESCE(C.CARD_PRICE, 0)
              + COALESCE(C.GIFT_PRICE, 0)
              + COALESCE(C.SALE_PRICE, 0)                                                        AS TOTAL_SALES_PRICE
             , COALESCE(C.CASH_PRICE, 0) + COALESCE(C.CARD_PRICE, 0) + COALESCE(C.GIFT_PRICE, 0) AS SALES_PRICE
             , COALESCE(C.CASH_PRICE, 0) + COALESCE(C.CARD_PRICE, 0)                             AS PUR_PRICE
             , COALESCE(C.CASH_PRICE, 0)                                                         AS CASH_PRICE
             , COALESCE(C.CARD_PRICE, 0)                                                         AS CARD_PRICE
             , COALESCE(C.GIFT_PRICE, 0)                                                         AS GIFT_PRICE
             , COALESCE(C.SALE_PRICE, 0)                                                         AS SALE_PRICE
             , COALESCE(SUM(B.PRICE), 0)
               - (COALESCE(C.CASH_PRICE, 0)
                   + COALESCE(C.CARD_PRICE, 0)
                   + COALESCE(C.GIFT_PRICE, 0)
                   + COALESCE(C.SALE_PRICE, 0))                                                  AS RCV_PRICE
        FROM OPT_SALES A
            LEFT OUTER JOIN OPT_SALES_DETAIL B
                ON A.OPT_ID = B.OPT_ID
                    AND A.SALES_ID = B.SALES_ID
                LEFT OUTER JOIN OPT_SALES_PAY C
                ON B.OPT_ID = C.OPT_ID
                    AND B.SALES_ID = C.SALES_ID
            LEFT OUTER JOIN SUP_PRDT S
                ON B.SUP_PRDT_ID = S.SUP_PRDT_ID
            LEFT OUTER JOIN PRDT_MA P
                ON S.PRDT_ID = P.PRDT_ID
            LEFT OUTER JOIN CUST_MA D
                ON D.CUST_ID = A.CUST_ID
            <trim prefix="WHERE" prefixOverrides="AND | OR">
                <!-- 분류 -->
                <if test="@java.util.Objects@nonNull( salesTypeCode )">
                    AND A.SALES_TYPE_CODE = #{salesTypeCode}
                </if>
                <!-- 상품구분 -->
                <if test="@java.util.Objects@nonNull( prdtDivCode )">
                    AND P.PRDT_DIV_CODE = #{prdtDivCode}
                </if>
                <!-- 브랜드아이디 -->
                <if test="@java.util.Objects@nonNull( brnId )">
                    AND P.BRN_ID = #{brnId} :: numeric
                </if>
                <!-- 매입처아아디 -->
                <if test="@java.util.Objects@nonNull( supId )">
                    AND S.SUP_ID = #{supId} :: numeric
                </if>
                <!-- 상품아아디 -->
                <if test="@java.util.Objects@nonNull( prdtId )">
                    AND S.PRDT_ID = #{prdtId} :: numeric
                </if>
                <!-- 기준정보 pk -->
                <if test="@java.util.Objects@nonNull( supPrdtId )">
                    AND S.SUP_PRDT_ID = #{supPrdtId} :: numeric
                </if>
                <!-- 등록일자 -->
                <if test="@java.util.Objects@nonNull( startDate ) and @java.util.Objects@nonNull( endDate )">
                    AND TO_CHAR(A.REG_DATE, 'YYYY-MM-DD') BETWEEN #{startDate} AND #{endDate}
                </if>
            </trim>
        GROUP BY A.SALES_ID
               , A.SALES_TYPE_CODE
               , C.MNG_NAME
               , D.CUST_NAME
               , TO_CHAR(A.REG_DATE, 'YYYY-MM-DD')
               , C.CASH_PRICE
               , C.CARD_PRICE
               , C.GIFT_PRICE
               , C.SALE_PRICE
        ORDER BY A.SALES_ID DESC
    </select>

    <!-- 매출현황 매입처 가져오기 -->
    <select id="getSalesStatusSupplierInfo" parameterType="object" resultType="camelMap">
        SELECT B.SUP_ID
             , B.SUP_NAME
             , COALESCE(SUM(CC.PRICE), 0) AS TOTAL_PRICE
        FROM SUP_PRDT A
                 JOIN SUP_MA B
                      ON A.SUP_ID = B.SUP_ID
                 JOIN
                 (
                    SELECT SUP_PRDT_ID, PRICE, PUR_TYPE_CODE
                    FROM OPT_PUR C
                        LEFT OUTER JOIN OPT_PUR_DETAIL D
                            ON C.OPT_ID = D.OPT_ID
                            AND C.PUR_ID = D.PUR_ID
                 ) CC ON A.SUP_PRDT_ID = CC.SUP_PRDT_ID
        WHERE CC.PUR_TYPE_CODE IN ('BY101', 'BY102')
        GROUP BY B.SUP_ID
               , B.SUP_NAME
    </select>

    <!-- 매출현황 -->
    <select id="getSalesStatusInfo" parameterType="object" resultType="camelMap">
        SELECT (
                    SELECT COALESCE(SUM(PRICE), 0)
                    FROM OPT_PUR A
                         LEFT OUTER JOIN OPT_PUR_DETAIL B
                             ON A.OPT_ID = B.OPT_ID
                                AND A.PUR_ID = B.PUR_ID
                         LEFT OUTER JOIN opt_pur_prog_detail c
                             on b.opt_id = c.opt_id
                                and b.pur_id = c.pur_id
                                and b.sup_prdt_id = c.sup_prdt_id
                    WHERE A.PUR_TYPE_CODE IN ('BY101', 'BY102')
                      AND TO_CHAR(A.REG_DATE, 'YYYY-MM-DD') BETWEEN #{startDate} AND #{endDate}
                      and c.prog_stat_code != 'ST113'
               ) AS TOTAL_SALES_PRICE
             , (
                    SELECT COUNT(*)
                    FROM OPT_PUR A
                         LEFT OUTER JOIN OPT_PUR_DETAIL B
                             ON A.OPT_ID = B.OPT_ID
                                 AND A.PUR_ID = B.PUR_ID
                    WHERE A.PUR_TYPE_CODE IN ('BY101', 'BY102')
                      AND TO_CHAR(A.REG_DATE, 'YYYY-MM-DD') BETWEEN #{startDate} AND #{endDate}
               ) AS TOTAL_SALES_COUNT
             , (
                    SELECT COALESCE(SUM(CASH_PRICE), 0) + COALESCE(SUM(CARD_PRICE), 0)
                    FROM OPT_SALES_PAY
                    WHERE TO_CHAR(SALES_PAY_DATE, 'YYYY-MM-DD') BETWEEN #{startDate} AND #{endDate}
               ) AS TOTAL_PUR_PRICE
             , (
                    SELECT COALESCE(SUM(SALE_PRICE), 0)
                    FROM OPT_SALES_PAY
                    WHERE TO_CHAR(SALES_PAY_DATE, 'YYYY-MM-DD') BETWEEN #{startDate} AND #{endDate}
               ) AS TOTAL_SALE_PRICE
             , (
                    SELECT COALESCE(SUM(GIFT_PRICE), 0)
                    FROM OPT_SALES_PAY
                    WHERE TO_CHAR(SALES_PAY_DATE, 'YYYY-MM-DD') BETWEEN #{startDate} AND #{endDate}
               ) AS TOTAL_GIFT_PRICE
             , (
                    SELECT COUNT(*)
                    FROM OPT_SALES_PAY
                    WHERE TO_CHAR(SALES_PAY_DATE, 'YYYY-MM-DD') BETWEEN #{startDate} AND #{endDate}
               ) AS SALES_PAY_COUNT
             , (
                    SELECT SUM(COALESCE(A.PRICE, 0)
                            - (COALESCE(B.CASH_PRICE, 0)
                             + COALESCE(B.CARD_PRICE, 0)
                             + COALESCE(B.GIFT_PRICE, 0)
                             + COALESCE(B.SALE_PRICE, 0)))
                    FROM OPT_SALES C
                         LEFT OUTER JOIN OPT_SALES_DETAIL A
                             ON C.OPT_ID = A.OPT_ID
                                 AND C.SALES_ID = A.SALES_ID
                         LEFT OUTER JOIN OPT_SALES_PAY B
                             ON A.OPT_ID = B.OPT_ID
                                 AND A.SALES_ID = B.SALES_ID
                    WHERE TO_CHAR(C.REG_DATE, 'YYYY-MM-DD') BETWEEN #{startDate} AND #{endDate}
               ) AS TOTAL_RCV_PRICE
             , (
                    SELECT COUNT(*)
                    FROM OPT_SALES C
                         LEFT OUTER JOIN OPT_SALES_DETAIL A
                             ON C.OPT_ID = A.OPT_ID
                                 AND C.SALES_ID = A.SALES_ID
                         LEFT OUTER JOIN OPT_SALES_PAY B
                             ON A.OPT_ID = B.OPT_ID
                                 AND A.SALES_ID = B.SALES_ID
                    WHERE TO_CHAR(C.REG_DATE, 'YYYY-MM-DD') BETWEEN #{startDate} AND #{endDate}
               ) AS TOTAL_RCV_COUNT
             , (
                    SELECT COALESCE(SUM(PRICE), 0)
                    FROM OPT_PUR A
                         LEFT OUTER JOIN OPT_PUR_DETAIL B
                             ON A.OPT_ID = B.OPT_ID
                                 AND A.PUR_ID = B.PUR_ID
                    WHERE A.PUR_TYPE_CODE IN ('BY103', 'BY104')
                      AND TO_CHAR(A.REG_DATE, 'YYYY-MM-DD') BETWEEN #{startDate} AND #{endDate}
               ) AS TOTAL_RETURN_PRICE
             , (
                    SELECT COUNT(*)
                    FROM OPT_PUR A
                         LEFT OUTER JOIN OPT_PUR_DETAIL B
                             ON A.OPT_ID = B.OPT_ID
                                 AND A.PUR_ID = B.PUR_ID
                    WHERE A.PUR_TYPE_CODE IN ('BY103', 'BY104')
                      AND TO_CHAR(A.REG_DATE, 'YYYY-MM-DD') BETWEEN #{startDate} AND #{endDate}
               ) AS TOTAL_RETURN_COUNT
    </select>
</mapper>