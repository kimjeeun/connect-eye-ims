/*
 *  Description: 매출현황 main.js
 *  User: kyle
 *  Date: 2022-06-14
 */

import salesStatusInfo from './salesStatusInfo';

$(document).ready(() => {
  // 매출현황
  salesStatusInfo();
});
