/*
 *  Description: 매출현황
 *  User: kyle
 *  Date: 2022-06-15
 */
import moment from 'moment';

import gridUtils from '../../utils/gridUtils';
import commonUtils from '../../utils/commonUtils';
import searchBoxUtils from '../../utils/searchBoxUtils';

export default () => {
  /***** Static Start *****/
  const $container = $('#salesStatusInfo');

  // table
  const salesStatusInfoTable = new gridUtils.create({
    el: 'salesStatusInfoTable',
    rowHeaders: [],
    bodyHeight: 'auto',
    minBodyHeight: 288,
    columns: [
      {
        header: '거래일자',
        name: 'regDate',
        width: 100,
        align: 'center',
      },
      {
        header: '분류',
        name: 'salesType',
        align: 'center',
        width: 80,
        formatter: undefined,
      },
      {
        header: '고객명',
        name: 'custName',
        width: 90,
        align: 'center',
      },
      {
        header: '총 매출',
        name: 'totalSalesPrice',
        align: 'right',
        formatter: commonUtils.setComma,
      },
      {
        header: '매출액',
        name: 'salesPrice',
        width: 100,
        align: 'right',
        formatter: commonUtils.setComma,
      },
      {
        header: '입금액',
        name: 'purPrice',
        width: 100,
        align: 'right',
        formatter: commonUtils.setComma,
      },
      {
        header: '현금',
        name: 'cashPrice',
        width: 100,
        align: 'right',
        formatter: commonUtils.setComma,
      },
      {
        header: '카드',
        name: 'cardPrice',
        width: 100,
        align: 'right',
        formatter: commonUtils.setComma,
      },
      {
        header: '상품권',
        name: 'giftPrice',
        width: 100,
        align: 'right',
        formatter: commonUtils.setComma,
      },
      {
        header: '할인',
        name: 'salePrice',
        width: 100,
        align: 'right',
        formatter: commonUtils.setComma,
      },
      {
        header: '미수금',
        name: 'rcvPrice',
        width: 100,
        align: 'right',
        formatter: commonUtils.setComma,
      },
      {
        header: '담당자',
        name: 'mngName',
        width: 100,
        align: 'center',
      },
    ],

    // 그리드 생성 완료 시
    onGridMounted: (_gridThis) => {
      // 그리드 레이아웃 새로고침
      _gridThis.instance.refreshLayout();

      // 초기 날짜
      setDateAndGetSalesStatusInfo(moment().subtract(29, 'days'), moment());
    },

    // 그리드 업데이트 시  'resetData', 'restore', 'reloadData', 'readData', 'setPerPage'
    onGridUpdated: (_gridThis) => {
      // 그리드 레이아웃 새로고침
      _gridThis.instance.refreshLayout();
    },
  });

  // 매입처 검색박스 적용
  searchBoxUtils.create({
    $el: $container.find('#supName'),
    title: '매입처',
    placeholder: '매입처',
    ajax: {
      url: '/api/standardInfo/getSupplierList',
      data: (params) => ({ supName: params.term }),
      processResults: (_result) => {
        let result = searchBoxUtils.getResultCallback(_result);
        return {
          results: result.map((obj) => ({ id: obj.supId, text: obj.supName, data: obj })),
        };
      },
      delay: 250,
      cache: true,
    },
    noAdd: true,
    noResultCallback: () => {},
  });

  // 브랜드 검색박스 적용
  searchBoxUtils.create({
    $el: $container.find('#brnName'),
    title: '브랜드',
    placeholder: '브랜드',
    ajax: {
      url: '/api/standardInfo/getBrandList',
      data: (params) => ({ brnName: params.term }),
      processResults: (res) => {
        let result = searchBoxUtils.getResultCallback(res);
        return {
          results: result.map((obj) => ({ id: obj.brnId, text: obj.brnName, data: obj })),
        };
      },
      delay: 250,
      cache: true,
    },
    noAdd: true,
    noResultCallback: () => {},
  });

  // 상품 검색박스 적용, 무한 스크롤 적용
  searchBoxUtils.create({
    $el: $container.find('#prdtName'),
    title: '상품',
    placeholder: '상품',
    ajax: {
      url: '/api/standardInfo/getStandardInfoSearchBoxProductList',
      data: (params) => ({
        supId: $('#supId').val(),
        brnId: $('#brnId').val(),
        prdtName: params.term,
        page: params.page || 1,
        perPage: 10,
        isUse: 1,
      }),
      processResults: (res) => {
        let result = searchBoxUtils.getResultCallback(res);
        const { totalCount, page } = res;
        return {
          results: result.map((obj) => ({
            id: obj.supPrdtId,
            text: obj.prdtName,
            data: obj,
          })),
          pagination: {
            more: (page * 10) < totalCount,
          }
        };
      },
      cache: false,
    },
    noAdd: true,
    noResultCallback: () => {},
  });

  // 거래기간 검색 달력
  connecteye.app.ui.createDatePicker({
    containerEl: '#salesStatusInfo .btn-calendar',
    ranges: true,
    callback: setDateAndGetSalesStatusInfo,
  });

  // selectbox 바인딩
  commonUtils.setSelectboxCode({ el: 'prdtDivCode', code: 'PO100' });
  commonUtils.setSelectboxCode({ el: 'salesTypeCode', code: 'SL100' });
  /***** Static End *****/

  /***** Event Start *****/
  // 매입처 선택 이벤트
  $container.find('#supName').on('change', (_evt) => {
    const resultObj = searchBoxUtils.getResultObject($(_evt.currentTarget));
    if (resultObj) {
      const { supId } = resultObj;
      // 매입처 아이디
      $container.find('input[name=supId]').val(supId);
    }
    selectEventHandler();
  });

  // 브랜드 선택 이벤트
  $container.find('#brnName').on('change', (_evt) => {
    let resultObj = searchBoxUtils.getResultObject($(_evt.currentTarget));
    if (resultObj) {
      const { brnId } = resultObj;
      // 브랜드 아이디
      $container.find('input[name=brnId]').val(brnId);
    }
    selectEventHandler();
  });

  // 상품 선택 이벤트
  $container.find('#prdtName').on('change', (_evt) => {
    const resultObj = searchBoxUtils.getResultObject($(_evt.currentTarget));
    if (resultObj) {
      const { supPrdtId } = resultObj;
      // 기준정보 아이디
      $container.find('input[name=supPrdtId]').val(supPrdtId);
    }
    selectEventHandler();
  });

  // 검색 버튼 이벤트
  $container.find('.btn-select').on('click', () => {
    getSalesStatusInfoList();
    getSalesStatusSupplierInfo();
  });
  $container.find('.search-group').on('keyup', (_evt) => {
    if (_evt.keyCode === 13) {
      getSalesStatusInfoList();
      getSalesStatusSupplierInfo();
    }
  });

  // 검색조건 초기화 이벤트
  $container.find('.btn-filter-reset').on('click', () => resetFilter());
  /***** Event End *****/

  /***** Business Start *****/
  /**
   * 날짜세팅 후 매출현황 목록 가져오기
   *
   * @param {Object} _start
   * @param {Object} _end
   */
  function setDateAndGetSalesStatusInfo(_start, _end) {
    const startDate = _start.format('YYYY-MM-DD');
    const endDate = _end.format('YYYY-MM-DD');
    const searchSalesDate = `${startDate} ~ ${endDate}`;
    $container.find('.search-sales-date').text(searchSalesDate);
    $container.find('#startDate').val(startDate);
    $container.find('#endDate').val(endDate);
    getSalesStatusSupplierInfo();
    getSalesStatusInfoList();
  }

  /**
   * 매출현황 목록 가져오기
   */
  function getSalesStatusInfoList() {
    const $form = $container.find('.search-group');
    const formSerializeData = commonUtils.serializeObject($form);
    // 그리드 로딩바
    salesStatusInfoTable.grid.dispatch('setLoadingState', 'LOADING');
    let options = {
      type: 'GET',
      url: '/api/salesStatusInfo/getSalesStatusInfoList',
      paramData: formSerializeData || null,
      callback: (_res) => {
        const totalCount = _res.length;
        // 데이터 건수
        $container.find('.result-cnt').text(commonUtils.setComma(totalCount));
        if (commonUtils.isNotEmpty(_res)) {
          // 그리드에 데이터 넣음
          salesStatusInfoTable.grid.resetData(_res);
          // hideLoadingBar
          salesStatusInfoTable.grid.dispatch('setLoadingState', 'DONE');
        } else {
          salesStatusInfoTable.grid.dispatch('setLoadingState', 'EMPTY');
        }
      },
    };
    commonUtils.callAjax(options, true);
  }

  /**
   * 매출현황 가져오기
   */
  function getSalesStatusSupplierInfo() {
    const $form = $container.find('.search-group');
    const formSerializeData = commonUtils.serializeObject($form);
    let options = {
      type: 'GET',
      url: '/api/salesStatusInfo/getSalesStatusInfo',
      paramData: formSerializeData || null,
      callback: (_res) => {
        const {
          totalSalesCount,
          salesPayCount,
          totalRcvCount,
          totalReturnCount,
          totalSalesPrice,
          totalPurPrice,
          totalSalePrice,
          totalGiftPrice,
          totalRcvPrice,
          totalReturnPrice,
        } = _res[0];
        $container
          .find('.total-sales-count')
          .text(`(${totalSalesCount})`)
          .end()
          .find('.sales-pay-count')
          .text(`(${salesPayCount})`)
          .end()
          .find('.total-rcv-count')
          .text(`(${totalRcvCount})`)
          .end()
          .find('.total-return-count')
          .text(`(${totalReturnCount})`)
          .end()
          .find('.total-sales-price')
          .text(commonUtils.setComma(totalSalesPrice))
          .end()
          .find('.total-pur-price')
          .text(commonUtils.setComma(totalPurPrice))
          .end()
          .find('.total-sale-price')
          .text(commonUtils.setComma(totalSalePrice))
          .end()
          .find('.total-gift-price')
          .text(commonUtils.setComma(totalGiftPrice))
          .end()
          .find('.total-rcv-price')
          .text(commonUtils.setComma(totalRcvPrice))
          .end()
          .find('.total-return-price')
          .text(commonUtils.setComma(totalReturnPrice))
          .end();
      },
    };
    commonUtils.callAjax(options, true);
  }

  /**
   * 선택 이벤트 핸들러
   */
  function selectEventHandler() {
    const supId = $('#supId').val();
    const brnId = $('#brnId').val();
    if (commonUtils.isEmpty(supId) || commonUtils.isEmpty(brnId)) {
      return false;
    }
    $container.find('.product-control').prop('disabled', false);
  }

  /**
   * 검색조건 초기화
   */
  function resetFilter() {
    const $form = $container.find('.search-group');

    // 검색조건 초기화
    $container
        .find('#supName')
        .empty()
        .trigger('change')
        .end()
        .find('#brnName')
        .empty()
        .trigger('change')
        .end()
        .find('#prdtName')
        .empty()
        .trigger('change');

    commonUtils.formReset($form.find('input, select'));
    $container.find('.product-control').prop('disabled', true);
  }
  /***** Business End *****/
};
