/*
 *  Description: workspace main.js
 *  User: kyle
 *  Date: 2021-12-09
 */
require('../resources/css/optician.css');
require('../../../node_modules/bootstrap/dist/css/bootstrap.min.css');

import optician from './optician';
import opticianReg from './opticianReg';


$(document).ready(() => {
  // 안경원정보
  optician();
  // 안경원정보 생성
  opticianReg();
});
