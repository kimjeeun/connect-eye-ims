/*
 *  Description: opticianReg.js
 *  User: bella
 *  Date: 2022-04-27
 */
import commonUtils from '../../utils/commonUtils';

require('jquery-validation');

export default () => {
  /***** Static Start *****/
  const $container = $('#Optician');
  const $navs = $container.find('.page-navs');

  const $form = $('#formOpticianInfo');
  const $showSearchBtn = $form.find('.show-search');

  // 유효성체크
  $form.validate({
    rules: {
      regOptName: {
        required: true,
        maxlength: 10,
      },
      regAddress: {
        required: true,
        maxlength: 30,
      },
      regAddressDetail: {
        required: true,
        maxlength: 30,
      },
      regPhone: {
        required: true,
        phonekor: true,
      },
      regBusinessNumber: {
        required: true,
        businessNumber: true,
      },
    },
    messages: {
      regOptName: {
        minlength: '상호명을 10글자 이하로 입력해주세요.',
      },
      regAddress: {
        minlength: '주소를 30글자 이하로 입력해주세요.',
      },
      regAddressDetail: {
        minlength: '상세주소를 30글자 이하로 입력해주세요.',
      },
    },
    errorPlacement: (_error, _element) => _element.parent().children().last().after(_error),
    submitHandler: () => {
      // 약관 체크 확인
      if (
        !$container.find('#agree1').is(':checked') ||
        !$container.find('#agree2').is(':checked')
      ) {
        connecteye.app.ui.alert.warning({
          text: `필수 약관 동의 사항을 체크해주세요`,
        });
        return false;
      }
      runInsertSubmit();
    },
  });

  // 유효성체크 메세지 설정
  commonUtils.setJqueryValidationLanguageKor();
  /***** Static End *****/

  /***** Event Start *****/
  // 주소찾기(카카오 우편번호서비스 api)
  const daumPostcode = commonUtils.getDaumPostCode($form);
  $showSearchBtn.on('click', () => daumPostcode.open());

  // 약관보기(이용약관)
  // @fixme 공용 동의서 필요

  // 약관보기(개인정보수집및이용)
  // @fixme 공용 동의서 필요

  // 취소 버튼 클릭 이벤트
  $form.find('.btn-cancel').on('click', () => {
    $container.find('.row-projects').removeClass('d-none').siblings().addClass('d-none');
    $navs.removeClass('d-none');

    $form.validate().resetForm();
  });
  /***** Event End *****/

  /***** Business Start *****/
  /**
   * 안경원정보 등록
   */
  function runInsertSubmit() {
    // form 전송 ajax 처리
    connecteye.app.ui.alert
      .confirm({
        title: '안경원을 생성할까요?',
        text: '취소를 누르면 내용을 수정 할 수 있어요',
        confirmBtnTxt: '생성',
        cancelBtnTxt: '취소',
        showLoaderOnConfirm: true,
        preConfirm: () => {},
      })
      .then((_result) => {
        if (_result.isConfirmed) {
          connecteye.app.ui.showCardLoading();
          saveOpticianInfo('/api/optician/insertOpticianInfo', () => {
            connecteye.app.ui.hideCardLoading();
            connecteye.app.ui.alert
              .success({
                title: '생성 완료!',
                text: '생선한 안경원이 추가됐어요',
                denyButtonText: '계속 추가하기',
                showDenyButton: true,
              })
              .then((_SuccessResult) => {
                if (_SuccessResult.isConfirmed) {
                  $container
                    .find('.row-projects')
                    .addClass('d-none')
                    .siblings()
                    .removeClass('d-none');
                }
              });
          });
        } else {
          connecteye.app.ui.alert.error({
            title: '생성 취소',
            text: '안경원 수정 후 다시 생성 할 수 있어요 :)',
            icon: 'error',
          });
        }
      });
  }
  /**
   * 안경원정보 저장
   *
   * @param {String} _url
   * @param {function} _callback
   */
  async function saveOpticianInfo(_url, _callback) {
    commonUtils.callAjax(
      {
        type: 'POST',
        url: _url,
        dataType: 'form',
        paramData: await commonUtils.getAjaxFormData({
          // 이미지와 같이 formData Request 할 경우
          // 이미지와 같이 타입, 바이너리 객체로 넘겨줘야됨
          formData: commonUtils.getBlobObject({
            data: commonUtils.serializeObject($form),
            option: {
              type: 'application/json',
            },
          }),
        }),
        callback: _callback,
      },
      true,
    );
  }
  /***** Business End *****/
};
