/*
 *  Description: optician.js
 *  User: kyle
 *  Date: 2022-04-14
 */
import commonUtils from '../../utils/commonUtils';

export default () => {
  /***** Static Start *****/
  const $container = $('#Optician');
  const $navs = $container.find('.page-navs');

  // 안경원정보 목록 가져오기
  // @fixme 파라미터 서버로 이동 필요
  getOpticianInfoList({
    optUpperId: '1',
    optTypeCode: 'AT102', //체인점유형
  });
  /***** Static End *****/

  /***** Event Start *****/
  $container.find('.row-projects').on('loadDom', function (_evt, _data) {
    // 안경원정보 목록 가져오기
    getOpticianInfoList({
      optUpperId: '2',
      optTypeCode: 'AT102', //체인점유형
    });
  });

  // 안경원 생성 버튼 클릭 이벤트
  $container.find('.btn-opt-create').on('click', function () {
    $container.find('.row-projects').addClass('d-none').siblings().removeClass('d-none');
    $navs.addClass('d-none');
  });

  // @fixme 안경원 삭제 필요
 /***** Event End *****/

  /***** Business Start *****/
  /**
   * 안경원정보 목록 가져오기
   *
   * @param {Object} _paramData
   */
  function getOpticianInfoList(_paramData) {
    let options = {
      type: 'POST',
      url: '/api/optician/getOpticianInfoList',
      paramData: _paramData || null,
      callback: (res) => {
        const { list } = res;

        // 탭 총 안경원 수
        const $navLink = $container.find('.nav-link');
        const $count = $navLink.find('.count');
        $count.text(`(${list.length})`);

        // 안경원 목록
        list.forEach((_item) => {
          const $rowProjects = $container.find('.row-projects');

          const $optician = $('<div/>', { class: 'optician animated fadeIn' });
          const $card = $('<div/>', { class: 'card' });
          const $imagecheck = $('<div/>', { class: 'imagecheck' });

          // 체크박스
          const $chkInput = $('<input/>', {
            name: 'imagecheck',
            type: 'radio',
            value: '1',
            class: 'imagecheck-input',
            checked: 'checked', // @fixme 이벤트 정의 필요
          });

          // 안경원정보
          const $imagecheckFigure = $('<figure/>', { class: 'imagecheck-figure' });

          const $optName = $('<span/>', { class: 'card-img-top imagecheck-image' });

          const $cardBody = $('<div/>', { class: 'card-body' });
          const $address = $('<p/>', { class: 'card-text d-flex address' });
          const $iconAddress = $('<span/>', {
            class: 'fas fa-address-book text-primary icon-address mr-2',
          });
          const $textAddress = $('<span/>', { class: 'text-address' });
          const $tel = $('<p/>', { class: 'card-text' });
          const $iconTel = $('<span/>', { class: 'fas fa-phone text-primary mr-2' });
          const $totalMem = $('<div/>', {
            class: 'd-flex justify-content-center align-items-center width-full',
            style: 'font-size: 14px',
          });
          const $iconTotalMem = $('<span/>', { class: 'human mr-2' });

          // append
          $rowProjects.append($optician);
          $optician.append($card);
          $card.append($imagecheck);

          $imagecheck.append($chkInput);
          $imagecheck.append($imagecheckFigure);

          $imagecheckFigure.append($optName);
          $optName.text(_item.optName);
          $imagecheckFigure.append($cardBody);

          $cardBody.append($address);
          $address.append($iconAddress);
          $address.append($textAddress).text(_item.address);

          $cardBody.append($tel);
          $tel.append($iconTel);
          $tel.append(_item.tel);

          $cardBody.append($totalMem);
          $totalMem.append($iconTotalMem);
          $totalMem.append(
            $('<span/>')
              .append('멤버 ')
              .append($('<span/>', { class: 'member-count text-dark' }).text(_item.totalMem))
              .append('명'),
          );
        });
      },
    };
    commonUtils.callAjax(options, true);
  }
  /***** Business End *****/
};
