/*
 *  Description: 브랜드/상품 추가
 *  User: kyle
 *  Date: 2022-01-19
 */
import fileUploadUtils from '../../utils/fileUploadUtils';
import searchBoxUtils from '../../utils/searchBoxUtils';
import commonUtils from '../../utils/commonUtils';

require('jquery-validation');

export default () => {
  /***** Static Start *****/
  const $container = $('#brandProductInfo');
  const $form = $container.find('#formBrn');

  // 파일 업로드
  const imageUpload = fileUploadUtils.init({
    el: 'regPrdtImage',
    title: '파일을 여기로 드래그하세요',
  });

  // 브랜드 검색박스 적용
  // searchBoxUtils.create({
  //   $el: $container.find('#regBrnName'),
  //   title: '브랜드',
  //   placeholder: '브랜드',
  //   ajax: {
  //     url: '/api/brandProductInfo/getBrandList',
  //     data: (params) => ({ brnName: params.term }),
  //     processResults: (res) => {
  //       let result = searchBoxUtils.getResultCallback(res);
  //       return {
  //         results: result.map((obj) => ({ id: obj.brnId, text: obj.brnName, data: obj })),
  //       };
  //     },
  //     delay: 250,
  //     cache: true,
  //   },
  //   noResultCallback: () => showInsertBrandInfo(),
  // });

  // 유효성체크
  $form.validate({
    rules: {
      regBrnName: {
        required: true,
      },
      regPrdtName: {
        required: true,
        rangelength: [2, 20],
      },
      regPrdtDivCode: {
        required: true,
      },
      regPrdtTypeCode: {
        required: true,
      },
      regPrdtColorCode: {
        required: true,
      },
      regPrdtDesignCode: {
        required: true,
      },
    },
    errorPlacement: (_error, _element) => _element.parent().children().last().after(_error),
    submitHandler: (_form) =>
      $(_form).data('type') !== 'insert' ? runUpdateSubmit() : runInsertSubmit(),
  });

  // TODO: 수정해야됨
  // 유효성체크 메세지 설정
  commonUtils.setJqueryValidationLanguageKor();

  // selectbox 바인딩
  commonUtils.setSelectboxCode({ el: 'regPrdtDivCode', code: 'PO100', defaultTxt: '선택' });
  commonUtils.setSelectboxCode({ el: 'regPrdtColorCode', code: 'CR100', defaultTxt: '선택' });
  commonUtils.setSelectboxCode({ el: 'regPrdtTypeCode', code: 'PT100', defaultTxt: '선택' });
  commonUtils.setSelectboxCode({ el: 'regPrdtDesignCode', code: 'DG100', defaultTxt: '선택' });
  /***** Static End *****/

  /***** Event Start *****/
  // 상품추가 화면 dom 로드 시
  $container.find('.card[data-area=brReg]').on('loadDom', function (_evt, _data) {
    $container.parent().scrollTop(0);
    bindBrandProductInfo({
      mode: _data?.mode,
      brnId: _data?.brnId,
    });
  });

  // 이전 버튼 이벤트
  $container.find('.btn-prev').on('click', () => resetPage());

  // 상품 수정 버튼 클릭 이벤트
  $container.find('.btn-save').on('click', () => {
    $form.data('type', 'modify');
    $form.submit();
  });
  /***** Event End *****/

  /***** Business Start *****/
  /**
   * 수정 submit 실행
   */
  function runUpdateSubmit() {
    // form 전송 ajax 처리
    connecteye.app.ui.alert
      .confirm({
        title: '브랜드를 수정할까요?',
        text: '취소를 누르면 내용을 수정 할 수 있어요',
        confirmBtnTxt: '수정',
        cancelBtnTxt: '취소',
        showLoaderOnConfirm: true,
        preConfirm: () => {
          $container.parent().scrollTop(0);
        },
      })
      .then((_result) => {
        if (_result.isConfirmed) {
          connecteye.app.ui.showCardLoading();
          saveBrandProductInfo('/api/brandProductInfo/updateBrandProductInfo', () => {
            connecteye.app.ui.hideCardLoading();
            connecteye.app.ui.alert
              .success({
                title: '수정 완료!',
                text: '입력한 브랜드가 수정됐어요',
              })
              .then((_SuccessResult) => {
                if (_SuccessResult.isConfirmed) {
                  showBrandProductInfoList();
                }
              });
          });
        } else {
          connecteye.app.ui.alert.error({
            title: '수정 취소',
            text: '브랜드를 다시 수정 할 수 있어요 :)',
            icon: 'error',
          });
        }
      });
  }

  /**
   * 브랜드/상품 정보 저장
   *
   * @param {String} _url
   * @param {function} _callback
   */
  async function saveBrandProductInfo(_url, _callback) {
    commonUtils.callAjax(
      {
        type: 'POST',
        url: _url,
        dataType: 'form',
        paramData: await commonUtils.getAjaxFormData({
          // 이미지와 같이 formData Request 할 경우
          // 이미지와 같이 타입, 바이너리 객체로 넘겨줘야됨
          formData: commonUtils.getBlobObject({
            data: commonUtils.serializeObject($form),
            option: {
              type: 'application/json',
            },
          }),
        }),
        callback: _callback,
      },
      true,
    );
  }

  /**
   * 브랜드/상품 정보 세팅
   *
   * @param {Object} _param
   */
  function bindBrandProductInfo(_param) {
    const $cardTitle = $container.find('.card-title');
    const { mode, brnId } = _param;
    if (mode !== 'modify') {
      connecteye.app.ui.hideCardLoading(0);
      $cardTitle.text('브랜드 추가');
      $container
        .find('.btn-add')
        .removeClass('d-none')
        .end()
        .find('.btn-reset')
        .removeClass('d-none')
        .end()
        .find('.btn-save')
        .addClass('d-none');
    } else {
      connecteye.app.ui.showCardLoading();
      commonUtils.callAjax({
        type: 'GET',
        url: '/api/brandProductInfo/getBrandProductInfo',
        paramData: {
          brnId,
          perPage: 1,
          page: 1,
        },
        callback: (_result) => setModifyBrandProductInfo(_result),
      });
    }
  }

  /**
   * 서버에서 수정정보 가져와서 세팅
   *
   * @param {Object} _result
   */
  function setModifyBrandProductInfo(_result) {
    const $cardTitle = $container.find('.card-title');
    const resultData = _result.contents[0];
    const { brnId, brnName } = resultData;
    $cardTitle.text('브랜드 수정');
    $container
      .find('.btn-add')
      .addClass('d-none')
      .end()
      .find('.btn-reset')
      .addClass('d-none')
      .end()
      .find('.btn-save')
      .removeClass('d-none');

    $container
      .find('#regBrnId').val(brnId).end()
      .find('#regBrnName').val(brnName);

    connecteye.app.ui.hideCardLoading();
  }

  /**
   * 브랜드/상품 목록 보여주기
   */
  function showBrandProductInfoList() {
    $container.find('.btn-prev').trigger('click');
    $container.find('.card[data-area=brList]').trigger('loadDom', { listReload: true });
  }

  /**
   * 브랜드/상품 목록 보여주기
   */
  function showInsertBrandInfo() {
    let inputValue = $('.select2-search__field').val().trimStart().trimEnd();
    connecteye.app.ui.alert.custom().fire({
      input: 'text',
      text: '브랜드 추가',
      inputValue: inputValue,
      inputPlaceholder: '브랜드명',
      showCancelButton: true,
      cancelButtonText: '취소',
      confirmButtonText: '추가',
      confirmButtonColor: '#2e2e2e',
      showLoaderOnConfirm: true,
      buttonsStyling: false,
      reverseButtons: true,
      customClass: {
        confirmButton: 'btn-custom-2',
        cancelButton: 'btn-custom-3 mr-2',
      },
      inputValidator: (_value) => !_value && '브랜드명을 입력해주세요!',
      preConfirm: (_inputValue) => getInsertBrandInfo(_inputValue),
    });
  }

  /**
   * 브랜드 추가 후 정보 가져오기
   *
   * @param {String} _inputValue
   */
  function getInsertBrandInfo(_inputValue) {
    commonUtils.callAjax(
      {
        type: 'POST',
        url: '/api/brandProductInfo/getInsertBrandInfo',
        paramData: {
          brnName: _inputValue,
        },
        callback: (_data) =>
          connecteye.app.ui.alert.success({
            title: '추가 완료!',
            text: '입력한 브랜드가 추가됐어요',
            didDestroy: () => {
              const resultData = _data[0];
              const { brnId, brnName } = resultData;
              // Create a DOM Option and pre-select by default
              let newOption = new Option(brnName, brnId, true, true);
              // Append it to the select
              $container.find('input[name=regBrnId]').val(brnId);
              $container.find('#regBrnName').append(newOption).trigger('change');
            },
          }),
      },
      true,
    );
  }

  /**
   * 브랜드 추가 화면 reset
   */
  async function resetPage() {
    const $formSupPrdt = $('#formPrdt');
    await $formSupPrdt
      .find('#regSupName')
      .empty()
      .trigger('change')
      .end()
      .find('#regBrnName')
      .empty()
      .trigger('change');
    commonUtils.formReset($formSupPrdt.find('input, select'));
    imageUpload.removeFiles();
    $container.parent().scrollTop(0);
  }
  /***** Business End *****/
};
