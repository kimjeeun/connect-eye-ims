/*
 *  Description: 브랜드/상품
 *  User: kyle
 *  Date: 2022-01-13
 */
import gridUtils from '../../utils/gridUtils';
import commonUtils from '../../utils/commonUtils';

export default () => {
  /***** Static Start *****/
  const $container = $('#brandProductInfo');
  // table
  const brandProductInfoTable = new gridUtils.create({
    el: 'brandProductInfoTable',
    data: {
      api: {
        readData: {
          url: '/api/brandProductInfo/getBrandProductInfo',
          method: 'GET',
        },
      },
    },
    pageOptions: {
      // 페이징 옵션
      perPage: 7, // 1페이지당 목록 갯수
      visiblePages: 5, // 페이징 넘버링 갯수
    },
    columns: [
      {
        header: '브랜드',
        name: 'brnName',
      },
      {
        header: '사용',
        name: 'isUse',
        width: 80,
        align: 'center',
        renderer: {
          type: gridUtils.setToggleSwitchRenderer,
          options: {
            url: '/api/brandProductInfo/updateBrandProductInfoUse',
            pkName: 'prdtId',
          },
        },
      },
    ],

    // 그리드 생성 완료 시
    onGridMounted: (_gridThis) => _gridThis.instance.refreshLayout(), // 그리드 레이아웃 새로고침

    // 그리드 업데이트 시  'resetData', 'restore', 'reloadData', 'readData', 'setPerPage'
    onGridUpdated: (_gridThis) => {
      const totalCount = _gridThis.instance.getPaginationTotalCount();

      // 데이터 건수
      $container.find('.result-cnt').text(commonUtils.setComma(totalCount));

      // 그리드 레이아웃 새로고침
      _gridThis.instance.refreshLayout();
    },
  });
  /***** Static End *****/

  /***** Event Start *****/
  // 브랜드/상품 리스트 화면 dom 로드 시
  $container.find('.card[data-area=brList]').on('loadDom', (_evt, _data) => {
    if (_data && _data.listReload) {
      brandProductInfoTable.grid.reloadData();
    }
  });

  // 그리드 클릭 이벤트
  brandProductInfoTable.grid.on('click', (_evt) => {
    const { columnName, targetType, instance, rowKey } = _evt;
    if (
      columnName !== 'isUse' &&
      targetType !== 'columnHeader' &&
      targetType !== 'rowHeader' &&
      targetType !== 'etc'
    ) {
      const $target = $container.find('.card[data-area=brReg]');
      $target
        .removeClass('d-none')
        .siblings('.card')
        .addClass('d-none')
        .end()
        .trigger('loadDom', {
          brnId: instance.getRow(rowKey).brnId,
          mode: 'modify',
        });
    }
  });

  // 검색 버튼 이벤트
  $container.find('.btn-select').on('click', () => getBrandProductInfoList());
  $container.find('.search-group').on('keyup', (_evt) => {
    if (_evt.keyCode === 13) {
      getBrandProductInfoList();
    }
  });

  // 상품추가 이벤트
  $container.find('.btn-reg-sup').on('click', (_evt) => getArea(_evt));

  // 이전 버튼 이벤트
  $container.find('.btn-prev').on('click', (_evt) => getArea(_evt));

  // 상품 선택삭제 이벤트
  $container.find('.btn-del-sup').on('click', async () => {
    const paramData = await brandProductInfoTable.grid.getCheckedRows().reduce((newArray, item) => {
      newArray.push({
        prdtId: item.prdtId,
        originImgPath: item.originImgPath,
      });
      return newArray;
    }, []);
    if (commonUtils.isEmpty(paramData)) {
      connecteye.app.ui.alert.warning({
        title: '주의!',
        text: '삭제하려는 상품을 선택해주세요',
      });
      return false;
    }
    deleteBrandProductInfo(paramData);
  });
  /***** Event End *****/

  /***** Business Start *****/
  /**
   * 브랜드/상품 목록 가져오기
   */
  function getBrandProductInfoList() {
    const $form = $container.find('.search-group');
    const formSerializeData = commonUtils.serializeObject($form);

    // args1: page, args2: paramData (GET 방식), args3: 그리드 초기화 여부
    brandProductInfoTable.grid.readData(1, formSerializeData, true);
  }
  /**
   * 브랜드/상품 목록 가져오기
   *
   * @param {String} _paramData
   */
  function deleteBrandProductInfo(_paramData) {
    connecteye.app.ui.alert
      .confirm({
        title: '상품을 삭제할까요?',
        text: '상품과 연결된 기준정보가 삭제됩니다',
        confirmBtnTxt: '삭제',
        cancelBtnTxt: '취소',
        showLoaderOnConfirm: true,
        preConfirm: () => {
          $container.parent().scrollTop(0);
        },
      })
      .then((_result) => {
        if (_result.isConfirmed) {
          connecteye.app.ui.showCardLoading();
          commonUtils.callAjax({
            type: 'DELETE',
            url: '/api/brandProductInfo/deleteBrandProductInfo',
            paramData: { jsonStr: JSON.stringify(_paramData) },
            callback: () => {
              connecteye.app.ui.hideCardLoading();
              connecteye.app.ui.alert
                .success({
                  title: '삭제 완료!',
                  text: '선택한 상품이 삭제됐어요',
                })
                .then((_SuccessResult) => {
                  if (_SuccessResult.isConfirmed) {
                    getBrandProductInfoList();
                  }
                });
            },
          });
        }
      });
  }
  /**
   * 영역 가져오기
   *
   * @param {Object} _evt
   */
  async function getArea(_evt) {
    const $el = $(_evt.currentTarget);
    const target = $el.data('target');
    const $parentArea = $container.find(`.page-inner .card[data-area=${target}]`);

    // 자신을 제외하고 d-none 클래스 넣어줌
    await $parentArea
      .removeClass('d-none')
      .siblings('.card')
      .addClass('d-none')
      .end()
      .trigger('loadDom');

    // 스크롤 상위로
    $container.parent().scrollTop(0);

    // 그리드 레이아웃 리프레쉬
    brandProductInfoTable.grid.refreshLayout();
  }
  /***** Business End *****/
};
