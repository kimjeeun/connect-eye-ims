/*
 *  Description: brandProductInfo main.js
 *  User: kyle
 *  Date: 2021-12-09
 */
import brandProductInfo from './brandProductInfo';
import brandProductInfoReg from './brandProductInfoReg';

$(document).ready(async () => {
  // 상품상세
  brandProductInfo();
  // 상품추가
  brandProductInfoReg();
});
