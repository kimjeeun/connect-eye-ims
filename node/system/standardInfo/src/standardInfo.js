/*
 *  Description: 기준정보 목록
 *  User: kyle
 *  Date: 2021-11-24
 */
import Tree from 'tui-tree';

import commonUtils from '../../utils/commonUtils';
import gridUtils from '../../utils/gridUtils';

require('../../../node_modules/tui-tree/dist/tui-tree.min.css');

export default () => {
  /***** Static Start *****/
  const $container = $('#standardInfo');
  const $tree = $('#standardInfoTree');
  const prdtName = $('#standardInfoSearch').val();
  // 기준정보 요약 트리
  // const tree = new Tree($tree[0], {
  //   nodeDefaultState: 'opened',
  //   data: [
  //     {
  //       text: '나스월드',
  //       children: [
  //         {
  //           text: '매튜',
  //           state: 'opened',
  //           children: [{ text: '1MT-ARTY-23' }, { text: '1MT-ARTY-23' }],
  //         },
  //         {
  //           text: '톰브라운',
  //           state: 'opened',
  //           children: [{ text: '1MT-ARTY-23' }, { text: '1MT-ARTY-23' }],
  //         },
  //       ],
  //     },
  //     {
  //       text: '지오  ',
  //       state: 'opened',
  //       children: [{ text: '구찌', children: [{ text: '구찌 2022 신상' }] }],
  //     },
  //   ],
  // }).enableFeature('Selectable', {
  //   selectedClassName: 'tui-tree-selected',
  // });

  // table
  const standardInfoTable = new gridUtils.create({
    el: 'standardInfoTable',
    data: {
      api: {
        readData: {
          url: '/api/standardInfo/getStandardInfoList',
          method: 'GET',
          initParams: {
            prdtName: prdtName || '',
            orderBy: 'prdt_name asc'
          },
        },
      },
    },
    pageOptions: {
      // 페이징 옵션
      perPage: 7, // 1페이지당 목록 갯수
      visiblePages: 5, // 페이징 넘버링 갯수
    },
    columns: [
      {
        header: '사진',
        name: 'previewImgPath',
        width: 60,
        renderer: { type: gridUtils.setImageRenderer },
        align: 'center',
      },
      { header: '상품구분', name: 'prdtDiv', width: 100, align: 'center' },
      {
        header: '브랜드',
        name: 'brnName',
        width: 100,
        ellipsis: true,
        resizable: true,
        align: 'center'
      },
      {
        header: '상품명',
        name: 'prdtName',
        ellipsis: true,
        resizable: true,
      },
      { header: '비고', name: 'remarks', width: 140, ellipsis: true, resizable: true, },
      { header: '색상', name: 'prdtColor', width: 80, align: 'center' },
      { header: '유형', name: 'prdtType', width: 100, align: 'center' },
      { header: '디자인', name: 'prdtDesign', width: 100, align: 'center' },
      { header: '매입처', name: 'supName', width: 100, align: 'center', resizable: true, },
      {
        header: '매입단가',
        name: 'purPrice',
        width: 100,
        align: 'right',
        formatter: commonUtils.setComma,
      },
      {
        header: '판매단가',
        name: 'salesPrice',
        width: 100,
        align: 'right',
        formatter: commonUtils.setComma,
      },
      // { header: '바코드', name: 'barcode', width: 140, align: 'center' },
      { header: '생성일', name: 'regDate', width: 90, align: 'center' },
      {
        header: '사용',
        name: 'isUse',
        width: 80,
        align: 'center',
        renderer: {
          type: gridUtils.setToggleSwitchRenderer,
          options: {
            url: '/api/standardInfo/updateStandardInfoUse',
            pkName: 'supPrdtId',
          },
        },
      },
    ],
    // 그리드 생성 완료 시
    onGridMounted: (_gridThis) => _gridThis.instance.refreshLayout(), // 그리드 레이아웃 새로고침

    // 그리드 업데이트 시  'resetData', 'restore', 'reloadData', 'readData', 'setPerPage'
    onGridUpdated: (_gridThis) => {
      const totalCount = _gridThis.instance.getPaginationTotalCount();

      // 데이터 건수
      $container.find('.result-cnt').text(commonUtils.setComma(totalCount));

      // 그리드 레이아웃 새로고침
      _gridThis.instance.refreshLayout();
    },
  });

  // 기준정보 현황
  // getStandardInfoStatus();

  // selectbox 바인딩
  commonUtils.setSelectboxCode({ el: 'prdtDivCode', code: 'PO100' });
  commonUtils.setSelectboxCode({ el: 'prdtColorCode', code: 'CR100' });
  commonUtils.setSelectboxCode({ el: 'prdtTypeCode', code: 'PT100' });
  commonUtils.setSelectboxCode({ el: 'prdtDesignCode', code: 'DG100' });
  /***** Static End *****/

  /***** Event Start *****/
  // 기준정보 리스트 화면 dom 로드 시
  $container.find('.card[data-area=supList]').on('loadDom', (_evt, _data) => {
    if (_data && _data.listReload) {
      standardInfoTable.grid.reloadData();
    }
    if (_data && _data.statusReload) {
      // getStandardInfoStatus();
    }
  });

  // 그리드 사진클릭 이벤트
  standardInfoTable.grid.on('click', (_evt) =>
    gridUtils.setSelectPictureHandler(_evt, 'previewImgPath', 'originImgPath'),
  );

  // 그리드 상품명 클릭 이벤트
  standardInfoTable.grid.on('click', (_evt) => {
    const { columnName, targetType, instance, rowKey } = _evt;
    if (
      columnName === 'prdtName' &&
      targetType !== 'columnHeader' &&
      targetType !== 'rowHeader' &&
      targetType !== 'etc'
    ) {
      const $target = $container.find('.card[data-area=supReg]');
      $target
        .removeClass('d-none')
        .siblings('.card')
        .addClass('d-none')
        .end()
        .trigger('loadDom', {
          supPrdtId: instance.getRow(rowKey).supPrdtId,
          mode: 'modify',
        });
    }
  });

  // 검색 버튼 이벤트
  $container.find('.btn-select').on('click', () => {
    getStandardInfoList();
    // getStandardInfoStatus();
  });
  $container.find('.search-group').on('keyup', (_evt) => {
    if (_evt.keyCode === 13) {
      getStandardInfoList();
      // getStandardInfoStatus();
    }
  });

  // 기준정보 상품추가 이벤트
  $container.find('.btn-reg-sup').on('click', (_evt) => getArea(_evt));

  // 기준정보 상품 선택삭제 이벤트
  $container.find('.btn-del-sup').on('click', async () => {
    const pks = await standardInfoTable.grid.getCheckedRows().map((item) => item.supPrdtId);
    if (commonUtils.isEmpty(pks)) {
      connecteye.app.ui.alert.warning({
        title: '주의!',
        text: '삭제하려는 기준정보를 선택해주세요',
      });
      return false;
    }
    deleteStandardInfo(pks);
  });

  // 이전 버튼 이벤트
  $container.find('.btn-prev').on('click', (_evt) => getArea(_evt));
  /***** Event End *****/

  /***** Business Start *****/
  /**
   * 기준정보 목록 가져오기
   */
  function getStandardInfoList() {
    const $form = $container.find('.search-group');
    const formSerializeData = commonUtils.serializeObject($form);

    // args1: page, args2: paramData (GET 방식), args3: 그리드 초기화 여부
    standardInfoTable.grid.readData(1, formSerializeData, true);
  }
  /**
   * 기준정보 삭제
   *
   * @param {String} _pks
   */
  function deleteStandardInfo(_pks) {
    connecteye.app.ui.alert
      .confirm({
        title: '기준정보를 삭제할까요?',
        confirmBtnTxt: '삭제',
        cancelBtnTxt: '취소',
        showLoaderOnConfirm: true,
        preConfirm: () => {},
      })
      .then((_result) => {
        if (_result.isConfirmed) {
          connecteye.app.ui.showCardLoading();
          commonUtils.callAjax({
            type: 'DELETE',
            url: '/api/standardInfo/deleteStandardInfo',
            paramData: { pks: _pks },
            callback: () => {
              connecteye.app.ui.hideCardLoading();
              connecteye.app.ui.alert
                .success({
                  title: '삭제 완료!',
                  text: '선택한 기준정보가 삭제됐어요',
                })
                .then((_SuccessResult) => {
                  if (_SuccessResult.isConfirmed) {
                    getStandardInfoList();
                    // getStandardInfoStatus();
                  }
                });
            },
          });
        }
      });
  }
  /**
   * 기준정보 현황 가져오기
   *
   * @param {String} _paramData
   */
  function getStandardInfoStatus(_paramData) {
    let options = {
      type: 'GET',
      url: '/api/standardInfo/getStandardInfoStatus',
      paramData: _paramData || null,
      callback: (res) => {
        const { supCnt, brnCnt, prdtCnt } = res[0];
        $container
          .find('.list-group-item.supplier span')
          .text(supCnt)
          .end()
          .find('.list-group-item.brand span')
          .text(brnCnt)
          .end()
          .find('.list-group-item.product span')
          .text(prdtCnt)
          .end();
      },
    };
    commonUtils.callAjax(options, true);
  }
  /**
   * 영역 가져오기
   *
   * @param {Object} _evt
   */
  async function getArea(_evt) {
    const $el = $(_evt.currentTarget);
    const target = $el.data('target');
    const $parentArea = $container.find(`.page-inner .card[data-area=${target}]`);

    // 자신을 제외하고 d-none 클래스 넣어줌
    await $parentArea
      .removeClass('d-none')
      .siblings('.card')
      .addClass('d-none')
      .end()
      .trigger('loadDom');

    $container.parent().scrollTop(0);
    standardInfoTable.grid.refreshLayout();
  }
  /***** Business End *****/
};
