/* eslint-disable no-undef */
/*
 *  Description: 기준정보 추가
 *  User: kyle
 *  Date: 2021-12-22
 */
import toMilliseconds from '@sindresorhus/to-milliseconds';
import { every, merge } from 'lodash-es';

import searchBoxUtils from '../../utils/searchBoxUtils';
import commonUtils from '../../utils/commonUtils';
import fileUploadUtils from '../../utils/fileUploadUtils';
import gridUtils from '../../utils/gridUtils';
import barcodeUtils from '../../utils/barcodeUtils';

require('jquery-validation');

export default () => {
  /***** Static Start *****/
  const $container = $('#standardInfo');
  const $form = $container.find('#formSupPrdt');

  // 파일 업로드
  let productImages = [];
  const imageUpload = fileUploadUtils.init({
    el: 'regPrdtImage',
    title: '파일을 여기로 드래그하세요',
  });

  // 바코드
  barcodeUtils.init({
    $inputElement: $container.find('#regBarcode'),
    $scanButtonElement: $container.find('#btnBarcodeScan'),
  });

  // table
  const supRegTable = new gridUtils.create({
    el: 'supRegTable',
    bodyHeight: 622,
    rowHeaders: [],
    scrollY: true,
    columns: [
      {
        header: '',
        name: '',
        width: 20,
        align: 'center',
        renderer: {
          type: gridUtils.deleteButtonRenderer,
          options: {
            isAmount: true,
            $currentAmount: $container.find('.current-amount'),
          },
        },
      },
      {
        header: '사진',
        name: 'regPrdtImage',
        width: 60,
        renderer: { type: gridUtils.setImageRenderer },
        align: 'center',
      },
      {
        header: '매입처',
        name: 'regSupName',
        width: 100,
        align: 'center',
        resizable: true,
      },
      {
        header: '브랜드',
        name: 'regBrnName',
        width: 100,
        ellipsis: true,
        align: 'center',
        resizable: true,
      },
      {
        header: '상품명',
        name: 'regPrdtName',
        ellipsis: true,
        resizable: true,
      },
      {
        header: '비고',
        name: 'regRemarks',
        width: 140,
        ellipsis: true,
        resizable: true,
      },
      {
        header: '매입단가',
        name: 'regPurPrice',
        width: 100,
        align: 'right',
        formatter: commonUtils.setComma,
      },
      {
        header: '판매단가',
        name: 'regSalesPrice',
        width: 100,
        align: 'right',
        formatter: commonUtils.setComma,
      },
      {
        header: '유형',
        name: 'regPrdtTypeCode',
        width: 100,
        align: 'center',
        formatter: 'listItemText',
        copyOptions: {
          useListItemText: true,
        },
        editor: {
          type: 'select',
          options: {
            instantApply: true,
            listItems: gridUtils.setSelectboxCode('PT100'),
          },
        },
      },
      {
        header: '색상',
        name: 'regPrdtColorCode',
        width: 80,
        align: 'center',
        formatter: 'listItemText',
        copyOptions: {
          useListItemText: true,
        },
        editor: {
          type: 'select',
          options: {
            instantApply: true,
            listItems: gridUtils.setSelectboxCode('CR100'),
          },
        },
      },
      {
        header: '디자인',
        name: 'regPrdtDesignCode',
        width: 100,
        align: 'center',
        formatter: 'listItemText',
        copyOptions: {
          useListItemText: true,
        },
        editor: {
          type: 'select',
          options: {
            instantApply: true,
            listItems: gridUtils.setSelectboxCode('DG100'),
          },
        },
      },
    ],
    summary: {
      height: 40,
      position: 'bottom',
      columnContent: {
        amount: {
          template(valueMap) {
            return `총 ${commonUtils.setComma(valueMap.sum)}개 `;
          },
        },
        price: {
          template(valueMap) {
            return `${commonUtils.setComma(valueMap.sum)}원`;
          },
        },
      },
    },

    // 그리드 생성 완료 시
    onGridMounted: (_gridThis) => {
      // 그리드 레이아웃 새로고침
      _gridThis.instance.refreshLayout();
    },

    // 그리드 업데이트 시  'resetData', 'restore', 'reloadData', 'readData', 'setPerPage'
    onGridUpdated: (_gridThis) => {
      // 그리드 레이아웃 새로고침
      _gridThis.instance.refreshLayout();

      // hideLoadingBar
      _gridThis.instance.dispatch('setLoadingState', 'DONE');
    },
  });

  // 매입처 검색박스 적용
  searchBoxUtils.create({
    $el: $container.find('#regSupName'),
    title: '매입처',
    placeholder: '매입처',
    ajax: {
      url: '/api/standardInfo/getSupplierList',
      data: (params) => ({ supName: params.term }),
      processResults: (res) => {
        let result = searchBoxUtils.getResultCallback(res);
        return {
          results: result.map((obj) => ({ id: obj.supId, text: obj.supName, data: obj })),
        };
      },
      delay: 250,
      cache: true,
    },
    noResultCallback: () => showInsertSupplierInfo(),
  });

  // 브랜드 검색박스 적용
  searchBoxUtils.create({
    $el: $container.find('#regBrnName'),
    title: '브랜드',
    placeholder: '브랜드',
    ajax: {
      url: '/api/standardInfo/getBrandList',
      data: (params) => ({
        supId: $container.find('input[name=regSupId]').val(),
        brnName: params.term,
      }),
      processResults: (res) => {
        let result = searchBoxUtils.getResultCallback(res);
        return {
          results: result.map((obj) => ({ id: obj.brnId, text: obj.brnName, data: obj })),
        };
      },
      delay: 250,
      cache: true,
    },
    noResultCallback: () => showInsertBrandInfo(),
  });

  // 유효성체크
  $form.validate({
    rules: {
      regSupName: {
        required: true,
      },
      regBrnName: {
        required: true,
      },
      regPrdtName: {
        required: true,
        rangelength: [2, 100],
      },
      regBarcode: {
        digits: true,
      },
      regPrdtDivCode: {
        required: true,
      },
      regPurPrice: {
        required: true,
        price: true,
      },
      regSalesPrice: {
        required: true,
        price: true,
      },
    },
    errorPlacement: (_error, _element) => _element.parent().children().last().after(_error),
    submitHandler: (_form) => setFormDataWithSubmit(_form),
  });

  // TODO: 수정해야됨
  // 유효성체크 메세지 설정
  commonUtils.setJqueryValidationLanguageKor();

  // selectbox 바인딩
  commonUtils.setSelectboxCode({ el: 'regPrdtDivCode', code: 'PO100', defaultTxt: '선택' });
  commonUtils.setSelectboxCode({ el: 'regPrdtColorCode', code: 'CR100', defaultTxt: '선택' });
  commonUtils.setSelectboxCode({ el: 'regPrdtTypeCode', code: 'PT100', defaultTxt: '선택' });
  commonUtils.setSelectboxCode({ el: 'regPrdtDesignCode', code: 'DG100', defaultTxt: '선택' });
  /***** Static End *****/

  /***** Event Start *****/
  // 기준정보추가 화면 dom 로드 시
  $container.find('.card[data-area=supReg]').on('loadDom', function (_evt, _data) {
    bindStandardInfo({
      mode: _data?.mode,
      supPrdtId: _data?.supPrdtId,
    });
    $container.parent().scrollTop(0);
    supRegTable.grid.refreshLayout();
  });

  // form change event
  $form.on('change', function (_evt) {
    const $target = $(_evt.target);
    if (commonUtils.isNotEmpty($target.val())) {
      $target.siblings('label').remove();
    }
  });

  // 기준정보 정보 입력 후 적용 버튼 클릭 시 이벤트
  $container.find('#btnSave').on('click', async () => {
    $form.data('type', 'save');
    $form.submit();
  });

  // 이전 버튼 이벤트
  $container.find('.btn-prev').on('click', () => resetPage());

  // 매입처 선택 이벤트
  $container.find('#regSupName').on('change', (_evt) => {
    const resultObj = searchBoxUtils.getResultObject($(_evt.currentTarget));
    if (resultObj) {
      const { supId } = resultObj;
      // 매입처 아이디
      $container
        .find('input[name=regSupId]')
        .val(supId)
        .end()
        .find('select[name=regBrnName]')
        .prop('disabled', false)
        .end()
        .find('select[name=regBrnName]')
        .text('');
    }
  });

  // 브랜드 선택 이벤트
  $container.find('#regBrnName').on('change', (_evt) => {
    let resultObj = searchBoxUtils.getResultObject($(_evt.currentTarget));
    if (resultObj) {
      const { brnId } = resultObj;
      // 브랜드 아이디
      $container.find('input[name=regBrnId]').val(brnId);
    }
  });

  // 바코드 자동생성 버튼 이벤트
  $container.find('#btnBarcode').on('click', () => {
    const barcodeNum = getBarcodeNumber();
    $('#regBarcode').val(barcodeNum).trigger('change');
  });

  // 단가 이벤트
  $container.find('#regPurPrice').on({
    keyup: commonUtils.setPriceEventHandler,
    change: commonUtils.setPriceEventHandler,
  });
  $container.find('#regSalesPrice').on({
    keyup: commonUtils.setPriceEventHandler,
    change: commonUtils.setPriceEventHandler,
  });

  // 기준정보 추가 form reset 버튼 이벤트
  $container.find('.btn-reset').on('click', () => resetPage());

  // 기준정보 추가 버튼 클릭 이벤트
  $container.find('.btn-add').on('click', () => {
    $form.data('type', 'insert');
    setFormDataWithSubmit($form);
  });

  // 기준정보 수정 버튼 클릭 이벤트
  $container.find('.btn-modify').on('click', () => {
    $form.data('type', 'modify');
    $form.submit();
  });

  // 그리드 사진클릭 이벤트
  supRegTable.grid.on('click', (_evt) => gridUtils.setSelectPictureHandler(_evt, 'regPrdtImage'));

  // 엑셀 업로드 버튼 클릭 이벤트
  $container.find('.btn-excel').on('click', () => showExcelFileUploadPopup());

  /***** Event End *****/

  /***** Business Start *****/
  /**
   * 추가 submit 실행
   */
  function runInsertSubmit() {
    // form 전송 ajax 처리
    connecteye.app.ui.alert
      .confirm({
        title: '기준정보를 추가할까요?',
        text: '취소를 누르면 내용을 수정 할 수 있어요',
        confirmBtnTxt: '추가',
        cancelBtnTxt: '취소',
        showLoaderOnConfirm: true,
        preConfirm: () => {},
      })
      .then((_result) => {
        if (_result.isConfirmed) {
          connecteye.app.ui.showCardLoading();
          insertStandardInfoList('/api/standardInfo/insertStandardInfoList', () => {
            connecteye.app.ui.hideCardLoading();
            connecteye.app.ui.alert
              .success({
                title: '추가 완료!',
                text: '입력한 기준정보가 추가됐어요',
                denyButtonText: '계속 추가하기',
                showDenyButton: true,
              })
              .then((_SuccessResult) => {
                if (_SuccessResult.isConfirmed) {
                  showStandardInfoList();
                } else if (_SuccessResult.isDenied) {
                  resetPage();
                }
              });
          });
        } else {
          connecteye.app.ui.alert.error({
            title: '추가 취소',
            text: '기준정보 수정 후 다시 추가 할 수 있어요 :)',
            icon: 'error',
          });
        }
      });
  }

  /**
   * 수정 submit 실행
   */
  function runUpdateSubmit() {
    // form 전송 ajax 처리
    connecteye.app.ui.alert
      .confirm({
        title: '기준정보를 수정할까요?',
        text: '취소를 누르면 내용을 수정 할 수 있어요',
        confirmBtnTxt: '수정',
        cancelBtnTxt: '취소',
        showLoaderOnConfirm: true,
        preConfirm: () => {},
      })
      .then((_result) => {
        if (_result.isConfirmed) {
          connecteye.app.ui.showCardLoading();
          saveStandardInfo('/api/standardInfo/updateStandardInfo', () => {
            connecteye.app.ui.hideCardLoading();
            connecteye.app.ui.alert
              .success({
                title: '수정 완료!',
                text: '입력한 기준정보가 수정됐어요',
              })
              .then((_SuccessResult) => {
                if (_SuccessResult.isConfirmed) {
                  showStandardInfoList();
                }
              });
          });
        } else {
          connecteye.app.ui.alert.error({
            title: '수정 취소',
            text: '기준정보를 다시 수정 할 수 있어요 :)',
            icon: 'error',
          });
        }
      });
  }

  /**
   * 기준정보 목록 보여주기
   */
  function showStandardInfoList() {
    $container.find('.btn-prev').trigger('click');
    $container.find('.card[data-area=supList]').trigger('loadDom', {
      listReload: true,
      statusReload: true,
    });
  }

  /**
   * 기준정보 데이터 세팅
   *
   * @param {Object} _param
   */
  function bindStandardInfo(_param) {
    const $cardTitle = $container.find('.card-title');
    const { mode, supPrdtId } = _param;
    if (mode !== 'modify') {
      connecteye.app.ui.hideCardLoading(0);
      $cardTitle.text('기준정보 추가');
      $container
        .find('.btn-add')
        .removeClass('d-none')
        .end()
        .find('.btn-reset')
        .removeClass('d-none')
        .end()
        .find('.btn-modify')
        .addClass('d-none')
        .end()
        .find('.btn-excel')
        .removeClass('d-none')
        .end()
        .find('.btn-save')
        .removeClass('d-none')
        .end()
        .find('.sup-prdt-container')
        .addClass('col-md-4').removeClass('col')
        .end()
        .find('#supRegTableWrap')
        .parent()
        .removeClass('d-none');
      $('#formSupPrdt')
        .find('input, select, button')
        .each(function (index, item) {
          const id = item.id;
          if (id === 'regBrnName') {
            item.disabled = true;
          } else {
            item.disabled = false;
          }
        });
    } else {
      connecteye.app.ui.showCardLoading();
      commonUtils.callAjax({
        type: 'GET',
        url: '/api/standardInfo/getStandardInfoList',
        paramData: {
          supPrdtId: supPrdtId,
          perPage: 1,
          page: 1,
        },
        callback: (_result) => setModifyStandardInfo(_result),
      });
    }
  }

  /**
   * 서버에서 수정정보 가져와서 세팅
   *
   * @param {Object} _result
   */
  function setModifyStandardInfo(_result) {
    const $cardTitle = $container.find('.card-title');
    const resultData = _result.contents[0];
    const { brnId, brnName, supId, supName, originImgPath } = resultData;
    $cardTitle.text('기준정보 수정');
    $container
      .find('.btn-add')
      .addClass('d-none')
      .end()
      .find('.btn-reset')
      .addClass('d-none')
      .end()
      .find('.btn-modify')
      .removeClass('d-none')
      .end()
      .find('.btn-save')
      .addClass('d-none')
      .end()
      .find('.btn-excel')
      .addClass('d-none')
      .end()
      .find('.sup-prdt-container')
      .addClass('col').removeClass('col-md-4')
      .end()
      .find('#supRegTableWrap')
      .parent()
      .addClass('d-none');
    $('#formSupPrdt')
      .find('input, select, button')
      .each(async function (index, item) {
        // form type
        let { type, id, name } = item;
        // regRepPhone -> repPhone 맵핑 로직
        let initial = name.slice(3);
        let resultStr = initial.replace(/\b[A-Z]/, (letter) => letter.toLowerCase());

        // 수정 할수 없는 컨트롤 지정
        if (
          id === 'regSupName' ||
          id === 'regBrnName'
        ) {
          item.disabled = true;
        }

        if (type !== 'file') {
          if ($(item).hasClass('select2-hidden-accessible')) {
            // TODO 나중에 리펙토링 하자...
            let brnOption = new Option(brnName, brnId, true, true);
            // Append it to the select
            $container.find('#regBrnName').append(brnOption).trigger('change');

            let supOption = new Option(supName, supId, true, true);
            // Append it to the select
            $container.find('#regSupName').append(supOption).trigger('change');
          } else {
            item.value =
              id === 'regPurPrice' || id === 'regSalesPrice'
                ? commonUtils.setComma(resultData[resultStr] || 0)
                : resultData[resultStr] || '';
          }
        }
        if (type === 'file' && originImgPath) {
          let blobToFile = await fileUploadUtils.convertURLtoFile(originImgPath);
          imageUpload.addFile(blobToFile);
        }
      });
    connecteye.app.ui.hideCardLoading();
  }

  /**
   * 기준정보 목록 정보 저장
   *
   * @param {String} _url
   * @param {function} _callback
   */
  async function insertStandardInfoList(_url, _callback) {
    const ajaxFormData = new FormData();
    const standardInfoData = supRegTable.grid.getData();

    // 기준정보 추가내역 리스트
    ajaxFormData.append(
      'formData',
      commonUtils.getBlobObject({
        data: standardInfoData,
        option: {
          type: 'application/json',
        },
      }),
    );

    // 기준정보 추가내역 이미지파일 리스트
    productImages.forEach((item) => {
      ajaxFormData.append('productImages', item);
    });

    // ajax 비동기 통신
    commonUtils.callAjax(
      {
        type: 'POST',
        url: _url,
        dataType: 'form',
        paramData: ajaxFormData,
        callback: _callback,
      },
      true,
    );
  }

  /**
   * 기준정보 정보 저장
   *
   * @param {String} _url
   * @param {function} _callback
   */
  async function saveStandardInfo(_url, _callback) {
    commonUtils.callAjax(
      {
        type: 'POST',
        url: _url,
        dataType: 'form',
        paramData: await commonUtils.getAjaxFormData({
          // 이미지와 같이 formData Request 할 경우
          // 이미지와 같이 타입, 바이너리 객체로 넘겨줘야됨
          formData: commonUtils.getBlobObject({
            data: commonUtils.serializeObject($form),
            option: {
              type: 'application/json',
            },
          }),
          productImage: imageUpload.getFile()?.file,
        }),
        callback: _callback,
      },
      true,
    );
  }

  /**
   * 브랜드 추가 후 정보 가져오기
   *
   * @param {String} _inputValue
   */
  function getInsertBrandInfo(_inputValue) {
    commonUtils.callAjax(
      {
        type: 'POST',
        url: '/api/standardInfo/getInsertBrandInfo',
        paramData: {
          supId: $container.find('input[name=regSupId]').val(),
          brnName: _inputValue,
        },
        callback: (_data) =>
          connecteye.app.ui.alert.success({
            title: '추가 완료!',
            text: '입력한 브랜드가 추가됐어요',
            didDestroy: () => {
              const resultData = _data[0];
              const { brnId, brnName } = resultData;
              // Create a DOM Option and pre-select by default
              let newOption = new Option(brnName, brnId, true, true);
              // Append it to the select
              $container.find('input[name=regBrnId]').val(brnId);
              $container.find('#regBrnName').append(newOption).trigger('change');
            },
          }),
      },
      true,
    );
  }

  /**
   * 브랜드 추가 팝업 보이기
   */
  function showInsertBrandInfo() {
    let inputValue = $('.select2-search__field').val().trimStart().trimEnd();
    connecteye.app.ui.alert.custom().fire({
      input: 'text',
      text: '브랜드 추가',
      inputValue: inputValue,
      inputPlaceholder: '브랜드명',
      showCancelButton: true,
      cancelButtonText: '취소',
      confirmButtonText: '추가',
      confirmButtonColor: '#2e2e2e',
      showLoaderOnConfirm: true,
      buttonsStyling: false,
      reverseButtons: true,
      customClass: {
        confirmButton: 'btn-custom-2',
        cancelButton: 'btn-custom-3 mr-2',
      },
      inputValidator: (_value) => !_value && '브랜드명을 입력해주세요!',
      preConfirm: (_inputValue) => getInsertBrandInfo(_inputValue),
    });
  }

  /**
   * 매입처 추가
   */
  function insertSupplierInfo() {
    commonUtils.callAjax({
      type: 'POST',
      url: '/api/standardInfo/getInsertSupplierInfo',
      paramData: commonUtils.serializeObject($('#formSup')),
      callback: (_data) =>
        connecteye.app.ui.alert.success({
          title: '추가 완료!',
          text: '입력한 매입처가 추가됐어요',
          didDestroy: () => {
            const resultData = _data[0];
            const { supId, supName } = resultData;
            // Create a DOM Option and pre-select by default
            let newOption = new Option(supName, supId, true, true);
            // Append it to the select
            $container.find('input[name=regSupId]').val(supId);
            $container.find('#regSupName').append(newOption).trigger('change');
            $container.find('#regBrnName').prop('disabled', false);
          },
        }),
    });
  }

  /**
   * 매입처 추가 팝업 보이기
   */
  function showInsertSupplierInfo() {
    let inputValue = $('.select2-search__field').val().trimStart().trimEnd();
    connecteye.app.ui.alert.custom().fire({
      html:
        `<div>매입처 추가</div>` +
        `<form id="formSup">` +
        `<label for="regSupName">매입처명</label>` +
        `<input id="regSupName" class="swal2-input" name="regSupName" placeholder="매입처명" value="${inputValue}" />` +
        `</form>`,
      focusConfirm: false,
      showCancelButton: true,
      cancelButtonText: '취소',
      confirmButtonText: '추가',
      confirmButtonColor: '#2e2e2e',
      showLoaderOnConfirm: true,
      buttonsStyling: false,
      reverseButtons: true,
      allowOutsideClick: false,
      customClass: {
        htmlContainer: 'custom-html-container',
        confirmButton: 'btn-custom-2',
        cancelButton: 'btn-custom-3 mr-2',
      },
      preConfirm: async () => {
        let isContinue = await isValidSupplierInfo();
        if (isContinue) {
          insertSupplierInfo();
        }
      },
      didRender() {
        $('#regRepPhone, #regMngPhone, #regFax').on('keyup', function () {
          $(this).val(commonUtils.setPhoneNumber(this.value));
        });
      },
    });
  }

  /**
   * 매입처 유효성체크
   */
  function isValidSupplierInfo() {
    let isContinue = true;
    $('.custom-html-container input').each((index, item) => {
      const { id, value, name } = item;
      if (commonUtils.isEmpty(item.value)) {
        connecteye.app.ui.alert
          .custom()
          .showValidationMessage(`${item.placeholder}를 입력해주세요`);
        isContinue = false;
        return false;
      }
      if (
        (id === 'regRepPhone' || id === 'regMngPhone' || id === 'regFax') &&
        !commonUtils.isPhone(value)
      ) {
        connecteye.app.ui.alert
          .custom()
          .showValidationMessage(`${item.placeholder}를 확인해주세요`);
        isContinue = false;
        return false;
      }
    });
    return isContinue;
  }

  /**
   * 기준정보 적용
   */
  function addRegData() {
    const obj = commonUtils.serializeObject($('#formSupPrdt'));

    if (commonUtils.isEmpty(obj)) {
      connecteye.app.ui.alert.warning({
        text: '매입처, 브랜드, 상품 정보를 입력해주세요',
      });
      return false;
    }

    if (
      commonUtils.isNotEmpty(supRegTable.grid.getData()) &&
      every(supRegTable.grid.getData(), ['regBarcode', obj.regBarcode])
    ) {
      connecteye.app.ui.alert.warning({
        text: '바코드는 중복 될 수 없어요',
      });
      return false;
    }

    if (commonUtils.isNotEmpty(imageUpload.getFile())) {
      productImages.push(imageUpload.getFile().file);
      obj.regPrdtImage = URL.createObjectURL(imageUpload.getFile().file);
    }
    obj.regSupName = $container.find('#regSupName option:selected').text();
    obj.regBrnName = $container.find('#regBrnName option:selected').text();
    supRegTable.grid.appendRow(obj);
  }

  /**
   * 바코드 번호 생성
   */
  function getBarcodeNumber() {
    const date = new Date();
    const currentMilliseconds = toMilliseconds({
      days: date.getDate(),
      hours: date.getHours(),
      minutes: date.getMinutes(),
      seconds: date.getSeconds(),
      milliseconds: date.getMilliseconds(),
    });

    // 년도 뒤 1자리 + 현재 월 + 현재 날짜 밀리초
    let barcode =
      date.getFullYear().toString().substr(-1) +
      (date.getMonth() + 1).toString().padStart(2, '0') +
      currentMilliseconds.toString();

    // 바코드 자릿수 체크
    switch (barcode.length) {
      case 12:
        barcode = barcode + Math.floor(Math.random() * 10).toString();
        break;
    }
    return barcode;
  }

  /**
   * 상품정보 초기화
   */
  function resetPrdtInfo() {
    // 상품정보 초기화
    $container.find('.product').val('');
    imageUpload.removeFiles();
  }

  /**
   * 기준정보 추가 화면 reset
   */
  async function resetPage() {
    const $formSupPrdt = $('#formSupPrdt');
    await $formSupPrdt
      .find('#regSupName')
      .empty()
      .trigger('change')
      .end()
      .find('#regBrnName')
      .empty()
      .trigger('change');

    commonUtils.formReset($formSupPrdt.find('input, select'));

    imageUpload.removeFiles();

    $container.parent().scrollTop(0);

    productImages = [];

    // 그리드 데이터 삭제
    resetGridData(supRegTable.grid);
  }

  /**
   * 그리드 데이터 삭제
   */
  function resetGridData(_grid) {
    _grid.getData().forEach((item) => {
      // 사용 후 메모리에서 삭제
      if (commonUtils.isNotEmpty(item.regPrdtImage)) {
        URL.revokeObjectURL(item.regPrdtImage);
      }
    });
    _grid.clear();
  }

  /**
   * form 데이터 세팅 후 Submit
   */
  async function setFormDataWithSubmit(_form) {
    const $form = $(_form);
    const formType = $form.data('type');

    switch (formType) {
      case 'insert':
        if (commonUtils.isNotEmpty(supRegTable.grid.getData())) {
          supRegTable.grid.getData().forEach((item) => {
            Object.keys(item).forEach((objKey) => {
              if (objKey.includes('Price')) {
                supRegTable.grid.setValue(
                  item.rowKey,
                  objKey,
                  commonUtils.removeComma(item[objKey]),
                );
              }
            });
          });
          runInsertSubmit();
        } else {
          connecteye.app.ui.alert.warning({
            text: '기준정보 추가내역이 없어요',
          });
        }
        break;
      case 'modify':
        $form.find('input').each(function () {
          const type = this.type;
          const name = this.name;
          if (type === 'text' && name.includes('Price')) {
            this.value = commonUtils.removeComma(this.value);
          }
        });
        runUpdateSubmit();
        break;
      case 'save':
        addRegData();
        $container.find('#regPrdtName').focus();
        // resetPrdtInfo();
        break;
    }
  }

  /**
   * 엑셀 업로드 팝업 보이기
   *
   */
  function showExcelFileUploadPopup() {
    let excelUpload;
    const regSupName = $container.find('#regSupName').text();
    const regBrnName = $container.find('#regBrnName').text();

    if (commonUtils.isEmpty(regSupName) || commonUtils.isEmpty(regBrnName)) {
      connecteye.app.ui.alert.warning({
        text: '매입처, 브랜드를 선택해주세요',
      });
      return false;
    }

    connecteye.app.ui.alert.custom().fire({
      html:
        `<div class="form-group form-show-validation row">` +
        `<label for="standardInfoFile" class="col-3 pt-2 text-right">` +
        `템플릿 <a href="https://connect-eye-temp.s3.ap-northeast-2.amazonaws.com/%EA%B8%B0%EC%A4%80%EC%A0%95%EB%B3%B4_%ED%85%9C%ED%94%8C%EB%A6%BF_1.1.xlsx" id="downloadTemplate" class="fas fa-download ml-1 text-dark download-template"></a>` +
        `</label>` +
        `<div class="col-9">` +
        `<input type="file" class="filepond" id="standardInfoFile" name="standardInfoFile" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"/>` +
        `</div>` +
        `</div>`,
      showCancelButton: true,
      cancelButtonText: '취소',
      confirmButtonText: '추가',
      confirmButtonColor: '#2e2e2e',
      buttonsStyling: false,
      reverseButtons: true,
      allowOutsideClick: false,
      customClass: {
        htmlContainer: 'custom-file-container',
        confirmButton: 'btn-custom-2',
        cancelButton: 'btn-custom-3 mr-2',
      },
      preConfirm: () => {
        if (commonUtils.isEmpty(excelUpload.getFile())) {
          connecteye.app.ui.alert.warning({
            text: '파일을 추가해주세요',
          });
          return false;
        }
        insertExcelData(excelUpload.getFile());
      },
      willOpen() {
        excelUpload = fileUploadUtils.init({
          el: 'standardInfoFile',
          title: '파일을 여기로 드래그하세요',
          fileType: 'csv, xlsx 만 가능',
        });

        // 툴팁, 팝오버
        connecteye.app.ui.createTippy({
          element: '#downloadTemplate',
          content: '기준정보 템플릿을 다운로드 할 수 있어요!',
          placement: 'top',
          theme: 'gradient',
        });
      },
      didRender() {},
    });
  }

  /**
   * 엑셀 데이터 세팅
   *
   * @param {Object} _file
   */
  function insertExcelData(_file) {
    connecteye.app.ui.showCardLoading();

    const regSupId = $container.find('#regSupName').val();
    const regBrnId = $container.find('#regBrnName').val();

    // 파일 읽기 클래스
    let reader = new FileReader();

    // 읽기가 로드 됐을 때
    reader.onload = async () => {
      commonUtils.callAjax(
        {
          type: 'POST',
          url: '/api/standardInfo/insertExcelStandardInfoList',
          dataType: 'form',
          paramData: await commonUtils.getAjaxFormData({
            formData: commonUtils.getBlobObject({
              data: { regSupId: regSupId, regBrnId: regBrnId },
              option: {
                type: 'application/json',
              },
            }),
            excelFile: _file.file,
          }),
          callback: (_data) => {
            connecteye.app.ui.hideCardLoading();
            connecteye.app.ui.alert
              .success({
                title: '기준정보 엑셀 업로드 완료!',
                text: '기준정보가 추가됐어요',
              })
              .then((_SuccessResult) => {
                if (_SuccessResult.isConfirmed) {
                  showStandardInfoList();
                }
              });
          },
        },
        true,
      );
    };
    reader.readAsBinaryString(_file.file);
  }
  /***** Business End *****/
};
