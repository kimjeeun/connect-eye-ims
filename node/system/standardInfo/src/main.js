/*
 *  Description: standardInfo main.js
 *  User: kyle
 *  Date: 2021-12-09
 */
import standardInfo from './standardInfo';
import standardInfoReg from './standardInfoReg';

$(document).ready(() => {
  // 기준정보
  standardInfo();
  // 기준정보 등록 스크립트 import
  standardInfoReg();
});
