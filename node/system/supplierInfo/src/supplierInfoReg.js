/*
 *  Description: 매입처 수정
 *  User: kyle
 *  Date: 2022-01-13
 */
import commonUtils from '../../utils/commonUtils';

require('jquery-validation');

export default () => {
  /***** Static Start *****/
  const $container = $('#supplierInfo');
  const $form = $container.find('#formSup');

  const daumPostcode = commonUtils.getDaumPostCode($form);

  // 유효성체크
  $form.validate({
    rules: {
      regSupName: {
        required: true,
        rangelength: [2, 100],
      },
      regRepPhone: {
        phonekor: true,
      },
      regMngPhone: {
        phonekor: true,
      },
      regFax: {
        phonekor: true,
      },
      regComment: {
        rangelength: [2, 100],
      },
    },
    errorPlacement: (_error, _element) => _element.parent().children().last().after(_error),
    submitHandler: (_form) =>
      $(_form).data('type') !== 'insert' ? runUpdateSubmit() : runInsertSubmit(),
  });
  // TODO: 수정해야됨
  // 유효성체크 메세지 설정
  commonUtils.setJqueryValidationLanguageKor();
  /***** Static End *****/

  /***** Event Start *****/
  // 매입처 수정 화면 dom 로드 시
  $container.find('.card[data-area=supReg]').on('loadDom', function (_evt, _data) {
    $container.parent().scrollTop(0);
    bindSupplierInfo({
      mode: _data?.mode,
      supId: _data?.supId,
    });
  });

  // 매입처 추가 버튼 클릭 이벤트
  $container.find('.btn-add').on('click', () => {
    $form.data('type', 'insert');
    $form.submit();
  });

  // 매입처 수정 버튼 클릭 이벤트
  $container.find('.btn-save').on('click', () => {
    $form.data('type', 'modify');
    $form.submit();
  });

  // 전화번호 이벤트
  $container.find('#regRepPhone, #regMngPhone, #regFax').on('keyup', function () {
    $(this).val(commonUtils.setPhoneNumber(this.value));
  });

  // 이전 버튼 이벤트
  $container.find('.btn-prev').on('click', () => resetPage());

  // 매입처 추가 form reset 버튼 이벤트
  $container.find('.btn-reset').on('click', () => resetPage());

  // 주소 찾기 버튼 클릭 이벤트
  $container.find('.search-icon').on('click', () => daumPostcode.open());
  /***** Event End *****/

  /***** Business Start *****/
  /**
   * 추가 submit 실행
   */
  function runInsertSubmit() {
    // form 전송 ajax 처리
    connecteye.app.ui.alert
      .confirm({
        title: '매입처를 추가할까요?',
        text: '취소를 누르면 내용을 수정 할 수 있어요',
        confirmBtnTxt: '추가',
        cancelBtnTxt: '취소',
        showLoaderOnConfirm: true,
        preConfirm: () => {
          $container.parent().scrollTop(0);
        },
      })
      .then((_result) => {
        if (_result.isConfirmed) {
          connecteye.app.ui.showCardLoading();
          saveSupplierInfo('/api/supplierInfo/insertSupplierInfo', () => {
            connecteye.app.ui.hideCardLoading();
            connecteye.app.ui.alert
              .success({
                title: '추가 완료!',
                text: '입력한 매입처가 추가됐어요',
                denyButtonText: '계속 추가하기',
                showDenyButton: true,
              })
              .then((_SuccessResult) => {
                if (_SuccessResult.isConfirmed) {
                  showSupplierInfoList();
                } else if (_SuccessResult.isDenied) {
                  resetPage();
                }
              });
          });
        } else {
          connecteye.app.ui.alert.error({
            title: '추가 취소',
            text: '매입처 정보 수정 후 다시 추가 할 수 있어요 :)',
            icon: 'error',
          });
        }
      });
  }
  /**
   * 수정 submit 실행
   */
  function runUpdateSubmit() {
    // form 전송 ajax 처리
    connecteye.app.ui.alert
      .confirm({
        title: '매입처를 수정할까요?',
        text: '취소를 누르면 내용을 수정 할 수 있어요',
        confirmBtnTxt: '수정',
        cancelBtnTxt: '취소',
        showLoaderOnConfirm: true,
        preConfirm: () => {
          $container.parent().scrollTop(0);
        },
      })
      .then((_result) => {
        if (_result.isConfirmed) {
          connecteye.app.ui.showCardLoading();
          saveSupplierInfo('/api/supplierInfo/updateSupplierInfo', () => {
            connecteye.app.ui.hideCardLoading();
            connecteye.app.ui.alert
              .success({
                title: '수정 완료!',
                text: '입력한 매입처가 수정됐어요',
              })
              .then((_SuccessResult) => {
                if (_SuccessResult.isConfirmed) {
                  showSupplierInfoList();
                }
              });
          });
        } else {
          connecteye.app.ui.alert.error({
            title: '수정 취소',
            text: '매입처 정보를 다시 수정 할 수 있어요 :)',
            icon: 'error',
          });
        }
      });
  }
  /**
   * 매입처 목록 보여주기
   */
  function showSupplierInfoList() {
    $container.find('.btn-prev').trigger('click');
    $container.find('.card[data-area=supList]').trigger('loadDom', { listReload: true });
  }
  /**
   * 매입처 정보 세팅
   *
   * @param {Object} _param
   */
  function bindSupplierInfo(_param) {
    const $cardTitle = $container.find('.card-title');
    const { mode, supId } = _param;
    if (mode !== 'modify') {
      connecteye.app.ui.hideCardLoading(0);
      $cardTitle.text('매입처 추가');
      $container
        .find('.btn-add')
        .removeClass('d-none')
        .end()
        .find('.btn-reset')
        .removeClass('d-none')
        .end()
        .find('.btn-save')
        .addClass('d-none');
    } else {
      connecteye.app.ui.showCardLoading();
      commonUtils.callAjax({
        type: 'GET',
        url: '/api/supplierInfo/getSupplierInfo',
        paramData: {
          supId: supId,
          perPage: 1,
          page: 1,
        },
        callback: (_result) => setModifySupplierInfo(_result),
      });
    }
  }
  /**
   * 수정정보 가져와서 세팅
   *
   * @param {Object} _result
   */
  function setModifySupplierInfo(_result) {
    const $cardTitle = $container.find('.card-title');
    const resultData = _result.contents[0];
    $cardTitle.text('매입처 수정');
    $container
      .find('.btn-add')
      .addClass('d-none')
      .end()
      .find('.btn-reset')
      .addClass('d-none')
      .end()
      .find('.btn-save')
      .removeClass('d-none');
    $form.find('input, select, button').each(function (index, item) {
      // regRepPhone -> repPhone 맵핑 로직
      let initial = item.name.slice(3),
        resultStr = initial.replace(/\b[A-Z]/, (letter) => letter.toLowerCase());
      item.value = resultData[resultStr] || '';
    });
    connecteye.app.ui.hideCardLoading();
  }
  /**
   * 매입처 정보 저장
   *
   * @param {String} _url
   * @param {function} _callback
   */
  function saveSupplierInfo(_url, _callback) {
    commonUtils.callAjax(
      {
        type: 'POST',
        url: _url,
        paramData: commonUtils.serializeObject($form),
        callback: _callback,
      },
      true,
    );
  }
  /**
   * 화면 reset
   */
  function resetPage() {
    const $formSup = $('#formSup');
    commonUtils.formReset($formSup.find('input, select'));
    $container.parent().scrollTop(0);
  }
  /***** Business End *****/
};
