/*
 *  Description: supplierInfo main.js
 *  User: kyle
 *  Date: 2021-12-09
 */
import supplierInfo from './supplierInfo';
import supplierInfoReg from './supplierInfoReg';

$(document).ready(async () => {
  // 매입처상세
  supplierInfo();
  // 매입처 수정
  supplierInfoReg();
});
