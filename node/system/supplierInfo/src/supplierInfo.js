/*
 *  Description: 매입처
 *  User: kyle
 *  Date: 2022-01-13
 */
import gridUtils from '../../utils/gridUtils';
import commonUtils from '../../utils/commonUtils';

export default () => {
  /***** Static Start *****/
  const $container = $('#supplierInfo');
  // table
  const supplierInfoTable = new gridUtils.create({
    el: 'supplierInfoTable',
    data: {
      api: {
        readData: {
          url: '/api/supplierInfo/getSupplierInfo',
          method: 'GET',
        },
      },
    },
    pageOptions: {
      // 페이징 옵션
      perPage: 7, // 1페이지당 목록 갯수
      visiblePages: 5, // 페이징 넘버링 갯수
    },
    columns: [
      { header: '매입처', name: 'supName', width: 120, ellipsis: true },
      { header: '대표자', name: 'repName', width: 100, align: 'center' },
      { header: '대표번호', name: 'repPhone', width: 120, align: 'center' },
      { header: '담당자', name: 'mgrName', width: 80, align: 'center' },
      { header: '담당번호', name: 'mngPhone', width: 120, align: 'center' },
      { header: 'FAX', name: 'fax', width: 120, align: 'center' },
      { header: '주소', name: 'address', ellipsis: true },
      {
        header: '사용',
        name: 'isUse',
        width: 80,
        align: 'center',
        renderer: {
          type: gridUtils.setToggleSwitchRenderer,
          options: {
            url: '/api/supplierInfo/updateSupplierInfoUse',
            pkName: 'supId',
          },
        },
      },
    ],
    // 그리드 생성 완료 시
    onGridMounted: (_gridThis) => _gridThis.instance.refreshLayout(), // 그리드 레이아웃 새로고침

    // 그리드 업데이트 시  'resetData', 'restore', 'reloadData', 'readData', 'setPerPage'
    onGridUpdated: (_gridThis) => {
      const totalCount = _gridThis.instance.getPaginationTotalCount();
      // 데이터 건수
      $container.find('.result-cnt').text(commonUtils.setComma(totalCount));
      // 그리드 레이아웃 새로고침
      _gridThis.instance.refreshLayout();
    },
  });
  /***** Static End *****/

  /***** Event Start *****/
  // 매입처 리스트 화면 dom 로드 시
  $container.find('.card[data-area=supList]').on('loadDom', (_evt, _data) => {
    if (_data && _data.listReload) {
      supplierInfoTable.grid.reloadData();
    }
  });

  // 그리드 클릭 이벤트
  supplierInfoTable.grid.on('click', (_evt) => {
    const { columnName, targetType, instance, rowKey } = _evt;
    if (
      columnName !== 'isUse' &&
      targetType !== 'columnHeader' &&
      targetType !== 'rowHeader' &&
      targetType !== 'etc'
    ) {
      const $target = $container.find('.card[data-area=supReg]');
      $target
        .removeClass('d-none')
        .siblings('.card')
        .addClass('d-none')
        .end()
        .trigger('loadDom', {
          supId: instance.getRow(rowKey).supId,
          mode: 'modify',
        });
    }
  });

  // 검색 버튼 이벤트
  $container.find('.btn-select').on('click', () => getSupplierInfo());
  $container.find('.search-group').on('keyup', (_evt) => {
    if (_evt.keyCode === 13) {
      getSupplierInfo();
    }
  });

  // 엑셀업로드 버튼 클릭 이벤트
  $container.find('.btn-reg-excel').on('click', (_evt) => {

  });

  // 매입처 추가 이벤트
  $container.find('.btn-reg-sup').on('click', (_evt) => getArea(_evt));

  // 이전 버튼 이벤트
  $container.find('.btn-prev').on('click', (_evt) => getArea(_evt));

  // 기준정보 상품 선택삭제 이벤트
  $container.find('.btn-del-sup').on('click', async () => {
    const pks = await supplierInfoTable.grid.getCheckedRows().map((item) => item.supId);
    if (commonUtils.isEmpty(pks)) {
      connecteye.app.ui.alert.warning({
        title: '주의!',
        text: '삭제하려는 기준정보를 선택해주세요',
      });
      return false;
    }
    deleteSupplierInfo(pks);
  });
  /***** Event End *****/

  /***** Business Start *****/
  /**
   * 매입처 정보 가져오기
   */
  function getSupplierInfo() {
    const $form = $container.find('.search-group');
    const formSerializeData = commonUtils.serializeObject($form);

    // args1: page, args2: paramData (GET 방식), args3: 그리드 초기화 여부
    supplierInfoTable.grid.readData(1, formSerializeData, true);
  }
  /**
   * 매입처 삭제
   *
   * @param {String} _pks
   */
  function deleteSupplierInfo(_pks) {
    connecteye.app.ui.alert
      .confirm({
        title: '매입처를 삭제할까요?',
        text: '매입처와 연결된 기준정보가 삭제됩니다',
        confirmBtnTxt: '삭제',
        cancelBtnTxt: '취소',
        showLoaderOnConfirm: true,
        preConfirm: () => {
          $container.parent().scrollTop(0);
        },
      })
      .then((_result) => {
        if (_result.isConfirmed) {
          connecteye.app.ui.showCardLoading();
          commonUtils.callAjax({
            type: 'DELETE',
            url: '/api/supplierInfo/deleteSupplierInfo',
            paramData: { pks: _pks },
            callback: () => {
              connecteye.app.ui.hideCardLoading();
              connecteye.app.ui.alert
                .success({
                  title: '삭제 완료!',
                  text: '선택한 매입처가 삭제됐어요',
                })
                .then((_SuccessResult) => {
                  if (_SuccessResult.isConfirmed) {
                    getSupplierInfo();
                  }
                });
            },
          });
        }
      });
  }
  /**
   * 영역 가져오기
   *
   * @param {Object} _evt
   */
  async function getArea(_evt) {
    const $el = $(_evt.currentTarget);
    const target = $el.data('target');
    const $parentArea = $container.find(`.page-inner .card[data-area=${target}]`);

    // 자신을 제외하고 d-none 클래스 넣어줌
    await $parentArea
      .removeClass('d-none')
      .siblings('.card')
      .addClass('d-none')
      .end()
      .trigger('loadDom');

    $container.parent().scrollTop(0);
    supplierInfoTable.grid.refreshLayout();
  }
  /***** Business End *****/
};
