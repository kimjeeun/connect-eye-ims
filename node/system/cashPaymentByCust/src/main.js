/*
 *  Description: cashPaymentByCust main.js
 *  User: kyle
 *  Date: 2022-05-24
 */
import cashPaymentByCust from './cashPaymentByCust';

$(document).ready(() => {
  // 금전출납 고객별 납부
  cashPaymentByCust();
});
