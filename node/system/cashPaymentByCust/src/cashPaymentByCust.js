/*
 *  Description: 금전출납 고객별 납부
 *  User: kyle
 *  Date: 2022-04-11
 */
import moment from 'moment';
import gridUtils from '../../utils/gridUtils';
import commonUtils from '../../utils/commonUtils';
import sumBy from 'lodash-es/sumBy';

export default () => {
  /***** Static Start *****/
  const $container = $('#cashPaymentByCust');

  // 툴팁, 팝오버
  const tippy = connecteye.app.ui.createTippy({
    element: document.getElementById('cashPaymentByCustTable'),
    content: '현금/카드/상품권/할인 금액을 입력해주세요!',
    placement: 'top-end',
    theme: 'gradient',
    trigger: 'click',
    onUntrigger(instance) {
      instance.disable();
    },
  });

  // table
  const cashPaymentByCustTable = new gridUtils.create({
    el: 'cashPaymentByCustTable',
    rowHeaders: [],
    bodyHeight: 'auto',
    minBodyHeight: 105,
    columnOptions: {
      // 1개의 컬럼을 고정
      frozenCount: 1,
    },
    columns: [
      {
        header: '판매일자',
        name: 'regDate',
        width: 100,
        ellipsis: true,
        align: 'center',
      },
      {
        header: '고객명',
        name: 'custName',
        width: 90,
        align: 'center',
      },
      {
        header: '매입처',
        width: 90,
        name: 'supName',
        align: 'center',
        ellipsis: true,
      },
      {
        header: '상품구분',
        name: 'prdtDiv',
        width: 90,
        align: 'center',
      },
      {
        header: '브랜드',
        name: 'brnName',
        width: 90,
        ellipsis: true,
      },
      {
        header: '상품명',
        name: 'prdtName',
        width: 100,
        ellipsis: true,
      },
      {
        header: '수량',
        name: 'amount',
        width: 70,
        align: 'center',
      },
      {
        header: '판매총액',
        name: 'price',
        width: 90,
        align: 'right',
        formatter: commonUtils.setComma,
        renderer: {
          styles: {
            color: (props) => (props.value < 0 ? '#f25961' : '#333'),
          },
        },
      },
      {
        header: '미수금',
        name: 'rcvPrice',
        width: 90,
        align: 'right',
        formatter: commonUtils.setComma,
        renderer: {
          styles: {
            color: (props) => (props.value < 0 ? '#f25961' : '#333'),
          },
        },
      },
      {
        header: '현금',
        name: 'cashPrice',
        width: 90,
        align: 'right',
        defaultValue: '0',
        formatter: commonUtils.setComma,
        editor: {
          type: 'text',
        },
      },
      {
        header: '카드',
        name: 'cardPrice',
        width: 90,
        align: 'right',
        defaultValue: '0',
        formatter: commonUtils.setComma,
        editor: {
          type: 'text',
        },
      },
      {
        header: '상품권',
        name: 'giftPrice',
        width: 90,
        align: 'right',
        defaultValue: '0',
        formatter: commonUtils.setComma,
        editor: {
          type: 'text',
        },
      },
      {
        header: '할인',
        name: 'salePrice',
        width: 90,
        align: 'right',
        defaultValue: '0',
        formatter: commonUtils.setComma,
        editor: {
          type: 'text',
        },
      },
    ],
    // 그리드 생성 완료 시
    onGridMounted: (_gridThis) => {
      // 금전출납 정보 가져오기
      getCashPaymentInfo();
      // 툴팁
      tippy.show();
    },
    // 그리드 업데이트 시  'resetData', 'restore', 'reloadData', 'readData', 'setPerPage'
    onGridUpdated: (_gridThis) => {
      // 그리드 레이아웃 새로고침
      _gridThis.instance.refreshLayout();

      // 초기 납부정보 세팅
      setInitCashPaymentInfo(_gridThis);
    },
  });

  // 납부일 datepicker
  connecteye.app.ui.createDatePicker({
    containerEl: '#cashPaymentByCust .pay-date',
    parentEl: '.pay-date-container',
    startDate: moment(),
    singleDatePicker: true,
    ranges: false,
    callback: () => {},
  });
  /***** Static End *****/

  /***** Event Start *****/
  // 금전출납 리스트 화면 dom 로드 시
  $container
    .find('.card[data-area=cashPaymentByCust]')
    .on('loadDom', (_evt, _data) => {});

  // 그리드 이벤트
  cashPaymentByCustTable.grid.on('editingFinish', (_evt) => setCashPaymentInfo(_evt));

  // 납부정보 입력 후 납부하기 버튼 클릭 시 이벤트
  $container.find('#btnCashPaymentByCust').on('click', () => cashPaymentByCustHandler());

  // 초기화
  $container.find('#btnCashPaymentByCustReset').on('click', () => resetPage());

  // 이전페이지 초기화
  $container.find('.btn-prev').on('click', () => resetPage());
  /***** Event End *****/

  /***** Business Start *****/
  /**
   * 금전출납 정보 가져오기
   */
  function getCashPaymentInfo() {
    // 그리드 로딩바
    cashPaymentByCustTable.grid.dispatch('setLoadingState', 'LOADING');
    let options = {
      type: 'GET',
      url: '/api/optometrySalesInfo/getCashPaymentByCustInfo',
      paramData: {
        custId: $container.find('#custId').val(),
      },
      callback: (_res) => {
        if (commonUtils.isNotEmpty(_res)) {
          // 그리드에 데이터 넣음
          cashPaymentByCustTable.grid.resetData(_res);
          // hideLoadingBar
          cashPaymentByCustTable.grid.dispatch('setLoadingState', 'DONE');
        } else {
          cashPaymentByCustTable.grid.dispatch('setLoadingState', 'EMPTY');
        }
      },
    };
    commonUtils.callAjax(options, true);
  }
  /**
   * 초기 납부정보 세팅
   *
   * @param {Object} _gridThis
   */
  function setInitCashPaymentInfo(_gridThis) {
    const gridData = _gridThis.instance.getData();
    const { rcvPrice } = gridData.reduce((obj, { rcvPrice }) => {
      (obj['rcvPrice'] = (parseInt(obj['rcvPrice']) || 0) + parseInt(rcvPrice || 0)), obj;
      return obj;
    }, {});
    $container.find('.total-rcv-price').text(commonUtils.setComma(rcvPrice));
  }

  /**
   * 납부정보 세팅
   *
   * @param _evt
   */
  function setCashPaymentInfo(_evt) {
    // 이벤트 데이터
    const { instance, value, columnName, rowKey, triggeredByKey } = _evt;

    if (commonUtils.isNumeric(value)) {
      // 선택한 Row 데이터
      const rowData = instance.getRow(rowKey);
      const { rcvPrice, cardPrice, cashPrice, giftPrice, salePrice } = rowData;

      // 그리드 데이터
      const gridData = instance.getData();
      const {
        totalCashPrice,
        totalCardPrice,
        totalSalePrice,
        totalGiftPrice,
        totalFinalRcvPrice,
      } = gridData.reduce(
        (obj, item) => {
          obj.totalFinalRcvPrice = (parseInt(item['rcvPrice']) || 0) + parseInt(obj.totalFinalRcvPrice || 0);
          obj.totalCashPrice = (parseInt(item['cashPrice']) || 0) + parseInt(obj.totalCashPrice || 0);
          obj.totalCardPrice = (parseInt(item['cardPrice']) || 0) + parseInt(obj.totalCardPrice || 0);
          obj.totalGiftPrice = (parseInt(item['giftPrice']) || 0) + parseInt(obj.totalGiftPrice || 0);
          obj.totalSalePrice = (parseInt(item['salePrice']) || 0) + parseInt(obj.totalSalePrice || 0);
          return obj;
        },
        {},
      );

      const totalRcvPrice = Number(totalCashPrice) + Number(totalCardPrice) + Number(totalGiftPrice);

      // 현재 입력 금액 데이터
      const $totalRcvPrice = $container.find('.total-rcv-price');
      const $returnRcvPrice = $container.find('.return-rcv-price');

      // element, value 구분
      const compareValue = rcvPrice - Number(salePrice);
      const setCurrentValue = Number(cashPrice) + Number(cardPrice) + Number(giftPrice);
      const setTotalValue = totalFinalRcvPrice - totalSalePrice;
      const totalCompareValue = Math.abs(Number(commonUtils.removeComma($totalRcvPrice.text())));

      if (Math.abs(setCurrentValue) > Math.abs(compareValue) || Math.abs(setTotalValue) > totalCompareValue) {
        instance.setValue(rowKey, columnName, 0);
        instance.startEditing(rowKey, columnName);
        connecteye.app.ui.alert.warning({
          text: '금액이 맞지않아요',
          icon: 'error',
        });
        return false;
      }

      // 미수금 데이터 넣어줌
      $returnRcvPrice.text(commonUtils.setComma(totalRcvPrice));
      $totalRcvPrice.text(commonUtils.setComma(setTotalValue));
    } else {
      connecteye.app.ui.alert.warning({
        text: '숫자만 입력해주세요.',
        icon: 'error',
      });
      if (!triggeredByKey) {
        instance.startEditing(rowKey, columnName);
      }
    }
  }

  /**
   * 금전출납 고객별 납부하기 버튼 핸들러
   */
  function cashPaymentByCustHandler() {
    const $totalRcvPrice = $container.find('.total-rcv-price');
    const $returnRcvPrice = $container.find('.return-rcv-price');

    const totalRcvPrice = Math.abs(parseInt(commonUtils.removeComma($totalRcvPrice.text())));
    const returnRcvPrice = Number(commonUtils.removeComma($returnRcvPrice.text()));

    // 그리드 데이터 할인금액이 null 일경우 0으로 넣어줌
    let gridData = cashPaymentByCustTable.grid.getData();
    gridData.forEach((item) => {
      if (!item.salePrice) {
        item.salePrice = 0;
      }
    });

    // 현금/카드 유효성 체크
    const totalCashPrice = sumBy(gridData, 'cashPrice');
    const totalCardPrice = sumBy(gridData, 'cardPrice');
    const totalPrice = totalCardPrice + totalCashPrice;

    // 현금/카드 유효성 체크
    if (!totalPrice) {
      connecteye.app.ui.alert.warning({
        text: '현금 또는 카드 정보를 입력해주세요',
      });
      return false;
    }

    if (totalRcvPrice !== returnRcvPrice) {
      connecteye.app.ui.alert.warning({
        text: '납부 금액이 맞지않아요',
      });
      return false;
    }
    // 납부실행
    runCashPaymentHistory();
  }

  /**
   * 금전출납 납부 등록 실행
   */
  function runCashPaymentHistory() {
    // 납부내역 등록
    connecteye.app.ui.alert
      .confirm({
        title: '납부를 실행할까요?',
        text: '취소를 누르면 내용을 수정 할 수 있어요',
        confirmBtnTxt: '추가',
        cancelBtnTxt: '취소',
        showLoaderOnConfirm: true,
        preConfirm: () => {},
      })
      .then((_result) => {
        if (_result.isConfirmed) {
          insertCashPaymentByCust();
        } else {
          connecteye.app.ui.alert.error({
            title: '납부 취소',
            text: '납부정보 수정 후 다시 추가 할 수 있어요 :)',
            icon: 'error',
          });
        }
      });
  }

  /**
   * 금전출납 납부내역 등록
   */
  function insertCashPaymentByCust() {
    const cashPaymentInfoList = cashPaymentByCustTable.grid.getData();

    connecteye.app.ui.showCardLoading();
    commonUtils.callAjax({
      type: 'POST',
      url: '/api/optometrySalesInfo/insertCashPaymentByCust',
      paramData: {
        cashPaymentInfoList: JSON.stringify(cashPaymentInfoList),
      },
      callback: () => {
        connecteye.app.ui.hideCardLoading();
        connecteye.app.ui.alert
          .success({
            title: '납부 완료!',
            text: '입력한 납부정보가 등록됐어요',
            showDenyButton: false,
          })
          .then((_SuccessResult) => {
            if (_SuccessResult.isConfirmed) {
              // 검안매출로 이동
              const custId = $container.find('#custId').val();
              connecteye.app.ui.movePage(`/page/optometrySalesInfoPage.ce?custId=${custId}`);
            }
          });
      },
    });
  }

  /**
   * 초기화
   */
  function resetPage() {
    cashPaymentByCustTable.grid.setColumnValues('cashPrice');
    cashPaymentByCustTable.grid.setColumnValues('cardPrice');
    cashPaymentByCustTable.grid.setColumnValues('salePrice');
    cashPaymentByCustTable.grid.setColumnValues('giftPrice');
    $container.find('.cash-control').text('0').end().parent().scrollTop(0);
  }
  /***** Business End *****/
};
