/*
 *  Description: cashPaymentInfo main.js
 *  User: kyle
 *  Date: 2021-12-09
 */
import cashPaymentBySupplier from './cashPaymentBySupplier';

$(document).ready(() => {
  // 금전출납 매입처별 납부
  cashPaymentBySupplier();
});
