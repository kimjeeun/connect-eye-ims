/*
 *  Description: 금전출납 매입처별 납부
 *  User: kyle
 *  Date: 2022-04-11
 */
import gridUtils from '../../utils/gridUtils';
import commonUtils from '../../utils/commonUtils';
import moment from 'moment';
import sumBy from 'lodash-es/sumBy';

export default () => {
  /***** Static Start *****/
  const $container = $('#cashPaymentBySupplier');

  // 툴팁, 팝오버
  const tippy = connecteye.app.ui.createTippy({
    element: document.getElementById('cashPaymentBySupplierTable'),
    content: '현금/카드/할인 금액을 입력해주세요!',
    placement: 'top-end',
    theme: 'gradient',
    trigger: 'click',
    onUntrigger(instance) {
      instance.disable();
    },
  });

  // table
  const cashPaymentBySupplierTable = new gridUtils.create({
    el: 'cashPaymentBySupplierTable',
    rowHeaders: [],
    bodyHeight: 'auto',
    minBodyHeight: 205,
    columnOptions: {
      // 1개의 컬럼을 고정
      frozenCount: 1,
    },
    columns: [
      {
        header: '문서번호',
        name: 'purId',
        width: 200,
        ellipsis: true,
        align: 'center',
      },
      {
        header: '분류',
        name: 'purType',
        width: 70,
        align: 'center',
      },
      {
        header: '매입처',
        width: 90,
        name: 'supName',
        align: 'center',
        ellipsis: true,
      },
      {
        header: '상품구분',
        name: 'prdtDiv',
        width: 90,
        align: 'center',
      },
      {
        header: '브랜드',
        name: 'brnName',
        width: 90,
        ellipsis: true,
      },
      {
        header: '상품명',
        name: 'prdtName',
        width: 100,
        ellipsis: true,
      },
      {
        header: '평균단가',
        name: 'avgPrice',
        width: 90,
        align: 'right',
        formatter: commonUtils.setComma,
        renderer: {
          styles: {
            color: (props) => (props.value < 0 ? '#f25961' : '#333'),
          },
        },
      },
      {
        header: '수량',
        name: 'amount',
        width: 70,
        align: 'center',
      },
      {
        header: '금액',
        name: 'price',
        width: 90,
        align: 'right',
        formatter: commonUtils.setComma,
        renderer: {
          styles: {
            color: (props) => (props.value < 0 ? '#f25961' : '#333'),
          },
        },
      },
      {
        header: '미지급금',
        name: 'unpaidPrice',
        width: 90,
        align: 'right',
        formatter: commonUtils.setComma,
      },
      {
        header: '미수금',
        name: 'rcvPrice',
        width: 90,
        align: 'right',
        formatter: commonUtils.setComma,
        renderer: {
          styles: {
            color: (props) => (props.value < 0 ? '#f25961' : '#333'),
          },
        },
      },
      {
        header: '현금',
        name: 'cashPrice',
        width: 90,
        align: 'right',
        defaultValue: '0',
        formatter: commonUtils.setComma,
        editor: {
          type: 'text',
        },
      },
      {
        header: '카드',
        name: 'cardPrice',
        width: 90,
        align: 'right',
        defaultValue: '0',
        formatter: commonUtils.setComma,
        editor: {
          type: 'text',
        },
      },
      {
        header: '할인',
        name: 'salePrice',
        width: 90,
        align: 'right',
        defaultValue: '0',
        formatter: commonUtils.setComma,
        editor: {
          type: 'text',
        },
      },
    ],
    // 그리드 생성 완료 시
    onGridMounted: (_gridThis) => {
      // 금전출납 정보 가져오기
      getCashPaymentInfo();
      // 툴팁
      tippy.show();
    },
    // 그리드 업데이트 시  'resetData', 'restore', 'reloadData', 'readData', 'setPerPage'
    onGridUpdated: (_gridThis) => {
      // 그리드 레이아웃 새로고침
      _gridThis.instance.refreshLayout();

      // 초기 납부정보 세팅
      setInitCashPaymentInfo(_gridThis);
    },
  });

  // 납부일 datepicker
  connecteye.app.ui.createDatePicker({
    containerEl: '#cardPaymentBySupplierInfo .pay-date',
    parentEl: '.pay-date-container',
    startDate: moment(),
    singleDatePicker: true,
    ranges: false,
    callback: () => {},
  });
  /***** Static End *****/

  /***** Event Start *****/
  // 금전출납 리스트 화면 dom 로드 시
  $container
    .find('.card[data-area=cashPaymentBySupplier]')
    .on('loadDom', async (_evt, _data) => {});

  // 그리드 이벤트
  cashPaymentBySupplierTable.grid.on('editingFinish', (_evt) => setCashPaymentInfo(_evt));

  // 납부정보 입력 후 납부하기 버튼 클릭 시 이벤트
  $container.find('#btnCashPaymentBySupplier').on('click', () => cashPaymentBySupplierHandler());

  // 초기화
  $container.find('#btnCashPaymentBySupplierReset').on('click', () => resetPage());

  // 이전페이지 초기화
  $container.find('.btn-prev').on('click', () => resetPage());
  /***** Event End *****/

  /***** Business Start *****/
  /**
   * 금전출납 정보 가져오기
   */
  function getCashPaymentInfo() {
    // 그리드 로딩바
    cashPaymentBySupplierTable.grid.dispatch('setLoadingState', 'LOADING');
    let options = {
      type: 'GET',
      url: '/api/cashPaymentInfo/getCashPaymentInfo',
      paramData: {
        supId: $container.find('#supId').val(),
      },
      callback: (_res) => {
        if (commonUtils.isNotEmpty(_res)) {
          // 그리드에 데이터 넣음
          cashPaymentBySupplierTable.grid.resetData(_res);
          // hideLoadingBar
          cashPaymentBySupplierTable.grid.dispatch('setLoadingState', 'DONE');
        } else {
          cashPaymentBySupplierTable.grid.dispatch('setLoadingState', 'EMPTY');
        }
      },
    };
    commonUtils.callAjax(options, true);
  }
  /**
   * 초기 납부정보 세팅
   *
   * @param {Object} _gridThis
   */
  function setInitCashPaymentInfo(_gridThis) {
    const gridData = _gridThis.instance.getData();
    const { unpaidPrice, rcvPrice } = gridData.reduce((obj, { unpaidPrice, rcvPrice }) => {
      (obj['unpaidPrice'] = (parseInt(obj['unpaidPrice']) || 0) + parseInt(unpaidPrice || 0)), obj;
      (obj['rcvPrice'] = (parseInt(obj['rcvPrice']) || 0) + parseInt(rcvPrice || 0)), obj;
      return obj;
    }, {});
    $container
      .find('.total-unpaid-price')
      .text(commonUtils.setComma(unpaidPrice))
      .end()
      .find('.total-rcv-price')
      .text(commonUtils.setComma(rcvPrice));
  }

  /**
   * 납부정보 세팅
   *
   * @param _evt
   */
  function setCashPaymentInfo(_evt) {
    // 이벤트 데이터
    const { instance, value, columnName, rowKey, triggeredByKey } = _evt;

    if (commonUtils.isNumeric(value)) {
      // 선택한 Row 데이터
      const rowData = instance.getRow(rowKey);
      const { rcvPrice, cashPrice, cardPrice, salePrice, unpaidPrice } = rowData;

      // 그리드 데이터
      const gridData = instance.getData();

      const totalSalePrice = sumBy(gridData, 'salePrice');
      const totalUnpaidPrice = sumBy(gridData, 'unpaidPrice');

      // 현재 입력 금액 데이터
      const $totalUnpaidPrice = $container.find('.total-unpaid-price');
      const $paidPrice = $container.find('.paid-price');
      const $totalRcvPrice = $container.find('.total-rcv-price');
      const $returnRcvPrice = $container.find('.return-rcv-price');

      // element, value 구분
      const $totalElement = rcvPrice ? $totalRcvPrice: $totalUnpaidPrice;
      const $element = rcvPrice ? $returnRcvPrice : $paidPrice;
      const setValue = Number(cashPrice) + Number(cardPrice);
      const setTotalValue = rcvPrice
          ? (Number(rcvPrice) + totalSalePrice)
          : (totalUnpaidPrice - totalSalePrice);

      if (setValue > unpaidPrice) {
        instance.setValue(rowKey, columnName, 0);
        instance.startEditing(rowKey, columnName);
        connecteye.app.ui.alert.warning({
          text: '금액이 맞지않아요',
          icon: 'error',
        });
        return false;
      }

      // 납부한 미지급금, 받은 미수금 데이터 넣어줌
      $element.text(commonUtils.setComma(setValue));

      // 납부할 총 미지급금, 미수금 데이터 넣어줌
      $totalElement.text(commonUtils.setComma(setTotalValue));
    } else {
      connecteye.app.ui.alert.warning({
        text: '숫자만 입력해주세요.',
        icon: 'error',
      });
      if (!triggeredByKey) {
        instance.startEditing(rowKey, columnName);
      }
    }
  }

  /**
   * 금전출납 매입처별 납부하기 버튼 핸들러
   */
  function cashPaymentBySupplierHandler() {
    const $totalUnpaidPrice = $container.find('.total-unpaid-price');
    const $paidPrice = $container.find('.paid-price');
    const $totalRcvPrice = $container.find('.total-rcv-price');
    const $returnRcvPrice = $container.find('.return-rcv-price');

    const totalUnpaidPrice = Number(commonUtils.removeComma($totalUnpaidPrice.text()));
    const paidPrice = Number(commonUtils.removeComma($paidPrice.text()));
    const totalRcvPrice = Math.abs(Number(commonUtils.removeComma($totalRcvPrice.text())));
    const returnRcvPrice = Number(commonUtils.removeComma($returnRcvPrice.text()));

    // 그리드 데이터 할인금액이 null 일경우 0으로 넣어줌
    let gridData = cashPaymentBySupplierTable.grid.getData();
    gridData.forEach((item) => {
      if (!item.salePrice) {
        item.salePrice = 0;
      }
    });

    // 현금/카드 유효성 체크
    const totalCashPrice = sumBy(gridData, 'cashPrice');
    const totalCardPrice = sumBy(gridData, 'cardPrice');
    const totalPrice = totalCardPrice + totalCashPrice;

    // 현금/카드 유효성 체크
    if (!totalPrice) {
      connecteye.app.ui.alert.warning({
        text: '현금 또는 카드 정보를 입력해주세요',
      });
      return false;
    }

    if (totalUnpaidPrice !== paidPrice || totalRcvPrice !== returnRcvPrice) {
      connecteye.app.ui.alert.warning({
        text: '납부 금액이 맞지않아요',
      });
      return false;
    }
    // 납부실행
    runCashPaymentHistory();
  }

  /**
   * 금전출납 납부 등록 실행
   */
  function runCashPaymentHistory() {
    // 납부내역 등록
    connecteye.app.ui.alert
      .confirm({
        title: '납부를 실행할까요?',
        text: '취소를 누르면 내용을 수정 할 수 있어요',
        confirmBtnTxt: '추가',
        cancelBtnTxt: '취소',
        showLoaderOnConfirm: true,
        preConfirm: () => {},
      })
      .then((_result) => {
        if (_result.isConfirmed) {
          insertCashPaymentHistory();
        } else {
          connecteye.app.ui.alert.error({
            title: '납부 취소',
            text: '납부정보 수정 후 다시 추가 할 수 있어요 :)',
            icon: 'error',
          });
        }
      });
  }

  /**
   * 금전출납 납부내역 등록
   */
  function insertCashPaymentHistory() {
    const cashPaymentInfoList = cashPaymentBySupplierTable.grid.getData();

    connecteye.app.ui.showCardLoading();
    commonUtils.callAjax({
      type: 'POST',
      url: '/api/cashPaymentInfo/insertCashPaymentHistory',
      paramData: {
        cashPaymentInfoList: JSON.stringify(cashPaymentInfoList),
      },
      callback: () => {
        connecteye.app.ui.hideCardLoading();
        connecteye.app.ui.alert
          .success({
            title: '납부 완료!',
            text: '입력한 납부정보가 등록됐어요',
            showDenyButton: false,
          })
          .then((_SuccessResult) => {
            if (_SuccessResult.isConfirmed) {
              // 금전출납 목록 조회
              connecteye.app.ui.movePage('/page/supplierPurchaseInfoPage.ce');
            }
          });
      },
    });
  }

  /**
   * 초기화
   */
  function resetPage() {
    cashPaymentBySupplierTable.grid.setColumnValues('cashPrice');
    cashPaymentBySupplierTable.grid.setColumnValues('cardPrice');
    cashPaymentBySupplierTable.grid.setColumnValues('salePrice');
    $container.find('.cash-control').text('0').end().parent().scrollTop(0);
  }
  /***** Business End *****/
};
