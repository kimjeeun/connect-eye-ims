/*
 *  Description: 팝업
 *  User: kyle
 *  Date: 2022-07-05
 */
import commonUtils from '../../utils/commonUtils';
import searchBoxUtils from '../../utils/searchBoxUtils';

export default () => {
  return {
    /**
     * 고객 추가 팝업 보이기
     */
    showInsertCustInfo() {
      let inputValue = $('.select2-search__field').val();
      connecteye.app.ui.alert.custom().fire({
        html:
          `<div>고객 추가</div>` +
          `<form id="formCust">` +
          `<label for="opCustName">고객명 <span class="required-label">*</span></label>` +
          `<input id="opCustName" class="form-control" name="opCustName" placeholder="고객명" value="${inputValue || ''}" />` +
          `<label for="opTelOne">연락처1 <span class="required-label">*</span></label>` +
          `<input id="opTelOne" class="form-control" name="opTelOne" placeholder="연락처1" />` +
          `</form>`,
        focusConfirm: false,
        showCancelButton: true,
        cancelButtonText: '취소',
        confirmButtonText: '추가',
        confirmButtonColor: '#2e2e2e',
        showLoaderOnConfirm: true,
        buttonsStyling: false,
        reverseButtons: true,
        allowOutsideClick: false,
        customClass: {
          htmlContainer: 'custom-html-container',
          confirmButton: 'btn-custom-2',
          cancelButton: 'btn-custom-3 mr-2',
        },
        preConfirm: async () => {
          let isContinue = await this.isValidCustInfo();
          if (isContinue) {
            this.insertCustInfo();
          }
        },
        didRender() {
          // form
          const $form = $('#formCust');
          // 주소찾기(카카오 우편번호서비스 api)
          const daumPostcode = commonUtils.getDaumPostCode($form);
          $('.cust-search-icon').on('click', () => daumPostcode.open());
          // 유효성체크 이벤트
          $('#opTelOne, #opTelTwo').on('keyup', function () {
            $(this).val(commonUtils.setPhoneNumber(this.value));
          });
          $('#opBirthday').on('keyup', function () {
            commonUtils.setBirthdayNumber(this);
          });
        },
      });
    },

    /**
     * 고객 추가
     */
    insertCustInfo() {
      commonUtils.callAjax({
        type: 'POST',
        url: '/api/optometrySalesInfo/getInsertCustInfo',
        paramData: commonUtils.serializeObject($('#formCust')),
        callback: (_data) =>
          connecteye.app.ui.alert.success({
            title: '추가 완료!',
            text: '입력한 고객 정보가 추가됐어요',
            didDestroy: () => {
              const resultData = _data[0];
              const { custId, custName, telOne } = resultData;
              // Create a DOM Option and pre-select by default
              let newOption = new Option(custName, custId, true, true);
              // Append it to the select
              // $container.find('input[name=regSupId]').val(custId);
              $('#optometrySalesInfo')
                .find('#custTelOne')
                .val(telOne)
                .end()
                .find('#custName')
                .val(custName)
                .end()
                .find('#btnSaveCustRelation')
                .prop('disabled', false)
                .end()
                .find('#custAll')
                .append(newOption)
                .trigger('change');
            },
          }),
      });
    },

    /**
     * 고객정보 유효성체크
     */
    isValidCustInfo() {
      let isContinue = true;
      $('.custom-html-container input').each((index, item) => {
        const { id, value } = item;
        if ((id === 'opCustName' || id === 'opTelOne') && commonUtils.isEmpty(value)) {
          connecteye.app.ui.alert
            .custom()
            .showValidationMessage(`${item.placeholder}를 입력해주세요`);
          isContinue = false;
          return false;
        }
        if (
          (id === 'opTelOne' || id === 'opTelTwo') &&
          commonUtils.isNotEmpty(value) &&
          !commonUtils.isPhone(value)
        ) {
          connecteye.app.ui.alert
            .custom()
            .showValidationMessage(`010-1234-5678 형식으로 입력해주세요`);
          isContinue = false;
          return false;
        }
        if (
          id === 'opBirthday' &&
          commonUtils.isNotEmpty(value) &&
          !commonUtils.isBirthday(value)
        ) {
          connecteye.app.ui.alert
            .custom()
            .showValidationMessage(`2022-01-01 형식으로 입력해주세요`);
          isContinue = false;
          return false;
        }
        if (id === 'opEmail' && commonUtils.isNotEmpty(value) && !commonUtils.isEmail(value)) {
          connecteye.app.ui.alert
            .custom()
            .showValidationMessage(`connecteye@gamil.com 형식으로 입력해주세요`);
          isContinue = false;
          return false;
        }
      });
      return isContinue;
    },
  };
};
