/*
 *  Description: ui.js
 *  User: kyle
 *  Date: 2021-11-02
 */
import chart from '@toast-ui/chart';
import moment from 'moment';
import tippy from 'tippy.js';
import progressbar from 'progressbar.js';
import perfectscrollbar from 'perfect-scrollbar';
import commonUtils from '../../utils/commonUtils';
import alertUtils from '../../utils/alertUtils';

require('daterangepicker');
require('../../../node_modules/daterangepicker/daterangepicker.css');
require('@toast-ui/chart/dist/toastui-chart.min.css');
require('../../../node_modules/perfect-scrollbar/css/perfect-scrollbar.css');
require('../../../node_modules/tippy.js/dist/tippy.css');

export default () => {
  return {
    alert: alertUtils,
    scrollbar: null,
    progressbar: null,
    init() {
      $(document).ready(() => {
        /***** Static Start *****/
        // 로그인페이지 제외
        if (!window.location.href.includes('login') && !window.location.href.includes('optician')) {

          // 카메라 크로스브라우징
          navigator.getUserMedia =
            navigator.mediaDevices.getUserMedia ||
            navigator.getUserMedia ||
            navigator.webkitGetUserMedia ||
            navigator.mozGetUserMedia ||
            navigator.msGetUserMedia;

          // 프로그레스바
          this.progressbar = this.createProgressbar($('.progress-contents'));

          // 스크롤바
          this.scrollbar = new perfectscrollbar(document.querySelector('.main-panel .content'));

          // popstate 이벤트
          window.onpopstate = async () => {
            // 프로그레스바 초기 세팅
            const $progressContents = $('.progress-contents');
            await $progressContents.parent().fadeIn(50);
            let percent = 0.15;
            let isLoading = true;
            this.progressbar.animate(percent);

            // 프로그레스바 진행률 1초마다 5퍼센트 씩 올라감
            // isLoading false 일 경우 100퍼센트 채워진 후 화면 넘어감
            let interval = setInterval(() => {
              if (percent < 1 && isLoading) {
                percent += 0.05;
                this.progressbar.animate(percent);
              } else {
                clearInterval(interval);
                this.loadCompleteProgressbar();
              }
            }, 600);

            // 페이지 GET 요청
            commonUtils.callAjax(
              {
                type: 'GET',
                url: history.state.path,
                dataType: 'html',
                callback: (_data) => {
                  $('.main-panel .content').empty().append(_data);
                  isLoading = false;
                },
              },
              true,
            );
          };

          // chart
          this.createTestChart();

          // 프로필 이미지 있을 경우
          if (commonUtils.isNotEmpty(USER_INFO.picture)) {
            $('.avatar-img').attr('src', USER_INFO.picture);
          }
        }
        // 달력 locale 한글 세팅
        moment.locale('ko');
        /***** Static End *****/

        /***** Event Start *****/
        // 사이드메뉴 클릭 이벤트
        $('.nav-item .nav-collapse > li').on('click', function () {
          $(this)
            .parents('.nav-item')
            .addClass('active')
            .siblings('.nav-item:not(.optician)')
            .not(this)
            .removeClass('active');
          $(this).parents('.sidebar-content').find('.nav-collapse > li').removeClass('active');
          $(this).addClass('active').siblings().not(this).removeClass('active');
          $('html').removeClass('nav_open');
        });
        $('#search-nav input').on({
          focusin: function () {
            $('#search-nav').addClass('focus');
          },
          focusout: function () {
            $('#search-nav').removeClass('focus');
          },
          keydown: (_evt) => {
            if (_evt.keyCode === 13) {
              _evt.preventDefault();
              this.movePage('/page/standardInfoPage.ce');
            }
          },
        });
        $('.member-optician').on('click', () => {
          connecteye.app.ui.alert.tired();
          // location.href = '/page/opticianPage.ce';
        });
        /***** Event End *****/
      });
    },
    showCardLoading() {
      $('.main-panel .card').addClass('is-loading');
    },
    hideCardLoading() {
      $('.main-panel .card').removeClass('is-loading');
    },
    createProgressbar(_$el) {
      return new progressbar.Line(_$el[0], {
        strokeWidth: 1,
        easing: 'easeInOut',
        duration: 500,
        color: '#4b96e6',
        trailColor: '#eee',
        trailWidth: 1,
        svgStyle: { width: '100%', height: '100%' },
        from: { color: '#e53662' },
        to: { color: '#1572e8' },
        step: (state, bar) => bar.path.setAttribute('stroke', state.color),
      });
    },
    loadCompleteProgressbar() {
      const $progressContents = $('.progress-contents');
      this.progressbar.animate(1, () => {
        $progressContents.parent().fadeOut(250, () => {
          this.progressbar.set(0);
        });
      });
    },
    movePage(_navigator) {
      const pageUrl = typeof _navigator === 'object' ? $(_navigator).attr('data-url') : _navigator;
      if (pageUrl !== location.pathname) {
        // history.pushState({ path: pageUrl }, null, pageUrl);
        history.pushState({ path: pageUrl }, null);
        // event 발생시키기
        dispatchEvent(new PopStateEvent('popstate', { state: null }));
      }
    },
    createTippy(_param) {
      const { element } = _param;
      return tippy(element, _param);
    },
    createTestChart() {
      const el = document.getElementById('testChart');
      const data = {
        categories: [
          '01/01/22',
          '02/01/22',
          '03/01/22',
          '04/01/22',
          '05/01/22',
          '06/01/22',
          '07/01/22',
          '08/01/22',
          '09/01/22',
          '10/01/22',
          '11/01/22',
          '12/01/22',
        ],
        series: [
          {
            name: '입고',
            data: [4, 11, 17, 21, 25, 27, 24, 13, 6, 6, 8, 11],
          },
          {
            name: '출고',
            data: [3, 5, 7, 9, 12, 15, 17, 17, 15, 10, 6, 3],
          },
          {
            name: '주문',
            data: [22, 20, 29, 18, 15, 12, 11, 13, 15, 17, 19, 21],
          },
          {
            name: '반품',
            data: [4, 12, 16, 18, 16, 10, 4, 2, 5, 7, 8, 1],
          },
          {
            name: '위탁반품',
            data: [13, 13, 13, 10, 6, 3, 0, 1, 8, 5, 9, 10],
          },
        ],
      };
      const options = {
        chart: { title: '재고 입출고 현황', width: 'auto', height: 'auto' },
        xAxis: {
          title: '월',
        },
        yAxis: {
          title: '수량',
        },
        tooltip: {
          formatter: (value) => `${value}개`,
        },
        legend: {
          align: 'bottom',
        },
      };
      return chart.lineChart({ el, data, options });
    },
    createDatePicker(_param) {
      $('body > .daterangepicker').remove();
      const { containerEl, parentEl, singleDatePicker, startDate, ranges, callback } = _param;
      const opt = {
        parentEl: parentEl,
        opens: 'center',
        alwaysShowCalendars: true,
        singleDatePicker: singleDatePicker,
        startDate: startDate || moment().subtract(29, 'days'),
        endDate: moment(),
        locale: {
          format: 'YYYY-MM-DD',
          separator: ' ~ ',
          applyLabel: '적용',
          cancelLabel: '닫기',
          customRangeLabel: '사용자 지정',
        },
      };
      if (ranges) {
        opt.ranges = {
          오늘: [moment(), moment()],
          어제: [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          '지난 30일': [moment().subtract(29, 'days'), moment()],
          '이번 달': [moment().startOf('month'), moment().endOf('month')],
          '지난 달': [
            moment().subtract(1, 'month').startOf('month'),
            moment().subtract(1, 'month').endOf('month'),
          ],
        };
      }
      try {
        $(containerEl).daterangepicker(opt, callback);
      } catch (e) {
        console.log(e);
      }
    },
  };
};
