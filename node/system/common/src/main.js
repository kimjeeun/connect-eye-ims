/*
 *  Description: common main.js
 *  User: kyle
 *  Date: 2021-12-09
 */
require('../resources/css/reset.css');
require('../resources/css/form.css');
require('../resources/css/defaultLayout.css');
require('../../../node_modules/bootstrap/dist/css/bootstrap.min.css');

require('jquery-ui-bundle');
require('jquery-ui-touch-punch');
require('jquery.scrollbar');

window.jQuery = require('jquery');
window.$ = window.jQuery;

require('popper.js');
require('bootstrap');

import ui from './ui';
import popup from './popup';

const app = {};
app.ui = ui();
app.ui.init();
app.popup = popup();

export { app };
