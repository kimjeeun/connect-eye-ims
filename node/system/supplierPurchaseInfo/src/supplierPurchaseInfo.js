/*
 *  Description: 매입
 *  User: kyle
 *  Date: 2022-01-13
 */
import moment from 'moment';

import gridUtils from '../../utils/gridUtils';
import commonUtils from '../../utils/commonUtils';

export default async () => {
  /***** Static Start *****/
  const $container = $('#supplierPurchaseInfo');
  // table
  const supplierPurchaseInfoTable = new gridUtils.create({
    el: 'supplierPurchaseInfoTable',
    rowHeaders: [],
    bodyHeight: 'auto',
    minBodyHeight: 288,
    treeColumnOptions: {
      name: 'purId',
      useCascadingCheckbox: true,
    },
    columnOptions: {
      // 1개의 컬럼을 고정
      frozenCount: 1,
    },
    columns: [
      {
        header: '문서번호',
        name: 'purId',
        width: 240,
        ellipsis: true,
        resizable: true,
      },
      {
        header: '구매요청일',
        name: 'regDate',
        width: 100,
        align: 'center',
      },
      {
        header: '분류',
        name: 'purType',
        width: 100,
        align: 'center',
      },
      {
        header: '매입처',
        width: 100,
        name: 'supName',
        align: 'center',
        ellipsis: true,
        resizable: true,
      },
      {
        header: '상품구분',
        name: 'prdtDiv',
        width: 120,
        align: 'center',
      },
      {
        header: '브랜드',
        name: 'brnName',
        width: 200,
        ellipsis: true,
        resizable: true,
      },
      {
        header: '상품명',
        name: 'prdtName',
        ellipsis: true,
        resizable: true,
      },
      {
        header: '평균단가',
        name: 'avgPrice',
        width: 90,
        align: 'right',
        formatter: commonUtils.setComma,
      },
      {
        header: '수량',
        name: 'amount',
        width: 80,
        align: 'center',
      },
      {
        header: '금액',
        name: 'price',
        width: 90,
        align: 'right',
        formatter: commonUtils.setComma,
      },
      {
        header: '진행단계',
        name: 'progStat',
        width: 80,
        align: 'center',
        renderer: {
          type: gridUtils.purStatusRenderer,
        },
      },
      {
        header: '진행상세',
        name: 'detailStat',
        width: 180,
        align: 'center',
        renderer: {
          type: gridUtils.detailStatusRenderer,
        },
      },
    ],
    // 그리드 생성 완료 시
    onGridMounted: (_gridThis) => {
      // 그리드 레이아웃 새로고침
      _gridThis.instance.refreshLayout();
      // 초기 날짜
      setDateAndGetSupplierPurchaseInfo(moment().subtract(29, 'days'), moment());
    },
    // 그리드 업데이트 시  'resetData', 'restore', 'reloadData', 'readData', 'setPerPage'
    onGridUpdated: (_gridThis) => {
      // 그리드 레이아웃 새로고침
      _gridThis.instance.refreshLayout();
    },
  });
  // 거래기간 검색 달력
  connecteye.app.ui.createDatePicker({
    containerEl: '#supplierPurchaseInfo .btn-calendar',
    ranges: true,
    callback: setDateAndGetSupplierPurchaseInfo,
  });

  // selectbox 바인딩
  commonUtils.setSelectboxCode({ el: 'prdtDivCode', code: 'PO100' });
  commonUtils.setSelectboxCode({ el: 'purStatCode', code: 'ST100', exceptionCodeList: ['ST101'] });
  /***** Static End *****/

  /***** Event Start *****/
  // 매입입고 리스트 화면 dom 로드 시
  $container.find('.card[data-area=supPurList]').on('loadDom', (_evt, _data) => {
    if (_data && _data.listReload) {
      getSupplierPurchaseStatus();
      getSupplierPurchaseInfo();
    }
  });

  // 그리드 클릭 이벤트
  supplierPurchaseInfoTable.grid.on('click', (_evt) => {
    const { rowKey, instance, targetType } = _evt;
    const rowData = instance.getRow(rowKey);
    if (
      commonUtils.isNotEmpty(instance.getChildRows(rowKey)) &&
      targetType !== 'columnHeader' &&
      targetType !== 'rowHeader' &&
      targetType !== 'etc'
    ) {
      const $target = $container.find('.card[data-area=purReg]');
      $target.removeClass('d-none').siblings('.card').addClass('d-none').end().trigger('loadDom', {
        rowData: rowData,
      });
    }
  });

  // 검색 버튼 이벤트
  $container.find('.btn-select').on('click', () => {
    getSupplierPurchaseStatus();
    getSupplierPurchaseInfo();
  });
  $container.find('.search-group').on('keyup', (_evt) => {
    if (_evt.keyCode === 13) {
      getSupplierPurchaseStatus();
      getSupplierPurchaseInfo();
    }
  });

  //
  $container.find('.btn-calendar').on('click', function () {});

  // 전체열기
  $container.find('.btn-expand-all').on('click', () => supplierPurchaseInfoTable.grid.expandAll());

  // 전체닫기
  $container
    .find('.btn-collapse-all')
    .on('click', () => supplierPurchaseInfoTable.grid.collapseAll());

  // 거래내역추가 이벤트
  $container.find('.btn-reg-pur').on('click', (_evt) => getArea(_evt));

  // 이전 버튼 이벤트
  $container.find('.btn-prev').on('click', (_evt) => getArea(_evt));
  /***** Event End *****/

  /***** Business Start *****/
  /**
   * 매입 입고 목록 가져오기
   */
  function getSupplierPurchaseInfo() {
    const $form = $container.find('.search-group');
    const formSerializeData = commonUtils.serializeObject($form);
    // 그리드 로딩바
    supplierPurchaseInfoTable.grid.dispatch('setLoadingState', 'LOADING');
    let options = {
      type: 'GET',
      url: '/api/supplierPurchaseInfo/getSupplierPurchaseInfo',
      paramData: formSerializeData || null,
      callback: (_res) => {
        const totalCount = _res.length;
        // 데이터 건수
        $container.find('.result-cnt').text(commonUtils.setComma(totalCount));
        if (commonUtils.isNotEmpty(_res)) {
          // 그리드에 데이터 넣음
          supplierPurchaseInfoTable.grid.resetData(_res);
          // hideLoadingBar
          supplierPurchaseInfoTable.grid.dispatch('setLoadingState', 'DONE');
        } else {
          supplierPurchaseInfoTable.grid.dispatch('setLoadingState', 'EMPTY');
        }
      },
    };
    commonUtils.callAjax(options, true);
  }
  /**
   * 매입입고 현황 가져오기
   */
  function getSupplierPurchaseStatus() {
    const $form = $container.find('.search-group');
    const formSerializeData = commonUtils.serializeObject($form);
    let options = {
      type: 'GET',
      url: '/api/supplierPurchaseInfo/getSupplierPurchaseStatus',
      paramData: formSerializeData || null,
      callback: (_res) => {
        const {
          purCnt1,
          purCnt2,
          purCnt3,
          purCnt4,
          purSum1,
          purSum2,
          purSum3,
          purSum4,
          stadyCnt,
          cancelCnt,
          purCnt,
          completeCnt,
        } = _res[0];
        $container
          .find('.pur-cnt1')
          .text(`(${purCnt1})`)
          .end()
          .find('.pur-cnt2')
          .text(`(${purCnt2})`)
          .end()
          .find('.pur-cnt3')
          .text(`(${purCnt3})`)
          .end()
          .find('.pur-cnt4')
          .text(`(${purCnt4})`)
          .end()
          .find('.pur-sum1')
          .text(commonUtils.setComma(purSum1))
          .end()
          .find('.pur-sum2')
          .text(commonUtils.setComma(purSum2))
          .end()
          .find('.pur-sum3')
          .text(commonUtils.setComma(purSum3))
          .end()
          .find('.pur-sum4')
          .text(commonUtils.setComma(purSum4))
          .end()
          .find('.complete-cnt')
          .text(completeCnt)
          .end()
          .find('.stady-cnt')
          .text(stadyCnt)
          .end()
          .find('.cancel-cnt')
          .text(cancelCnt)
          .end()
          .find('.pur-cnt')
          .text(purCnt)
          .end();
      },
    };
    commonUtils.callAjax(options, true);
  }
  /**
   * 날짜세팅 후 매입입고 정보 가져오기
   *
   * @param {Object} _start
   * @param {Object} _end
   */
  function setDateAndGetSupplierPurchaseInfo(_start, _end) {
    const startDate = _start.format('YYYY-MM-DD');
    const endDate = _end.format('YYYY-MM-DD');
    const searchPurDate = `${startDate} ~ ${endDate}`;
    $container.find('.search-pur-date').text(searchPurDate);
    $container.find('#startDate').val(startDate);
    $container.find('#endDate').val(endDate);
    getSupplierPurchaseStatus();
    getSupplierPurchaseInfo();
  }

  /**
   * 영역 가져오기
   *
   * @param {Object} _evt
   */
  async function getArea(_evt) {
    const $el = $(_evt.currentTarget);
    const target = $el.data('target');
    const $parentArea = $container.find(`.page-inner .card[data-area=${target}]`);

    // 자신을 제외하고 d-none 클래스 넣어줌
    await $parentArea
      .removeClass('d-none')
      .siblings('.card')
      .addClass('d-none')
      .end()
      .trigger('loadDom');

    switch (target) {
      case 'supPurList':
        getSupplierPurchaseInfo();
        getSupplierPurchaseStatus();
        break;
      default:
        break;
    }

    $container.parent().scrollTop(0);
    supplierPurchaseInfoTable.grid.refreshLayout();
  }
  /***** Business End *****/
};
