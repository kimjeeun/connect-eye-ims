/*
 *  Description: 매입 입고등록
 *  User: kyle
 *  Date: 2022-03-16
 */
import moment from 'moment';
import { sumBy } from 'lodash-es';

import gridUtils from '../../utils/gridUtils';
import commonUtils from '../../utils/commonUtils';

export default () => {
  /***** Static Start *****/
  const $container = $('#supplierPurchaseInfo');
  // table
  const purRegTable = new gridUtils.create({
    el: 'purRegTable',
    rowHeaders: [],
    bodyHeight: 'auto',
    minBodyHeight: 80,
    treeColumnOptions: {
      name: 'regDate',
    },
    columns: [
      {
        header: '구매요청일',
        name: 'regDate',
        width: 160,
        align: 'center',
      },
      {
        header: '분류',
        name: 'purType',
        width: 70,
        align: 'center',
      },
      {
        header: '매입처',
        width: 90,
        name: 'supName',
        align: 'center',
        ellipsis: true,
        resizable: true,
      },
      {
        header: '상품구분',
        name: 'prdtDiv',
        width: 90,
        align: 'center',
      },
      {
        header: '브랜드',
        name: 'brnName',
        width: 90,
        ellipsis: true,
        resizable: true,
      },
      {
        header: '상품명',
        name: 'prdtName',
        ellipsis: true,
        resizable: true,
      },
      {
        header: '평균단가',
        name: 'avgPrice',
        width: 90,
        align: 'right',
        formatter: commonUtils.setComma,
      },
      {
        header: '수량',
        name: 'amount',
        width: 70,
        align: 'center',
      },
      {
        header: '금액',
        name: 'price',
        width: 90,
        align: 'right',
        formatter: commonUtils.setComma,
      },
      {
        header: '진행단계',
        name: 'progStat',
        width: 70,
        align: 'center',
        renderer: {
          type: gridUtils.purStatusRenderer,
        },
      },
      {
        header: '진행상세',
        name: 'detailStat',
        width: 180,
        align: 'center',
        renderer: {
          type: gridUtils.detailStatusRenderer,
        },
      },
    ],
    // 그리드 생성 완료 시
    onGridMounted: (_gridThis) => {},
    // 그리드 업데이트 시  'resetData', 'restore', 'reloadData', 'readData', 'setPerPage'
    onGridUpdated: (_gridThis) => {
      // 그리드 레이아웃 새로고침
      _gridThis.instance.refreshLayout();
    },
  });
  // table
  const progStatModifyTable = new gridUtils.create({
    el: 'progStatModifyTable',
    rowHeaders: [],
    bodyHeight: 'auto',
    minBodyHeight: 194,
    scrollX: 'false',
    columns: [
      {
        header: '',
        name: '',
        width: 20,
        align: 'center',
        renderer: {
          type: gridUtils.deleteButtonRenderer,
          options: {
            isAmount: true,
            $currentAmount: $container.find('.current-amount'),
          },
        },
      },
      {
        header: '상태',
        name: 'typeCode',
        align: 'center',
        formatter: 'listItemText',
        copyOptions: {
          useListItemText: true,
        },
        editor: {
          type: 'select',
          options: {
            instantApply: true,
            listItems: gridUtils.setSelectboxCode('ST110', ['ST111']),
          },
        },
      },
      {
        header: '수량',
        name: 'amount',
        align: 'center',
        editor: 'text',
      },
      {
        header: '입고일',
        name: 'wrngDate',
        align: 'center',
        editor: {
          type: 'datePicker',
          options: {
            language: 'ko',
            format: 'yyyy-MM-dd',
            instantApply: true,
          },
        },
      },
    ],
    // 그리드 생성 완료 시
    onGridMounted: (_gridThis) => {},
    // 그리드 업데이트 시  'resetData', 'restore', 'reloadData', 'readData', 'setPerPage'
    onGridUpdated: (_gridThis) => {
      // 그리드 레이아웃 새로고침
      _gridThis.instance.refreshLayout();
    },
  });
  // table
  const purHistoryTable = new gridUtils.create({
    el: 'purHistoryTable',
    rowHeaders: [],
    minBodyHeight: 180,
    scrollX: 'false',
    columns: [
      {
        header: '',
        name: '',
        width: 20,
        align: 'center',
        renderer: {
          type: gridUtils.deleteButtonRenderer,
          options: {
            isAmount: false,
            $currentAmount: '',
            callback: runDeletePurchaseHistory,
          },
        },
      },
      {
        header: '입고일',
        name: 'wrngDate',
        align: 'center',
        sortable: true,
        sortingType: 'desc',
      },
      {
        header: '담당자',
        name: 'userName',
        align: 'center',
      },
      {
        header: '수량',
        name: 'amount',
        align: 'center',
      },
      {
        header: '금액',
        name: 'price',
        align: 'center',
        formatter: commonUtils.setComma,
      },
    ],
    // 그리드 생성 완료 시
    onGridMounted: (_gridThis) => {},
    // 그리드 업데이트 시  'resetData', 'restore', 'reloadData', 'readData', 'setPerPage'
    onGridUpdated: (_gridThis) => {
      // 그리드 레이아웃 새로고침
      _gridThis.instance.refreshLayout();
    },
  });
  // table
  const cancelHistoryTable = new gridUtils.create({
    el: 'cancelHistoryTable',
    rowHeaders: [],
    minBodyHeight: 180,
    scrollX: 'false',
    columns: [
      {
        header: '취소일',
        name: 'wrngDate',
        align: 'center',
        sortable: true,
        sortingType: 'desc',
      },
      {
        header: '담당자',
        name: 'userName',
        align: 'center',
      },
      {
        header: '수량',
        name: 'amount',
        align: 'center',
      },
      {
        header: '금액',
        name: 'price',
        align: 'center',
        formatter: commonUtils.setComma,
      },
    ],
    // 그리드 생성 완료 시
    onGridMounted: (_gridThis) => {},
    // 그리드 업데이트 시  'resetData', 'restore', 'reloadData', 'readData', 'setPerPage'
    onGridUpdated: (_gridThis) => {
      // 그리드 레이아웃 새로고침
      _gridThis.instance.refreshLayout();
    },
  });
  /***** Static End *****/

  /***** Event Start *****/
  // 매입입고 리스트 화면 dom 로드 시
  $container.find('.card[data-area=purReg]').on('loadDom', async (_evt, _data) => {
    const { rowData } = _data;
    const { purId } = rowData;

    // 입고등록 목록 테이블
    $container.find('#initPurId').val(purId);
    getSupplierPurchaseInfo();

    // 입고등록 정보수정 테이블
    progStatModifyTable.grid.refreshLayout();

    // 입고이력 테이블
    purHistoryTable.grid.refreshLayout();

    // 취소이력 테이블
    cancelHistoryTable.grid.refreshLayout();

    // 초기화
    resetPage();
  });

  // 진행 업데이트 추가 버튼 클릭 이벤트
  $container.find('#btnProgUpdate').on('click', () => processUpdateValidation());

  // 진행 업데이트 테이블 편집끝날 시점 이벤트
  progStatModifyTable.grid.on('editingFinish', async (_evt) => {
    const { rowKey, columnName, instance, value, triggeredByKey } = _evt;
    const { amount } = await getTotalAmount(instance, true);
    const columnAmount = parseInt(value);
    const stadyCnt = parseInt($container.find('.reg-stady-cnt').text());

    // 사용자가 업데이트하려는 수량이 대기수량보다 많을 경우를 비교할 총 수량 비교
    if (columnAmount === 0 || amount > stadyCnt) {
      connecteye.app.ui.alert.warning({
        text: `수량 ${stadyCnt}건 내 업데이트 가능해요`,
      });
      if (!triggeredByKey) {
        instance.startEditing(rowKey, columnName);
      }
    }
  });

  // 발주정보 테이블 클릭 이벤트
  purRegTable.grid.on('click', () => {
    // 진행내용 정보 세팅
    setProgStatusInfo();
  });

  // 등록 버튼 클릭 이벤트
  $container.find('#btnPurReg').on('click', () => runInsert());

  // 초기화
  $container.find('#btnPurRegReset').on('click', () => resetPage());
  /***** Event End *****/

  /***** Business Start *****/
  /**
   * 매입 입고 목록 가져오기
   */
  function getSupplierPurchaseInfo() {
    const grid = purRegTable.grid;
    const purId = $container.find('#initPurId').val();

    // 그리드 로딩바
    grid.dispatch('setLoadingState', 'LOADING');
    let options = {
      type: 'GET',
      url: '/api/supplierPurchaseInfo/getSupplierPurchaseInfo',
      paramData: { purId } || null,
      callback: async (_res) => {
        if (commonUtils.isNotEmpty(_res)) {
          await grid.resetData(_res);
          await grid.expandAll();
          // hideLoadingBar
          await grid.dispatch('setLoadingState', 'DONE');
        } else {
          grid.dispatch('setLoadingState', 'EMPTY');
        }
      },
    };
    commonUtils.callAjax(options, true);
  }

  /**
   * 사용자가 업데이트하려는 수량이 대기수량보다 많을 경우를 비교할 총 수량 가져오는 obj
   *
   * @param {Object} _grid
   * @param {boolean} _isEditing
   */
  async function getTotalAmount(_grid, _isEditing) {
    return await _grid.getData().reduce((obj, { amount }) => {
      (obj['amount'] =
        (parseInt(obj['amount']) || 0) + parseInt(amount || 0) + (_isEditing ? 0 : 1)),
        obj;
      return obj;
    }, {});
  }

  /**
   * 입고등록 시작
   */
  function runInsert() {
    // form 전송 ajax 처리
    connecteye.app.ui.alert
      .confirm({
        title: '입고 등록할까요?',
        text: '취소를 누르면 내용을 수정 할 수 있어요',
        confirmBtnTxt: '등록',
        cancelBtnTxt: '취소',
        showLoaderOnConfirm: true,
        preConfirm: () => {},
      })
      .then((_result) => {
        if (_result.isConfirmed) {
          insertPurchaseRegistInfo();
        } else {
          connecteye.app.ui.alert.error({
            title: '등록 취소',
            text: '진행내용 수정 후 다시 추가 할 수 있어요 :)',
            icon: 'error',
          });
        }
      });
  }

  /**
   * 입고등록 추가 ajax
   */
  async function insertPurchaseRegistInfo() {
    // 그리드 편집모드 해제
    progStatModifyTable.grid.finishEditing();

    // 거래내역 데이터 정의
    const purchaseData = progStatModifyTable.grid.getData();
    const { rowKey } = purRegTable.grid.getFocusedCell();
    const purRegTableData = await purRegTable.grid.getRow(rowKey);
    const { purCnt, cancelCnt, stadyCnt } = purRegTableData;

    connecteye.app.ui.showCardLoading();
    commonUtils.callAjax({
      type: 'POST',
      url: '/api/supplierPurchaseInfo/insertPurchaseRegistInfo',
      paramData: {
        purId: $container.find('#purId').val(),
        supPrdtId: $container.find('#purSupPrdtId').val(),
        purTypeCode: $container.find('#purTypeCode').val(),
        purRightRegCheck: null,
        purCnt: purCnt,
        cancelCnt: cancelCnt,
        stadyCnt: stadyCnt,
        unitPrice: commonUtils.removeComma($container.find('#purUnitPrice').val()),
        purchaseData: JSON.stringify(purchaseData),
      },
      callback: () => {
        connecteye.app.ui.hideCardLoading();
        connecteye.app.ui.alert
          .success({
            title: '등록 완료!',
            text: '입력한 입고 진행내용이 등록됐어요',
            denyButtonText: '계속 추가하기',
            showDenyButton: true,
          })
          .then((_SuccessResult) => {
            if (_SuccessResult.isConfirmed) {
              // 매입입고 목록 조회
              showSupplierPurchaseInfoList();
              // 초기화
              resetPage();
            } else if (_SuccessResult.isDenied) {
              //초기화
              resetPage();
              // 매입입고 정보 가져오기
              getSupplierPurchaseInfo();
            }
          });
      },
    });
  }

  /**
   * 매입입고 목록 보여주기
   */
  function showSupplierPurchaseInfoList() {
    $container.find('.btn-prev').trigger('click');
    $container.find('.card[data-area=supPurList]').trigger('loadDom', { listReload: true });
  }

  /**
   * 매입 입고이력 삭제 실행
   *
   * @return {Object} _data
   */
  function runDeletePurchaseHistory(_data) {
    // 입고이력 데이터
    const { amount } = _data;
    const purchaseData = {
      purId: $container.find('#purId').val(),
      supPrdtId: $container.find('#purSupPrdtId').val(),
      unitPrice: commonUtils.removeComma($container.find('#purUnitPrice').val()),
      stadyCnt: Number($container.find('#modStadyCnt').val()) + amount,
      purCnt: Number($container.find('#modPurCnt').val()) - amount,
    };

    // 오브젝트 합쳐줌
    const paramData = Object.assign(_data, purchaseData);

    connecteye.app.ui.alert
      .confirm({
        title: '입고 이력을 삭제를 실행할까요?',
        text: '취소를 누르면 내용을 이전 화면으로 되돌아가요',
        confirmBtnTxt: '실행',
        cancelBtnTxt: '취소',
        showLoaderOnConfirm: true,
        preConfirm: () => {},
      })
      .then((_result) => {
        if (_result.isConfirmed) {
          deletePurchaseHistory(paramData);
        }
      });
  }

  /**
   * 매입 입고이력 삭제
   *
   * @return {Object} _data
   */
  function deletePurchaseHistory(_data) {
    connecteye.app.ui.showCardLoading();
    commonUtils.callAjax({
      type: 'POST',
      url: '/api/supplierPurchaseInfo/deletePurchaseHistory',
      paramData: _data,
      callback: () => {
        const { rowKey } = _data;
        purHistoryTable.grid.removeRow(rowKey);
        connecteye.app.ui.hideCardLoading();
        connecteye.app.ui.alert
          .success({
            title: '입고 이력 삭제 완료!',
            text: '선택한 입고 이력이 삭제됐어요',
            showDenyButton: false,
          })
          .then((_SuccessResult) => {
            if (_SuccessResult.isConfirmed) {
              //초기화
              resetPage();
              // 매입입고 정보 가져오기
              getSupplierPurchaseInfo();
            }
          });
      },
    });
  }


  /**
   * 입고/취소이력 목록 가져오기
   *
   * @param {Object} _data
   * @param {String} _typeCode
   */
  function getHistoryInfo(_data, _typeCode) {
    let grid_;
    let $resultCnt_;
    const { purId, supPrdtId } = _data;
    switch (_typeCode) {
      // 입고
      case 'ST112':
        grid_ = purHistoryTable.grid;
        $resultCnt_ = $container.find('.pur-history-cnt');
        break;
      // 취소
      case 'ST113':
        grid_ = cancelHistoryTable.grid;
        $resultCnt_ = $container.find('.cancel-history-cnt');
        break;
    }
    let options = {
      type: 'GET',
      url: '/api/supplierPurchaseInfo/getHistoryInfo',
      paramData: {
        purId: purId,
        supPrdtId: supPrdtId,
        typeCode: _typeCode,
      },
      callback: (_res) => {
        if (commonUtils.isNotEmpty(_res)) {
          const totalCount = _res.length;
          // 데이터 건수
          $resultCnt_.text(commonUtils.setComma(totalCount));
          // 그리드에 데이터 넣음
          grid_.resetData(_res);
        } else {
          grid_.dispatch('setLoadingState', 'EMPTY');
          // 건수 초기화
          $resultCnt_.text('0');
        }
      },
    };
    commonUtils.callAjax(options, true);
  }

  /**
   * 진행상태 업데이트 추가 유효성체크
   */
  async function processUpdateValidation() {
    const { grid } = progStatModifyTable;
    const amount = sumBy(grid.getData(), 'amount');
    const supPrdtId = $container.find('#purSupPrdtId').val();
    const stadyCnt = parseInt($container.find('.reg-stady-cnt').text());
    const purId = $container.find('#purId').val();
    const currentDate = moment().format('yyyy-MM-DD');

    // purId 있을 경우 넘어감
    if (commonUtils.isEmpty(purId)) {
      connecteye.app.ui.alert.warning({
        text: '발주 정보에서 상품 선택 후 추가해주세요.',
      });
      return false;
    }

    // 진행가능한 수량체크
    if (!parseInt(stadyCnt)) {
      connecteye.app.ui.alert.warning({
        text: '업데이트 가능한 수량이 없어요',
      });
      return false;
    }

    // 사용자가 업데이트하려는 수량이 대기수량보다 많을 경우를 비교할 총 수량 비교
    if (amount >= stadyCnt) {
      connecteye.app.ui.alert.warning({
        text: `수량 ${stadyCnt}건 내 업데이트 가능해요`,
      });
      return false;
    }

    // 그리드에 데이터 넣어줌
    grid.appendRow({
      supPrdtId: supPrdtId,
      typeCode: 'ST112',
      amount: 1,
      wrngDate: currentDate,
    });
  }

  /**
   * 진행내용 정보 세팅
   */
  function setProgStatusInfo() {
    const grid = purRegTable.grid;
    const modifyGrid = progStatModifyTable.grid;
    const { rowKey } = grid.getFocusedCell();
    const rowData = grid.getRow(rowKey);
    const {
      supName,
      prdtDiv,
      brnName,
      prdtName,
      unitPrice,
      stadyCnt,
      purCnt,
      purId,
      purTypeCode,
      supPrdtId,
    } = rowData;

    // 그리드 트리구조 부모를 제외한 자식들만
    if (grid.getDepth(rowKey) > 1) {
      $container.find('#purId').val(purId);
      $container.find('#purTypeCode').val(purTypeCode);
      $container.find('#purSupPrdtId').val(supPrdtId);
      $container.find('#purSupName').val(supName);
      $container.find('#purPrdtDiv').val(prdtDiv);
      $container.find('#purBrnName').val(brnName);
      $container.find('#purPrdtName').val(prdtName);
      $container.find('#purUnitPrice').val(commonUtils.setComma(unitPrice));
      $container.find('#modStadyCnt').val(stadyCnt);
      $container.find('#modPurCnt').val(purCnt);
      $container.find('.reg-stady-cnt').text(stadyCnt);

      modifyGrid.clear();

      if (stadyCnt) {
        modifyGrid.appendRow({
          supPrdtId: supPrdtId,
          typeCode: 'ST112',
          amount: stadyCnt,
          wrngDate: moment().format('yyyy-MM-DD')
        });
      }

      getHistoryInfo(rowData, 'ST112');
      getHistoryInfo(rowData, 'ST113');
    }

    // 그리드 ROW 선택
    gridUtils.setSelectionRange(grid, rowKey);
  }

  /**
   * 초기화
   */
  async function resetPage() {
    const $formPurReg = $('#formPurReg');
    commonUtils.formReset($formPurReg.find('input'));
    $container
      .find('.reg-stady-cnt')
      .text('0')
      .end()
      .find('.pur-history-cnt')
      .text('0')
      .end()
      .find('.cancel-history-cnt')
      .text('0');
    progStatModifyTable.grid.clear();
    purHistoryTable.grid.clear();
    cancelHistoryTable.grid.clear();
    $container.parent().scrollTop(0);
  }
  /***** Business End *****/
};
