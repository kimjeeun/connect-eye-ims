/*
 *  Description: 매입 거래내역 추가
 *  User: kyle
 *  Date: 2022-02-10
 */
import gridUtils from '../../utils/gridUtils';
import commonUtils from '../../utils/commonUtils';
import searchBoxUtils from '../../utils/searchBoxUtils';
import barcodeUtils from '../../utils/barcodeUtils';

export default () => {
  /***** Static Start *****/
  const $container = $('#supplierPurchaseInfo');

  // 바코드
  barcodeUtils.init({
    $inputElement: $container.find('#barcodeSearch'),
    $scanButtonElement: $container.find('#btnBarcodeScan'),
    callback: getStandardInfoByBarcode,
  });

  // table
  const supPurRegTable = new gridUtils.create({
    el: 'supPurRegTable',
    bodyHeight: 520,
    rowHeaders: [],
    scrollX: 'false',
    scrollY: true,
    columns: [
      {
        header: '',
        name: '',
        width: 20,
        align: 'center',
        renderer: {
          type: gridUtils.deleteButtonRenderer,
          options: {
            isAmount: true,
            $currentAmount: $container.find('.current-amount'),
          },
        },
      },
      {
        header: '사진',
        name: 'previewImgPath',
        width: 60,
        renderer: { type: gridUtils.setImageRenderer },
        align: 'center',
      },
      {
        header: '상품구분',
        name: 'prdtDiv',
        width: 90,
        align: 'center',
      },
      {
        header: '상품명',
        name: 'prdtName',
        ellipsis: true,
      },
      {
        header: '매입단가',
        name: 'unitPrice',
        width: 90,
        align: 'center',
        formatter: commonUtils.setComma,
      },
      {
        header: '수량',
        name: 'amount',
        width: 80,
        align: 'center',
      },
      {
        header: '금액',
        name: 'price',
        width: 90,
        align: 'center',
        formatter: commonUtils.setComma,
      },
    ],
    summary: {
      height: 40,
      position: 'bottom',
      columnContent: {
        amount: {
          template(valueMap) {
            return `총 ${commonUtils.setComma(valueMap.sum)}개 `;
          },
        },
        price: {
          template(valueMap) {
            return `${commonUtils.setComma(valueMap.sum)}원`;
          },
        },
      },
    },

    // 그리드 생성 완료 시
    onGridMounted: (_gridThis) => {
      // 그리드 레이아웃 새로고침
      _gridThis.instance.refreshLayout();
    },

    // 그리드 업데이트 시  'resetData', 'restore', 'reloadData', 'readData', 'setPerPage'
    onGridUpdated: (_gridThis) => {
      // 그리드 레이아웃 새로고침
      _gridThis.instance.refreshLayout();

      // hideLoadingBar
      _gridThis.instance.dispatch('setLoadingState', 'DONE');
    },
  });

  // 매입처 검색박스 적용
  searchBoxUtils.create({
    $el: $container.find('#regSupName'),
    title: '매입처',
    placeholder: '매입처',
    ajax: {
      url: '/api/standardInfo/getSupplierList',
      data: (params) => ({
        supName: params.term,
        orderBy: 'sup_name desc',
      }),
      processResults: (_result) => {
        let result = searchBoxUtils.getResultCallback(_result);
        return {
          results: result.map((obj) => ({ id: obj.supId, text: obj.supName, data: obj })),
        };
      },
      cache: false,
    },
    noAdd: true,
    noResultCallback: () => {},
  });

  // 브랜드 검색박스 적용
  searchBoxUtils.create({
    $el: $container.find('#regBrnName'),
    title: '브랜드',
    placeholder: '브랜드',
    ajax: {
      url: '/api/standardInfo/getBrandList',
      data: (params) => ({
        supId: $container.find('input[name=regSupId]').val(),
        brnName: params.term,
        orderBy: 'brn_name desc',
      }),
      processResults: (res) => {
        let result = searchBoxUtils.getResultCallback(res);
        return {
          results: result.map((obj) => ({ id: obj.brnId, text: obj.brnName, data: obj })),
        };
      },
      cache: false,
    },
    noAdd: true,
    noResultCallback: () => {},
  });

  // 상품 검색박스 적용, 무한 스크롤 적용
  searchBoxUtils.create({
    $el: $container.find('#regPrdtName'),
    title: '상품',
    placeholder: '상품',
    ajax: {
      url: '/api/standardInfo/getStandardInfoSearchBoxProductList',
      data: (params) => ({
        supId: $('#regSupId').val(),
        brnId: $('#regBrnId').val(),
        orderBy: 'prdt_name desc',
        prdtName: params.term,
        page: params.page || 1,
        perPage: 10,
        isUse: 1,
      }),
      processResults: (res) => {
        let result = searchBoxUtils.getResultCallback(res);
        const { totalCount, page } = res;
        return {
          results: result.map((obj) => ({
            id: obj.supPrdtId,
            text: obj.prdtName,
            data: obj,
          })),
          pagination: {
            more: (page * 10) < totalCount,
          },
        };
      },
      cache: true,
    },
    noAdd: true,
    noResultCallback: () => {},
  });
  /***** Static End *****/

  /***** Event Start *****/
  // 매입입고 리스트 화면 dom 로드 시
  $container.find('.card[data-area=supPurReg]').on('loadDom', (_evt, _data) => {
    supPurRegTable.grid.refreshLayout();
  });

  // 거래내역 정보 입력 후 적용 버튼 클릭 시 이벤트
  $container.find('.btn-save').on('click', () => saveSupplierPurchaseInfo());

  // 거래내역 분류 Radio 버튼 변경 이벤트
  $container.find('input[name=purStat]').on('change', (_evt) => {
    const $purRightRegCheck = $container.find('#purRightRegCheck');
    const currentValue = $(_evt.currentTarget).val();
    switch (currentValue) {
      case 'BY103':
      case 'BY104':
        $purRightRegCheck.prop('disabled', true).prop('checked', false);
        break;
      default:
        $purRightRegCheck.prop('disabled', false).prop('checked', true);
        break;
    }
  });

  // 단가 이벤트
  $container.find('#regUnitPrice').on({
    keyup: commonUtils.setPriceEventHandler,
    change: commonUtils.setPriceEventHandler,
  });
  $container.find('#regPrice').on({
    keyup: commonUtils.setPriceEventHandler,
    change: commonUtils.setPriceEventHandler,
  });

  // 그리드 사진클릭 이벤트
  supPurRegTable.grid.on('click', (_evt) => gridUtils.setSelectPictureHandler(_evt, 'previewImgPath'));

  // 초기화
  $container.find('#btnSupPurRegReset').on('click', () => resetPage());
  $container.find('.btn-prev').on('click', () => resetPage());

  // 매입처 선택 이벤트
  $container.find('#regSupName').on('change', (_evt) => {
    const resultObj = searchBoxUtils.getResultObject($(_evt.currentTarget));
    const purDocNumber = $container.find('.pur-doc-number').text();
    if (resultObj) {
      const { supId, supName } = resultObj;
      // 매입처 아이디
      $container.find('input[name=regSupId]').val(supId);
      // 브랜드 활성화
      $container.find('select[name=regBrnName]').prop('disabled', false);

      if (commonUtils.isNotEmpty(purDocNumber) && !purDocNumber.includes(supName)) {
        // 상품정보 초기화
        resetPrdtInfo();
      } else {
        selectEventHandler();
      }
    }
  });

  // 브랜드 선택 이벤트
  $container.find('#regBrnName').on('change', (_evt) => {
    let resultObj = searchBoxUtils.getResultObject($(_evt.currentTarget));
    if (resultObj) {
      // 브랜드 아이디
      $container.find('input[name=regBrnId]').val(resultObj.brnId);
    }
    selectEventHandler();
  });

  // 상품 선택 이벤트
  $container.find('#regPrdtName').on('change', (_evt) => {
    const resultObj = searchBoxUtils.getResultObject($(_evt.currentTarget));
    if (resultObj) {
      const { supPrdtId, barcode, purPrice, salesPrice } = resultObj;
      // 기준정보 아이디
      $container.find('input[name=regSupPrdtId]').val(supPrdtId);
      // 바코드
      $container.find('input[name=regBarcode]').val(barcode);
      // 단가
      $container.find('input[name=regSalesPrice]').val(commonUtils.setComma(salesPrice));
      // 판매단가
      $container.find('input[name=regUnitPrice]').val(commonUtils.setComma(purPrice));
      // 수량
      $container.find('input[name=regAmount]').val(1);
      // 금액
      $container.find('input[name=regPrice]').val(commonUtils.setComma(purPrice));
    }
    selectEventHandler();
  });

  // 거래내역 추가 버튼 클릭 이벤트
  $container.find('#btnSupPurReg').on('click', () =>
    commonUtils.isNotEmpty(supPurRegTable.grid.getData()) &&
    $container.find('input[name=purStat]:checked').length
      ? runInsert()
      : connecteye.app.ui.alert.warning({
          text: '거래내역 정보 입력 후 추가해주세요',
        }),
  );

  // 바코드로 기준정보 검색 버튼 이벤트
  $container.find('#formSupPur .search-icon').on('click', () => {
    // 바코드
    const barcode = $container.find('#barcodeSearch').val();

    // 바코드로 기준정보 가져오기
    getStandardInfoByBarcode({ barcode: barcode });
  });
  $container.find('#barcodeSearch').on('keyup', (_evt) => {
    if (_evt.keyCode === 13) {
      // 바코드
      const barcode = $(_evt.currentTarget).val();

      // 바코드로 기준정보 가져오기
      getStandardInfoByBarcode({ barcode: barcode });
    }
  });

  // 단가, 수량 입력 이벤트
  $container.find('#regUnitPrice').on('keyup', setInputChangeEventTrigger);
  $container.find('#regAmount').on('keyup', setInputChangeEventTrigger);
  /***** Event End *****/

  /***** Business Start *****/
  /**
   * 선택 이벤트 핸들러
   */
  function selectEventHandler() {
    const supId = $('#regSupId').val();
    const brnId = $('#regBrnId').val();
    if (commonUtils.isEmpty(supId) || commonUtils.isEmpty(brnId)) {
      return false;
    }
    $container
      .find('.product-control:not(#regBarcode):not(#regSalesPrice)')
      .prop('disabled', false);
  }

  /**
   * 거래내역에 상품 정보 적용
   */
  function saveSupplierPurchaseInfo() {
    if (addRegData()) {
      // return str 220321-다스-001
      getPurDocumentName();
    }
    // 상품정보 초기화
    resetPrdtInfoWithBrand();
  }

  /**
   * 바코드로 기준정보 가져오기
   *
   * @param {Object} _param
   */
  function getStandardInfoByBarcode(_param) {
    commonUtils.callAjax({
      type: 'GET',
      url: '/api/standardInfo/getStandardInfoProductList',
      paramData: _param,
      callback: (_res) => {
        if (commonUtils.isNotEmpty(_res)) {
          // 기준정보 필드 세팅
          setStandardInfo(_res);

          // 매입처, 브랜드 셀렉트박스 이벤트
          selectEventHandler();

          // 즉시적용 체크 시
          const barcodeRight = $container.find('input[name=barcodeRight]').is(':checked');
          if (barcodeRight) {
            saveSupplierPurchaseInfo();
          }
        } else {
          connecteye.app.ui.alert.warning({
            text: '해당 바코드로 등록된 기준정보가 없어요',
          });
        }
      },
    });
  }

  /**
   * 기준정보 필드 세팅
   *
   * @param {Object} _res
   */
  function setStandardInfo(_res) {
    const {
      supId,
      supName,
      prdtId,
      prdtName,
      brnId,
      brnName,
      supPrdtId,
      barcode,
      purPrice,
      salesPrice,
    } = _res[0];

    const supOption = $('<option/>', {
      text: supName,
      value: supId,
      'data-result': JSON.stringify(_res[0]),
    });

    const prdtOption = $('<option/>', {
      text: prdtName,
      value: prdtId,
      'data-result': JSON.stringify(_res[0]),
    });

    const brnOption = $('<option/>', {
      text: brnName,
      value: brnId,
      'data-result': JSON.stringify(_res[0]),
    });

    // 매입처 아이디
    $container.find('input[name=regSupId]').val(supId);
    // 브랜드 아이디
    $container.find('input[name=regBrnId]').val(brnId);
    // 상품 아이디
    $container.find('input[name=regPrdtId]').val(prdtId);
    // 매입처명
    $container.find('select[name=regSupName]').empty().append(supOption);
    // 브랜드명
    $container.find('select[name=regBrnName]').empty().append(brnOption);
    // 상품명
    $container.find('select[name=regPrdtName]').empty().append(prdtOption);
    // 기준정보 아이디
    $container.find('input[name=regSupPrdtId]').val(supPrdtId);
    // 바코드
    $container.find('input[name=regBarcode]').val(barcode);
    // 단가
    $container.find('input[name=regSalesPrice]').val(commonUtils.setComma(salesPrice));
    // 판매단가
    $container.find('input[name=regUnitPrice]').val(commonUtils.setComma(purPrice));
    // 수량
    $container.find('input[name=regAmount]').val(1);
    // 금액
    $container.find('input[name=regPrice]').val(commonUtils.setComma(purPrice));
  }

  /**
   * 추가 실행
   */
  function runInsert() {
    // form 전송 ajax 처리
    connecteye.app.ui.alert
      .confirm({
        title: '거래내역을 추가할까요?',
        text: '취소를 누르면 내용을 수정 할 수 있어요',
        confirmBtnTxt: '추가',
        cancelBtnTxt: '취소',
        showLoaderOnConfirm: true,
        preConfirm: () => {},
      })
      .then((_result) => {
        if (_result.isConfirmed) {
          insertSupplierPurchaseInfo();
        } else {
          connecteye.app.ui.alert.error({
            title: '추가 취소',
            text: '거래내역 수정 후 다시 추가 할 수 있어요 :)',
            icon: 'error',
          });
        }
      });
  }

  /**
   * 거래내역 추가 ajax
   */
  function insertSupplierPurchaseInfo() {
    const purchaseData = supPurRegTable.grid.getData();
    const purTypeCode = $container.find('input[name=purStat]:checked').val();
    const purRightRegCheck = $container.find('input[name=purRightRegCheck]:checked').val();
    const purchaseDocNumber = $container.find('.pur-doc-number').text();

    connecteye.app.ui.showCardLoading();
    commonUtils.callAjax({
      type: 'POST',
      url: '/api/supplierPurchaseInfo/insertSupplierPurchaseInfo',
      paramData: {
        purTypeCode: purTypeCode,
        purRightRegCheck: purRightRegCheck || 'NONE',
        purchaseDocNumber: purchaseDocNumber,
        purchaseData: JSON.stringify(purchaseData),
      },
      callback: () => {
        connecteye.app.ui.hideCardLoading();
        connecteye.app.ui.alert
          .success({
            title: '추가 완료!',
            text: '입력한 기준정보가 추가됐어요',
            denyButtonText: '계속 추가하기',
            showDenyButton: true,
          })
          .then((_SuccessResult) => {
            if (_SuccessResult.isConfirmed) {
              // 거래내역 목록 조회
              showSupplierPurchaseInfoList();
            } else if (_SuccessResult.isDenied) {
              resetPage();
            }
          });
      },
    });
  }

  /**
   * 거래내역 추가
   */
  function addRegData() {
    const obj = searchBoxUtils.getResultObject($('#regPrdtName'));
    const purStatCode = $container.find('input[name=purStat]:checked').val();
    const price = parseInt(commonUtils.removeComma($('#regPrice').val()));
    const unitPrice = parseInt(commonUtils.removeComma($('#regUnitPrice').val()));
    const amount = parseInt($('#regAmount').val());

    if (commonUtils.isNotEmpty(obj)) {
      obj.price = price || obj.salesPrice;
      obj.unitPrice = unitPrice || obj.purPrice;
      obj.amount = amount || 1;
      obj.typeCode = purStatCode !== 'BY101' && purStatCode !== 'BY102' ? 'ST114' : 'ST112';
      supPurRegTable.grid.appendRow(obj);
      return true;
    } else {
      connecteye.app.ui.alert.warning({
        text: '매입처, 브랜드, 상품 정보를 입력해주세요',
      });
    }
  }

  /**
   * 거래내역 문서번호 세팅
   */
  async function getPurDocumentName() {
    const purName = await getDateWithSupName();
    commonUtils.callAjax({
      type: 'GET',
      url: '/api/supplierPurchaseInfo/getPurIdCount',
      paramData: { purName: purName },
      callback: (res) => {
        let { purCnt } = res[0];
        purCnt = (purCnt + 1).toString().padStart(3, '0');
        $container.find('.pur-doc-number').text(`${purName}-${purCnt}`);
      },
    });
  }

  /**
   * 날짜, 매입처 이름 가져오기
   */
  function getDateWithSupName() {
    const { supName } = searchBoxUtils.getResultObject($('#regSupName'));
    const currentDate = new Date();
    const year = currentDate.getFullYear().toString().substr(2);
    const month = (currentDate.getMonth() + 1).toString().padStart(2, '0');
    const date = currentDate.getDate().toString().padStart(2, '0');
    return `${year}${month}${date}-${supName}`;
  }

  /**
   * 매입입고 목록 보여주기
   */
  function showSupplierPurchaseInfoList() {
    $container.find('.btn-prev').trigger('click');
    $container.find('.card[data-area=supPurList]').trigger('loadDom', { listReload: true });
  }

  /**
   * 단가, 수량 입력 트리거
   *
   * @param {Object} _this
   */
  function setInputChangeEventTrigger(_this) {
    const $price = $container.find('#regPrice');
    const amount = parseInt($container.find('#regAmount').val());
    const unitPrice = parseInt(commonUtils.removeComma($container.find('#regUnitPrice').val()));

    const { value, name } = _this.currentTarget;
    const currentValue = commonUtils.removeComma(value);

    if (commonUtils.isNotEmpty(currentValue)) {
      if (commonUtils.isNumeric(currentValue)) {
        let price;
        const numValue = commonUtils.isNotEmpty(currentValue) ? parseInt(currentValue) : 0;
        switch (name) {
          case 'regUnitPrice':
            price = String(amount * numValue);
            break;
          case 'regAmount':
            price = String(unitPrice * numValue);
            break;
        }
        $(_this.currentTarget).val(commonUtils.setComma(currentValue.replace(/(^0+)/, '')));
        $price.val(commonUtils.setComma(price.replace(/(^0+)/, '')));
      } else {
        connecteye.app.ui.alert.warning({
          text: '숫자만 입력해주세요.',
          icon: 'error',
        });
        $(_this.currentTarget).val('0');
      }
    }
  }

  /**
   * 상품정보 초기화
   */
  function resetPrdtInfoWithBrand() {
    // 상품정보 초기화
    $container
      .find('.product-control')
      .val('')
      .end()
      .find('#regPrdtName')
      .empty()
      .trigger('change');
  }

  /**
   * 상품정보 초기화
   */
  function resetPrdtInfo() {
    const $formSupPur = $('#formSupPur');

    // 상품정보 초기화
    $formSupPur
      .find('#regBrnName')
      .empty()
      .trigger('change')
      .end()
      .find('#regPrdtName')
      .empty()
      .trigger('change')
      .end();

    $container.find('.product-control').val('');
    $container.find('.pur-doc-number').text('');
    $container.find('.product-control').prop('disabled', true);

    supPurRegTable.grid.clear();
  }

  /**
   * 초기화
   */
  async function resetPage() {
    const $formSupPur = $('#formSupPur');
    await $formSupPur
      .find('#regSupName')
      .empty()
      .trigger('change')
      .end()
      .find('#regBrnName')
      .empty()
      .trigger('change')
      .end()
      .find('#regPrdtName')
      .empty()
      .trigger('change');
    commonUtils.formReset($formSupPur.find('input[type=text], select'));
    $container.find('.pur-doc-number').text('');
    $container.find('.product-control').prop('disabled', true);
    $container.find('select[name=regBrnName]').prop('disabled', true);
    $container.find('#purRightRegCheck').prop('checked', false);
    $container.parent().scrollTop(0);
    supPurRegTable.grid.clear();
  }
  /***** Business End *****/
};
