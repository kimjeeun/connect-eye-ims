/*
 *  Description: supplierPurchaseInfo main.js
 *  User: kyle
 *  Date: 2021-12-09
 */
import supplierPurchaseInfo from './supplierPurchaseInfo';
import supplierPurchaseInfoReg from './supplierPurchaseInfoReg';
import purchaseReg from './purchaseReg';

$(document).ready(async () => {
  // 매입입고
  supplierPurchaseInfo();
  // 매입입고 거래내역 추가
  supplierPurchaseInfoReg();
  // 매입입고 입고등록
  purchaseReg();
});
