/*
 *  Description: 금전출납
 *  User: kyle
 *  Date: 2022-01-13
 */
import moment from 'moment';
import listJs from 'list.js';
import perfectscrollbar from 'perfect-scrollbar';

import gridUtils from '../../utils/gridUtils';
import commonUtils from '../../utils/commonUtils';

export default () => {
  /***** Static Start *****/
  const $container = $('#cashPaymentInfo');

  // table
  const cashPaymentInfoTable = new gridUtils.create({
    el: 'cashPaymentInfoTable',
    bodyHeight: 'auto',
    minBodyHeight: 380,
    rowHeaders: [],
    columnOptions: {
      // 1개의 컬럼을 고정
      frozenCount: 1,
    },
    columns: [
      {
        header: '문서번호',
        name: 'purId',
        width: 240,
      },
      {
        header: '분류',
        name: 'purType',
        width: 100,
        align: 'center',
      },
      {
        header: '매입처',
        name: 'supName',
        align: 'center',
        ellipsis: true,
        resizable: true,
      },
      {
        header: '브랜드',
        name: 'brnName',
        align: 'center',
        ellipsis: true,
        resizable: true,
      },
      {
        header: '수량',
        name: 'amount',
        width: 80,
        align: 'center',
      },
      {
        header: '금액',
        name: 'price',
        width: 90,
        align: 'right',
        formatter: commonUtils.setComma,
        renderer: {
          styles: {
            color: (props) => (props.value < 0 ? '#f25961' : '#333'),
          },
        },
      },
      {
        header: '미납금',
        name: 'unpaidAmount',
        width: 90,
        align: 'right',
        formatter: commonUtils.setComma,
        renderer: {
          styles: {
            color: (props) => (props.value < 0 ? '#f25961' : '#333'),
          },
        },
      },
    ],

    // 그리드 생성 완료 시
    onGridMounted: (_gridThis) => {
      // 그리드 레이아웃 새로고침
      _gridThis.instance.refreshLayout();

      // 초기 날짜
      setDateAndGetCashPaymentInfo(moment().subtract(29, 'days'), moment());
    },

    // 그리드 업데이트 시  'resetData', 'restore', 'reloadData', 'readData', 'setPerPage'
    onGridUpdated: (_gridThis) => {
      // 그리드 레이아웃 새로고침
      _gridThis.instance.refreshLayout();
    },
  });

  // 거래기간 검색 달력
  connecteye.app.ui.createDatePicker({
    containerEl: '#cashPaymentInfo .btn-calendar',
    parentEl: '.total-cash-container',
    ranges: true,
    callback: setDateAndGetCashPaymentInfo,
  });

  // 매입처목록 영역 스크롤바
  new perfectscrollbar($container.find('.supplier-list')[0]);

  // 미지급/미수금이 발생한 매입처 목록 생성
  createSupplierInfoWithUnpaidList();

  // 검색 selectbox 바인딩
  commonUtils.setSelectboxCode({ el: 'prdtDivCode', code: 'PO100' });
  /***** Static End *****/

  /***** Event Start *****/
  // 금전출납 리스트 화면 dom 로드 시
  $container.find('.card[data-area=cashPaymentList]').on('loadDom', (_evt, _data) => {
    if (_data && _data.listReload) {
      getCashPaymentInfo();
      getCashPaymentStatus();

      // 미지급/미수금이 발생한 매입처 목록 생성
      createSupplierInfoWithUnpaidList();
    }
  });

  // 검색 버튼 이벤트
  $container.find('.btn-select').on('click', () => {
    getCashPaymentInfo();
    getCashPaymentStatus();
  });
  $container.find('.search-group').on('keyup', (_evt) => {
    if (_evt.keyCode === 13) {
      getCashPaymentInfo();
      getCashPaymentStatus();
    }
  });

  // 이전 버튼 이벤트
  $container.find('.btn-prev').on('click', (_evt) => getArea(_evt));
  /***** Event End *****/

  /***** Business Start *****/
  /**
   * 금전출납 정보 가져오기
   */
  function getCashPaymentInfo() {
    const $form = $container.find('.search-group');
    const formSerializeData = commonUtils.serializeObject($form);
    // 그리드 로딩바
    cashPaymentInfoTable.grid.dispatch('setLoadingState', 'LOADING');
    let options = {
      type: 'GET',
      url: '/api/cashPaymentInfo/getCashPaymentInfo',
      paramData: formSerializeData || null,
      callback: (_res) => {
        const totalCount = _res.length;
        // 데이터 건수
        $container.find('.result-cnt').text(commonUtils.setComma(totalCount));
        if (commonUtils.isNotEmpty(_res)) {
          // 그리드에 데이터 넣음
          cashPaymentInfoTable.grid.resetData(_res);
          // hideLoadingBar
          cashPaymentInfoTable.grid.dispatch('setLoadingState', 'DONE');
        } else {
          cashPaymentInfoTable.grid.dispatch('setLoadingState', 'EMPTY');
        }
      },
    };
    commonUtils.callAjax(options, true);
  }

  /**
   * 금전출납 정보 가져오기
   */
  function getCashPaymentStatus() {
    const $form = $container.find('.search-group');
    const formSerializeData = commonUtils.serializeObject($form);
    let options = {
      type: 'GET',
      url: '/api/cashPaymentInfo/getCashPaymentStatus',
      paramData: formSerializeData || null,
      callback: (_res) => {
        const {
          totalBuyPrice,
          totalPayPrice,
          totalSalePrice,
          totalUnpaidPrice,
          totalReturnPrice,
          rcvPrice,
        } = _res[0];
        $container
          .find('.total-cash-container')
          .find('.total-buy-price')
          .text(commonUtils.setComma(totalBuyPrice))
          .end()
          .find('.total-pay-price')
          .text(commonUtils.setComma(totalPayPrice))
          .end()
          .find('.total-sale-price')
          .text(commonUtils.setComma(totalSalePrice))
          .end()
          .find('.total-unpaid-price')
          .text(commonUtils.setComma(totalUnpaidPrice))
          .end()
          .find('.total-return-price')
          .text(commonUtils.setComma(totalReturnPrice))
          .end()
          .find('.rcv-price')
          .text(commonUtils.setComma(rcvPrice))
          .end();
      },
    };
    commonUtils.callAjax(options, true);
  }

  /**
   * 날짜세팅 후 금전출납 정보 가져오기
   *
   * @param {Object} _start
   * @param {Object} _end
   */
  function setDateAndGetCashPaymentInfo(_start, _end) {
    const startDate = _start.format('YYYY-MM-DD');
    const endDate = _end.format('YYYY-MM-DD');
    const searchCashDate = `${startDate} ~ ${endDate}`;
    $container.find('.search-cash-date').text(searchCashDate);
    $container.find('#startDate').val(startDate);
    $container.find('#endDate').val(endDate);
    getCashPaymentStatus();
    getCashPaymentInfo();
  }

  /**
   * 미지급/미수금이 발생한 매입처 목록 생성
   */
  function createSupplierInfoWithUnpaidList() {
    commonUtils.callAjax({
      type: 'GET',
      url: '/api/supplierPurchaseInfo/getSupplierInfoWithUnpaid',
      paramData: null,
      callback: async (_res) => {
        if (_res.length) {
          await getSupplierInfoWithUnPaidList(_res);
        }
        $container.find('.supplier-search').removeClass('is-loading');
      },
    });
  }

  /**
   * 미지급/미수금이 발생한 매입처 목록 가져오기
   *
   * @param {Object} _res
   */
  async function getSupplierInfoWithUnPaidList(_res) {
    const { buyPrice, unpaidPrice, rcvPrice } = await groupBySum(_res);

    // 총 매입처 검색 현황
    $container
      .find('#supplierList .buy-price')
      .text(commonUtils.setComma(buyPrice))
      .end()
      .find('#supplierList .unpaid-price')
      .text(commonUtils.setComma(unpaidPrice))
      .end()
      .find('#supplierList .rcv-price')
      .text(commonUtils.setComma(rcvPrice))
      .end()
      .find('#supplierAll')
      .attr('placeholder', `미납 매입처 (${_res.length})`);

    // 검색 목록 박스 생성
    await createListBox(_res);
  }

  /**
   * 검색 목록 박스 생성
   *
   * @param {Object} _res
   */
  async function createListBox(_res) {
    // 전체 매입처 목록 검색박스
    $container.find('#supplierList .list').empty();
    const options = {
      item: (values) => {
        const { supName, rcvPrice, buyPrice, unpaidPrice, supId } = values;
        return (
          `<div class="d-block">` +
          `<div class="d-flex border-bottom">` +
          `<div class="col col-flex text-center name">${commonUtils.setComma(supName)}</div>` +
          `<div class="col col-flex"></div>` +
          `<div class="col col-flex">` +
          `<button class="btn btn-border btn-custom btn-xs btn-pay" data-target="cashPaymentOne" data-id="${supId}">납부</button>` +
          `</div>` +
          `</div>` +
          `<div class="d-flex border-bottom">` +
          `<div class="col col-flex font-weight-bold">${commonUtils.setComma(buyPrice)}</div>` +
          `<div class="col col-flex font-weight-bold text-primary">${commonUtils.setComma(
            unpaidPrice,
          )}</div>` +
          `<div class="col col-flex font-weight-bold text-danger">${commonUtils.setComma(
            rcvPrice,
          )}</div>` +
          `</div>` +
          `</div>`
        );
      },
    };
    await new listJs('supplierList', options, _res);

    // 납부버튼 이벤트
    $container.find('#supplierList .btn-pay').on('click', (_evt) => {
      const $element = $(_evt.currentTarget);
      const $target = $container.find('.card[data-area=cashPaymentOne]');
      $target.removeClass('d-none').siblings('.card').addClass('d-none').end().trigger('loadDom', {
        supId: $element.attr('data-id'),
      });
    });
  }

  /**
   * 그룹 합산 오브젝트
   *
   *
   * @param {Object} _res
   * @return {Object}
   */
  function groupBySum(_res) {
    return _res.reduce((obj, { buyPrice, unpaidPrice, rcvPrice }) => {
      (obj['buyPrice'] = (parseInt(obj['buyPrice']) || 0) + parseInt(buyPrice || 0)), obj;
      (obj['unpaidPrice'] = (parseInt(obj['unpaidPrice']) || 0) + parseInt(unpaidPrice || 0)), obj;
      (obj['rcvPrice'] = (parseInt(obj['rcvPrice']) || 0) + parseInt(rcvPrice || 0)), obj;
      return obj;
    }, {});
  }

  /**
   * 영역 가져오기
   *
   * @param {Object} _evt
   */
  async function getArea(_evt) {
    const $el = $(_evt.currentTarget);
    const target = $el.data('target');
    const $parentArea = $container.find(`.page-inner .card[data-area=${target}]`);

    // 자신을 제외하고 d-none 클래스 넣어줌
    await $parentArea
      .removeClass('d-none')
      .siblings('.card')
      .addClass('d-none')
      .end()
      .trigger('loadDom');
    $container.parent().scrollTop(0);
    cashPaymentInfoTable.grid.refreshLayout();
  }
  /***** Business End *****/
};
