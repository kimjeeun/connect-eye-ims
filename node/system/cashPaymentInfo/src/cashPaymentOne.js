/*
 *  Description: 금전출납 건별 납부
 *  User: kyle
 *  Date: 2022-01-13
 */
import moment from 'moment';
import gridUtils from '../../utils/gridUtils';
import commonUtils from '../../utils/commonUtils';
import searchBoxUtils from '../../utils/searchBoxUtils';

export default () => {
  /***** Static Start *****/
  const $container = $('#cashPaymentInfo');

  // table
  const cashPaymentOneTable = new gridUtils.create({
    el: 'cashPaymentOneTable',
    rowHeaders: [],
    bodyHeight: 'auto',
    minBodyHeight: 80,
    columnOptions: {
      // 1개의 컬럼을 고정
      frozenCount: 1,
    },
    columns: [
      {
        header: '문서번호',
        name: 'purId',
        width: 240,
        align: 'center',
      },
      {
        header: '분류',
        name: 'purType',
        width: 70,
        align: 'center',
      },
      {
        header: '브랜드',
        name: 'brnName',
        align: 'center',
        ellipsis: true,
      },
      {
        header: '상품명',
        name: 'prdtName',
        align: 'center',
        ellipsis: true,
      },
      {
        header: '평균단가',
        name: 'avgPrice',
        width: 100,
        align: 'center',
        formatter: commonUtils.setComma,
        renderer: {
          styles: {
            color: (props) => (props.value < 0 ? '#f25961' : '#333'),
          },
        },
      },
      {
        header: '수량',
        name: 'amount',
        width: 75,
        align: 'center',
      },
      {
        header: '금액',
        name: 'price',
        width: 100,
        align: 'center',
        formatter: commonUtils.setComma,
        renderer: {
          styles: {
            color: (props) => (props.value < 0 ? '#f25961' : '#333'),
          },
        },
      },
      {
        header: '진행단계',
        name: 'progStat',
        width: 75,
        align: 'center',
        renderer: {
          type: gridUtils.purStatusRenderer,
        },
      },
      {
        header: '미납금',
        name: 'unpaidAmount',
        width: 100,
        align: 'center',
        formatter: commonUtils.setComma,
        renderer: {
          styles: {
            color: (props) => (props.value < 0 ? '#f25961' : '#333'),
          },
        },
      },
    ],

    // 그리드 생성 완료 시
    onGridMounted: (_gridThis) => {
      // 그리드 레이아웃 새로고침
      _gridThis.instance.refreshLayout();
    },

    // 그리드 업데이트 시  'resetData', 'restore', 'reloadData', 'readData', 'setPerPage'
    onGridUpdated: (_gridThis) => {
      // 그리드 레이아웃 새로고침
      _gridThis.instance.refreshLayout();
    },
  });

  // table
  const cashPaymentHistoryTable = new gridUtils.create({
    el: 'cashPaymentHistoryTable',
    rowHeaders: [],
    minBodyHeight: 180,
    scrollX: 'false',
    columns: [
      {
        header: '',
        name: '',
        width: 20,
        align: 'center',
        renderer: {
          type: gridUtils.deleteButtonRenderer,
          options: {
            isAmount: false,
            $currentAmount: '',
            callback: runDeleteCashPaymentHistory,
          },
        },
      },
      {
        header: '납부일',
        name: 'payDate',
        align: 'center',
      },
      {
        header: '카드',
        name: 'cardPrice',
        align: 'center',
        formatter: commonUtils.setComma,
      },
      {
        header: '현금',
        name: 'cashPrice',
        align: 'center',
        formatter: commonUtils.setComma,
      },
      {
        header: '할인',
        name: 'salePrice',
        align: 'center',
        formatter: commonUtils.setComma,
      },
      {
        header: '총 납부액',
        name: 'payPrice',
        align: 'center',
        formatter: commonUtils.setComma,
      },
    ],

    // 그리드 생성 완료 시
    onGridMounted: (_gridThis) => {},

    // 그리드 업데이트 시  'resetData', 'restore', 'reloadData', 'readData', 'setPerPage'
    onGridUpdated: (_gridThis) => {
      // 그리드 레이아웃 새로고침
      _gridThis.instance.refreshLayout();
    },
  });

  // 거래기간 검색 달력
  connecteye.app.ui.createDatePicker({
    containerEl: '#cashPaymentInfo .btn-calendar-2',
    parentEl: '.total-cash-update-container',
    ranges: true,
    callback: setDateAndGetCashPaymentInfo,
  });

  // 납부일 선택
  connecteye.app.ui.createDatePicker({
    containerEl: '#cashPaymentInfo #payDate',
    parentEl: '.date-picker-container',
    startDate: moment(),
    singleDatePicker: true,
    ranges: false,
    callback: () => {},
  });
  /***** Static End *****/

  /***** Event Start *****/
  // 금전출납 리스트 화면 dom 로드 시
  $container.find('.card[data-area=cashPaymentOne]').on('loadDom', async (_evt, _data) => {
    const { supId } = _data;

    // 발주/반품 목록 테이블
    cashPaymentOneTable.grid.refreshLayout();

    // 납부내역 목록 테이블
    cashPaymentHistoryTable.grid.refreshLayout();

    // 매입처 아이디값 저장
    $container.find('#searchSupId').val(supId);

    // 최상단 이동
    $container.parent().scrollTop(0);

    // 초기 날짜
    setDateAndGetCashPaymentInfo(moment().subtract(29, 'days'), moment());
  });

  // 그리드 클릭 이벤트
  cashPaymentOneTable.grid.on('click', (_evt) => {
    const { rowKey, instance, targetType } = _evt;
    const rowData = instance.getRow(rowKey);
    const depth = instance.getDepth(rowKey);

    if (
      rowData &&
      depth < 2 &&
      targetType !== 'columnHeader' &&
      targetType !== 'rowHeader' &&
      targetType !== 'etc'
    ) {
      const { purId, amount, payYn, unpaidAmount } = rowData;

      // 그리드 트리구조 부모를 제외한 자식들만
      // 미지급금 납부할 금액
      const payPrice = Math.abs(unpaidAmount);
      $container
        .find('.pay-price')
        .text(commonUtils.setComma(payPrice))
        .end()
        .find('#payPrice')
        .val(payPrice);

      // 문서번호, 기준정보아이디, 수량, 납부여부
      $container
        .find('#purId')
        .val(purId)
        .end()
        .find('#amount')
        .val(amount)
        .end()
        .find('#payYn')
        .val(payYn);

      // 납부내역 가져오기
      getCashPaymentHistory(rowData);

      // 그리드 ROW 선택
      gridUtils.setSelectionRange(cashPaymentOneTable.grid, rowKey);
    }
  });

  // 단가 이벤트
  $container.find('#cashPrice').on({
    keyup: commonUtils.setPriceEventHandler,
    change: commonUtils.setPriceEventHandler,
  });
  $container.find('#cardPrice').on({
    keyup: commonUtils.setPriceEventHandler,
    change: commonUtils.setPriceEventHandler,
  });
  $container.find('#salePrice').on({
    keyup: commonUtils.setPriceEventHandler,
    change: commonUtils.setPriceEventHandler,
  });

  // 납부정보 입력 후 납부하기 버튼 클릭 시 이벤트
  $container.find('#btnCashOnePayment').on('click', () => {
    if (isRegData() && isCashPaymentValidation()) {
      runInsertCashPayment();
    }
  });

  // 금전출납 납부정보 입력 금액 이벤트
  $container.find('#cashPrice').on('keyup', setInputChangeEventTrigger);
  $container.find('#cardPrice').on('keyup', setInputChangeEventTrigger);
  $container.find('#salePrice').on('keyup', setInputChangeEventTrigger);

  // 초기화
  $container.find('#btnCashOneReset').on('click', () => resetPage());

  // 이전페이지 버튼 클릭 이벤트
  $container.find('.btn-prev').on('click', () => {
    // 이전페이지 다시 불러오기
    $container.find('.card[data-area=cashPaymentList]').trigger('loadDom', { listReload: true });
    // 페이지 초기화
    resetPage();
  });
  /***** Event End *****/

  /***** Business Start *****/
  /**
   * 매입처별 금전출납 내역 가져오기
   *
   * @param {Object} _data
   */
  function getCashPaymentBySupplier(_data) {
    connecteye.app.ui.showCardLoading();
    let options = {
      type: 'GET',
      url: '/api/cashPaymentInfo/getCashPaymentInfo',
      paramData: _data,
      callback: (_res) => {
        if (commonUtils.isNotEmpty(_res)) {
          $container.find('.by-supplier-name').text(`[ ${_res[0].supName} ]`);
          // 그리드에 데이터 넣음
          cashPaymentOneTable.grid.resetData(_res);
        } else {
          connecteye.app.ui.alert.warning({
            text: '조회 결과가 없어요',
          });
        }
        connecteye.app.ui.hideCardLoading();
      },
    };
    commonUtils.callAjax(options, true);
  }

  /**
   * 금전출납 현황 가져오기
   *
   * @param {Object} _rowData
   */
  function getCashPaymentStatus(_rowData) {
    const { purType } = _rowData;
    connecteye.app.ui.showCardLoading();
    let options = {
      type: 'GET',
      url: '/api/cashPaymentInfo/getCashPaymentStatus',
      paramData: _rowData,
      callback: (_res) => {
        let {
          totalBuyPrice,
          totalPayPrice,
          totalSalePrice,
          totalUnpaidPrice,
          totalReturnPrice,
          rcvPrice,
        } = _res[0];

        // 매입서분류별 금액 0원 처리
        switch (purType) {
          case '매입':
          case '위탁':
            totalReturnPrice = 0;
            rcvPrice = 0;
            break;
          case '반품':
          case '위탁반품':
            totalUnpaidPrice = 0;
            break;
        }
        $container
          .find('.total-cash-update-container')
          .find('.total-buy-price')
          .text(commonUtils.setComma(totalBuyPrice || 0))
          .end()
          .find('.total-pay-price')
          .text(commonUtils.setComma(totalPayPrice || 0))
          .end()
          .find('.total-sale-price')
          .text(commonUtils.setComma(totalSalePrice || 0))
          .end()
          .find('.total-unpaid-price')
          .text(commonUtils.setComma(totalUnpaidPrice || 0))
          .end()
          .find('.total-return-price')
          .text(commonUtils.setComma(totalReturnPrice || 0))
          .end()
          .find('.rcv-price')
          .text(commonUtils.setComma(rcvPrice || 0))
          .end();
        connecteye.app.ui.hideCardLoading();
      },
    };
    commonUtils.callAjax(options, true);
  }

  /**
   * 금전출납 납부내역 가져오기
   */
  function getCashPaymentHistory(_param) {
    let options = {
      type: 'GET',
      url: '/api/cashPaymentInfo/getCashPaymentHistory',
      paramData: _param || null,
      callback: (_res) => {
        // 그리드에 데이터 넣음
        cashPaymentHistoryTable.grid.resetData(_res);
      },
    };
    commonUtils.callAjax(options, true);
  }

  /**
   * 납부 실행
   */
  function runInsertCashPayment() {
    // form 전송 ajax 처리
    connecteye.app.ui.alert
      .confirm({
        title: '납부를 실행할까요?',
        text: '취소를 누르면 내용을 수정 할 수 있어요',
        confirmBtnTxt: '추가',
        cancelBtnTxt: '취소',
        showLoaderOnConfirm: true,
        preConfirm: () => {},
      })
      .then((_result) => {
        if (_result.isConfirmed) {
          insertCashPaymentHistory();
        } else {
          connecteye.app.ui.alert.error({
            title: '납부 취소',
            text: '납부정보 수정 후 다시 추가 할 수 있어요 :)',
            icon: 'error',
          });
        }
      });
  }

  /**
   * 금전출납 납부내역 등록
   */
  function insertCashPaymentHistory() {
    const formData = commonUtils.serializeObject($('#formCashPaymentOne'));

    connecteye.app.ui.showCardLoading();
    commonUtils.callAjax({
      type: 'POST',
      url: '/api/cashPaymentInfo/insertCashPaymentHistory',
      paramData: formData,
      callback: () => {
        connecteye.app.ui.hideCardLoading();
        connecteye.app.ui.alert
          .success({
            title: '납부 완료!',
            text: '입력한 납부정보가 등록됐어요',
            denyButtonText: '계속 하기',
            showDenyButton: true,
          })
          .then((_SuccessResult) => {
            if (_SuccessResult.isConfirmed) {
              // 금전출납 목록 조회
              showCashPaymentInfoList();
            } else if (_SuccessResult.isDenied) {
              const formSupplierData = commonUtils.serializeObject($('#formCashBySupplier'));
              // 페이지 초기화
              resetPage();
              // 금전출납 매입처별 조회
              getCashPaymentBySupplier(formSupplierData);
              // 금전출납 현황조회
              getCashPaymentStatus(formSupplierData);
            }
          });
      },
    });
  }

  /**
   * 금전출납 납부내역 삭제 실행
   *
   * @return {Object} _data
   */
  function runDeleteCashPaymentHistory(_data) {
    connecteye.app.ui.alert
      .confirm({
        title: '납부 삭제를 실행할까요?',
        text: '취소를 누르면 내용을 이전 화면으로 되돌아가요',
        confirmBtnTxt: '실행',
        cancelBtnTxt: '취소',
        showLoaderOnConfirm: true,
        preConfirm: () => {},
      })
      .then((_result) => {
        if (_result.isConfirmed) {
          deleteCashPaymentHistory(_data);
        }
      });
  }

  /**
   * 금전출납 납부내역 삭제
   *
   * @return {Object} _data
   */
  function deleteCashPaymentHistory(_data) {
    connecteye.app.ui.showCardLoading();
    commonUtils.callAjax({
      type: 'POST',
      url: '/api/cashPaymentInfo/deleteCashPaymentHistory',
      paramData: _data,
      callback: () => {
        const { rowKey } = _data;
        cashPaymentHistoryTable.grid.removeRow(rowKey);
        connecteye.app.ui.hideCardLoading();
        connecteye.app.ui.alert
          .success({
            title: '납부 삭제 완료!',
            text: '선택한 납부정보가 삭제됐어요',
            showDenyButton: false,
          })
          .then((_SuccessResult) => {
            if (_SuccessResult.isConfirmed) {
              const formSupplierData = commonUtils.serializeObject($('#formCashBySupplier'));
              // 금전출납 매입처별 조회
              getCashPaymentBySupplier(formSupplierData);
              // 금전출납 현황조회
              getCashPaymentStatus(formSupplierData);
            }
          });
      },
    });
  }

  /**
   * 금전출납 목록 보여주기
   */
  function showCashPaymentInfoList() {
    $container.find('.btn-prev').trigger('click');
    $container.find('.card[data-area=cashPaymentList]').trigger('loadDom', { listReload: true });
  }

  /**
   * 정보 입력 유무체크
   *
   * @return {boolean} boolean
   */
  function isRegData() {
    const obj = commonUtils.serializeObject($('#formCashPaymentOne'));
    const { cardPrice, cashPrice, payDate } = obj;
    const payPrice =
      Number(commonUtils.removeComma(cardPrice || 0)) +
      Number(commonUtils.removeComma(cashPrice || 0));

    if (payPrice && commonUtils.isNotEmpty(payDate)) {
      return true;
    } else {
      connecteye.app.ui.alert.warning({
        text: '납부정보를 입력해주세요',
      });
    }
  }

  /**
   * 금전출납 정보 입력 유무체크
   *
   * @return {boolean} boolean
   */
  function isCashPaymentValidation() {
    const value = $container.find('#purId').val();
    const payYn = $container.find('#payYn').val();
    // 현금
    const cashPrice = Number(commonUtils.removeComma($container.find('#cashPrice').val()));
    // 카드
    const cardPrice = Number(commonUtils.removeComma($container.find('#cardPrice').val()));
    // 할인
    const salePrice = Number(commonUtils.removeComma($container.find('#salePrice').val()));
    // 총 금액
    const totalPrice = cashPrice + cardPrice;
    // 납부금액
    const payPrice = Number(commonUtils.removeComma($container.find('.pay-price').text()));
    // 납부상태
    const $payStatus = $container.find('#payStatus');

    if (commonUtils.isEmpty(value)) {
      connecteye.app.ui.alert.warning({
        text: '발주/반품 정보를 선택해주세요',
      });
      return false;
    }
    if (payYn !== 'N') {
      connecteye.app.ui.alert.warning({
        text: '납부 완료된 상품이에요',
      });
      return false;
    }
    if (totalPrice > payPrice) {
      connecteye.app.ui.alert.warning({
        text: '납부 가능금액을 초과 할 수 없어요',
      });
      return false;
    }

    // 납부상태 설정
    totalPrice === payPrice ? $payStatus.val('payCompleted') : $payStatus.val('payNotCompleted');

    // comma 삭제
    $container.find('#cashPrice').val(cashPrice);
    $container.find('#cardPrice').val(cardPrice);
    $container.find('#salePrice').val(salePrice);

    return true;
  }

  /**
   * 입력 트리거
   *
   * @param {Object} _this
   */
  function setInputChangeEventTrigger(_this) {
    const $totalInputPrice = $container.find('.total-input-price');
    const $payPrice = $container.find('.pay-price');
    const payYn = $container.find('#payYn').val();
    const payPrice = Number(commonUtils.removeComma($container.find('#payPrice').val()));
    const cashPrice = Number(commonUtils.removeComma($container.find('#cashPrice').val()));
    const cardPrice = Number(commonUtils.removeComma($container.find('#cardPrice').val()));

    const { value, name } = _this.currentTarget;
    const currentValue = commonUtils.removeComma(value);

    // 납부 가능금액 체크
    if (payYn === 'Y') {
      connecteye.app.ui.alert.warning({
        text: '납부 완료된 상품이에요',
        icon: 'error',
      });
      $(_this.currentTarget).val('0');
      return false;
    }

    if (commonUtils.isNotEmpty(currentValue)) {
      if (commonUtils.isNumeric(currentValue)) {
        let totalPrice;
        const numValue = commonUtils.isNotEmpty(currentValue) ? Number(currentValue) : 0;
        switch (name) {
          case 'cashPrice':
            totalPrice = String(numValue + cardPrice);
            break;
          case 'cardPrice':
            totalPrice = String(numValue + cashPrice);
            break;
          case 'salePrice':
            totalPrice = String(cardPrice + cashPrice);
            $payPrice.text(commonUtils.setComma(payPrice - numValue));
            break;
        }
        totalPrice = totalPrice.replace(/(^0+)/, '');
        $(_this.currentTarget).val(
          currentValue !== '0' ? commonUtils.setComma(currentValue.replace(/(^0+)/, '')) : '0',
        );
        $totalInputPrice.text(commonUtils.setComma(totalPrice));
      } else {
        connecteye.app.ui.alert.warning({
          text: '숫자만 입력해주세요.',
          icon: 'error',
        });
        $(_this.currentTarget).val('0');
      }
    }
  }

  /**
   * 날짜세팅 후 납부 정보 가져오기
   *
   * @param {Object} _start
   * @param {Object} _end
   */
  function setDateAndGetCashPaymentInfo(_start, _end) {
    const startDate = _start.format('YYYY-MM-DD');
    const endDate = _end.format('YYYY-MM-DD');
    const searchCashDate = `${startDate} ~ ${endDate}`;

    $container.find('.search-cash-date-2').text(searchCashDate);
    $container.find('#startDateOne').val(startDate);
    $container.find('#endDateOne').val(endDate);

    const formData = commonUtils.serializeObject($('#formCashBySupplier'));
    getCashPaymentBySupplier(formData);
    getCashPaymentStatus(formData);
  }

  /**
   * 초기화
   */
  async function resetPage() {
    const $formCashPaymentOne = $('#formCashPaymentOne');
    commonUtils.formReset($formCashPaymentOne.find('input:not(#payDate)'), '0');
    $container.find('.total-input-price').text('0');
    $container.find('.pay-price').text('0');
    cashPaymentHistoryTable.grid.clear();
    $container.parent().scrollTop(0);
  }
  /***** Business End *****/
};
