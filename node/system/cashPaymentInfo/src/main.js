/*
 *  Description: cashPaymentInfo main.js
 *  User: kyle
 *  Date: 2021-12-09
 */
import cashPaymentInfo from './cashPaymentInfo';
import cashPaymentOne from './cashPaymentOne';

$(document).ready(() => {
  // 금전출납
  cashPaymentInfo();
  // 금전출납 건별 납부
  cashPaymentOne();
});
