/*
 *  Description: optometrySalesInfo main.js
 *  User: kyle
 *  Date: 2021-12-09
 */
import optometrySalesInfo from './optometrySalesInfo';
import optometryCustAnalysis from './optometryCustAnalysis';
import optometrySalesInfoReg from './optometrySalesInfoReg';
import optometryExtSalesInfoReg from './optometryExtSalesInfoReg';
import optometryCustPay from './optometryCustPay';

$(document).ready(() => {
  // 검안매출 정보
  optometrySalesInfo();

  // 검안매출 고객분석
  optometryCustAnalysis();

  // 검안매출 등록
  optometrySalesInfoReg();

  // 기타매출 등록
  optometryExtSalesInfoReg();

  // 고객별 금전출납
  optometryCustPay();
});
