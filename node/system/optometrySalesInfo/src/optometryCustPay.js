/*
 *  Description: 금전출납 고객별 납부
 *  User: kyle
 *  Date: 2022-04-11
 */
import moment from 'moment';
import sumBy from 'lodash-es/sumBy';

import gridUtils from '../../utils/gridUtils';
import commonUtils from '../../utils/commonUtils';

export default () => {
  /***** Static Start *****/
  const $container = $('#optometrySalesInfo');

  // table
  const optometryCustPayTable = new gridUtils.create({
    el: 'optometryCustPayTable',
    rowHeaders: [],
    bodyHeight: 'auto',
    minBodyHeight: 80,
    columnOptions: {
      // 1개의 컬럼을 고정
      frozenCount: 1,
    },
    columns: [
      {
        header: '판매일자',
        name: 'regDate',
        width: 120,
        align: 'center',
        ellipsis: true,
      },
      {
        header: '브랜드',
        name: 'brnName',
        align: 'center',
        ellipsis: true,
      },
      {
        header: '상품명',
        name: 'prdtName',
        align: 'center',
        ellipsis: true,
      },
      {
        header: '수량',
        name: 'amount',
        width: 90,
        align: 'center',
      },
      {
        header: '금액',
        name: 'price',
        width: 100,
        align: 'center',
        formatter: commonUtils.setComma,
        renderer: {
          styles: {
            color: (props) => (props.value < 0 ? '#f25961' : '#333'),
          },
        },
      },
      {
        header: '미수금',
        name: 'rcvPrice',
        width: 100,
        align: 'center',
        formatter: commonUtils.setComma,
        renderer: {
          styles: {
            color: (props) => (props.value < 0 ? '#f25961' : '#333'),
          },
        },
      },
    ],
    // 그리드 생성 완료 시
    onGridMounted: (_gridThis) => {
      // 그리드 레이아웃 새로고침
      _gridThis.instance.refreshLayout();
    },
    // 그리드 업데이트 시  'resetData', 'restore', 'reloadData', 'readData', 'setPerPage'
    onGridUpdated: (_gridThis) => {
      // 그리드 레이아웃 새로고침
      _gridThis.instance.refreshLayout();
    },
  });

  // table
  const optometryCustPayHistoryTable = new gridUtils.create({
    el: 'optometryCustPayHistoryTable',
    rowHeaders: [],
    minBodyHeight: 195,
    scrollX: 'false',
    columns: [
      {
        header: '',
        name: '',
        width: 20,
        align: 'center',
        renderer: {
          type: gridUtils.deleteButtonRenderer,
          options: {
            isAmount: false,
            $currentAmount: '',
            callback: runDeleteOptometryCustPayHistory,
          },
        },
      },
      {
        header: '납부일',
        name: 'salesPayDate',
        align: 'center',
      },
      {
        header: '카드',
        name: 'cardPrice',
        align: 'center',
        formatter: commonUtils.setComma,
      },
      {
        header: '현금',
        name: 'cashPrice',
        align: 'center',
        formatter: commonUtils.setComma,
      },
      {
        header: '상품권',
        name: 'giftPrice',
        align: 'center',
        formatter: commonUtils.setComma,
      },
      {
        header: '할인',
        name: 'salePrice',
        align: 'center',
        formatter: commonUtils.setComma,
      },
      {
        header: '총 납부액',
        name: 'payPrice',
        align: 'center',
        formatter: commonUtils.setComma,
      },
    ],

    // 그리드 생성 완료 시
    onGridMounted: (_gridThis) => {
      // 그리드 레이아웃 새로고침
      _gridThis.instance.refreshLayout();
    },

    // 그리드 업데이트 시  'resetData', 'restore', 'reloadData', 'readData', 'setPerPage'
    onGridUpdated: (_gridThis) => {
      // 그리드 레이아웃 새로고침
      _gridThis.instance.refreshLayout();
    },
  });

  // 전체거래기간 datepicker
  connecteye.app.ui.createDatePicker({
    containerEl: '#formOptometryCust .btn-calendar-2',
    parentEl: '.total-cash-update-container',
    ranges: true,
    callback: setDateAndGetOptometryCustPayInfo,
  });

  // 납부일 datepicker
  connecteye.app.ui.createDatePicker({
    containerEl: '#optometrySalesInfo #custPayDate',
    parentEl: '.date-picker-container-2',
    startDate: moment(),
    singleDatePicker: true,
    ranges: false,
    callback: () => {},
  });
  /***** Static End *****/

  /***** Event Start *****/
  // 금전출납 리스트 화면 dom 로드 시
  $container.find('.card[data-area=optometryCustPay]').on('loadDom', (_evt, _data) => {
    const { custId } = _data;

    // 발주/반품 목록 테이블
    optometryCustPayTable.grid.refreshLayout();

    // 납부내역 목록 테이블
    optometryCustPayHistoryTable.grid.refreshLayout();

    // 매입처 아이디값 저장
    $container.find('#searchCustId').val(custId);

    // 최상단 이동
    $container.parent().scrollTop(0);

    // 초기 날짜
    setDateAndGetOptometryCustPayInfo(moment().subtract(29, 'days'), moment());
  });

  // 그리드 클릭 이벤트
  optometryCustPayTable.grid.on('click', (_evt) => {
    const { rowKey, instance, targetType } = _evt;
    const rowData = instance.getRow(rowKey);

    if (
      rowData &&
      targetType !== 'columnHeader' &&
      targetType !== 'rowHeader' &&
      targetType !== 'etc'
    ) {
      const { custId, salesId, payYn, rcvPrice } = rowData;

      // 그리드 트리구조 부모를 제외한 자식들만
      // 미지급금 납부할 금액
      const payPrice = Math.abs(rcvPrice);
      $container
        .find('.pay-price')
        .text(commonUtils.setComma(payPrice))
        .end()
        .find('#custPayPrice')
        .val(payPrice);

      $container
        .find('#custPayId')
        .val(custId)
        .end()
        .find('#custSalesId')
        .val(salesId)
        .end()
        .find('#custPayYn')
        .val(payYn);

      // 납부내역 가져오기
      getOptometryCustPayHistory(rowData);

      // 그리드 ROW 선택
      gridUtils.setSelectionRange(optometryCustPayTable.grid, rowKey);
    }
  });

  // 초기화
  $container.find('#btnOptometryCustPayReset').on('click', () => resetPage());

  // 이전페이지 버튼 클릭 이벤트
  $container.find('.btn-prev').on('click', () => {
    // 페이지 초기화
    resetPage();
  });

  // 납부정보 입력 후 납부하기 버튼 클릭 시 이벤트
  $container.find('#btnOptometryCustPay').on('click', () => {
    if (isRegData() && isOptometryCustPayValidation()) {
      runInsertOptometryCustPay();
    }
  });

  // 금전출납 납부정보 입력 금액 이벤트
  $container.find('#custCashPrice').on('keyup', setInputChangeEventTrigger);
  $container.find('#custCardPrice').on('keyup', setInputChangeEventTrigger);
  $container.find('#custGiftPrice').on('keyup', setInputChangeEventTrigger);
  $container.find('#custSalePrice').on('keyup', setInputChangeEventTrigger);
  /***** Event End *****/

  /***** Business Start *****/
  /**
   * 매입처별 금전출납 내역 가져오기
   *
   * @param {Object} _data
   */
  function getOptometryCustPay(_data) {
    connecteye.app.ui.showCardLoading();
    let options = {
      type: 'GET',
      url: '/api/optometrySalesInfo/getOptometryCustPayInfo',
      paramData: _data,
      callback: (_res) => {
        if (commonUtils.isNotEmpty(_res)) {
          const sumPrice = sumBy(_res, 'price');
          const sumSalePrice = sumBy(_res, 'sumSalePrice');
          const sumRcvPrice = Math.abs(sumBy(_res, 'rcvPrice'));
          const sumSalesPrice = sumPrice - sumRcvPrice - sumSalePrice;

          // 고객명
          $container.find('.by-cust-name').text(`[ ${_res[0].custName} ]`);

          // 거래건수
          $container.find('.total-sales-cnt').text(_res.length);

          // 매출액
          $container.find('.total-sales-price').text(commonUtils.setComma(sumSalesPrice));

          // 총 미수금
          $container.find('.total-rcv-price').text(commonUtils.setComma(sumRcvPrice));

          // 그리드에 데이터 넣음
          optometryCustPayTable.grid.resetData(_res);
        } else {
          connecteye.app.ui.alert.warning({
            text: '조회 결과가 없어요',
          });
        }
        connecteye.app.ui.hideCardLoading();
      },
    };
    commonUtils.callAjax(options, true);
  }

  /**
   * 금전출납 납부내역 가져오기
   */
  function getOptometryCustPayHistory(_param) {
    let options = {
      type: 'GET',
      url: '/api/optometrySalesInfo/getOptometryCustPayHistory',
      paramData: _param || null,
      callback: (_res) => {
        // 그리드에 데이터 넣음
        optometryCustPayHistoryTable.grid.resetData(_res);
      },
    };
    commonUtils.callAjax(options, true);
  }

  /**
   * 납부 실행
   */
  function runInsertOptometryCustPay() {
    // form 전송 ajax 처리
    connecteye.app.ui.alert
      .confirm({
        title: '납부를 실행할까요?',
        text: '취소를 누르면 내용을 수정 할 수 있어요',
        confirmBtnTxt: '추가',
        cancelBtnTxt: '취소',
        showLoaderOnConfirm: true,
        preConfirm: () => {},
      })
      .then((_result) => {
        if (_result.isConfirmed) {
          insertOptometryCustPayHistory();
        } else {
          connecteye.app.ui.alert.error({
            title: '납부 취소',
            text: '납부정보 수정 후 다시 추가 할 수 있어요 :)',
            icon: 'error',
          });
        }
      });
  }

  /**
   * 금전출납 납부내역 등록
   */
  function insertOptometryCustPayHistory() {
    const formData = commonUtils.serializeObject($('#formOptometryCustPay'));

    connecteye.app.ui.showCardLoading();
    commonUtils.callAjax({
      type: 'POST',
      url: '/api/optometrySalesInfo/insertOptometryCustPayHistory',
      paramData: formData,
      callback: () => {
        connecteye.app.ui.hideCardLoading();
        connecteye.app.ui.alert
          .success({
            title: '납부 완료!',
            text: '입력한 납부정보가 등록됐어요',
            denyButtonText: '계속 하기',
            showDenyButton: true,
          })
          .then((_SuccessResult) => {
            if (_SuccessResult.isConfirmed) {
              // 금전출납 목록 조회
              showOptometryInfoList();
            } else if (_SuccessResult.isDenied) {
              // 페이지 초기화
              resetPage();
              // 금전출납 매입처별 조회
              getOptometryCustPay(formData);
            }
          });
      },
    });
  }

  /**
   * 금전출납 납부내역 삭제 실행
   *
   * @return {Object} _data
   */
  function runDeleteOptometryCustPayHistory(_data) {
    connecteye.app.ui.alert
      .confirm({
        title: '납부 삭제를 실행할까요?',
        text: '취소를 누르면 내용을 이전 화면으로 되돌아가요',
        confirmBtnTxt: '실행',
        cancelBtnTxt: '취소',
        showLoaderOnConfirm: true,
        preConfirm: () => {},
      })
      .then((_result) => {
        if (_result.isConfirmed) {
          deleteOptometryCustPayHistory(_data);
        }
      });
  }

  /**
   * 금전출납 납부내역 삭제
   *
   * @return {Object} _data
   */
  function deleteOptometryCustPayHistory(_data) {
    connecteye.app.ui.showCardLoading();
    commonUtils.callAjax({
      type: 'POST',
      url: '/api/optometrySalesInfo/deleteOptometryCustPayHistory',
      paramData: _data,
      callback: () => {
        const { rowKey } = _data;
        optometryCustPayHistoryTable.grid.removeRow(rowKey);
        connecteye.app.ui.hideCardLoading();
        connecteye.app.ui.alert
          .success({
            title: '납부 삭제 완료!',
            text: '선택한 납부정보가 삭제됐어요',
            showDenyButton: false,
          })
          .then((_SuccessResult) => {
            if (_SuccessResult.isConfirmed) {
              const formOptometryCustPay = commonUtils.serializeObject($('#formOptometryCustPay'));
              // 금전출납 매입처별 조회
              getOptometryCustPay(formOptometryCustPay);
            }
          });
      },
    });
  }

  /**
   * 금전출납 목록 보여주기
   */
  function showOptometryInfoList() {
    $container.find('.btn-prev').trigger('click');
  }

  /**
   * 날짜세팅 후 납부 정보 가져오기
   *
   * @param {Object} _start
   * @param {Object} _end
   */
  function setDateAndGetOptometryCustPayInfo(_start, _end) {
    const startDate = _start.format('YYYY-MM-DD');
    const endDate = _end.format('YYYY-MM-DD');
    const searchCashDate = `${startDate} ~ ${endDate}`;

    $container.find('.search-cash-date-2').text(searchCashDate);
    $container.find('#startDateOne').val(startDate);
    $container.find('#endDateOne').val(endDate);

    const formData = commonUtils.serializeObject($('#formOptometryCust'));
    getOptometryCustPay(formData);
  }

  /**
   * 정보 입력 유무체크
   *
   * @return {boolean} boolean
   */
  function isRegData() {
    const obj = commonUtils.serializeObject($('#formOptometryCustPay'));
    const { cardPrice, cashPrice, giftPrice, payDate } = obj;
    const payPrice =
      Number(commonUtils.removeComma(cardPrice)) +
      Number(commonUtils.removeComma(cashPrice)) +
      Number(commonUtils.removeComma(giftPrice));

    if (payPrice && commonUtils.isNotEmpty(payDate)) {
      return true;
    } else {
      connecteye.app.ui.alert.warning({
        text: '납부정보를 입력해주세요',
      });
    }
  }

  /**
   * 금전출납 정보 입력 유무체크
   *
   * @return {boolean} boolean
   */
  function isOptometryCustPayValidation() {
    const value = $container.find('#custPayId').val();
    const payYn = $container.find('#custPayYn').val();
    // 현금
    const cashPrice = Number(commonUtils.removeComma($container.find('#custCashPrice').val()));
    // 카드
    const cardPrice = Number(commonUtils.removeComma($container.find('#custCardPrice').val()));
    // 상품권
    const giftPrice = Number(commonUtils.removeComma($container.find('#custGiftPrice').val()));
    // 할인
    const salePrice = Number(commonUtils.removeComma($container.find('#custSalePrice').val()));
    // 총 금액
    const totalPrice = cashPrice + cardPrice + giftPrice;
    // 납부금액
    const payPrice = Number(commonUtils.removeComma($container.find('.pay-price').text()));
    // 납부상태
    const $payStatus = $container.find('#custPayStatus');

    if (commonUtils.isEmpty(value)) {
      connecteye.app.ui.alert.warning({
        text: '발주/반품 정보를 선택해주세요',
      });
      return false;
    }
    if (payYn !== 'N') {
      connecteye.app.ui.alert.warning({
        text: '납부 완료된 상품이에요',
      });
      return false;
    }
    if (totalPrice > payPrice) {
      connecteye.app.ui.alert.warning({
        text: '납부 가능금액을 초과 할 수 없어요',
      });
      return false;
    }

    // 납부상태 설정
    totalPrice === payPrice ? $payStatus.val('payCompleted') : $payStatus.val('payNotCompleted');

    // comma 삭제
    $container.find('#custCashPrice').val(cashPrice);
    $container.find('#custCardPrice').val(cardPrice);
    $container.find('#custGiftPrice').val(giftPrice);
    $container.find('#custSalePrice').val(salePrice);

    return true;
  }

  /**
   * 입력 트리거
   *
   * @param {Object} _this
   */
  function setInputChangeEventTrigger(_this) {
    const $totalInputPrice = $container.find('.total-input-price');
    const $payPrice = $container.find('.pay-price');
    const payYn = $container.find('#custPayYn').val();
    const payPrice = Number(commonUtils.removeComma($container.find('#custPayPrice').val()));
    const cashPrice = Number(commonUtils.removeComma($container.find('#custCashPrice').val()));
    const cardPrice = Number(commonUtils.removeComma($container.find('#custCardPrice').val()));
    const giftPrice = Number(commonUtils.removeComma($container.find('#custGiftPrice').val()));

    const { value, name } = _this.currentTarget;
    const currentValue = commonUtils.removeComma(value);

    // 납부 가능금액 체크
    if (payYn === 'Y') {
      connecteye.app.ui.alert.warning({
        text: '납부 완료된 상품이에요',
        icon: 'error',
      });
      $(_this.currentTarget).val('0');
      return false;
    }

    if (commonUtils.isNotEmpty(currentValue)) {
      if (commonUtils.isNumeric(currentValue)) {
        let totalPrice;
        const numValue = commonUtils.isNotEmpty(currentValue) ? Number(currentValue) : 0;
        switch (name) {
          case 'cashPrice':
            totalPrice = String(numValue + cardPrice + giftPrice);
            break;
          case 'cardPrice':
            totalPrice = String(numValue + cashPrice + giftPrice);
            break;
          case 'giftPrice':
            totalPrice = String(numValue + cashPrice + cardPrice);
            break;
          case 'salePrice':
            totalPrice = String(cardPrice + cashPrice + giftPrice);
            $payPrice.text(commonUtils.setComma(payPrice - numValue));
            break;
        }
        totalPrice = totalPrice.replace(/(^0+)/, '');
        $(_this.currentTarget).val(
          currentValue !== '0' ? commonUtils.setComma(currentValue.replace(/(^0+)/, '')) : '0',
        );
        $totalInputPrice.text(commonUtils.setComma(totalPrice));
      } else {
        connecteye.app.ui.alert.warning({
          text: '숫자만 입력해주세요.',
          icon: 'error',
        });
        $(_this.currentTarget).val('0');
      }
    }
  }

  /**
   * 초기화
   */
  function resetPage() {
    const $formOptometryCustPay = $('#formOptometryCustPay');
    commonUtils.formReset($formOptometryCustPay.find('input:not(#custPayDate)'), '0');
    $container.find('.total-input-price').text('0');
    $container.find('.rcv-price').text('0');
    optometryCustPayHistoryTable.grid.clear();
    $container.parent().scrollTop(0);
  }
  /***** Business End *****/
};
