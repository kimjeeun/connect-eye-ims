/*
 *  Description: 매출 검안매출 등록
 *  User: kyle
 *  Date: 2022-05-20
 */
import moment from 'moment';
import perfectscrollbar from 'perfect-scrollbar';
import { sumBy, some, isEqual } from 'lodash-es';

import gridUtils from '../../utils/gridUtils';
import commonUtils from '../../utils/commonUtils';
import searchBoxUtils from '../../utils/searchBoxUtils';
import barcodeUtils from '../../utils/barcodeUtils';

export default () => {
  /***** Static Start *****/
  const $container = $('#optometrySalesInfo');


  // 검안정보 지난 거래 기록 영역 스크롤바
  let optmtHistoryInfoScrollbar;

  // 바코드
  barcodeUtils.init({
    $inputElement: $container.find('#barcodeSearch'),
    $scanButtonElement: $container.find('#btnBarcodeScan'),
    callback: getStandardInfoByBarcode,
  });

  // table
  const optmtRegTable = new gridUtils.create({
    el: 'optmtRegTable',
    bodyHeight: 'auto',
    minBodyHeight: 120,
    rowHeaders: [],
    scrollX: 'false',
    scrollY: true,
    columns: [
      {
        header: '',
        name: '',
        width: 20,
        align: 'center',
        renderer: {
          type: gridUtils.deleteButtonRenderer,
          options: {
            isAmount: false,
          },
        },
      },
      {
        header: '사진',
        name: 'previewImgPath',
        width: 60,
        renderer: { type: gridUtils.setImageRenderer },
        align: 'center',
      },
      {
        header: '상품구분',
        name: 'prdtDiv',
        width: 100,
        align: 'center',
      },
      {
        header: '브랜드명',
        name: 'brnName',
        align: 'center',
      },
      {
        header: '상품명',
        name: 'prdtName',
        ellipsis: true,
      },
      {
        header: '단가',
        name: 'unitPrice',
        width: 100,
        align: 'center',
        formatter: commonUtils.setComma,
      },
      {
        header: '수량',
        name: 'amount',
        width: 80,
        align: 'center',
      },
      {
        header: '판매금액',
        name: 'price',
        width: 100,
        align: 'center',
        formatter: commonUtils.setComma,
      },
    ],
    summary: {
      height: 40,
      position: 'bottom',
      columnContent: {
        amount: {
          template(valueMap) {
            return `총 ${commonUtils.setComma(valueMap.sum)}개`;
          },
        },
        price: {
          template(valueMap) {
            return `${commonUtils.setComma(valueMap.sum)}원`;
          },
        },
      },
    },
    // 그리드 생성 완료 시
    onGridMounted: (_gridThis) => {
      // 그리드 레이아웃 새로고침
      _gridThis.instance.refreshLayout();
    },
    // 그리드 업데이트 시  'resetData', 'restore', 'reloadData', 'readData', 'setPerPage'
    onGridUpdated: (_gridThis) => {
      // 그리드 레이아웃 새로고침
      _gridThis.instance.refreshLayout();
      // hideLoadingBar
      _gridThis.instance.dispatch('setLoadingState', 'DONE');
    },
  });

  // table
  const custRelationTable = new gridUtils.create({
    el: 'custRelationTable',
    bodyHeight: 'auto',
    minBodyHeight: 120,
    rowHeaders: [],
    scrollX: 'false',
    scrollY: true,
    columns: [
      {
        header: '고객명',
        name: 'custName',
        align: 'center',
      },
      {
        header: '관계',
        name: 'custRelation',
        align: 'center',
        editor: {
          type: 'text',
        },
      },
      {
        header: '생년월일',
        name: 'birthday',
        align: 'center',
      },
    ],
    // 그리드 생성 완료 시
    onGridMounted: (_gridThis) => {
      // 그리드 레이아웃 새로고침
      _gridThis.instance.refreshLayout();
    },
    // 그리드 업데이트 시  'resetData', 'restore', 'reloadData', 'readData', 'setPerPage'
    onGridUpdated: (_gridThis) => {
      // 그리드 레이아웃 새로고침
      _gridThis.instance.refreshLayout();
      // hideLoadingBar
      _gridThis.instance.dispatch('setLoadingState', 'DONE');
    },
  });

  // 브랜드 검색박스 적용
  searchBoxUtils.create({
    $el: $container.find('#opBrnName'),
    title: '브랜드',
    placeholder: '브랜드-매입처',
    ajax: {
      url: '/api/standardInfo/getBrandList',
      data: (params) => ({ brnName: params.term }),
      processResults: (res) => {
        let result = searchBoxUtils.getResultCallback(res);
        return {
          results: result.map((obj) => ({ id: obj.brnId, text: obj.supBrnName, data: obj })),
        };
      },
      cache: true,
    },
    noAdd: true,
  });

  // 상품 검색박스 적용, 무한 스크롤 적용
  searchBoxUtils.create({
    $el: $container.find('#opPrdtName'),
    title: '상품',
    placeholder: '상품',
    ajax: {
      url: '/api/standardInfo/getStandardInfoSearchBoxProductList',
      data: (params) => ({
        supId: $('#opSupId').val(),
        brnId: $('#opBrnId').val(),
        prdtDivCode: $container.find('#opPrdtDivCode').val(),
        prdtName: params.term,
        page: params.page || 1,
        perPage: 10,
        isUse: 1,
      }),
      processResults: (res) => {
        let result = searchBoxUtils.getResultCallback(res);
        const { totalCount, page } = res;
        return {
          results: result.map((obj) => ({
            id: obj.supPrdtId,
            text: obj.prdtName,
            data: obj,
          })),
          pagination: {
            more: page * 10 < totalCount,
          },
        };
      },
      cache: false,
    },
    noAdd: true,
  });

  // 매출일자 선택
  connecteye.app.ui.createDatePicker({
    containerEl: '#optometrySalesInfo #salesDate',
    parentEl: '.sales-date-container',
    startDate: moment(),
    singleDatePicker: true,
    ranges: false,
    callback: () => {},
  });
  // 방문예정 선택
  connecteye.app.ui.createDatePicker({
    containerEl: '#optometrySalesInfo #willVisitDate',
    parentEl: '.visit-date-container',
    startDate: moment(),
    singleDatePicker: true,
    ranges: false,
    callback: () => {},
  });
  connecteye.app.ui.createDatePicker({
    containerEl: '#optometrySalesInfo #custWillVisitDate',
    parentEl: '.visit-date-container-2',
    startDate: moment(),
    singleDatePicker: true,
    ranges: false,
    callback: () => {},
  });

  commonUtils.setSelectboxCode({ el: 'opPrdtDivCode', code: 'PO100', defaultTxt: '선택' });
  commonUtils.setSelectboxCode({ el: 'odBase', code: 'BS100', defaultTxt: '선택' });
  commonUtils.setSelectboxCode({ el: 'osBase', code: 'BS100', defaultTxt: '선택' });
  /***** Static End *****/

  /***** Event Start *****/
  // 검안매출 등록 화면 dom 로드 시
  $container.find('.card[data-area=optmtReg]').on('loadDom', (_evt, _data) => {});

  // 최근 검안 버튼 클릭 이벤트
  $container.find('#btnRecentOptInfo').on('click', () => getRecentOptometryInfo());

  // 가족관계 등록 버튼 이벤트
  $container.find('#btnRegCustRelation').on('click', () => setCustRelationGrid());

  // 그리드 사진클릭 이벤트
  optmtRegTable.grid.on('click', (_evt) =>
    gridUtils.setSelectPictureHandler(_evt, 'previewImgPath', 'originImgPath'),
  );

  // 고객 선택 이벤트
  $container.find('#custAll').on('change', (_evt) => {
    const $target = $(_evt.currentTarget);

    // 고객아이디가 있으면
    if (commonUtils.isNotEmpty($target.val())) {
      // 초기화 후
      resetInfo();
      resetPage();

      // 고객정보 불러옴
      getOptometryHistoryInfo({
        custId: $target.val(),
      });
      $container.find('.btn-cash-payment').prop('disabled', false);
    }
  });

  // 고객 정보 적용 버튼 클릭 이벤트
  $container.find('#btnSaveCustRelation').on('click', () => saveCustInfo());

  // 고객 주소 검색 버튼 클릭 이벤트
  $container.find('#formCustInfo .search').on('click', () => showDaumPost());
  $container.find('#custAddress').on('keyup', (_evt) => {
    if (_evt.keyCode === 13) {
      showDaumPost();
    }
  });

  // 등록 버튼 클릭 시 이벤트
  $container.find('#btnOptometryPay').on('click', () => {
    if (isOptometrySalesInfoValidation()) {
      runInsert();
    }
  });

  // 수정 버튼 클릭 시 이벤트
  $container.find('#btnOptometryPayModify').on('click', () => {
    if (isOptometrySalesInfoValidation()) {
      runUpdate();
    }
  });

  // 삭제 버튼 클릭 시 이벤트
  $container.find('#btnOptometryPayDelete').on('click', () => runDelete());

  // 상품정보 수량 Change 이벤트
  $container.find('#opAmount').on('keyup', (_evt) => {
    // const value = Number(_evt.currentTarget.value);
    // const $currentAmount = $container.find('.current-amount');
    // const currentAmount = Number($currentAmount.text());
    //
    // // 입력한 값이 현재 적용 가능수량이랑 비교
    // if (value > currentAmount) {
    //   _evt.currentTarget.value = '0';
    //   connecteye.app.ui.alert.warning({
    //     text: `${currentAmount}개를 초과 할수 없어요`,
    //   });
    //   return false;
    // }
  });

  // 거래내역 정보 입력 후 적용 버튼 클릭 시 이벤트
  $container.find('#btnSalesSave').on('click', () => addRegData());

  // 매출처 선택 이벤트
  $container.find('#opSupName').on('change', (_evt) => {
    const resultObj = searchBoxUtils.getResultObject($(_evt.currentTarget));
    if (resultObj) {
      const { supId } = resultObj;
      // 매출처 아이디
      $container.find('input[name=opSupId]').val(supId);
    }
    selectEventHandler();
  });

  // 브랜드 선택 이벤트
  $container.find('#opBrnName').on('change', (_evt) => {
    let resultObj = searchBoxUtils.getResultObject($(_evt.currentTarget));
    if (resultObj) {
      const { supId, brnId } = resultObj;
      // 매입처 아이디
      $container.find('input[name=opSupId]').val(supId);
      // 브랜드 아이디
      $container.find('input[name=opBrnId]').val(brnId);
    }
    selectEventHandler();
  });

  // 상품 선택 이벤트
  $container.find('#opPrdtName').on('change', (_evt) => {
    const resultObj = searchBoxUtils.getResultObject($(_evt.currentTarget));
    if (resultObj) {
      const { supPrdtId, barcode, salesPrice, currentAmount } = resultObj;
      // 기준정보 아이디
      $container.find('input[name=opSupPrdtId]').val(supPrdtId);
      // 바코드
      $container.find('input[name=opBarcode]').val(barcode);
      // 판매단가
      $container.find('input[name=opUnitPrice]').val(commonUtils.setComma(salesPrice));
      // 수량
      $container.find('input[name=opAmount]').val(1);
      // 판매금액
      $container.find('input[name=opPrice]').val(commonUtils.setComma(salesPrice));
      // 현재수량
      $container.find('.current-amount').text(currentAmount || 0);
    }
    selectEventHandler();
  });

  // 검안정보 입력 이벤트
  $container.find('.optometry:not(textarea)').on({
    keydown: setOptometryInputEventHandler,
    focusin: (_evt) => {
      const $element = $(_evt.currentTarget);
      if (!Number($element.val())) {
        $element.val('');
      }
    },
    focusout: (_evt) => {
      const $element = $(_evt.currentTarget);
      if (commonUtils.isEmpty($element.val())) {
        $element.val('0');
      }
    },
  });

  // 결제정보
  $container.find('.pay-control').on({
    focusin: (_evt) => {
      const $element = $(_evt.currentTarget);
      if (commonUtils.isEmpty($element.val())) {
        $element.val('');
      }
    },
    focusout: (_evt) => {
      const $element = $(_evt.currentTarget);
      if (commonUtils.isEmpty($element.val())) {
        $element.trigger('keyup');
      }
    },
  });

  // 단가, 수량 입력 이벤트
  $container.find('#opPrice').on({
    keyup: commonUtils.setPriceEventHandler,
    change: commonUtils.setPriceEventHandler,
  });
  $container.find('#opUnitPrice').on({
    keyup: commonUtils.setPriceEventHandler,
    change: commonUtils.setPriceEventHandler,
  });
  $container.find('#opUnitPrice').on('keyup', setInputAmountChangeEventTrigger);
  $container.find('#opAmount').on('keyup', setInputAmountChangeEventTrigger);

  // 납부정보 입력 금액 이벤트
  $container.find('#cashPrice').on({
    keyup: commonUtils.setPriceEventHandler,
    change: commonUtils.setPriceEventHandler,
  });
  $container.find('#cardPrice').on({
    keyup: commonUtils.setPriceEventHandler,
    change: commonUtils.setPriceEventHandler,
  });
  $container.find('#giftPrice').on({
    keyup: commonUtils.setPriceEventHandler,
    change: commonUtils.setPriceEventHandler,
  });
  $container.find('#salePrice').on({
    keyup: commonUtils.setPriceEventHandler,
    change: commonUtils.setPriceEventHandler,
  });

  // 고객정보 이벤트
  $container.find('#custTelOne, #custTelTwo').on('keyup', function () {
    $(this).val(commonUtils.setPhoneNumber(this.value));
  });
  $container.find('#custBirthday').on('change', function () {
    commonUtils.setBirthdayNumber(this);
    $container.find('.cust-age').text(`만 ${getBirthdayToAge(this.value)}세`);
  });

  // 신규 반품 버튼 선택
  $container.find('.btn-optmt-reg').on('click', (_evt) => {
    $(_evt.currentTarget).siblings().removeClass('active').end().addClass('active');
    $container.find('.list-group-item').removeClass('active');
    resetInfo();
    resetPage();
  });

  // 현재 검안정보로 등록
  $container.find('#btnCurrentOptInfo').on('click', () => {
    resetInfo();
    resetPage(true);
  });

  // 바코드로 기준정보 검색 버튼 이벤트
  $container.find('#formSales .search-icon').on('click', () => {
    // 바코드
    const barcode = $container.find('#barcodeSearch').val();

    // 바코드로 기준정보 가져오기
    getStandardInfoByBarcode({ barcode: barcode });
  });
  $container.find('#barcodeSearch').on('keyup', (_evt) => {
    if (_evt.keyCode === 13) {
      // 바코드
      const barcode = $(_evt.currentTarget).val();

      // 바코드로 기준정보 가져오기
      getStandardInfoByBarcode({ barcode: barcode });
    }
  });

  // 납부정보 입력 이벤트
  $container.find('#cashPrice').on('keyup', setInputChangeEventTrigger);
  $container.find('#cardPrice').on('keyup', setInputChangeEventTrigger);
  $container.find('#giftPrice').on('keyup', setInputChangeEventTrigger);
  $container.find('#salePrice').on('keyup', setInputChangeEventTrigger);
  /***** Event End *****/

  /***** Business Start *****/
  /**
   * 매출서 정보 목록 가져오기
   *
   * @param {Object} _data
   */
  function getSalesInfoList(_data) {
    connecteye.app.ui.showCardLoading();
    commonUtils.callAjax({
      type: 'GET',
      url: '/api/optometrySalesInfo/getSalesInfoList',
      paramData: _data,
      callback: async (_res) => {
        const { salesInfoList } = _res;
        await setSalesInfoList(salesInfoList);
        await connecteye.app.ui.hideCardLoading();
      },
    });
  }

  /**
   * 고객 정보 세팅
   *
   * @param {Array} _custList
   * @param {Array} _salesInfoList
   */
  function setCustList(_custList, _salesInfoList) {
    const {
      address,
      birthday,
      custId,
      custName,
      detailAddress,
      email,
      gender,
      memo,
      prvEyeCode,
      telOne,
      telTwo,
      willVisitDate,
    } = _custList[0];

    // 고객 검색 이름
    $container.find('#custAll').val(custName);
    $container.find('#custId').val(custId);

    // 고객 정보 넣어줌
    $container
      .find('#custName')
      .val(custName)
      .end()
      .find('#custGender')
      .val(gender)
      .end()
      .find('#custTelOne')
      .val(telOne)
      .end()
      .find('#custTelTwo')
      .val(telTwo)
      .end()
      .find('#custBirthday')
      .val(birthday)
      .end()
      .find('.cust-age')
      .text(`만 ${getBirthdayToAge(birthday)}세`)
      .end()
      .find('#custMemo')
      .val(memo)
      .end()
      .find('#custAddress')
      .val(address)
      .end()
      .find('#custAddrDetail')
      .val(detailAddress)
      .end()
      .find('#custEmail')
      .val(email)
      .end()
      .find('#custPrvEyeCode')
      .val(prvEyeCode)
      .end()
      .find('#custWillVisitDate')
      .val(willVisitDate)
      .end();

    // 총 미수금액
    const $custRcvPrice = $container.find('.total-rcv-price');
    const totalRcvPrice = sumBy(_salesInfoList, 'rcvPrice');

    $custRcvPrice.text(`${commonUtils.setComma(totalRcvPrice)}`);

    // 적용버튼 disabled 해제
    $container.find('#btnSaveCustRelation').prop('disabled', false);
  }

  /**
   * 매출서 목록 세팅
   *
   * @param {Array} _salesInfoList
   */
  function setSalesInfoList(_salesInfoList) {
    // 고객분석 버튼
    const $btnCustAnalysis = $container.find('.btn-cust-analysis');

    // 결과 목록 있는지 여부
    if (commonUtils.isEmpty(_salesInfoList)) {
      // 결과 목록이 없으면 empty 화면 및 고객분석 버튼 비활성화
      $btnCustAnalysis.prop('disabled', true);
    } else {
      // 결과 목록 세팅
      optmtRegTable.grid.clear();

      _salesInfoList.forEach(async (item, index) => {
        await getSalesInfo(item, index);
        await getOptometryInfoElement(item, index);
      });

      // 결과 목록 보이기
      $btnCustAnalysis.prop('disabled', false);
    }
  }

  /**
   * 매출서 정보 세팅
   *
   * @param {Object} _param
   * @param {Number} _index
   */
  function getSalesInfo(_param, _index) {
    // 매출서 내역
    const { salesdetailinfolist } = _param;

    // 매출서 정보
    const {
      salesId,
      totalPrice,
      sumPayPrice,
      sumSalePrice,
      sumCardPrice,
      sumCashPrice,
      sumGiftPrice,
      rcvPrice,
      regDate,
      mngName,
      memo,
    } = _param;

    if (commonUtils.isNotEmpty(salesdetailinfolist)) {

      salesdetailinfolist.forEach(item => optmtRegTable.grid.appendRow(item));

      $container
        .find('#sumPrice').val(commonUtils.setComma(totalPrice)).end()
        .find('#payPrice').val(commonUtils.setComma(sumPayPrice)).end()
        .find('#paySalePrice').val(commonUtils.setComma(sumSalePrice)).end()
        .find('#salePrice').val(commonUtils.setComma(sumSalePrice)).end()
        .find('#cardPrice').val(commonUtils.setComma(sumCardPrice)).end()
        .find('#cashPrice').val(commonUtils.setComma(sumCashPrice)).end()
        .find('#giftPrice').val(commonUtils.setComma(sumGiftPrice)).end()
        .find('#rcvPrice').val(commonUtils.setComma(rcvPrice)).end()
        .find('#salesId').val(salesId).end()
        .find('#originRcvPrice').val(totalPrice).end()
        .find('#salesDate').val(regDate).end()
        .find('#memo').val(memo).end()
        .find('#mngName').val(mngName).end();
        // .find('#willVisitDate').val(commonUtils.setComma(mngName)).end()
    }
  }

  /**
   * 검안정보 element 세팅
   *
   * @param {Object} _param
   * @param {Number} _index
   */
  function getOptometryInfoElement(_param, _index) {
    // 매출서 내역, 검안 정보
    const { optometryinfolist } = _param;

    setRecentOptometryInfo({ data: optometryinfolist, type: 'od' });
    setRecentOptometryInfo({ data: optometryinfolist, type: 'os' });
    $container.find('textarea[name=comment]').val(optometryinfolist[1].comment);
  }

  /**
   * 고객 가족관계 정보 가져오기
   *
   */
  function getCustFamilyInfo() {
    commonUtils.callAjax({
      type: 'GET',
      url: '/api/optometrySalesInfo/getCustFamilyInfo',
      paramData: {
        custId: $container.find('#custId').val(),
      },
      callback: (_res) => {
        if (commonUtils.isNotEmpty(_res)) {
          // 그리드에 데이터 넣음
          custRelationTable.grid.resetData(_res);
          // hideLoadingBar
          custRelationTable.grid.dispatch('setLoadingState', 'DONE');
        } else {
          custRelationTable.grid.dispatch('setLoadingState', 'EMPTY');
        }
      },
    });
  }

  /**
   * 고객, 가족관계 정보 저장
   *
   */
  function saveCustInfo() {
    const $optometryCustContainer = $container.find('.optometry-cust-container');

    // 고객 정보
    const custIdInfo = { custId: $container.find('#custId').val() };
    const custInfoObj = commonUtils.serializeObject($('#formCustInfo'));
    const custInfo = Object.assign(custIdInfo, custInfoObj);

    // 고객 가족관계 정보
    custRelationTable.grid.finishEditing();
    const custFamilyInfo = custRelationTable.grid.getData();

    $optometryCustContainer.addClass('is-loading');
    commonUtils.callAjax({
      type: 'POST',
      url: '/api/optometrySalesInfo/saveCustFamilyInfo',
      paramData: {
        custInfo: JSON.stringify(custInfo),
        custFamilyInfo: JSON.stringify(custFamilyInfo),
      },
      callback: () => {
        $optometryCustContainer.removeClass('is-loading');
      },
    });
  }

  /**
   * 지난 거래 기록 가져오기
   *
   * @param {Object} _data
   */
  function getOptometryHistoryInfo(_data) {
    connecteye.app.ui.showCardLoading();
    commonUtils.callAjax({
      type: 'GET',
      url: '/api/optometrySalesInfo/getSalesInfoList',
      paramData: _data,
      callback: async (_res) => {
        const { custList, salesInfoList } = _res;

        // 고객정보 세팅
        await setCustList(custList, salesInfoList);

        // 고객 가족관계 세팅
        await getCustFamilyInfo();

        // 지난 거래기록 세팅
        await setOptometryHistoryInfo(salesInfoList);
        connecteye.app.ui.hideCardLoading();
      },
    });
  }

  /**
   * 지난 거래 기록 세팅
   *
   * @param {Object} _salesInfoList
   */
  function setOptometryHistoryInfo(_salesInfoList) {

    // 매출서 목록 컨테이너
    const $salesInfoContainer = $container.find('.optometry-history-info');

    // 매출서 목록 초기화
    $salesInfoContainer.find('li').remove();
    $salesInfoContainer.find('div').not('.optometry-history-nodata').remove();
    $salesInfoContainer.find('.optometry-history-nodata').removeClass('d-none');

    _salesInfoList.forEach((item) => {
      // 매출서 기본정보
      const { regDate, salesId, custId, salesType } = item;

      // 매출서 내역
      const { salesdetailinfolist } = item;
      if (commonUtils.isNotEmpty(salesdetailinfolist)) {
        const $listGroupItem = $('<li/>', { class: 'list-group-item list-group-item-action' })
          .on('click', (_evt) => {
            const $currentElement = $(_evt.currentTarget);

            // 매출서 정보 가져오기
            getSalesInfoList({ custId, salesId });

            // UI
            $currentElement
              .siblings().removeClass('active').end()
              .addClass('active');
            $container
              .find('#btnSalesSave').prop('disabled', true).end()
              .find('.btn-cash-payment').prop('disabled', false).end()
              .find('.btn-optmt-reg').removeClass('active').end()
              .find('#btnOptometryPay').addClass('d-none').end()
              .find('#btnOptometryPayModify').removeClass('d-none').end()
              .find('#btnOptometryPayDelete').removeClass('d-none');

            // 그리드 삭제버튼 가리기
            optmtRegTable.grid.hideColumn('');
          })
          .text(`${regDate} (${salesType})`);

        $salesInfoContainer.append($listGroupItem);

        $salesInfoContainer.find('.optometry-history-nodata').addClass('d-none');
      }
    });

    if (optmtHistoryInfoScrollbar) {
      optmtHistoryInfoScrollbar.destroy();
      optmtHistoryInfoScrollbar = new perfectscrollbar($salesInfoContainer[0]);
    } else {
      optmtHistoryInfoScrollbar = new perfectscrollbar($salesInfoContainer[0])
    }
  }

  /**
   *  거래내역 추가
   */
  function addRegData() {
    const $currentAmount = $container.find('.current-amount');
    const currentAmount = Number($currentAmount.text());
    const inputAmount = Number($container.find('#opAmount').val());
    const prdtName = $container.find('#select2-opPrdtName-container').text();

    // 가능 수량 체크
    // if (currentAmount <= 0) {
    //   // form 전송 ajax 처리
    //   connecteye.app.ui.alert
    //     .confirm({
    //       title: '출고량 확인',
    //       html:
    //         `<span class="text-danger font-weight-bold">재고를 초과한 제품이 있습니다</span><br>` +
    //         `<span class="text-danger font-weight-bold mb-2">출고 처리 하시겠습니까?</span><br>` +
    //         `<span></span><br>` +
    //         `<span class="font-weight-bold">상품명 : ${prdtName}</span>`,
    //       confirmBtnTxt: '확인',
    //       cancelBtnTxt: '취소',
    //       preConfirm: () => {},
    //     })
    //     .then((_result) => {
    //       if (_result.isConfirmed) {
    //         setRegDataInfo();
    //       }
    //     });
    //   return false;
    // }

    // 검안매출 상품 데이터 세팅
    setRegDataInfo();
  }

  /**
   * 검안매출 상품 데이터 세팅
   */
  function setRegDataInfo() {
    const obj = searchBoxUtils.getResultObject($('#opPrdtName'));

    const amount = Number($('#opAmount').val());
    const price = Number(commonUtils.removeComma($('#opPrice').val()));
    const unitPrice = Number(commonUtils.removeComma($('#opUnitPrice').val()));

    // 오브젝트 null 체크
    if (commonUtils.isNotEmpty(obj)) {
      obj.price = price || obj.salesPrice;
      obj.unitPrice = unitPrice || obj.purPrice;
      obj.amount = amount || 1;
      optmtRegTable.grid.appendRow(obj);

      // 미수금
      const totalRcvPrice = sumBy(optmtRegTable.grid.getData(), 'price');
      $container
        .find('#sumPrice')
        .val(commonUtils.setComma(totalRcvPrice))
        .end()
        .find('#originRcvPrice')
        .val(totalRcvPrice);

      // 매입처, 브랜드, 상품정보
      resetInfo();

      return true;
    } else {
      connecteye.app.ui.alert.warning({
        text: '매입처, 브랜드, 상품 정보를 입력해주세요',
      });
    }
  }

  /**
   * 단가, 수량 입력 트리거
   *
   * @param {Object} _this
   */
  function setInputAmountChangeEventTrigger(_this) {
    const $price = $container.find('#opPrice');
    const amount = Number($container.find('#opAmount').val());
    const unitPrice = Number(commonUtils.removeComma($container.find('#opUnitPrice').val()));

    const { value, name } = _this.currentTarget;
    const currentValue = commonUtils.removeComma(value);

    if (commonUtils.isNotEmpty(currentValue)) {
      if (commonUtils.isNumeric(currentValue)) {
        let price;
        const numValue = commonUtils.isNotEmpty(currentValue) ? parseInt(currentValue) : 0;
        switch (name) {
          case 'opUnitPrice':
            price = String(amount * numValue);
            break;
          case 'opAmount':
            price = String(unitPrice * numValue);
            break;
        }
        $(_this.currentTarget).val(commonUtils.setComma(currentValue.replace(/(^0+)/, '')));
        $price.val(commonUtils.setComma(price.replace(/(^0+)/, '')));
      } else {
        connecteye.app.ui.alert.warning({
          text: '숫자만 입력해주세요.',
          icon: 'error',
        });
        $(_this.currentTarget).val('0');
      }
    }
  }

  /**
   * 입력 트리거
   *
   * @param {Object} _this
   */
  function setInputChangeEventTrigger(_this) {
    const $payPrice = $container.find('#payPrice');
    const $rcvPrice = $container.find('#rcvPrice');
    const $paySalePrice = $container.find('#paySalePrice');

    const originRcvPrice = Number($container.find('#originRcvPrice').val());
    const payPrice = Number(commonUtils.removeComma($payPrice.val()));
    const cashPrice = Number(commonUtils.removeComma($container.find('#cashPrice').val()));
    const cardPrice = Number(commonUtils.removeComma($container.find('#cardPrice').val()));
    const giftPrice = Number(commonUtils.removeComma($container.find('#giftPrice').val()));
    const salePrice = Number(commonUtils.removeComma($container.find('#salePrice').val()));

    const { value, name } = _this.currentTarget;
    const currentValue = Number(commonUtils.removeComma(value)) || 0;

    // 숫자 체크
    if (!commonUtils.isNumeric(currentValue)) {
      connecteye.app.ui.alert.warning({
        text: '숫자만 입력해주세요.',
        icon: 'error',
      });
      $(_this.currentTarget).val('0');
      return false;
    }

    if (!currentValue) {
      $payPrice.val(commonUtils.setComma(cardPrice + cashPrice + giftPrice));
      $rcvPrice.val(commonUtils.setComma(originRcvPrice - payPrice));
      $(_this.currentTarget).val('0');

      // 지불 0원 처리
      if (name === 'salePrice') {
        $paySalePrice.val('0');
      }
      return false;
    }

    let totalPrice;
    switch (name) {
      case 'cashPrice':
        totalPrice = String(currentValue + cardPrice + giftPrice + salePrice);
        break;
      case 'cardPrice':
        totalPrice = String(currentValue + cashPrice + giftPrice + salePrice);
        break;
      case 'giftPrice':
        totalPrice = String(currentValue + cashPrice + cardPrice + salePrice);
        break;
      case 'salePrice':
        totalPrice = String(currentValue + cardPrice + cashPrice + giftPrice);
        $paySalePrice.val(commonUtils.setComma(currentValue));
        break;
    }
    totalPrice = totalPrice.replace(/(^0+)/, '');
    $(_this.currentTarget).val(
      currentValue ? commonUtils.setComma(String(currentValue).replace(/(^0+)/, '')) : 0,
    );
    $payPrice.val(commonUtils.setComma(totalPrice - salePrice));
    $rcvPrice.val(commonUtils.setComma(originRcvPrice - totalPrice));
  }

  /**
   * 선택 이벤트 핸들러
   */
  function selectEventHandler() {
    const supId = $('#opSupId').val();
    const brnId = $('#opBrnId').val();
    if (commonUtils.isEmpty(supId) || commonUtils.isEmpty(brnId)) {
      return false;
    }
    $container.find('.product-control:not(#opBarcode)').prop('disabled', false);
  }

  /**
   * 최근 검안 데이터 가져오기
   */
  function getRecentOptometryInfo() {
    if (!isCustValue()) {
      return false;
    }
    commonUtils.callAjax({
      type: 'GET',
      url: '/api/optometrySalesInfo/getOptometryInfoList',
      paramData: { custId: $container.find('#custId').val() },
      callback: (_data) => {
        if (commonUtils.isEmpty(_data)) {
          connecteye.app.ui.alert.warning({
            text: `검안정보가 없어요`,
          });
          $container.find('.table-optometry input').val('0');
          $container.find('.table-optometry textarea').val('');
          return false;
        }
        setRecentOptometryInfo({ data: _data, type: 'od' });
        setRecentOptometryInfo({ data: _data, type: 'os' });
        $container.find('textarea[name=comment]').val(_data[1].comment);
      },
    });
  }

  /**
   * 최근검안 데이터 넣어주기
   *
   * @param {Object} _param {data, type 'od', 'os'}
   */
  function setRecentOptometryInfo(_param) {
    const { data, type } = _param;

    // 검안 우안 좌안 테이블영역에 데이터 넣어주기
    $container.find(`.optometry-${type}`).each((index, item) => {
      const odData = data.filter((item) => item.oculusType === `${type.toUpperCase()}`)[0];
      const { name } = item;
      let value = odData[name];

      switch (name) {
        case 'sph':
        case 'cyl':
        case 'CCyl':
        case 'CSph':
        case 'add':
        case 'CAdd':
        case 'CBc':
          value = value ? odData[name].toFixed(2) : 0;
          break;
        case 'base':
          value = odData.baseCode;
          break;
        case 'nakedEye':
        case 'correction':
        case 'CDia':
          value = value ? odData[name].toFixed(1) : 0;
          break;
      }
      $container.find(`.optometry-${type}[name=${name}]`).val(value);
    });
  }

  /**
   * 추가 실행
   */
  function runInsert() {
    // form 전송 ajax 처리
    connecteye.app.ui.alert
      .confirm({
        title: '거래내역을 추가할까요?',
        text: '취소를 누르면 내용을 수정 할 수 있어요',
        confirmBtnTxt: '추가',
        cancelBtnTxt: '취소',
        showLoaderOnConfirm: true,
        preConfirm: () => {},
      })
      .then((_result) => {
        if (_result.isConfirmed) {
          insertOptometrySalesInfo();
        } else {
          connecteye.app.ui.alert.error({
            title: '추가 취소',
            text: '거래내역 수정 후 다시 추가 할 수 있어요 :)',
            icon: 'error',
          });
        }
      });
  }

  /**
   * 검안매출 거래내역 추가 ajax
   */
  function insertOptometrySalesInfo() {
    const optmtSalesInfo = optmtRegTable.grid.getData();

    // 검안정보 OD, OS
    const optometryOdInfo = convertOptometryInfoToObject('od');
    const optometryOsInfo = convertOptometryInfoToObject('os');
    const optometryList = Array(optometryOdInfo, optometryOsInfo);

    // 검안매출 납부정보
    const optometryPayInfo = commonUtils.serializeObject($('#formCashPaymentOne'));

    connecteye.app.ui.showCardLoading();
    commonUtils.callAjax({
      type: 'POST',
      url: '/api/optometrySalesInfo/getInsertOptometrySaleInfo',
      paramData: {
        custId: $container.find('#custId').val(),
        salesTypeCode: $container.find('.btn-optmt-reg.active').val() || 'SL101',
        optmtSalesInfo: JSON.stringify(optmtSalesInfo),
        optometryList: JSON.stringify(optometryList),
        optometryPayInfo: JSON.stringify(optometryPayInfo),
      },
      callback: (_res) => {
        connecteye.app.ui.hideCardLoading();
        connecteye.app.ui.alert
          .success({
            title: '추가 완료!',
            text: '입력한 검안매출 거래내역이 추가됐어요',
            showDenyButton: false,
          })
          .then((_SuccessResult) => {
            if (_SuccessResult.isConfirmed) {
              // 거래내역 정보 초기화
              resetInfo();
              resetPage();

              // 거래내역 목록 조회
              const custId = $container.find('#custId').val();
              getOptometryHistoryInfo({ custId });
            }
          });
      },
    });
  }

  /**
   * 수정 실행
   */
  function runUpdate() {
    // form 전송 ajax 처리
    connecteye.app.ui.alert
      .confirm({
        title: '거래내역을 수정할까요?',
        text: '취소를 누르면 내용을 수정 할 수 있어요',
        confirmBtnTxt: '수정',
        cancelBtnTxt: '취소',
        showLoaderOnConfirm: true,
        preConfirm: () => {},
      })
      .then((_result) => {
        if (_result.isConfirmed) {
          updateOptometrySalesInfo();
        }
      });
  }

  /**
   * 검안매출 거래내역 수정 ajax
   */
  function updateOptometrySalesInfo() {
    // 검안매출 납부정보
    const optometryPayInfo = commonUtils.serializeObject($('#formCashPaymentOne'));

    connecteye.app.ui.showCardLoading();
    commonUtils.callAjax({
      type: 'POST',
      url: '/api/optometrySalesInfo/updateOptometrySales',
      paramData: optometryPayInfo,
      callback: (_res) => {
        connecteye.app.ui.hideCardLoading();
        connecteye.app.ui.alert
          .success({
            title: '수정 완료!',
            text: '입력한 검안매출 거래내역이 수정됐어요',
            showDenyButton: false,
          })
          .then((_SuccessResult) => {
            if (_SuccessResult.isConfirmed) {
              // 거래내역 목록 조회
              const custId = $container.find('#custId').val();
              getOptometryHistoryInfo({ custId });
            }
          });
      },
    });
  }

  /**
   * 검안 매출 삭제 실행
   */
  function runDelete() {
    connecteye.app.ui.alert
      .confirm({
        title: '매출 삭제를 실행할까요?',
        text: '매출에 관련된 정보가 모두 삭제돼요',
        confirmBtnTxt: '실행',
        cancelBtnTxt: '취소',
        showLoaderOnConfirm: true,
        preConfirm: () => {},
      })
      .then((_result) => {
        if (_result.isConfirmed) {
          deleteOptometrySales();
        }
      });
  }

  /**
   * 검안 매출 삭제
   */
  function deleteOptometrySales() {
    connecteye.app.ui.showCardLoading();
    commonUtils.callAjax({
      type: 'POST',
      url: '/api/optometrySalesInfo/deleteOptometrySales',
      paramData: {
        salesId: $container.find('#salesId').val(),
      },
      callback: () => {
        connecteye.app.ui.hideCardLoading();
        connecteye.app.ui.alert
          .success({
            title: '매출 삭제 완료!',
            text: '선택한 매출정보가 삭제됐어요',
            showDenyButton: false,
          })
          .then((_SuccessResult) => {
            if (_SuccessResult.isConfirmed) {
              // 검안매출 조회
              const custId  = $container.find('#custId').val();
              getSalesInfoList({ custId });
            }
          });
      },
    });
  }

  /**
   * 검안매출 정보 입력 유무체크
   *
   * @return {boolean} boolean
   */
  function isOptometrySalesInfoValidation() {
    const cashPrice = Number(commonUtils.removeComma($container.find('#cashPrice').val()));
    const cardPrice = Number(commonUtils.removeComma($container.find('#cardPrice').val()));
    const salePrice = Number(commonUtils.removeComma($container.find('#salePrice').val()));
    const giftPrice = Number(commonUtils.removeComma($container.find('#giftPrice').val()));
    const sumPrice = Number(commonUtils.removeComma($container.find('#sumPrice').val())) - salePrice;
    const totalPrice = cashPrice + cardPrice + giftPrice;

    // 납부상태
    const $payStatus = $container.find('#payStatus');

    if (!isCustValue()) {
      return false;
    }

    if (commonUtils.isEmpty(optmtRegTable.grid.getData())) {
      connecteye.app.ui.alert.warning({
        text: '거래 내역을 입력해주세요',
      });
      return false;
    }

    if (commonUtils.isEmptyInput($('#formCashPaymentOne'), ['mngName', 'memo'])) {
      connecteye.app.ui.alert.warning({
        text: '납부 정보를 입력해주세요',
      });
      return false;
    }

    if (totalPrice > sumPrice) {
      connecteye.app.ui.alert.warning({
        text: '납부 가능금액을 초과 할 수 없어요',
      });
      return false;
    }

    // 납부상태 설정
    totalPrice === sumPrice ? $payStatus.val('payCompleted') : $payStatus.val('payNotCompleted');

    // comma 삭제
    $container.find('#cashPrice').val(cashPrice);
    $container.find('#cardPrice').val(cardPrice);
    $container.find('#salePrice').val(salePrice);
    $container.find('#giftPrice').val(giftPrice);

    return true;
  }

  /**
   * 고객선택 유무
   */
  function isCustValue() {
    const custValue = $container.find('#custId').val();
    if (commonUtils.isEmpty(custValue)) {
      connecteye.app.ui.alert.warning({
        text: `고객을 선택해주세요`,
      });
      return false;
    }
    return true;
  }

  /**
   * 검안정보 -> 오브젝트
   *
   * @param {Object} _type
   */
  function convertOptometryInfoToObject(_type) {
    let obj = null;
    const elements = $container.find(`.optometry-${_type}`);

    let arr = elements.serializeArray();
    if (arr) {
      obj = { oculusType: _type.toUpperCase() };
      for (let formData of arr) {
        // 빈 값이 아닐경우
        const { name, value } = formData;
        if (value) {
          obj[name] = value;
        }
      }
    }
    return obj;
  }

  /**
   * 검안정보 input 핸들러
   */
  async function setOptometryInputEventHandler(_evt) {
    if (_evt.keyCode === 9 || _evt.keyCode === 13) {
      await setOptometryInfoEventHandler(_evt);

      if (_evt.keyCode !== 9) {
        const $currentTarget = $(_evt.currentTarget);
        const currentTabindex = $currentTarget.attr('tabindex');
        const $nextTab = $container.find(`.optometry[tabindex=${Number(currentTabindex) + 1}]`);

        $nextTab.focus().val('');
      }
    }
  }

  /**
   * 검안정보 input 세팅
   */
  function setOptometryInfoEventHandler(_evt) {
    const $currentTarget = $(_evt.currentTarget);
    const currentTargetName = $currentTarget.attr('name');
    const currentTargetDataValue = $currentTarget.attr('data-value');
    let currentTargetValue = $currentTarget.val();
    let changeValue;

    if (currentTargetValue !== currentTargetDataValue) {
      $currentTarget.attr('data-status', 'isEmpty');
    }

    if (Number(currentTargetValue) && $currentTarget.attr('data-status') === 'isEmpty') {
      switch (currentTargetName) {
        // 300 -> -3.00, 2000 -> -20.00
        case 'sph':
        case 'cyl':
        case 'CCyl':
        case 'CSph':
          currentTargetValue = currentTargetValue.replace('-', '');
          changeValue = currentTargetValue.includes('+')
            ? `+${(currentTargetValue / 100).toFixed(2)}`
            : `-${(currentTargetValue / 100).toFixed(2)}`;
          break;

        // 0 ~ 180
        case 'axis':
        case 'CAxis':
          changeValue =
            Number(currentTargetValue) < 0 || Number(currentTargetValue) > 180
              ? '0'
              : currentTargetValue;
          break;

        // 300 -> +3.00, 2000 -> +20.00
        case 'add':
        case 'CAdd':
          changeValue = `+${(currentTargetValue / 100).toFixed(2)}`;
          break;

        // 3 -> 3.00, 20 -> 20.00
        case 'CBc':
          changeValue = `${Number(currentTargetValue).toFixed(2)}`;
          break;

        // 10 -> 10.0, 200 -> 20.0
        case 'CDia':
          changeValue = `${Number(currentTargetValue).toFixed(1)}`;
          break;

        default:
          changeValue = currentTargetValue;
          break;
      }

      $currentTarget.attr('data-status', 'isNotEmpty').attr('data-value', changeValue);
    } else {
      changeValue = currentTargetValue;
    }

    if (Number(currentTargetValue) === 0) {
      $currentTarget.attr('data-status', 'isEmpty');
      changeValue = '0';
      return false;
    }

    $currentTarget.val(changeValue);
  }

  /**
   * 나이 계산
   *
   * @param {String} _birthday 'YYYY-MM-DD'
   */
  function getBirthdayToAge(_birthday) {
    if (commonUtils.isEmpty(_birthday)) {
      return 0;
    }
    const today = new Date();
    let age = today.getFullYear() - Number(_birthday.slice(0, 4));
    let month = today.getMonth() - Number(_birthday.slice(5, 7));
    if (month < 0 || (month === 0 && today.getDate() < Number(_birthday.slice(8, 10)))) {
      return age - 1;
    }
    return age;
  }

  /**
   * 가족관계 그리드 세팅
   */
  function setCustRelationGrid() {
    const $custRelation = $('#custRelation');

    // 검색된 고객명이 있을 경우
    if (commonUtils.isNotEmpty($custRelation.val())) {
      const custId = $container.find('#custId').val();
      const currentCustName = $container.find('#custName').val();
      const { custName, birthday } = searchBoxUtils.getResultObject($('#custRelation'));

      // 중복된 이름이 있으면
      if (some(custRelationTable.grid.getData(), ['custName', custName]) ||
          isEqual(currentCustName, custName)) {
        return false;
      }

      // 가족관계 그리드 Row 추가
      custRelationTable.grid.appendRow({
        custId,
        custName,
        custRelation: '관계 입력',
        birthday,
      });
    }
  }

  /**
   * 바코드로 기준정보 가져오기
   *
   * @param {Object} _param
   */
  function getStandardInfoByBarcode(_param) {
    commonUtils.callAjax({
      type: 'GET',
      url: '/api/standardInfo/getStandardInfoProductList',
      paramData: _param,
      callback: (_res) => {
        if (commonUtils.isNotEmpty(_res)) {
          // 기준정보 필드 세팅
          setStandardInfo(_res);

          // 매입처, 브랜드 셀렉트박스 이벤트
          selectEventHandler();

          // 즉시적용 체크 시
          const barcodeRight = $container.find('input[name=barcodeRight]').is(':checked');
          if (barcodeRight) {
            addRegData();
          }
        } else {
          connecteye.app.ui.alert.warning({
            text: '해당 바코드로 등록된 기준정보가 없어요',
          });
        }
      },
    });
  }

  /**
   * 주소검색창 보여주기
   *
   */
  function showDaumPost() {
    const $formCustInfo = $('#formCustInfo');
    const daumPostcode = commonUtils.getDaumPostCode($formCustInfo);

    daumPostcode.open({
      q: $formCustInfo.find('#custAddress').val(),
      left: Math.round(window.screenX + (window.outerWidth / 2) - 250),
      top: Math.round(window.screenY + (window.outerHeight / 2) - 300),
    });
  }

  /**
   * 기준정보 필드 세팅
   *
   * @param {Object} _res
   */
  function setStandardInfo(_res) {
    const { supId, prdtId, prdtName, brnId, brnName, supPrdtId, barcode, salesPrice, currentAmount } = _res[0];

    const prdtOption = $('<option/>', {
      text: prdtName,
      value: prdtId,
      'data-result': JSON.stringify(_res[0]),
    });

    const brnOption = $('<option/>', {
      text: brnName,
      value: brnId,
      'data-result': JSON.stringify(_res[0]),
    });

    $container
      .find('.current-amount').text(currentAmount).end()
      .find('input[name=opSupId]').val(supId).end()
      .find('input[name=opBrnId]').val(brnId).end()
      .find('input[name=opPrdtId]').val(prdtId).end()
      .find('select[name=opBrnName]').empty().append(brnOption).end()
      .find('select[name=opPrdtName]').empty().append(prdtOption).end()
      .find('input[name=opSupPrdtId]').val(supPrdtId).end()
      .find('input[name=opBarcode]').val(barcode).end()
      .find('input[name=opUnitPrice]').val(commonUtils.setComma(salesPrice)).end()
      .find('input[name=opAmount]').val(1).end()
      .find('input[name=opPrice]').val(commonUtils.setComma(salesPrice));
  }

  /**
   * 매입처,브랜드, 상품정보 초기화
   */
  function resetInfo() {
    // 상품정보 초기화
    $container
      .find('.product-control').val('').end()
      .find('#opBrnName').empty().trigger('change').end()
      .find('#opPrdtName').empty().trigger('change');
  }

  /**
   * 초기화
   */
  function resetPage(_isOptometryInfo) {
    const $OptometrySalesContainer = $('.optometry-sales-container');
    const $form = $OptometrySalesContainer.find('input, select, textarea')
      .not('#barcodeRight')
      .not('#salesDate')
      .not('#willVisitDate')
      .not('#cashReceiptYn')
      .not('.optometry');

    optmtRegTable.grid.showColumn('');
    optmtRegTable.grid.clear();

    commonUtils.formReset($form);

    $container
      .find('.btn-cash-payment').prop('disabled', true).end()
      .find('#btnSalesSave').prop('disabled', false).end()
      .find('#btnOptometryPay').removeClass('d-none').end()
      .find('#btnOptometryPayModify').addClass('d-none').end()
      .find('#btnOptometryPayDelete').addClass('d-none').end();

    // 검안정보 유무
    if (!_isOptometryInfo) {
      $OptometrySalesContainer
        .find('textarea.optometry').val('').end()
        .find('select.optometry').val('').end()
        .find('input.optometry').val('0');
    }

    $OptometrySalesContainer
      .find('.pay-info').val('0').end()
      .find('.pay-control').val('0').end()
      .find('.current-amount').text('0').end()
      .find('.total-rcv-price').text('0').end()
      .parent().scrollTop(0);
  }

  /***** Business End *****/
};
