/*
 *  Description: 매출 검안매출 정보
 *  User: kyle
 *  Date: 2022-05-11
 */
import commonUtils from '../../utils/commonUtils';
import searchBoxUtils from '../../utils/searchBoxUtils';

export default () => {
  /***** Static Start *****/
  const $container = $('#optometrySalesInfo');

  // 툴팁, 팝오버
  const tippy = connecteye.app.ui.createTippy({
    element: document.getElementById('searchBox'),
    content: '고객 이름을 선택해주세요!',
    placement: 'top-end',
    theme: 'gradient',
    trigger: 'manual',
    onCreate(instance) {
      instance.show();
    },
    onUntrigger(instance) {
      instance.disable();
    },
  });

  // 고객명 검색박스 적용
  searchBoxUtils.create({
    $el: $container.find('#custAll'),
    title: '고객명',
    placeholder: '고객검색',
    ajax: {
      url: '/api/optometrySalesInfo/getCustList',
      data: (params) => ({ custName: params.term }),
      processResults: (_result) => {
        let result = searchBoxUtils.getResultCallback(_result);
        return {
          results: result.map((obj) => ({ id: obj.custId, text: obj.custName, data: obj })),
        };
      },
      cache: true,
    },
    noAdd: false,
    noResultCallback: () => connecteye.app.popup.showInsertCustInfo(),
    // 검색박스 선택목록
    templateResult: (_state) => {
      // 검색중이 아닐때
      if (!_state.loading) {
        const { data, text } = _state;
        const { telOne, telTwo } = data;
        return `${text}  /  ${telOne || telTwo}`;
      }
    },
  });

  // 가족관계 검색박스 적용
  searchBoxUtils.create({
    $el: $container.find('#custRelation'),
    title: '고객명',
    placeholder: '가족관계',
    ajax: {
      url: '/api/optometrySalesInfo/getCustList',
      data: (params) => ({ custName: params.term }),
      processResults: (_result) => {
        let result = searchBoxUtils.getResultCallback(_result);
        return {
          results: result.map((obj) => ({ id: obj.custId, text: obj.custName, data: obj })),
        };
      },
      cache: true,
    },
    noAdd: true,
    noResultCallback: () => {},
    // 검색박스 선택목록
    templateResult: (_state) => {
      // 검색중이 아닐때
      if (!_state.loading) {
        const { data, text } = _state;
        const { telOne, telTwo } = data;
        return `${text}  /  ${telOne || telTwo}`;
      }
    },
  });
  /***** Static End *****/

  /***** Event Start *****/
  // 영역 이동 버튼 이벤트
  $container.find('.btn-move-area').on('click', (_evt) => getArea(_evt));

  // 고객 선택 이벤트
  $container.find('#custAll').on('change', (_evt) => {
    const $target = $(_evt.currentTarget);

    // 고객아이디가 있으면
    if (commonUtils.isNotEmpty($target.val())) {

      // 툴팁 가리기
      tippy.disable();
    }
  });

  // 고객 등록 버튼 클릭 이벤트
  $container.find('#btnRegCust').on('click', () => connecteye.app.popup.showInsertCustInfo());

  // 금전출납 버튼 클릭 이벤트
  $container.find('.btn-cash-payment').on('click', () => {
    // 정산 버튼 클릭 이벤트
    const $target = $container.find('.card[data-area=optometryCustPay]');
    $target
      .removeClass('d-none')
      .siblings('.card')
      .addClass('d-none')
      .end()
      .trigger('loadDom', {
        custId: $container.find('#custId').val(),
      });
  });

  // 이전 이동 버튼 이벤트
  $container.find('.btn-prev').on('click', (_evt) => getArea(_evt));
  /***** Event End *****/

  /***** Business Start *****/
  /**
   * 영역 가져오기
   *
   * @param {Object} _evt
   */
  function getArea(_evt) {
    const $el = $(_evt.currentTarget);
    const target = $el.data('target');
    const $parentArea = $container.find(`.page-inner .card[data-area=${target}]`);
    const custAll = $container.find('#custAll option:selected').text();

    // 자신을 제외하고 d-none 클래스 넣어줌
    $parentArea.removeClass('d-none').siblings('.card').addClass('d-none');

    // 영역 별 trigger 세팅
    switch (target) {
      case 'optmtReg':
        const custObj = $container.find('#custAll option').last().data('result');
        $parentArea.trigger('loadDom', { custObj: custObj });
        break;
      case 'custSelect':
        $parentArea.trigger('loadDom', { custName: custAll });
        break;
      case 'optmtList':
        const custId = $container.find('#custId').val();
        if (commonUtils.isNotEmpty(custId)) {
          $parentArea.trigger('loadDom', { custId: custId });
        }
        break;
      default:
        $parentArea.trigger('loadDom');
        break;
    }
  }
  /***** Business End *****/
};
