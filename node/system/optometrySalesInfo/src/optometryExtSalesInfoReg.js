/*
 *  Description: 매출 검안매출 기타매출등록
 *  User: kyle
 *  Date: 2022-05-20
 */
import commonUtils from '../../utils/commonUtils';
import gridUtils from '../../utils/gridUtils';
import searchBoxUtils from '../../utils/searchBoxUtils';

export default () => {
  /***** Static Start *****/
  const $container = $('#optometrySalesInfo');

  // table
  const extRegTable = new gridUtils.create({
    el: 'extRegTable',
    bodyHeight: 543,
    rowHeaders: [],
    scrollX: 'false',
    scrollY: true,
    columns: [
      {
        header: '',
        name: '',
        width: 20,
        align: 'center',
        renderer: {
          type: gridUtils.deleteButtonRenderer,
          options: {
            isAmount: true,
            $currentAmount: $container.find('.ext-current-amount'),
          }
        },
      },
      {
        header: '상품구분',
        name: 'prdtDiv',
        width: 90,
        align: 'center',
      },
      {
        header: '상품명',
        name: 'prdtName',
        ellipsis: true,
      },
      {
        header: '단가',
        name: 'unitPrice',
        width: 90,
        align: 'center',
        formatter: commonUtils.setComma,
      },
      {
        header: '수량',
        name: 'amount',
        width: 80,
        align: 'center',
      },
      {
        header: '금액',
        name: 'price',
        width: 90,
        align: 'center',
        formatter: commonUtils.setComma,
      },
    ],
    summary: {
      height: 40,
      position: 'bottom',
      columnContent: {
        amount: {
          template(valueMap) {
            return `총 ${commonUtils.setComma(valueMap.sum)}개`;
          },
        },
        price: {
          template(valueMap) {
            return `${commonUtils.setComma(valueMap.sum)}원`;
          },
        },
      },
    },
    // 그리드 생성 완료 시
    onGridMounted: (_gridThis) => {
      // 그리드 레이아웃 새로고침
      _gridThis.instance.refreshLayout();
    },
    // 그리드 업데이트 시  'resetData', 'restore', 'reloadData', 'readData', 'setPerPage'
    onGridUpdated: (_gridThis) => {
      // 그리드 레이아웃 새로고침
      _gridThis.instance.refreshLayout();
      // hideLoadingBar
      _gridThis.instance.dispatch('setLoadingState', 'DONE');
    },
  });

  // 매입처 검색박스 적용
  searchBoxUtils.create({
    $el: $container.find('#regSupName'),
    title: '매입처',
    placeholder: '매입처',
    ajax: {
      url: '/api/standardInfo/getSupplierList',
      data: (params) => ({ supName: params.term }),
      processResults: (_result) => {
        let result = searchBoxUtils.getResultCallback(_result);
        return {
          results: result.map((obj) => ({ id: obj.supId, text: obj.supName, data: obj })),
        };
      },
      cache: true,
    },
    noAdd: true,
  });

  // 브랜드 검색박스 적용
  searchBoxUtils.create({
    $el: $container.find('#regBrnName'),
    title: '브랜드',
    placeholder: '브랜드',
    ajax: {
      url: '/api/standardInfo/getBrandList',
      data: (params) => ({ brnName: params.term }),
      processResults: (res) => {
        let result = searchBoxUtils.getResultCallback(res);
        return {
          results: result.map((obj) => ({ id: obj.brnId, text: obj.brnName, data: obj })),
        };
      },
      cache: true,
    },
    noAdd: true,
  });

  // 상품 검색박스 적용, 무한 스크롤 적용
  searchBoxUtils.create({
    $el: $container.find('#regPrdtName'),
    title: '상품',
    placeholder: '상품',
    ajax: {
      url: '/api/standardInfo/getStandardInfoSearchBoxProductList',
      data: (params) => ({
        supId: $('#regSupId').val(),
        brnId: $('#regBrnId').val(),
        prdtName: params.term,
        page: params.page || 1,
        perPage: 10,
        isUse: 1,
      }),
      processResults: (res) => {
        let result = searchBoxUtils.getResultCallback(res);
        const { totalCount, page } = res;
        return {
          results: result.map((obj) => ({
            id: obj.supPrdtId,
            text: obj.prdtName,
            data: obj,
          })),
          pagination: {
            more: (page * 10) < totalCount,
          }
        };
      },
      cache: false,
    },
    noAdd: true,
  });
  /***** Static End *****/

  /***** Event Start *****/
  // 기타매출 등록 화면 dom 로드 시
  $container.find('.card[data-area=extReg]').on('loadDom', (_evt, _data) => {
    extRegTable.grid.refreshLayout();
  });

  // 상품정보 수량 Change 이벤트
  $container.find('#regAmount').on('keyup', (_evt) => {
    const value = Number(_evt.currentTarget.value);
    const $currentAmount = $container.find('.ext-current-amount');
    const currentAmount = Number($currentAmount.text());

    // 입력한 값이 현재 적용 가능수량이랑 비교
    if (value > currentAmount) {
      _evt.currentTarget.value = '0';
      connecteye.app.ui.alert.warning({
        text: `${currentAmount}개를 초과 할수 없어요`,
      });
      return false;
    }
  });

  // 거래내역 정보 입력 후 적용 버튼 클릭 시 이벤트
  $container.find('#btnExtSave').on('click', () => addRegData());

  // 초기화
  $container.find('#btnExtRegReset').on('click', () => resetPage());

  // 매입처 선택 이벤트
  $container.find('#regSupName').on('change', (_evt) => {
    const resultObj = searchBoxUtils.getResultObject($(_evt.currentTarget));
    if (resultObj) {
      const { supId, repPhone, mngPhone } = resultObj;
      // 매입처 아이디
      $container.find('input[name=regSupId]').val(supId);
      // 대표번호
      $container.find('input[name=regRepPhone]').val(repPhone);
      // 담당자번호
      $container.find('input[name=regMngPhone]').val(mngPhone);
      // 상품정보 초기화
      resetPrdtInfo();
    }
    selectEventHandler();
  });

  // 브랜드 선택 이벤트
  $container.find('#regBrnName').on('change', (_evt) => {
    let resultObj = searchBoxUtils.getResultObject($(_evt.currentTarget));
    if (resultObj) {
      // 브랜드 아이디
      $container.find('input[name=regBrnId]').val(resultObj.brnId);
      // 상품정보 초기화
      resetPrdtInfo();
    }
    selectEventHandler();
  });

  // 상품 선택 이벤트
  $container.find('#regPrdtName').on('change', (_evt) => {
    const resultObj = searchBoxUtils.getResultObject($(_evt.currentTarget));
    if (resultObj) {
      const { supPrdtId, barcode, salesPrice, currentAmount } = resultObj;
      // 기준정보 아이디
      $container.find('input[name=regSupPrdtId]').val(supPrdtId);
      // 바코드
      $container.find('input[name=regBarcode]').val(barcode);
      // 단가
      $container.find('input[name=regUnitPrice]').val(commonUtils.setComma(salesPrice));
      // 수량
      $container.find('input[name=regAmount]').val(1);
      // 금액
      $container.find('input[name=regPrice]').val(commonUtils.setComma(salesPrice));
      // 현재수량
      $container.find('.ext-current-amount').text(currentAmount);
    }
    selectEventHandler();
  });

  // 단가, 수량 입력 이벤트
  $container.find('#regPrice').on({
    keyup: commonUtils.setPriceEventHandler,
    change: commonUtils.setPriceEventHandler,
  });
  $container.find('#regUnitPrice').on({
    keyup: commonUtils.setPriceEventHandler,
    change: commonUtils.setPriceEventHandler,
  });
  $container.find('#regUnitPrice').on('keyup', setInputChangeEventTrigger);
  $container.find('#regAmount').on('keyup', setInputChangeEventTrigger);

  // 거래내역 추가 버튼 클릭 이벤트
  $container.find('#btnExtReg').on('click', () =>
    commonUtils.isNotEmpty(extRegTable.grid.getData())
      ? runInsert()
      : connecteye.app.ui.alert.warning({
          text: '거래내역 정보 입력 후 추가해주세요',
        }),
  );

  /***** Event End *****/

  /***** Business Start *****/
  /**
   * 기타매출 거래내역 추가
   */
  function addRegData() {
    const obj = searchBoxUtils.getResultObject($('#regPrdtName'));

    const $currentAmount = $container.find('.ext-current-amount');
    const currentAmount = Number($currentAmount.text());
    const inputAmount = Number($container.find('#regAmount').val());

    const amount = Number($('#regAmount').val());
    const price = Number(commonUtils.removeComma($('#regPrice').val()));
    const unitPrice = Number(commonUtils.removeComma($('#regUnitPrice').val()));

    // 가능 수량 체크
    if (!currentAmount) {
      connecteye.app.ui.alert.warning({
        text: '적용 가능한 수량이 없어요',
      });
      return false;
    }

    // 오브젝트 null 체크
    if (commonUtils.isNotEmpty(obj)) {
      obj.price = price || obj.salesPrice;
      obj.unitPrice = unitPrice || obj.purPrice;
      obj.amount = amount || 1;
      extRegTable.grid.appendRow(obj);

      // 현재 적용 가능수량 - 입력수량
      $currentAmount.text(currentAmount - inputAmount);

      return true;
    } else {
      connecteye.app.ui.alert.warning({
        text: '매입처, 브랜드, 상품 정보를 입력해주세요',
      });
    }
  }

  /**
   * 추가 실행
   */
  function runInsert() {
    // form 전송 ajax 처리
    connecteye.app.ui.alert
      .confirm({
        title: '거래내역을 추가할까요?',
        text: '취소를 누르면 내용을 수정 할 수 있어요',
        confirmBtnTxt: '추가',
        cancelBtnTxt: '취소',
        showLoaderOnConfirm: true,
        preConfirm: () => {},
      })
      .then((_result) => {
        if (_result.isConfirmed) {
          insertExtSalesInfo();
        } else {
          connecteye.app.ui.alert.error({
            title: '추가 취소',
            text: '거래내역 수정 후 다시 추가 할 수 있어요 :)',
            icon: 'error',
          });
        }
      });
  }

  /**
   * 기타매출 거래내역 추가 ajax
   */
  function insertExtSalesInfo() {
    const extSalesInfo = extRegTable.grid.getData();
    connecteye.app.ui.showCardLoading();
    commonUtils.callAjax({
      type: 'POST',
      url: '/api/optometrySalesInfo/getInsertExtSalesInfo',
      paramData: {
        salesTypeCode: 'SL102',
        supId: $container.find('#regSupId').val(),
        extSalesInfo: JSON.stringify(extSalesInfo),
      },
      callback: (_res) => {
        connecteye.app.ui.hideCardLoading();
        connecteye.app.ui.alert
          .success({
            title: '추가 완료!',
            text: '입력한 기타매출 거래내역이 추가됐어요',
            denyButtonText: '계속 추가하기',
            showDenyButton: true,
          })
          .then((_SuccessResult) => {
            if (_SuccessResult.isConfirmed) {
              // 거래내역 목록 조회
              const param = _res[0];
              showSalesInfoList(param);
            } else if (_SuccessResult.isDenied) {
              resetPage();
            }
          });
      },
    });
  }

  /**
   * 매출 거래내역 목록 보여주기
   */
  function showSalesInfoList(_param) {
    const { custId } = _param;
    $container.find('.btn-prev').trigger('click');
    $container.find('.card[data-area=optmtList]').trigger('loadDom', { custId: custId });
  }

  /**
   * 선택 이벤트 핸들러
   */
  function selectEventHandler() {
    const supId = $('#regSupId').val();
    const brnId = $('#regBrnId').val();
    if (commonUtils.isEmpty(supId) || commonUtils.isEmpty(brnId)) {
      return false;
    }
    $container.find('.product-control:not(#regBarcode)').prop('disabled', false);
  }

  /**
   * 단가, 수량 입력 트리거
   *
   * @param {Object} _this
   */
  function setInputChangeEventTrigger(_this) {
    const $price = $container.find('#regPrice');
    const amount = Number(commonUtils.removeComma($container.find('#regAmount').val()));
    const unitPrice = Number(commonUtils.removeComma($container.find('#regUnitPrice').val()));

    const { value, name } = _this.currentTarget;
    const currentValue = commonUtils.removeComma(value);

    if (commonUtils.isNotEmpty(currentValue)) {
      if (commonUtils.isNumeric(currentValue)) {
        let price;
        const numValue = commonUtils.isNotEmpty(currentValue) ? Number(currentValue) : 0;
        switch (name) {
          case 'regUnitPrice':
            price = String(amount * numValue);
            break;
          case 'regAmount':
            price = String(unitPrice * numValue);
            break;
        }
        $(_this.currentTarget).val(commonUtils.setComma(currentValue.replace(/(^0+)/, '')));
        $price.val(commonUtils.setComma(price.replace(/(^0+)/, '')));
      } else {
        connecteye.app.ui.alert.warning({
          text: '숫자만 입력해주세요.',
          icon: 'error',
        });
        $(_this.currentTarget).val('0');
      }
    }
  }

  /**
   * 상품정보 초기화
   */
  function resetPrdtInfo() {
    // 상품정보 초기화
    $container
      .find('.product-control')
      .val('')
      .end()
      .find('#regPrdtName')
      .empty()
      .end()
      .find('.ext-current-amount').text('0')
      .end()
      .trigger('change');
    extRegTable.grid.clear();
  }

  /**
   * 초기화
   */
  async function resetPage() {
    const $formExtSales = $('#formExtSales');
    await $formExtSales
      .find('#regSupName')
      .empty()
      .trigger('change')
      .end()
      .find('#regBrnName')
      .empty()
      .trigger('change')
      .end()
      .find('#regPrdtName')
      .empty()
      .trigger('change');

    $container
      .find('.product-control').prop('disabled', true).end()
      .find('.ext-current-amount').text('0').end()
      .parent().scrollTop(0);

    commonUtils.formReset($formExtSales.find('input, select'));
    extRegTable.grid.clear();
  }
  /***** Business End *****/
};
