/*
 *  Description: login.js
 *  User: kyle
 *  Date: 2021-11-02
 */

import commonUtils from '../../utils/commonUtils';

require('jquery-validation');

export default () => {
  /***** Static Start *****/
  let working = false;

  const loginContainer = $('.container-login');
  const $form = loginContainer.find('.login-form');

  const signUpController = $('.container-signup');
  const $signUpForm = signUpController.find('.signup-form');

  // 유효성체크 메세지 설정
  commonUtils.setJqueryValidationLanguageKor();

  // 유효성 체크
  $signUpForm.validate({
    rules: {
      regUserId: {
        required: true,
        minLength: 6,
        maxLength: 20,
      },
      regPassword: {
        required: true,
        minLength: 8,
      },
      regEmail: {
        required: true,
      },
      regUserName: {
        required: true,
        minLength: 2,
      },
    },
    errorPlacement: (_error, _element) => _element.parent().children().last().after(_error),
    submitHandler: () => runSignupSubmit()
  });

  // 유효성 체크R
  /*$form.validate({
    rules: {
      userId: {
        required: true,
      },
      password: {
        required: true,
      }
    },
    errorPlacement: (_error, _element) => _element.parent().children().last().after(_error),
    submitHandler: () => runLoginSubmit()
  });*/
  /***** Static End *****/

  /***** Event Start *****/
  // 로그인 버튼
  /*document.querySelector('.btn-login').addEventListener('click', (e) => {
    e.preventDefault();

    if (working) {
      return;
    }

    working = true;

    let $this = $(e.currentTarget).parent(),
      $state = $this.find('.btn-login > .state');

    $this.addClass('loading');
    $state.html('로그인 중...');

    setTimeout(() => {
      $this.addClass('ok');
      $state.html('완료!');

      setTimeout(() => {
        $state.html('로그인');
        $this.removeClass('ok loading');
        working = false;
      }, 4000);
    }, 3000);
  });*/

/*  // 휴대전화 인증 버튼
  $('.show-mobile').on('click', function () {
    $('.container-mobile').addClass('active');
    $('.container-login').removeClass('active');
  });

  // 회원가입 버튼
  $('.show-signup').on('click', function () {
    $('.container-signup').addClass('active');
    $('.container-mobile').removeClass('active');
  });

  // 취소 버튼
  $('.show-login').on('click', function () {
    $('.container-login').addClass('active');
    $('.container-mobile').removeClass('active');
    $('.container-signup').removeClass('active');
    $('.container-find').removeClass('active');
  });

  // 아이디/비밀번호 찾기 버튼
  $('.show-find').on('click', function () {
    $('.container-find').addClass('active');
    $('.container-login').removeClass('active');
    $('.container-mobile').removeClass('active');
    $('.container-signup').removeClass('active');
  });*/

  // 본인인증 버튼
  document.querySelector('.container-mobile .auth').addEventListener('click', () => {
    const url = '/mobileAuthPopup.ce';

    const popup = window.open(url,
      'popupChk', 'width=500, height=550, top=100,left=100,fullscreen=no,menubar=no,status=no,toolbar=no,titlebar=yes,location=no,scrollbar=no');
  });

  // 간편로그인 버튼
  document.querySelectorAll('.social-login .icon').forEach((item) => {
    item.addEventListener('click', (e) => {
      location.href = '/oauth2/authorization/' + e.currentTarget.dataset.type;

      // window.open(
      //     '/oauth2/authorization/' + this.dataset.type,
      //     '카카오톡 로그인',
      //     'width=480, height=750, toolbar=no, menubar=no, scrollbars=no, resizable=yes'
      // );
    });
  });

  // 비밀번호 보기/숨기기 버튼
  document.querySelectorAll('.show-password').forEach((item) => {
    item.addEventListener('click', (e) => {
      const $input = $(e.currentTarget).siblings('input');
      $input.toggleClass('active');
      if ($input.hasClass('active')) {
        $(e.currentTarget).find('i').attr('class', 'fa fa-eye-slash');
        $input.attr('type', 'text');
      } else {
        $(e.currentTarget).find('i').attr('class', 'fa fa-eye')
        $input.attr('type', 'password');
      }
    })
  });

  // 회원가입 전 validation
  /***** Event End *****/
};
