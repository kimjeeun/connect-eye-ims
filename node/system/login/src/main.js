/*
 *  Description: page main.js
 *  User: kyle
 *  Date: 2021-12-09
 */
require('../resources/css/login.css');
require('../../../node_modules/bootstrap/dist/css/bootstrap.min.css');

import login from './login';

$(document).ready(() => {
  login();
});
