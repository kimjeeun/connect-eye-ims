/*
 *  Description: 재고현황
 *  User: kyle
 *  Date: 2022-01-13
 */
import moment from 'moment';
import gridUtils from '../../utils/gridUtils';
import commonUtils from '../../utils/commonUtils';

export default () => {
  /***** Static Start *****/
  const $container = $('#stockStatusInfo');

  // table
  const stockStatusInfoTable = new gridUtils.create({
    el: 'stockStatusInfoTable',
    rowHeaders: [],
    pageOptions: {
      // 페이징 옵션
      perPage: 7, // 1페이지당 목록 갯수
      visiblePages: 5, // 페이징 넘버링 갯수
    },
    data: {
      api: {
        readData: {
          url: '/api/stockStatusInfo/getStockInfo',
          method: 'GET',
          initParams: {
            orderBy: 'a.reg_date desc',
          },
        },
      },
    },
    minBodyHeight: 415,
    columns: [
      {
        header: '분류',
        name: 'rpsType',
        width: 100,
        align: 'center',
      },
      {
        header: '매입처',
        name: 'supName',
        align: 'center',
        width: 100,
        ellipsis: true,
        resizable: true,
      },
      {
        header: '상품구분',
        name: 'prdtDiv',
        width: 100,
        align: 'center',
      },
      {
        header: '브랜드',
        name: 'brnName',
        width: 120,
        ellipsis: true,
        resizable: true,
      },
      {
        header: '상품명',
        name: 'prdtName',
        ellipsis: true,
        resizable: true,
      },
      {
        header: '판매단가',
        name: 'salesPrice',
        width: 90,
        align: 'right',
        formatter: commonUtils.setComma,
      },
      {
        header: '수량',
        name: 'amount',
        width: 90,
        align: 'center',
      },
    ],

    // 그리드 생성 완료 시
    onGridMounted: (_gridThis) => {
      // 그리드 레이아웃 새로고침
      _gridThis.instance.refreshLayout();
      // 초기 날짜
      setDateAndGetStockInfo(moment().subtract(29, 'days'), moment(), false);
      // 현재 재고 현황
      getStockStatusInfo();
    },

    // 그리드 업데이트 시  'resetData', 'restore', 'reloadData', 'readData', 'setPerPage'
    onGridUpdated: (_gridThis) => {
      const totalCount = _gridThis.instance.getPaginationTotalCount();

      // 데이터 건수
      $container.find('.result-cnt').text(commonUtils.setComma(totalCount));

      // 그리드 레이아웃 새로고침
      _gridThis.instance.refreshLayout();
    },
  });

  // 거래기간 검색 달력
  connecteye.app.ui.createDatePicker({
    containerEl: '#stockStatusInfo .btn-calendar',
    ranges: true,
    callback: (start, end) => setDateAndGetStockInfo(start, end, true),
  });

  // selectbox 바인딩
  commonUtils.setSelectboxCode({ el: 'rpsTypeCode', code: 'BY100', exceptionCodeList: ['BY103', 'BY104'] });
  commonUtils.setSelectboxCode({ el: 'prdtDivCode', code: 'PO100' });
  /***** Static End *****/

  /***** Event Start *****/
  // 재고현황 리스트 화면 dom 로드 시
  $container.find('.card[data-area=stockStatusList]').on('loadDom', (_evt, _data) => {
    if (_data && _data.listReload) {
      stockStatusInfoTable.grid.reloadData();
      getStockStatusInfo();
    }
  });

  // 검색 버튼 이벤트
  $container.find('.btn-select').on('click', () => getStockInfo());
  $container.find('.search-group').on('keyup', (_evt) => {
    if (_evt.keyCode === 13) {
      getStockInfo();
    }
  });

  // 그리드 클릭 이벤트
  stockStatusInfoTable.grid.on('click', (_evt) => {
    const { targetType, instance, rowKey } = _evt;
    const rowData = instance.getRow(rowKey);
    if (targetType !== 'columnHeader' && targetType !== 'rowHeader' && targetType !== 'etc') {
      const $target = $container.find('.card[data-area=stockModify]');
      $target.removeClass('d-none').siblings('.card').addClass('d-none').end().trigger('loadDom', {
        rowData: rowData,
      });
    }
  });

  // 이전 버튼 이벤트
  $container.find('.btn-prev').on('click', (_evt) => getArea(_evt));
  /***** Event End *****/

  /***** Business Start *****/
  /**
   * 재고 정보 가져오기
   */
  function getStockInfo() {
    const $form = $container.find('.search-group');
    const formSerializeData = commonUtils.serializeObject($form);

    // args1: page, args2: paramData (GET 방식), args3: 그리드 초기화 여부
    stockStatusInfoTable.grid.readData(1, formSerializeData, true);
  }

  /**
   * 날짜세팅 후 재고 가져오기
   *
   * @param {Object} _start
   * @param {Object} _end
   * @param {Boolean} _isSearch
   */
  function setDateAndGetStockInfo(_start, _end, _isSearch) {
    const startDate = _start.format('YYYY-MM-DD');
    const endDate = _end.format('YYYY-MM-DD');
    const searchPurDate = `${startDate} ~ ${endDate}`;
    $container
      .find('.search-stock-date')
      .text(searchPurDate)
      .end()
      .find('#startDate')
      .val(startDate)
      .end()
      .find('#endDate')
      .val(endDate);

    if (_isSearch) {
      getStockInfo();
    }
  }

  /**
   * 제고현황 세팅
   *
   * @param {Object} _res
   */
  async function setStockStatusInfo(_res) {
    if (_res.length) {
      const {
        totalStockAmount,
        totalWearingAmount,
        totalReleaseAmount,
        totalReturnAmount,
        totalCancelAmount,
        modifyPrevAmount,
        modifyAmount,
        modifyDate,
      } = _res[0];
      const modifyStock = await setSignNum(modifyAmount - modifyPrevAmount);
      const modifyStockHistory = modifyPrevAmount
          ? `${modifyPrevAmount || 0} -> ${modifyAmount || 0}`
          : '';
      $container
        .find('.total-stock')
        .text(totalStockAmount)
        .end()
        .find('.wrng-stock')
        .text(totalWearingAmount)
        .end()
        .find('.return-stock')
        .text(totalReturnAmount)
        .end()
        .find('.release-stock')
        .text(totalReleaseAmount)
        .end()
        .find('.cancel-stock')
        .text(totalCancelAmount)
        .end()
        .find('.modify-date')
        .text(modifyDate)
        .end()
        .find('.modify-stock')
        .text(modifyStock)
        .end()
        .find('.modify-stock-history')
        .text(modifyStockHistory);
    }
  }

  /**
   * 재고현황 가져오기
   */
  function getStockStatusInfo() {
    const options = {
      type: 'GET',
      url: '/api/stockStatusInfo/getStockStatusInfo',
      callback: setStockStatusInfo,
    };
    commonUtils.callAjax(options, true);
  }

  /**
   * 양수/음수 표시문자열 리턴
   *
   * @param {int} num
   */
  function setSignNum(num) {
    const signNum = Math.sign(num);
    switch (signNum) {
      case 1:
        return `+${num}`;
      case 0:
        return `0`;
      case -1:
        return `${num}`;
    }
  }

  /**
   * 영역 가져오기
   *
   * @param {Object} _evt
   */
  async function getArea(_evt) {
    const $el = $(_evt.currentTarget);
    const target = $el.data('target');
    const $parentArea = $container.find(`.page-inner .card[data-area=${target}]`);

    // 자신을 제외하고 d-none 클래스 넣어줌
    await $parentArea
      .removeClass('d-none')
      .siblings('.card')
      .addClass('d-none')
      .end()
      .trigger('loadDom');

    $container.parent().scrollTop(0);
    stockStatusInfoTable.grid.refreshLayout();
  }
  /***** Business End *****/
};
