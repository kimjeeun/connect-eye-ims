/*
 *  Description: stockStatusInfo main.js
 *  User: kyle
 *  Date: 2021-12-09
 */
import stockStatusInfo from './stockStatusInfo';
import stockStatusInfoModify from './stockStatusInfoModify';

$(document).ready(async () => {
  // 재고현황
  stockStatusInfo();
  // 재고 수정
  stockStatusInfoModify();
});
