/*
 *  Description: 재고현황 수정
 *  User: kyle
 *  Date: 2022-04-20
 */
import gridUtils from '../../utils/gridUtils';
import commonUtils from '../../utils/commonUtils';

export default () => {
  /***** Static Start *****/
  const $container = $('#stockStatusInfo');

  // table
  const stockModifyTable = new gridUtils.create({
    el: 'stockModifyTable',
    rowHeaders: [],
    minBodyHeight: 80,
    columns: [
      {
        header: '분류',
        name: 'rpsType',
        width: 100,
        align: 'center',
      },
      {
        header: '매입처',
        name: 'supName',
        align: 'center',
        width: 100,
        ellipsis: true,
      },
      {
        header: '상품구분',
        name: 'prdtDiv',
        width: 100,
        align: 'center',
      },
      {
        header: '브랜드',
        name: 'brnName',
        width: 120,
        ellipsis: true,
      },
      {
        header: '상품명',
        name: 'prdtName',
        ellipsis: true,
      },
      {
        header: '판매단가',
        name: 'salesPrice',
        width: 90,
        align: 'right',
        formatter: commonUtils.setComma,
      },
      {
        header: '수량',
        name: 'amount',
        width: 90,
        align: 'center',
      },
      {
        header: '현재수량',
        name: 'currentAmount',
        width: 90,
        align: 'center',
        defaultValue: '0',
        editor: {
          type: 'text',
          useViewMode: true,
        },
      },
      {
        header: '증감수량',
        name: 'variationAmount',
        width: 90,
        align: 'center',
        defaultValue: '0',
      },
      {
        header: '적용내용',
        name: 'flcTypeCode',
        width: 100,
        align: 'center',
        defaultValue: 'SS101',
        formatter: 'listItemText',
        copyOptions: {
          useListItemText: true,
        },
        editor: {
          type: 'select',
          options: {
            instantApply: true,
            listItems: gridUtils.setSelectboxCode('SS100'),
          },
        },
      },
    ],

    // 그리드 생성 완료 시
    onGridMounted: (_gridThis) => {
      // 그리드 레이아웃 새로고침
      _gridThis.instance.refreshLayout();
    },

    // 그리드 업데이트 시  'resetData', 'restore', 'reloadData', 'readData', 'setPerPage'
    onGridUpdated: (_gridThis) => {
      // 그리드 레이아웃 새로고침
      _gridThis.instance.refreshLayout();
    },
  });

  // table
  const stockHistoryTable = new gridUtils.create({
    el: 'stockHistoryTable',
    rowHeaders: [],
    bodyHeight: 180,
    minBodyHeight: 180,
    scrollY: true,
    columns: [
      {
        header: '상태',
        name: 'purType',
        width: 100,
        align: 'center',
      },
      {
        header: '요청일',
        name: 'regDate',
        align: 'center',
      },
      {
        header: '입고일',
        name: 'wrngDate',
        sortable: true,
        sortingType: 'desc',
        align: 'center',
      },
      {
        header: '수량',
        name: 'amount',
        width: 100,
        align: 'center',
      },
    ],

    // 그리드 생성 완료 시
    onGridMounted: (_gridThis) => {},

    // 그리드 업데이트 시  'resetData', 'restore', 'reloadData', 'readData', 'setPerPage'
    onGridUpdated: (_gridThis) => {
      // 그리드 레이아웃 새로고침
      _gridThis.instance.refreshLayout();
    },
  });

  // table
  const releaseHistoryTable = new gridUtils.create({
    el: 'releaseHistoryTable',
    rowHeaders: [],
    bodyHeight: 180,
    minBodyHeight: 180,
    scrollY: true,
    columns: [
      {
        header: '출고일',
        name: 'regDate',
        sortable: true,
        sortingType: 'desc',
        width: 100,
        align: 'center',
      },
      {
        header: '고객명',
        name: 'custName',
        align: 'center',
      },
      {
        header: '수량',
        name: 'amount',
        align: 'center',
      },
      {
        header: '출고단가',
        name: 'unitPrice',
        width: 100,
        align: 'center',
        formatter: commonUtils.setComma,
      },
    ],

    // 그리드 생성 완료 시
    onGridMounted: (_gridThis) => {},

    // 그리드 업데이트 시  'resetData', 'restore', 'reloadData', 'readData', 'setPerPage'
    onGridUpdated: (_gridThis) => {
      // 그리드 레이아웃 새로고침
      _gridThis.instance.refreshLayout();
    },
  });

  // table
  const stockModifyHistoryTable = new gridUtils.create({
    el: 'stockModifyHistoryTable',
    rowHeaders: [],
    bodyHeight: 180,
    minBodyHeight: 180,
    scrollY: true,
    columns: [
      {
        header: '수정일',
        name: 'flcDate',
        sortable: true,
        sortingType: 'desc',
        width: 100,
        align: 'center',
      },
      {
        header: '담당자',
        name: 'userName',
        align: 'center',
      },
      {
        header: '수량',
        name: 'amount',
        align: 'center',
      },
      {
        header: '내용',
        name: 'flcType',
        align: 'center',
      },
    ],

    // 그리드 생성 완료 시
    onGridMounted: (_gridThis) => {},

    // 그리드 업데이트 시  'resetData', 'restore', 'reloadData', 'readData', 'setPerPage'
    onGridUpdated: (_gridThis) => {
      // 그리드 레이아웃 새로고침
      _gridThis.instance.refreshLayout();
    },
  });
  /***** Static End *****/

  /***** Event Start *****/
  // 재고현황 수정 리스트 화면 dom 로드 시
  $container.find('.card[data-area=stockModify]').on('loadDom', async (_evt, _data) => {
    const { rowData } = _data;

    // 재고현황 수정 테이블
    stockModifyTable.grid.resetData([rowData]);

    // 입고/반품 이력 테이블
    getWrngHistoryInfo(rowData);
    stockHistoryTable.grid.refreshLayout();

    // 출고 이력 테이블
    getReleaseHistoryInfo(rowData);
    releaseHistoryTable.grid.refreshLayout();

    // 재고수정 이력 테이블
    getStockModifyHistoryInfo(rowData);
    stockModifyHistoryTable.grid.refreshLayout();

    // 재고현황 정보수정 테이블
    stockModifyTable.grid.refreshLayout();
  });

  // 그리드 이벤트
  stockModifyTable.grid.on('editingFinish', (_evt) => {
    const { columnName } = _evt;
    if (columnName === 'currentAmount') {
      setVariationAmount(_evt);
    }
  });

  // 적용 버튼 클릭 이벤트
  $container.find('#btnStockModify').on('click', () => runInsert());

  // 초기화
  $container.find('#btnStockModifyReset').on('click', () => resetPage());
  /***** Event End *****/

  /***** Business Start *****/
  /**
   * 증감수량 세팅
   *
   * @param {Object} _evt
   */
  function setVariationAmount(_evt) {
    // 이벤트 데이터
    const { instance, value, columnName, rowKey } = _evt;

    // 숫자일 때
    if (commonUtils.isNumeric(value)) {
      // 컬럼이름이 현재수량 일 때
      if (columnName.toString() === 'currentAmount') {
        // 수량에서 현재수량을 빼준 후 증감수량에 넣어줌
        const amount = Number(instance.getValue(rowKey, 'amount'));
        const setValue = Math.abs(amount > 0 ? amount - Number(value) : amount + Number(value) );
        // 증감수량 계산
        let variationAmount = amount > value ? `-${setValue}` : `+${setValue}`;
        if (amount === Number(value)) {
          variationAmount = '0';
        }

        // 그리드 증감수량 컬럼에 데이터 넣어줌
        instance.setValue(rowKey, 'variationAmount', variationAmount);
      }
    } else {
      connecteye.app.ui.alert.warning({
        text: '숫자만 입력해주세요.',
        icon: 'error',
      });
      instance.startEditing(rowKey, columnName);
    }
  }

  /**
   * 출고 내역 가져오기
   *
   * @param {Object} _rowData
   */
  function getReleaseHistoryInfo(_rowData) {
    const { supPrdtId, rpsTypeCode } = _rowData;
    let options = {
      type: 'GET',
      url: '/api/stockStatusInfo/getReleaseHistoryInfo',
      paramData: {
        supPrdtId: supPrdtId,
        rpsTypeCode: rpsTypeCode,
      },
      callback: (_res) => {
        commonUtils.isNotEmpty(_res)
          ? setHistoryInfoData(_res, releaseHistoryTable.grid, 'release')
          : releaseHistoryTable.grid.dispatch('setLoadingState', 'EMPTY');
      },
    };
    commonUtils.callAjax(options, true);
  }

  /**
   * 입고/반품 내역 가져오기
   *
   * @param {Object} _rowData
   */
  function getWrngHistoryInfo(_rowData) {
    const { supPrdtId, rpsTypeCode } = _rowData;
    let options = {
      type: 'GET',
      url: '/api/stockStatusInfo/getWrngHistoryInfo',
      paramData: {
        supPrdtId: supPrdtId,
        rpsTypeCode: rpsTypeCode,
      },
      callback: (_res) => {
        commonUtils.isNotEmpty(_res)
          ? setHistoryInfoData(_res, stockHistoryTable.grid, 'stock')
          : stockHistoryTable.grid.dispatch('setLoadingState', 'EMPTY');
      },
    };
    commonUtils.callAjax(options, true);
  }

  /**
   * 내역 세팅
   *
   * @param {Object} _res
   * @param _grid
   * @param _type
   */
  function setHistoryInfoData(_res, _grid, _type) {
    // 그리드에 데이터 넣음
    _grid.resetData(_res);
    // hideLoadingBar
    _grid.dispatch('setLoadingState', 'DONE');
  }

  /**
   * 재고 수정 목록 가져오기
   *
   * @param {Object} _rowData
   */
  function getStockModifyHistoryInfo(_rowData) {
    let options = {
      type: 'GET',
      url: '/api/stockStatusInfo/getStockModifyHistoryInfo',
      paramData: _rowData,
      callback: (_res) => {
        if (commonUtils.isNotEmpty(_res)) {
          // 그리드에 데이터 넣음
          stockModifyHistoryTable.grid.resetData(_res);
          // hideLoadingBar
          stockModifyHistoryTable.grid.dispatch('setLoadingState', 'DONE');
        } else {
          stockModifyHistoryTable.grid.dispatch('setLoadingState', 'EMPTY');
        }
      },
    };
    commonUtils.callAjax(options, true);
  }

  /**
   * 재고 수정 시작
   */
  function runInsert() {
    // form 전송 ajax 처리
    connecteye.app.ui.alert
      .confirm({
        title: '재고를 수정할까요?',
        text: '취소를 누르면 내용을 수정 할 수 있어요',
        confirmBtnTxt: '적용',
        cancelBtnTxt: '취소',
        showLoaderOnConfirm: true,
        preConfirm: () => {},
      })
      .then((_result) => {
        if (_result.isConfirmed) {
          insertStockModifyInfo();
        } else {
          connecteye.app.ui.alert.error({
            title: '적용 취소',
            text: '진행내용 수정 후 다시 적용 할 수 있어요 :)',
            icon: 'error',
          });
        }
      });
  }

  /**
   * 재고 수정 추가 ajax
   */
  async function insertStockModifyInfo() {
    // 재고 데이터 정의
    const stockModifyData = await stockModifyTable.grid.getData();
    const { id } = USER_INFO;
    const { optId, supPrdtId, rpsTypeCode, salesPrice, amount, currentAmount, flcTypeCode, variationAmount } =
      stockModifyData[0];

    connecteye.app.ui.showCardLoading();
    commonUtils.callAjax({
      type: 'POST',
      url: '/api/stockStatusInfo/insertStockModifyInfo',
      paramData: {
        optId: optId,
        regId: id,
        supPrdtId: supPrdtId,
        rpsTypeCode: rpsTypeCode,
        price: Number(salesPrice) * Number(currentAmount),
        amount: currentAmount,
        variationAmount: variationAmount,
        prevAmount: amount,
        flcTypeCode: flcTypeCode || 'SS101',
      },
      callback: () => {
        connecteye.app.ui.hideCardLoading();
        connecteye.app.ui.alert
          .success({
            title: '적용 완료!',
            text: '입력한 재고 수정 내용이 적용됐어요',
            showDenyButton: false,
          })
          .then((_SuccessResult) => {
            if (_SuccessResult.isConfirmed) {
              // 재고현황 목록 조회
              showStockStatusInfoList();
              // 초기화
              resetPage();
            }
          });
      },
    });
  }

  /**
   * 재고현황 목록 보여주기
   */
  function showStockStatusInfoList() {
    $container.find('.btn-prev').trigger('click');
    $container.find('.card[data-area=stockStatusList]').trigger('loadDom', { listReload: true });
  }

  /**
   * 초기화
   */
  function resetPage() {
    $container.find('.stock-history-cnt').text('0');
    stockModifyHistoryTable.grid.setColumnValues('currentAmount', '0');
    $container.parent().scrollTop(0);
  }
  /***** Business End *****/
};
