/*
 *  Description: common commonUtils.js
 *  User: kyle
 *  Date: 2021-11-02
 */
const utils = {
  /**
   * ajax call
   *
   * @param {Object} _param
   * @param {Object} args 0: async, 1: errorMsg
   */
  callAjax: (_param, ...args) => {
    const { type, url, paramData, callback, dataType } = _param;
    const { async, errorMsg } = args;

    if (url) {
      let _async = async;
      if (!_async) _async = true;

      let _errorMsg = errorMsg;

      let header = $('meta[name=_csrf_header]').attr('content');
      let token = $('meta[name=_csrf]').attr('content');

      let option = {
        type: type,
        url: url,
        data: paramData,
        async: _async,
        beforeSend: (xhr) => {
          xhr.setRequestHeader(header, token);
          xhr.setRequestHeader('AJAX', true);
        },
        success: (res) => {
          const { result, data, message } = res;
          if (result) {
            if (callback) {
              callback(data);
            }
            return false;
          }
          if (message) {
            connecteye.app.ui.alert.error({
              text: message,
            });
            return false;
          }
          if (callback) {
            callback(res);
          }
        },
        error: (xhr) => {
          connecteye.app.ui.loadCompleteProgressbar();
          connecteye.app.ui.hideCardLoading();
          if (xhr.status === 403) {
            connecteye.app.ui.alert.error({
              title: '세션 만료',
              text: '세션이 만료되었어요. 로그인페이지로 이동합니다.',
              didDestroy: () => (location.href = '/loginPage.ce'),
            });
            return false;
          }
          if (_errorMsg) {
            connecteye.app.ui.alert.error({
              title: _errorMsg.title,
              text: _errorMsg.text,
            });
          } else {
            connecteye.app.ui.alert.error({
              text: '서버에 요청중 문제가 발생했어요.\n관리자에게 문의 주세요.',
            });
          }
        },
      };

      if (dataType === 'form') {
        option.processData = false;
        option.contentType = false;
      } else {
        option.dataType = dataType || null;
      }

      $.ajax(option);
    } else {
      connecteye.app.ui.alert.error({ text: '올바른 요청이 아닙니다.' });
      return false;
    }
  },

  /**
   * formData ajax
   *
   * @param {Object} _paramData
   * @return {Object} ajaxFormData
   */
  getAjaxFormData: (_paramData) => {
    const ajaxFormData = new FormData();
    for (let obj of Object.entries(_paramData)) {
      ajaxFormData.append(obj[0], obj[1]);
    }
    return ajaxFormData;
  },

  /**
   * 바이너리 객체로 가져옴
   *
   * @param {Object} _param
   * @return {Object} Blob
   */
  getBlobObject: (_param) => {
    const { data, option } = _param;
    return new Blob([JSON.stringify(data)], option);
  },

  /**
   * form dom 객체 serialize to Object
   *
   * @param {Object} _$form
   * @return {Object} obj
   */
  serializeObject: (_$form) => {
    let obj = null;
    try {
      // _form[0].tagName이 form tag일 경우
      if (_$form[0].tagName && _$form[0].tagName.toUpperCase() === 'FORM') {
        let arr = _$form.serializeArray();
        if (arr) {
          obj = {};
          for (let formData of arr) {
            // 빈 값이 아닐경우
            const { name, value } = formData;
            if (value) {
              obj[name] = value;
            }
          }
        }
      }
    } catch (e) {
      console.log(e.message);
      connecteye.app.ui.alert.error({ text: '문제가 발생했어요.' });
    } finally {
    }
    return obj;
  },

  /**
   * 숫자 콤마 세팅
   *
   * @param {Object} _obj
   * @return {Object} obj
   */
  setComma: (_obj) =>
    (_obj.value != null ? _obj.value : _obj).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ','),

  /**
   * 숫자 콤마 지우기
   *
   * @param {Object} _obj
   * @return {Object} obj
   */
  removeComma: (_obj) => (_obj.value != null ? _obj.value : _obj).toString().replace(/,/g, ''),

  /**
   * 셀렉트박스 코드 바인딩
   *
   * @param {Object} _param
   */
  setSelectboxCode: (_param) => {
    const { el, code, defaultTxt, exceptionCodeList } = _param;
    let $selectbox = $(`#${el}`);
    let codeValues = CODE_LIST.filter((item) => item.upperCode === code && item.isUse === 1);

    $selectbox.empty().append($('<option/>'));

    // 기본 텍스트
    if (defaultTxt) {
      $selectbox.empty().append($('<option/>', { text: defaultTxt, value: '' }));
    }

    for (let obj of codeValues) {
      const { code, codeValue } = obj;
      if (!exceptionCodeList || !exceptionCodeList.includes(code)) {
        let option = document.createElement('option');
        option.value = code;
        option.text = codeValue;
        $selectbox.append(option);
      }
    }
  },

  /**
   * 카카오 주소/우편번호 찾기 서비스 가져오기
   */
  getDaumPostCode(_$form) {
    const $regAddress = _$form.find('[id*="Address"]');
    const $regAddressDetail = _$form.find('[id*="AddrDetail"]');

    return new daum.Postcode({
      width: 500,
      height: 600,
      popupName: 'postcodePopup', // 여러개의 팝업창 오픈되는 것을 방지
      oncomplete: (data) => {
        // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.
        const { userSelectedType, roadAddress, jibunAddress, bname, buildingName, apartment } = data;
        let addr = ''; // 주소
        let extraAddr = ''; // 참고항목

        //사용자가 선택한 주소 타입에 따라 해당 주소 값을 set
        userSelectedType === 'R'
          ? // 사용자가 도로명 주소를 선택했을 경우(R)
            (addr = roadAddress)
          : // 사용자가 지번 주소를 선택했을 경우(J)
            (addr = jibunAddress);

        // 도로명 타입일때 참고항목을 조합
        if (userSelectedType === 'R') {
          // 법정동명이 있을 경우 추가 (법정리는 제외)
          // 법정동의 경우 마지막 문자가 "동/로/가"로 끝남
          if (this.isNotEmpty(bname) && /[동|로|가]$/g.test(bname)) {
            extraAddr += bname;
          }
          // 건물명이 있고, 공동주택일 경우 추가
          if (this.isNotEmpty(buildingName) && apartment === 'Y') {
            extraAddr += this.isNotEmpty(extraAddr) ? `, ${buildingName}` : buildingName;
          }
          // 표시할 참고항목이 있을 경우, 괄호까지 추가한 최종 문자열을 만듦
          if (this.isNotEmpty(extraAddr)) {
            extraAddr = `(${extraAddr})`;
          }
        }
        $regAddressDetail.val(extraAddr);

        // 우편번호
        // data.zonecode

        // 주소
        $regAddress.val(addr);

        // 상세주소로 커서 이동
        $regAddressDetail.focus();
      },
    });
  },

  /**
   * form 리셋
   *
   * @param {Object} _$els
   * @param {String} _defaultValue
   */
  formReset: (_$els, _defaultValue) => {
    return _$els.each(function () {
      const type = this.type;
      switch (type) {
        case 'text':
        case 'password':
        case 'hidden':
        case 'textarea':
          this.value = utils.isNotEmpty(_defaultValue) ? _defaultValue : '';
          break;
        case 'checkbox':
        case 'radio':
          this.checked = false;
          break;
        case 'select-one':
          this.selectedIndex = 0;
          break;
      }
    });
  },

  /**
   * 전화번호 하이픈 넣어주기
   *
   * @param {String} value
   */
  setPhoneNumber: (value) => {
    if (!value) {
      return '';
    }

    value = value.replace(/[^0-9]/g, '');

    let result = [];
    let restNumber = '';

    // 지역번호와 나머지 번호로 나누기
    if (value.startsWith('02')) {
      // 서울 02 지역번호
      result.push(value.substr(0, 2));
      restNumber = value.substring(2);
    } else if (value.startsWith('1')) {
      // 지역 번호가 없는 경우
      // 1xxx-yyyy
      restNumber = value;
    } else {
      // 나머지 3자리 지역번호
      // 0xx-yyyy-zzzz
      result.push(value.substr(0, 3));
      restNumber = value.substring(3);
    }

    if (restNumber.length === 7) {
      // 7자리만 남았을 때는 xxx-yyyy
      result.push(restNumber.substring(0, 3));
      result.push(restNumber.substring(3));
    } else {
      result.push(restNumber.substring(0, 4));
      result.push(restNumber.substring(4));
    }

    return result.filter((val) => val).join('-');
  },

  /**
   * 생년월일 - 하이픈 자동 생성
   *
   * @param {Object} obj
   */
  setBirthdayNumber: (obj) => {
    // 숫자와 하이픈(-)기호의 값만 존재하는 경우 실행
    if (obj.value.replace(/[0-9 \-]/g, '').length === 0) {
      // 하이픈(-)기호를 제거한다.
      let number = obj.value.replace(/[^0-9]/g, '');
      let ymd = '';

      // 문자열의 길이에 따라 Year, Month, Day 앞에 하이픈(-)기호를 삽입한다.
      if (number.length < 4) {
        return number;
      } else if (number.length < 6) {
        ymd += number.substr(0, 4);
        ymd += '-';
        ymd += number.substr(4);
      } else {
        ymd += number.substr(0, 4);
        ymd += '-';
        ymd += number.substr(4, 2);
        ymd += '-';
        ymd += number.substr(6);
      }
      obj.value = ymd;
    } else {
      obj.value = '';
    }
  },

  /**
   * 금액 이벤트 핸들러
   *
   * @param {Object} _evt
   */
  setPriceEventHandler(_evt) {
    const $element = $(_evt.currentTarget);
    const value = $element.val();
    const replaceValue = utils.removeComma(value.replace(/(^0+)/, '').trim());
    $element.val(utils.setComma(replaceValue));
  },

  /**
   * 빈값 처리
   *
   * @param {Object} _value
   */
  isEmpty: (_value) => {
    if (_value == null) {
      return true;
    }
    if (_value.length) {
      return _value.length === 0;
    }
    return Object.keys(_value).length === 0;
  },
  isNotEmpty(_value) {
    return !this.isEmpty(_value);
  },
  isEmptyInput: (_$form, _exceptions) => {
    let isRight = false;

    _$form.find('input[type=text], select, textarea').each(function (index, item) {
      const { value, name } = item;

      // 예외 처리
      if (_exceptions.includes(name)) {
        return false;
      }

      // 아무값없이 띄어쓰기만 있을 때도 빈 값으로 체크되도록 trim() 함수 호출
      if (value.trim() === '') {
        isRight = true;
        return false;
      }
    });

    return isRight;
  },
  /**
   * 숫차 체크
   *
   * @param {String} _str
   * @return {boolean} boolean
   */
  isNumeric(_str) {
    return !isNaN(_str) && !isNaN(parseFloat(_str));
  },
  /**
   * 전화번호 체크
   *
   * @param {Object} _value
   */
  isPhone(_value) {
    return /^(01[016789]{1}|02|0[3-9]{1}[0-9]{1})-[0-9]{3,4}-[0-9]{4}$/.test(_value);
  },
  /**
   * 생년월일 체크
   *
   * @param {Object} _value
   */
  isBirthday(_value) {
    return /^(19[0-9][0-9]|20\d{2})-(0[0-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/.test(_value);
  },
  /**
   * 이메일 체크
   *
   * @param {Object} _value
   */
  isEmail(_value) {
    return /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/.test(
      _value,
    );
  },
  /**
   * jquery validation 한글처리
   */
  setJqueryValidationLanguageKor() {
    // 유효성체크 메세지 설정
    $.extend($.validator.messages, {
      required: '필수 항목이에요.',
      remote: '항목을 수정하세요.',
      email: '유효하지 않은 이메일 주소입니다.',
      url: '유효하지 않은 URL 입니다.',
      date: '올바른 날짜를 입력하세요.',
      dateISO: '올바른 날짜(ISO)를 입력하세요.',
      number: '유효한 숫자가 아니에요.',
      digits: '숫자만 입력 가능합니다.',
      creditcard: '신용카드 번호가 바르지 않아요.',
      equalTo: '같은 값을 다시 입력하세요.',
      extension: '올바른 확장자가 아니에요.',
      maxlength: $.validator.format('{0}자를 넘을 수 없어요. '),
      minlength: $.validator.format('{0}자 이상 입력하세요.'),
      rangelength: $.validator.format('{0}글자 이상 {1}글자 이하를 입력하세요.'),
      range: $.validator.format('{0} 에서 {1} 사이의 값을 입력하세요.'),
      max: $.validator.format('{0}글자 이하 입력하세요.'),
      min: $.validator.format('{0}글자 이상 입력하세요.'),
    });
    $.validator.addMethod(
      'price',
      (value, element) => {
        return this.isNumeric(this.removeComma(element.value));
      },
      '숫자만 입력 가능합니다.',
    );
    $.validator.addMethod(
      'phonekor',
      (value, element) => {
        if (this.isNotEmpty(element.value)) {
          return this.isPhone(element.value);
        }
        return true;
      },
      '올바른 번호가 아니에요.',
    );
    $.validator.addMethod(
      'businessNumber',
      (value, element) => {
        return /([0-9]{3})-?([0-9]{2})-?([0-9]{5})/.test(element.value);
      },
      '올바른 사업자등록번호가 아니에요.',
    );
    $.validator.addMethod(
      'regex',
      function (value, element, regexp) {
        const regExp = new RegExp(regexp);
        return regExp.test(value);
      },
      '형식이 올바르지 않아요.',
    );
  },
};
export default utils;
