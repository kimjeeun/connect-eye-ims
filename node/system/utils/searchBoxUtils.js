/*
 *  Description: 검색박스 유틸
 *  User: kyle
 *  Date: 2021-12-23
 */
require('../../node_modules/select2/dist/css/select2.min.css');

import Select2 from 'select2';
import commonUtils from '../../system/utils/commonUtils';

const searchBoxUtils = {
  /**
   * 검색박스 생성
   *
   * @param {String} _param
   */
  create: (_param) => {
    const { $el, ajax, title, placeholder, noAdd, noResultCallback, templateResult } = _param;
    $el.select2({
      ajax: ajax || null,
      minimumInputLength: 0,
      selectionCssClass: 'form-control',
      placeholder: placeholder,
      language: {
        noResults: () => {
          let $div = $('<div/>').text('검색 결과가 없어요.. ');
          let $noSearch = $('<a/>', { class: 'no-search' });
          if (commonUtils.isNotEmpty(title)) {
            $noSearch.on('click', () => {
              if (noResultCallback && typeof noResultCallback === 'function') {
                noResultCallback();
              }
            });
            if (!noAdd) {
              $noSearch.text(`${title} 추가 하기`);
            }
          }
          $div.append($noSearch);
          return $div;
        },
        inputTooShort: (n) => `${n.minimum - n.input.length}글자 이상 입력해주세요.`,
        searching: () => '검색 중…',
        loadingMore: () => `더 많은 ${title} 가져오는 중…`,
        errorLoading: () => '검색 중 문제가 발생했어요.',
      },
      templateSelection: (state) => {
        const { element, data, text } = state;
        $(element).attr('data-result', JSON.stringify(data));
        return text;
      },
      templateResult: templateResult,
      escapeMarkup: (markup) => markup,
    });

    // 선택 input박스 foucs
    $el.on('select2:open', (_evt) => {
      const id = _evt.target.id;
      const target = document.querySelector(`[aria-controls=select2-${id}-results]`);
      target.focus();
    });
  },
  /**
   * 검색박스 결과 오브젝트
   *
   * @param {String} _$el
   * @return {Object} resultObj
   */
  getResultObject: (_$el) => {
    const resultObj = _$el.find('option:selected').attr('data-result');
    return resultObj ? JSON.parse(resultObj) : null;
  },
  /**
   * 검색박스 결과 Callback 함수
   *
   * @param {String} _res
   * @return {Array} data, array
   */
  getResultCallback: (_res) => {
    const { result, data, message } = _res;
    if (result) {
      return data;
    } else {
      connecteye.app.ui.alert.error({
        text: message,
      });
      return [];
    }
  },
};

export default searchBoxUtils;
