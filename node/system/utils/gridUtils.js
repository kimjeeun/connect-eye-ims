/*
 *  Description: common gridUtils.js
 *  User: kyle
 *  Date: 2021-11-02
 */
import Grid from 'tui-grid';
import commonUtils from '../utils/commonUtils';

require('../../node_modules/tui-pagination/dist/tui-pagination.min.css');
require('../../node_modules/tui-date-picker/dist/tui-date-picker.min.css');
require('../../node_modules/tui-grid/dist/tui-grid.min.css');

const utils = {
  /**
   * 그리드 생성
   *
   * @param {String} props
   */
  create: class {
    constructor(props) {
      const {
        el,
        data,
        columns,
        summary,
        scrollX,
        scrollY,
        bodyHeight,
        rowHeaders,
        minBodyHeight,
        onGridMounted,
        onGridUpdated,
        columnOptions,
        treeColumnOptions,
        pageOptions,
      } = props;
      this.grid = null;
      this.options = {
        el: document.getElementById(el),
        data: data,
        columns: columns,
        columnOptions: columnOptions,
        summary: summary,
        initialRequest: false, // 그리드 생성시에만 request
        scrollX: scrollX !== 'false', // X축 스크롤
        scrollY: scrollY || false, // Y축 스크롤
        selectionUnit: 'row', // 선택 영역
        rowHeaders: rowHeaders || ['checkbox'], // 체크박스
        rowHeight: 60, // 행 높이
        minBodyHeight: minBodyHeight || 440, // 그리드 최소 높이
        bodyHeight: bodyHeight || 'auto', // 그리드 높이
        onGridMounted: onGridMounted, // 그리드 생성 완료 시
        onGridUpdated: onGridUpdated, // 그래드 업데이트 시
      };
      if (treeColumnOptions) {
        this.options.treeColumnOptions = treeColumnOptions;
      }
      if (props.pageOptions) {
        this.options.pageOptions = pageOptions;
      }
      this.setInit();
    }
    setInit() {
      // 언어설정
      Grid.setLanguage('ko');
      // 테마설정
      Grid.applyTheme('clean', {
        row: {
          hover: {
            background: '#fafafa',
            showVerticalBorder: false,
          },
        },
      });
      // 그리드 객체 생성
      this.grid = new Grid(this.options);
      // 이벤트 바인딩
      this.grid.on({
        onGridMounted: this.options.onGridMounted,
        onGridUpdated: this.options.onGridUpdated,
      });
      // 목록 건별 보기
      $(this.options.el)
        .parent()
        .find('.result-select select')
        .on('change', (_evt) => {
          // perPage 갯수
          const perPage = parseInt(_evt.target.value);
          // 그리드 perPage 세팅
          this.grid.setPerPage(perPage);
        });

      return this.grid;
    }
  },

  /**
   * 토글 스위치 렌더러
   */
  setToggleSwitchRenderer: class {
    constructor(props) {
      this.el = document.createElement('div');
      this.render(props);
    }
    getElement() {
      return this.el;
    }
    switchEvent(_param) {
      const { evt, rowKey, grid } = _param;
      const { pkName, url } = _param.options;
      let paramData = grid.getRow(rowKey);
      paramData.isUse = evt.target.checked ? 1 : 0;
      paramData[pkName] = paramData[pkName];

      commonUtils.callAjax(
        {
          type: 'PUT',
          url: url,
          paramData: paramData,
        },
        true,
      );
    }
    render(props) {
      const { rowKey, value, grid, columnInfo } = props;
      let $input = document.createElement('input'),
        $label = document.createElement('label'),
        $span = document.createElement('span'),
        id = `switchCheck${rowKey}`;
      this.el.className = 'form-switch';
      $input.id = id;
      $input.className = 'switch';
      $input.type = 'checkbox';
      $input.checked = value === 1;
      $label.className = 'label-toggle';
      $label.setAttribute('for', id);
      $input.addEventListener('change', (evt) => {
        this.switchEvent({
          evt: evt,
          grid: grid,
          rowKey: rowKey,
          options: columnInfo.renderer.options,
        });
      });
      $(this.el).empty();
      $label.append($span);
      this.el.append($input);
      this.el.append($label);
    }
  },

  /**
   * 이미지 렌더러
   *
   * @param {String} props
   */
  setImageRenderer: class {
    constructor(props) {
      this.el = document.createElement('img');
      this.el.onerror = function () {
        this.onerror = null;
        this.src = '/static/images/downloading.png';
      };
      this.render(props);
    }
    getElement() {
      return this.el;
    }
    render(props) {
      this.el.src = String(props.value);
    }
  },

  /**
   * 진행단계 렌더러 정의
   *
   * @param {String} props
   */
  purStatusRenderer: class {
    constructor(props) {
      this.el = document.createElement('div');
      this.render(props);
    }
    getElement() {
      return this.el;
    }
    render(props) {
      $(this.el).empty();
      const { grid, rowKey, value } = props;
      const gridData = grid.getChildRows(rowKey);
      const $proceeding = $('<span/>', { class: 'badge badge-warning', text: '진행중' });
      const $complete = $('<span/>', { class: 'badge badge-success', text: '완료' });
      if (commonUtils.isNotEmpty(gridData)) {
        const parentRowData = grid.getRow(rowKey);
        parentRowData.progStat !== '완료'
          ? this.el.append($proceeding[0])
          : this.el.append($complete[0]);
      } else {
        switch (value) {
          case '완료':
            this.el.append($complete[0]);
            break;
          case '대기':
          case '진행중':
            this.el.append($proceeding[0]);
            break;
        }
      }
    }
  },

  /**
   * 진행상세 커스텀 렌더러 정의
   *
   * @param {String} props
   */
  detailStatusRenderer: class {
    constructor(props) {
      this.el = document.createElement('div');
      this.$stady = $('<span/>', { class: 'badge badge-dark' });
      this.$pur = $('<span/>', { class: 'badge badge-primary' });
      this.$cancel = $('<span/>', { class: 'badge badge-danger' });
      this.render(props);
    }
    getElement() {
      return this.el;
    }
    groupByStats(props) {
      const { grid, rowKey } = props;
      return grid.getChildRows(rowKey).reduce((obj, { cancelCnt, stadyCnt, purCnt }) => {
        (obj['stadyCnt'] = (parseInt(obj['stadyCnt']) || 0) + parseInt(stadyCnt || 0)), obj;
        (obj['purCnt'] = (parseInt(obj['purCnt']) || 0) + parseInt(purCnt || 0)), obj;
        (obj['cancelCnt'] = (parseInt(obj['cancelCnt']) || 0) + parseInt(cancelCnt || 0)), obj;
        return obj;
      }, {});
    }
    async render(props) {
      $(this.el).empty();
      const { grid, rowKey } = props;
      const statsData = await grid.getChildRows(rowKey);
      if (commonUtils.isNotEmpty(statsData)) {
        const statsData = await this.groupByStats(props);
        Object.keys(statsData).forEach((_item) => {
          const count = statsData[_item];
          if (count) {
            switch (_item) {
              case 'stadyCnt':
                this.$stady.text(`대기 ${count}`);
                break;
              case 'purCnt':
                this.$pur.text(`입고 ${count}`);
                break;
              case 'cancelCnt':
                this.$cancel.text(`취소 ${count}`);
                break;
            }
          }
        });
        this.el.append(this.$stady[0]);
        this.el.append(this.$pur[0]);
        this.el.append(this.$cancel[0]);
      } else {
        const { stadyCnt, purCnt, cancelCnt } = grid.getRow(rowKey);
        if (stadyCnt) {
          this.$stady.text(`대기 ${stadyCnt}`);
        }
        if (purCnt) {
          this.$pur.text(`입고 ${purCnt}`);
        }
        if (cancelCnt) {
          this.$cancel.text(`취소 ${cancelCnt}`);
        }
        this.el.append(this.$stady[0]);
        this.el.append(this.$pur[0]);
        this.el.append(this.$cancel[0]);
      }
    }
  },

  /**
   * 삭제버튼 렌더러 정의
   *
   * @param {String} props
   */
  deleteButtonRenderer: class {
    constructor(props) {
      this.el = document.createElement('div');
      this.render(props);
    }
    getElement() {
      return this.el;
    }
    render(props) {
      $(this.el).empty();
      const { grid, rowKey } = props;
      const { isAmount, $currentAmount, callback } = props.columnInfo.renderer.options;
      const currentData = grid.getRow(rowKey);
      const $deleteIcon = $('<span/>', { class: 'fas fa-times text-danger' }).on('click', () => {
        if (callback && typeof callback === 'function') {
          callback(currentData);
          return false;
        }
        if (isAmount) {
          const { amount } = grid.getRow(rowKey);
          const currentAmount = Number($currentAmount.text());
          $currentAmount.text(amount + currentAmount);
        }
        grid.removeRow(rowKey);
      });
      this.el.append($deleteIcon[0]);
    }
  },

  /**
   *  그리드 셀렉트박스 세팅
   *
   * @param {String} _code,
   * @param {String} _exceptionCodeList
   * @return {Array} codeFilterResult
   */
  setSelectboxCode(_code, _exceptionCodeList) {
    const filterList = CODE_LIST.filter(item => {
      const { code, upperCode } = item;
      if (commonUtils.isNotEmpty(_exceptionCodeList)) {
        return !_exceptionCodeList.includes(code) && upperCode === _code;
      }
      return upperCode === _code;
    });

    return filterList.reduce((newArray, item) => {
      const { code, codeValue } = item;
        newArray.push({
          text: codeValue,
          value: code,
        });
      return newArray;
    }, []);
  },

  /**
   * 그리드 트리 컬럼 펼치기
   *
   * @param {Object} _rowData
   * @return {Object} _rowData
   */
  treeColumnExpand(_rowData) {
    _rowData._attributes.expanded = true;
    return _rowData;
  },

  /**
   *  그리드 Row 선택
   *
   * @param {Object} _grid
   * @param {int} _rowKey
   */
  setSelectionRange(_grid, _rowKey) {
    const startRowKey = _grid.getIndexOfRow(_rowKey);
    _grid.setSelectionRange({
      start: [startRowKey, 0],
      end: [startRowKey, _grid.getColumns().length],
    });
  },

  /**
   *  그리드 사진 선택 핸들러
   *
   * @param {Object} _evt
   * @param {String} _columnName
   * @param {String} _originColumnName
   */
  setSelectPictureHandler(_evt, _columnName, _originColumnName) {
    const { columnName, targetType, instance, rowKey } = _evt;
    if (columnName === _columnName && targetType !== 'columnHeader' && targetType !== 'etc') {
      const previewImgPath = instance.getRow(rowKey)[_originColumnName || _columnName];
      if (previewImgPath) {
        connecteye.app.ui.alert.imagePreview({
          imageUrl: previewImgPath,
        });
      }
    }
  }
};
export default utils;
