/*
 *  Description: alertUtils
 *  User: kyle
 *  Date: 2021-12-20
 */
import SweetAlert from 'sweetalert2';

require('animate.css');
require('../../node_modules/sweetalert2/dist/sweetalert2.min.css');

const alert = {
  /**
   * alert 성공 팝업
   *
   * @param {String} _param
   * @return {Object} SweetAlert
   */
  success(_param) {
    const { title, text, showDenyButton, denyButtonText, didDestroy } = _param;
    return SweetAlert.fire({
      icon: 'success',
      title: title,
      text: text,
      confirmButtonColor: '#2e2e2e',
      confirmButtonText: '확인',
      allowOutsideClick: false,
      showDenyButton: showDenyButton || false,
      denyButtonText: denyButtonText || null,
      didDestroy: didDestroy || null,
    });
  },
  /**
   * alert 확인 팝업
   *
   * @param {String} _param
   * @return {Object} SweetAlert
   */
  confirm(_param) {
    const { title, text, html, showLoaderOnConfirm, confirmBtnTxt, cancelBtnTxt, preConfirm } = _param;
    const swalWithBootstrapButtons = SweetAlert.mixin({
      customClass: {
        confirmButton: 'btn-custom-2',
        cancelButton: 'btn-custom-3 mr-2',
      },
      buttonsStyling: false,
    });
    return swalWithBootstrapButtons.fire({
      title: title,
      text: text,
      html: html,
      icon: 'question',
      reverseButtons: true,
      allowOutsideClick: false,
      showCancelButton: true,
      showLoaderOnConfirm: showLoaderOnConfirm || false,
      confirmButtonText: confirmBtnTxt,
      cancelButtonText: cancelBtnTxt,
      preConfirm: preConfirm || null,
    });
  },
  /**
   * alert 이미지 팝업
   *
   * @param {String} _param
   */
  imagePreview(_param) {
    SweetAlert.fire({
      imageUrl: _param.imageUrl,
      padding: '2em 2em 1.25rem 2em',
      confirmButtonColor: '#2e2e2e',
      confirmButtonText: '확인',
    });
  },
  /**
   * alert 주의 팝업
   *
   * @param {String} _param
   */
  warning(_param) {
    const { title, text } = _param;
    SweetAlert.fire({
      icon: 'warning',
      title: title,
      text: text,
      confirmButtonColor: '#2e2e2e',
      confirmButtonText: '확인',
    });
  },
  /**
   * alert 실패 팝업
   *
   * @param {String} _param
   */
  error(_param) {
    const { title, text, didDestroy } = _param;
    SweetAlert.fire({
      icon: 'error',
      title: title,
      text: text,
      confirmButtonColor: '#2e2e2e',
      confirmButtonText: '확인',
      didDestroy: didDestroy || null,
    });
  },
  /**
   * alert 커스텀 팝업
   *
   * @return {Object} SweetAlert
   */
  custom() {
    return SweetAlert;
  },
  /**
   * alert 커스텀 팝업
   */
  tired() {
    SweetAlert.fire({
      text: '열심히 개발 진행중...',
      iconHtml: '<img src="/static/images/headache.png" style="width: 100px; height: 100px;">',
      showClass: {
        popup: 'animate__animated animate__shakeX',
      },
      hideClass: {
        popup: 'animate__animated animate__hinge',
      },
      customClass: {
        icon: 'no-border',
      },
      confirmButtonColor: '#2e2e2e',
      confirmButtonText: '확인',
    });
  },
};
export default alert;
