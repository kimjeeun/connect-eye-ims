/*
 *  Description: 파일 업로드
 *  User: kyle
 *  Date: 2021-12-28
 */

import * as filepond from 'filepond';
import filePondPluginFileValidateType from 'filepond-plugin-file-validate-type';
import FilePondPluginFileValidateSize from 'filepond-plugin-file-validate-size';
import filePondPluginImageExifOrientation from 'filepond-plugin-image-exif-orientation';
import filePondPluginImagePreview from 'filepond-plugin-image-preview';
import filePondPluginImageCrop from 'filepond-plugin-image-crop';
import filePondPluginImageResize from 'filepond-plugin-image-resize';
import filePondPluginImageTransform from 'filepond-plugin-image-transform';
import filePondPluginImageEdit from 'filepond-plugin-image-edit';

require('../../node_modules/filepond/dist/filepond.min.css');
require('../../node_modules/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.min.css');
require('../../node_modules/filepond-plugin-image-edit/dist/filepond-plugin-image-edit.min.css');

const fileUploadUtils = {
  param: null,
  /**
   * 파일업로드 생성 init
   *
   * @param {Object} _param
   * @return {Object} this.create()
   */
  init(_param) {
    filepond.registerPlugin(
      filePondPluginFileValidateType,
      FilePondPluginFileValidateSize,
      filePondPluginImageExifOrientation,
      filePondPluginImagePreview,
      filePondPluginImageCrop,
      filePondPluginImageResize,
      filePondPluginImageTransform,
      filePondPluginImageEdit,
    );
    this.param = _param;

    const { title, height, fileType } = this.param;
    this.options = {
      labelIdle: title,
      maxFileSize: '15MB',
      labelMaxFileSize: '파일 최대 크기 {filesize}',
      labelMaxFileSizeExceeded: '파일크기가 너무 큽니다',
      labelFileWaitingForSize: '입력 중..',
      labelTapToCancel: '탭 취소',
      labelTapToRetry: '탭 재시도',
      labelFileTypeNotAllowed: '올바른 확장자가 아니에요',
      labelFileLoadError: '로드 중 에러발생',
      fileValidateTypeLabelExpectedTypes: fileType || 'jpg, jpeg, png 만 가능',
      imagePreviewHeight: height || null,
      imageCropAspectRatio: '1',
      imageResizeTargetWidth: 200,
      imageResizeTargetHeight: 200,
      stylePanelLayout: 'compact',
      styleLoadIndicatorPosition: 'right',
      styleProgressIndicatorPosition: 'right',
      styleButtonRemoveItemPosition: 'right',
      styleButtonProcessItemPosition: 'right',
    };
    return this.create();
  },
  /**
   * 파일업로드 생성 함수
   */
  create() {
    return filepond.create(document.getElementById(this.param.el), this.options);
  },
  /**
   * 파일업로드 생성 함수
   *
   * @param {String} _url
   * @return {Object} FileInfo
   */
  async convertURLtoFile(_url) {
    const response = await fetch(_url);
    const data = await response.blob();
    const ext = _url.split('.').pop();
    const filename = _url.split('/').pop();
    const metadata = { type: `image/${ext}` };
    return new File([data], filename, metadata);
  },
};
export default fileUploadUtils;
