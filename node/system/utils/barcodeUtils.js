/*
 *  Description: 바코드 유틸
 *  User: kyle
 *  Date: 2022-07-14
 */
import { Html5Qrcode, Html5QrcodeSupportedFormats } from 'html5-qrcode';

const barcodeUtils = {
  init(_param) {
    // 바코드 스캐너
    this.barcodeFormats = [Html5QrcodeSupportedFormats.CODE_128];
    this.barcodeScanner = new Html5Qrcode('barcodeScanner', {
      formatsToSupport: this.barcodeFormats,
    });

    this.callback = _param.callback;

    this.$inputElement = _param.$inputElement;
    this.$scanButtonElement = _param.$scanButtonElement;
    this.$barcodeScannerWrap = $('.barcode-scanner-wrapper');
    this.$scanCancelButtonElement = $('.btn-scan-cancel');

    this.setEvent();
  },

  setEvent() {
    // 바코드 스캔 버튼 클릭 이벤트
    this.$scanButtonElement.on('click', () => this.runBarcodeScanner());

    // 바코드 스캔 취소 버튼 클릭 이벤트
    this.$scanCancelButtonElement.on('click', () => this.stopBarcodeScanner());
  },

  /**
   * 바코드 스캔 시작
   */
  runBarcodeScanner() {
    // 스캔화면
    this.$barcodeScannerWrap.fadeIn(100);
    // deviceId
    Html5Qrcode.getCameras()
      .then((devices) => {
        /**
         * { id: "id", label: "label" }
         */
        if (devices && devices.length) {
          const cameraId = devices[0].id;
          const isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
          const boxSize = isMobile ? 350: 500;

          // 스캔 불러오기
          this.barcodeScanner
            .start(
              { facingMode: 'environment' },
              {
                fps: 30,
                qrbox: { width: boxSize, height: boxSize },
              },
              (decodedText, decodedResult) => {
                // 바코드 감지 시
                this.$inputElement.val(decodedText);
                this.stopBarcodeScanner();

                // 감지 후 콜백함수
                if (typeof this.callback === 'function' && this.callback) {
                  this.callback({ barcode: decodedText });
                }
              },
            )
            .catch((err) => {
              // Start failed, handle it.
              console.log(err);
            });
        }
      })
      .catch((err) => {
        // handle err
      });
  },

  /**
   * 바코드 스캔 정지
   */
  stopBarcodeScanner() {
    this.$barcodeScannerWrap.fadeOut(150);
    this.barcodeScanner.stop();
  },
};
export default barcodeUtils;
