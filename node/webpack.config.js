/*
 *  Description: webpack bundle config
 *  User: kyle
 *  Date: 2021-12-09
 */
const path = require('path');
const fs = require('fs');
const glob = require('glob');
const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const CompressionPlugin = require('compression-webpack-plugin');

module.exports = (env, options) => {
  const config = {
    devtool: 'source-map',
    module: {
      rules: [
        {
          test: /\.css$/,
          use: [MiniCssExtractPlugin.loader, 'css-loader'],
        },
      ],
    },
    plugins: [
      new MiniCssExtractPlugin({
        filename: '../../css/[name]/[name].css',
      }),
      new webpack.ProvidePlugin({
        $: 'jquery',
        jquery: 'jquery',
        'window.jQuery': 'jquery',
        jQuery: 'jquery',
      }),
      new CompressionPlugin({
        filename: '[path][base].gz',
        test: /\.(js)$/,
        threshold: 10240,
        minRatio: 0.8,
        deleteOriginalAssets: false,
      }),
      // new BundleAnalyzerPlugin()
    ],
  };

  if (options.mode !== 'production') {
    config.plugins.push(new webpack.ProgressPlugin());
  }

  if (options.mode === 'production') {
    delete config.devtool;
  }

  const distRoot = path.join(__dirname, '../src/main/resources/static/js/');
  const configs = [];

  // common configuration
  configs.push(
    Object.assign({}, config, {
      entry: {
        common: ['./system/common/src/main.js'],
      },
      output: {
        filename: '[name].js',
        library: 'connecteye',
        libraryTarget: 'umd',
        path: path.join(distRoot, 'common'),
      },
    }),
  );

  // login configuration
  configs.push(
    Object.assign({}, config, {
      entry: {
        login: ['./system/login/src/main.js'],
      },
      output: {
        filename: '[name].js',
        path: path.join(distRoot, 'login'),
      },
    }),
  );

  // standardInfo configuration
  configs.push(
    Object.assign({}, config, {
      entry: {
        standardInfo: ['./system/standardInfo/src/main.js'],
      },
      output: {
        filename: '[name].js',
        path: path.join(distRoot, 'standardInfo'),
      },
    }),
  );

  // supplierInfo configuration
  configs.push(
    Object.assign({}, config, {
      entry: {
        supplierInfo: ['./system/supplierInfo/src/main.js'],
      },
      output: {
        filename: '[name].js',
        path: path.join(distRoot, 'supplierInfo'),
      },
    }),
  );

  // brandProductInfo configuration
  configs.push(
    Object.assign({}, config, {
      entry: {
        brandProductInfo: ['./system/brandProductInfo/src/main.js'],
      },
      output: {
        filename: '[name].js',
        path: path.join(distRoot, 'brandProductInfo'),
      },
    }),
  );

  // supplierPurchaseInfo configuration
  configs.push(
    Object.assign({}, config, {
      entry: {
        supplierPurchaseInfo: ['./system/supplierPurchaseInfo/src/main.js'],
      },
      output: {
        filename: '[name].js',
        path: path.join(distRoot, 'supplierPurchaseInfo'),
      },
    }),
  );

  // cashPaymentBySupplier configuration
  configs.push(
    Object.assign({}, config, {
      entry: {
        cashPaymentBySupplier: ['./system/cashPaymentBySupplier/src/main.js'],
      },
      output: {
        filename: '[name].js',
        path: path.join(distRoot, 'cashPaymentBySupplier'),
      },
    }),
  );

  // cashPaymentByCust configuration
  configs.push(
    Object.assign({}, config, {
      entry: {
        cashPaymentByCust: ['./system/cashPaymentByCust/src/main.js'],
      },
      output: {
        filename: '[name].js',
        path: path.join(distRoot, 'cashPaymentByCust'),
      },
    }),
  );

  // cashPaymentInfo configuration
  configs.push(
    Object.assign({}, config, {
      entry: {
        cashPaymentInfo: ['./system/cashPaymentInfo/src/main.js'],
      },
      output: {
        filename: '[name].js',
        path: path.join(distRoot, 'cashPaymentInfo'),
      },
    }),
  );

  // stockStatusInfo configuration
  configs.push(
    Object.assign({}, config, {
      entry: {
        stockStatusInfo: ['./system/stockStatusInfo/src/main.js'],
      },
      output: {
        filename: '[name].js',
        path: path.join(distRoot, 'stockStatusInfo'),
      },
    }),
  );

  // optician configuration
  configs.push(
    Object.assign({}, config, {
      entry: {
        optician: ['./system/optician/src/main.js'],
      },
      output: {
        filename: '[name].js',
        path: path.join(distRoot, 'optician'),
      },
    }),
  );

  // optometrySalesInfo configuration
  configs.push(
    Object.assign({}, config, {
      entry: {
          optometrySalesInfo: ['./system/optometrySalesInfo/src/main.js'],
      },
      output: {
        filename: '[name].js',
        path: path.join(distRoot, 'optometrySalesInfo'),
      },
    }),
  );

  // salesStatusInfo configuration
  configs.push(
    Object.assign({}, config, {
      entry: {
          salesStatusInfo: ['./system/salesStatusInfo/src/main.js'],
      },
      output: {
        filename: '[name].js',
        path: path.join(distRoot, 'salesStatusInfo'),
      },
    }),
  );

  // delete all dist files
  configs.forEach((config) => {
    const jsPath = config.output.path;
    let files = glob.sync('**/*', {
      cwd: jsPath,
    });
    files.forEach((file) => {
      const filePath = path.join(jsPath, file);
      if (options.mode !== 'production') {
        console.log('delete: ' + filePath);
      }
      fs.unlinkSync(filePath);
    });
    const cssPath = path.join(config.output.path, '../', 'css');
    files = glob.sync('**/*', {
      cwd: cssPath,
    });
    files.forEach((file) => {
      const filePath = path.join(cssPath, file);
      if (options.mode !== 'production') {
        console.log('delete: ' + filePath);
      }
      fs.unlinkSync(filePath);
    });
  });

  return configs;
};
